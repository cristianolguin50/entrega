// JavaScript Document

//Funci�n de un solo Submit
function submitonce(theform){
if (document.all||document.getElementById){
for (i=0;i<theform.length;i++){
var tempobj=theform.elements[i]
if(tempobj.type.toLowerCase()=="submit"||tempobj.type.toLowerCase()=="reset")
tempobj.disabled=true
}
}
return false
}
/*para aplicar este comportamiento es: <form method="POST" name="nombre-formulario" onSubmit="return submitonce(this)">*/
//------------------------------------------------------
//Funci�n para deshabilitar la tecla "ENTER"
function handleEnter (field, event) {
		var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
		if (keyCode == 13) {
		/*	var i;
			for (i = 0; i < field.form.elements.length; i++)
				if (field == field.form.elements[i])
					break;
			i = (i + 1) % field.form.elements.length;
			field.form.elements[i].focus();*/
			return false;
		} 
		else
		return true;
}      

/*Para aplicarlo 
<input type="text" onkeypress="return handleEnter(this, event)">*/
//------------------------------------------------------

//Funciones para poner bloques de ayuda con capas
var horizontal_offset="9px" //horizontal offset of hint box from anchor link

/////No se requiere de edici�n alguna

var vertical_offset="0" //horizontal offset of hint box from anchor link. No need to change.
var ie=document.all
var ns6=document.getElementById&&!document.all

function getposOffset(what, offsettype){
var totaloffset=(offsettype=="left")? what.offsetLeft : what.offsetTop;
var parentEl=what.offsetParent;
while (parentEl!=null){
totaloffset=(offsettype=="left")? totaloffset+parentEl.offsetLeft : totaloffset+parentEl.offsetTop;
parentEl=parentEl.offsetParent;
}
return totaloffset;
}

function iecompattest(){
return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body
}

function clearbrowseredge(obj, whichedge){
var edgeoffset=(whichedge=="rightedge")? parseInt(horizontal_offset)*-1 : parseInt(vertical_offset)*-1
if (whichedge=="rightedge"){
var windowedge=ie && !window.opera? iecompattest().scrollLeft+iecompattest().clientWidth-30 : window.pageXOffset+window.innerWidth-40
dropmenuobj.contentmeasure=dropmenuobj.offsetWidth
if (windowedge-dropmenuobj.x < dropmenuobj.contentmeasure)
edgeoffset=dropmenuobj.contentmeasure+obj.offsetWidth+parseInt(horizontal_offset)
}
else{
var windowedge=ie && !window.opera? iecompattest().scrollTop+iecompattest().clientHeight-15 : window.pageYOffset+window.innerHeight-18
dropmenuobj.contentmeasure=dropmenuobj.offsetHeight
if (windowedge-dropmenuobj.y < dropmenuobj.contentmeasure)
edgeoffset=dropmenuobj.contentmeasure-obj.offsetHeight
}
return edgeoffset
}

function showhint(menucontents, obj, e, tipwidth){
if ((ie||ns6) && document.getElementById("hintbox")){
dropmenuobj=document.getElementById("hintbox")
dropmenuobj.innerHTML=menucontents
dropmenuobj.style.left=dropmenuobj.style.top=-500
if (tipwidth!=""){
dropmenuobj.widthobj=dropmenuobj.style
dropmenuobj.widthobj.width=tipwidth
}
dropmenuobj.x=getposOffset(obj, "left")
dropmenuobj.y=getposOffset(obj, "top")
dropmenuobj.style.left=dropmenuobj.x-clearbrowseredge(obj, "rightedge")+obj.offsetWidth+"px"
dropmenuobj.style.top=dropmenuobj.y-clearbrowseredge(obj, "bottomedge")+"px"
dropmenuobj.style.visibility="visible"
obj.onmouseout=hidetip
}
}

function hidetip(e){
dropmenuobj.style.visibility="hidden"
dropmenuobj.style.left="-500px"
}

function createhintbox(){
var divblock=document.createElement("div")
divblock.setAttribute("id", "hintbox")
document.body.appendChild(divblock)
}

if (window.addEventListener)
window.addEventListener("load", createhintbox, false)
else if (window.attachEvent)
window.attachEvent("onload", createhintbox)
else if (document.getElementById)
window.onload=createhintbox

/* Para aplicarlo estos son los par�metros: <a href="#" class="hintanchor" onMouseover="showhint('texto a poner.', this, event, '150px')">[?]</a>*/
// aqui acaban las funciones para poner los cuadritos

//------------------------------------------------------

//Funci�n que muestra la hora y la fecha actual

var dayarray=new Array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","S�bado")
var montharray=new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre")

function getthedate(){
var mydate=new Date()
var year=mydate.getYear()
if (year < 1000)
year+=1900
var day=mydate.getDay()
var month=mydate.getMonth()
var daym=mydate.getDate()
if (daym<10)
daym="0"+daym
var hours=mydate.getHours()
var minutes=mydate.getMinutes()
var seconds=mydate.getSeconds()
var dn="AM"
if (hours>=12)
dn="PM"
if (hours>12){
hours=hours-12
}
if (hours==0)
hours=12
if (minutes<=9)
minutes="0"+minutes
if (seconds<=9)
seconds="0"+seconds
//change font size here
var cdate="<small><font color='000000' face='Arial'><b>"+dayarray[day]+", "+montharray[month]+" "+daym+", "+year+" "+hours+":"+minutes+":"+seconds+" "+dn
+"</b></font></small>"
if (document.all)
document.all.clock.innerHTML=cdate
else if (document.getElementById)
document.getElementById("clock").innerHTML=cdate
else
document.write(cdate)
}
if (!document.all&&!document.getElementById)
getthedate()
function goforit(){
if (document.all||document.getElementById)
setInterval("getthedate()",1000)
}

//para poner el reloj en algun lado de la p�gina, solo con esta l�nea de c�digo <span id="clock"></span>
//------------------------------------------------------
<!--

//Deshabilitar el bot�n derecho del rat�n
var message="El bot�n derecho del rat�n est� deshabilitado";

///////////////////////////////////
function clickIE4(){
if (event.button==2){
alert(message);
return false;
}
}

function clickNS4(e){
if (document.layers||document.getElementById&&!document.all){
if (e.which==2||e.which==3){
alert(message);
return false;
}
}
}

if (document.layers){
document.captureEvents(Event.MOUSEDOWN);
document.onmousedown=clickNS4;
}
else if (document.all&&!document.getElementById){
document.onmousedown=clickIE4;
}

document.oncontextmenu=new Function("alert(message);return false")

// --> 
//para aplicarlo: 
//------------------------------------------------------

//Funci�n para aplicar las may�sculas

function upper(field){
field.value=field.value.toUpperCase();
}

// Para aplicarla usar: onKeyPress="upper(this);"

//------------------------------------------------------

//FUNCION PARA VALIDAR CAMPOS NULOS
function checa(field) {
	if (field.value=="") {
		alert("Los campos marcados con * son obligatorios");
		//document.field.focus();
		return(false);
	}
}
//Para aplicar onBlur="checa(this)"

//------------------------------------------------------
//FUNCI�N PARA VALIDAR LETRAS
function validaletra(field) {
   var valid = "ABCDEFGHIJKLMN�OPQRSTUVWXYZabcdefghijklmn�opqrstuvwxyz���������� ";
   var ok = "yes";
   var temp;
   for (var i=0; i<field.value.length; i++) {
      temp = "" + field.value.substring(i, i+1);
      if (valid.indexOf(temp) == "-1") 
        ok = "no";
   }
   if (ok == "no") {
      alert("� E R R O R ! \n\nEste campo contiene caracteres no v�lidos");
      field.focus();
      field.select();
   }
}
//aplicaci�n: onBlur="validaletra(this)"
//------------------------------------------------------

//FUNCI�N PARA VALIDAR N�MEROS
function validanumero(field) {
var valid = "0123456789";
var ok = "yes";
var temp;
for (var i=0; i<field.value.length; i++) {
temp = "" + field.value.substring(i, i+1);
if (valid.indexOf(temp) == "-1") ok = "no";
}
if (ok == "no") {
alert("� E R R O R ! \n\nEste campo solo puede ser llenado con n�meros enteros positivos ");
field.focus();
field.select();
   }
}

//FUNCI�N PARA VALIDAR N�MEROS FLOTANTES
function validanumero2(field) {
var valid = "0123456789.";
var ok = "yes";
var temp;
for (var i=0; i<field.value.length; i++) {
temp = "" + field.value.substring(i, i+1);
if (valid.indexOf(temp) == "-1") ok = "no";
}
if (ok == "no") {
alert("� E R R O R ! \n\nEste campo solo puede ser llenado con n�meros");
field.focus();
field.select();
   }
}
//aplicaci�n: onBlur="validanumero(this)"
//------------------------------------------------------

//Funci�nes para validar un rango num�rico
/*Esta funci�n adem�s checa que se trate de un valor numerico entero forzosamente, con lo caul suprime el uso de la funci�n validanumero(); solo cu�ndo se trata de ingrear un valor con rango debe quitarse el valida numero, en ning�n otro caso no se deber� hacer*/
function isNumber(inputVal) {
	oneDecimal = false;
	inputStr = "" + inputVal;
	for (var i = 0; i < inputStr.length; i ++) {
		var oneChar = inputStr.charAt(i);
		if ((i == 0) && (oneChar == "-")) {
			continue;
		}
		if ((oneChar == ".") && (!oneDecimal)) {
			oneDecimal = true;
			continue;
		}
		if ((oneChar < "0") || (oneChar > "9")) {
			return false;
		}
	}
	return true;
}
function CKint(item, min_v, max_v) {
	var str;
	if (item.value == "")
		return;
	if (isNumber(item.value) == false) {
		alert("El valor que ingres� NO es un n�mero. Int�ntelo nuevamente.\n");
		item.focus();
	} else if (min_v < max_v) {
		if (item.value < min_v) {
			str = "El valor que ingres� en menor al permitido en el rango"
			str += min_v
			str += ".  Please try again.\n"
			alert(str);
			item.focus();
		} else if (item.value > max_v) {
			str = "El valor que ingres� en mayor al permitido en el rango"
			str += max_v
			str += ".  Int�ntelo nuevamente.\n"
			alert(str);
			item.focus();
		}
	}
}

/* Para aplicarlo utilizar onBlur="CKint(this, 1, 100)" donde el primer n�mero es el menor del rango y el segundo el mayor*/
/* Para solo utilizar la propiedad de que se trate de un val�r num�rico (entero o no) aplicarlo de la sigiente manera: onBlur="CKint(this, 0, 0)"*/
//------------------------------------------------------
//Funci�n para poner el foco en el primer  objeto del formulario

 function putFocus(formInst, elementInst) {
  if (document.forms.length > 0) {
   document.forms[formInst].elements[elementInst].focus();
  }
 }
 
// The second number in the "onLoad" command in the body
// tag determines the form's focus. Counting starts with '0'
// para aplicarlo: BODY onLoad="putFocus(0,1);"

//------------------------------------------------------

// Funci�n para convertirel texto a may�sculas desde el teclazo
function pulsar(e) {
 tecla = (document.all) ? e.keyCode : e.which;
if((tecla>=97) && (tecla<=122)){
	 return e.keyCode =   String.fromCharCode( e.keyCode ).toUpperCase().charCodeAt(0);
}

   /* if(tecla == 9 || tecla == 0) return true;
  if(tecla == 8) return true;
  if(window.Event){
      var pst = e.currentTarget.selectionStart;
      var string_start = e.currentTarget.value.substring(0,pst);
      var string_end = e.currentTarget.value.substring(pst ,e.currentTarget.value.length);
      e.currentTarget.value = string_start+ String.fromCharCode(tecla).toUpperCase()+ string_end;
      e.currentTarget.selectionStart = pst + 1;
      e.currentTarget.selectionEnd = pst + 1;
      e.stopPropagation();
      return false;
  }
  else if (window.event){
  
  return e.keyCode =   String.fromCharCode( e.keyCode ).toUpperCase().charCodeAt(0);
  }*/
}
//para usarla: onkeypress="return pulsar(event)"

function numer(valor){

if ((isNaN(valor.value)) || (valor.value=="")){
	valor.value=0;
}
}

var TieneFoco = true

function CambiarTieneFoco() {
	if (TieneFoco == true) {
		TieneFoco = false
		document.formModo.modo.value = "Normal"
	} else if (TieneFoco == false) {
		TieneFoco = true
		document.formModo.modo.value = "Siempre visible"
		DaFoco()
	}
}

function DaFoco() {
	if (TieneFoco == true)
		setTimeout("self.focus()",100)
}

function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

