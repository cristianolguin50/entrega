<?php
require('../Connections/bd2.php');
session_start();
$i = 0;
//--------------------Acta E-R-------------------------
mysql_select_db($database_bd2, $bd2);

$query_alter = array("ALTER TABLE `ioe006` CHANGE `responsable` `responsable` VARCHAR(150) NOT NULL",
"ALTER TABLE `ioe010` CHANGE `resp_elabora` `resp_elabora` VARCHAR(150) NOT NULL", 
"ALTER TABLE `ioe010` CHANGE `resp_autoriza` `resp_autoriza` VARCHAR(150) NOT NULL", 
"ALTER TABLE `ioe015` CHANGE `responsable` `responsable` VARCHAR(150) NOT NULL", 
"ALTER TABLE `ioe017` CHANGE `responsable` `responsable` VARCHAR(150) NOT NULL", 
"ALTER TABLE `ioe018` CHANGE `responsable` `responsable` VARCHAR(150) NOT NULL",
"ALTER TABLE `ial001` CHANGE `resp_autoriza` `resp_autoriza` VARCHAR(150) NULL DEFAULT NULL", 
"ALTER TABLE `iad006` CHANGE `responsable` `responsable` VARCHAR(150) NOT NULL", 
"ALTER TABLE `iad011` CHANGE `responsable` `responsable` VARCHAR(150) NOT NULL", 
"ALTER TABLE `iad013` CHANGE `responsable` `responsable` VARCHAR(150) NOT NULL", 
"ALTER TABLE `iad015` CHANGE `responsable` `responsable` VARCHAR(150) NOT NULL", 
"ALTER TABLE `iad017` CHANGE `responsable` `responsable` VARCHAR(150) NOT NULL", 
"ALTER TABLE `iad019` CHANGE `responsable` `responsable` VARCHAR(150) NOT NULL", 
"ALTER TABLE `iad023` CHANGE `autoridad` `autoridad` VARCHAR(150) NOT NULL", 
"ALTER TABLE `iad025` CHANGE `responsable` `responsable` VARCHAR(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL", 
"ALTER TABLE `ila005` CHANGE `responsable` `responsable` VARCHAR(150) NOT NULL", 
"ALTER TABLE `iop007` CHANGE `responsable` `responsable` VARCHAR(150) NOT NULL", 
"ALTER TABLE `iop006` CHANGE `responsable` `responsable` VARCHAR(150) NOT NULL", 
"ALTER TABLE `ipe007` CHANGE `responsable` `responsable` VARCHAR(150) NOT NULL");

$maxAlter = count($query_alter) -1;
for($i=0;$i<=$maxAlter;$i++){
	$dtAlter = mysql_query($query_alter[$i], $bd2) or die(mysql_error());	
}

echo "<br>Primera fase compleatada correctamente";
//------------------------------------------------------------

$query_update = array("UPDATE ioe006 INNER JOIN trabajador ON ioe006.responsable = trabajador.clave SET ioe006.responsable = trabajador.curp",
"UPDATE ioe010 INNER JOIN trabajador ON ioe010.resp_elabora = trabajador.clave SET ioe010.resp_elabora = trabajador.curp",
"UPDATE ioe010 INNER JOIN trabajador ON ioe010.resp_autoriza = trabajador.clave SET ioe010.resp_autoriza = trabajador.curp",
"UPDATE ioe015 INNER JOIN trabajador ON ioe015.responsable = trabajador.clave SET ioe015.responsable = trabajador.curp",
"UPDATE ioe017 INNER JOIN trabajador ON ioe017.responsable = trabajador.clave SET ioe017.responsable = trabajador.curp",
"UPDATE ioe018 INNER JOIN trabajador ON ioe018.responsable = trabajador.clave SET ioe018.responsable = trabajador.curp",
"UPDATE ial001 INNER JOIN trabajador ON ial001.resp_autoriza = trabajador.clave SET ial001.resp_autoriza = trabajador.curp",
"UPDATE iad006 INNER JOIN trabajador ON iad006.responsable = trabajador.clave SET iad006.responsable = trabajador.curp",
"UPDATE iad011 INNER JOIN trabajador ON iad011.responsable = trabajador.clave SET iad011.responsable = trabajador.curp",
"UPDATE iad013 INNER JOIN trabajador ON iad013.responsable = trabajador.clave SET iad013.responsable = trabajador.curp",
"UPDATE iad015 INNER JOIN trabajador ON iad015.responsable = trabajador.clave SET iad015.responsable = trabajador.curp",
"UPDATE iad017 INNER JOIN trabajador ON iad017.responsable = trabajador.clave SET iad017.responsable = trabajador.curp",
"UPDATE iad019 INNER JOIN trabajador ON iad019.responsable = trabajador.clave SET iad019.responsable = trabajador.curp",
"UPDATE iad023 INNER JOIN trabajador ON iad023.autoridad = trabajador.clave SET iad023.autoridad = trabajador.curp",
"UPDATE iad025 INNER JOIN trabajador ON iad025.responsable = trabajador.clave SET iad025.responsable = trabajador.curp",
"UPDATE ila005 INNER JOIN trabajador ON ila005.responsable = trabajador.clave SET ila005.responsable = trabajador.curp",
"UPDATE iop007 INNER JOIN trabajador ON iop007.responsable = trabajador.clave SET iop007.responsable = trabajador.curp",
"UPDATE iop006 INNER JOIN trabajador ON iop006.responsable = trabajador.clave SET iop006.responsable = trabajador.curp",
"UPDATE ipe007 INNER JOIN trabajador ON ipe007.responsable = trabajador.clave SET ipe007.responsable = trabajador.curp");

mysql_select_db($database_bd2, $bd2);

$maxAlter = count($query_update) -1;
for($i=0;$i<=$maxAlter;$i++){
	$dtAlter = mysql_query($query_update[$i], $bd2) or die(mysql_error());	
}

echo "<br>Segunda fase compleatada correctamente";
echo "<br>Elproceso se ha completado de manera exitosa";
?>