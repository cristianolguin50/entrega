<?php require_once('Connections/bd2.php'); ?>
<?php
//initialize the session
session_start();

// ** Logout the current user. **
$logoutAction = $_SERVER['PHP_SELF']."?doLogout=true";
if ((isset($_SERVER['QUERY_STRING'])) && ($_SERVER['QUERY_STRING'] != "")){
  $logoutAction .="&". htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_GET['doLogout'])) &&($_GET['doLogout']=="true")){
  //to fully log out a visitor we need to clear the session varialbles
  session_unregister('MM_Username');
  session_unregister('MM_UserGroup');
  session_unregister('clave_dependencia');
	
  $logoutGoTo = "salida.htm";
  if ($logoutGoTo) {
    header("Location: $logoutGoTo");
    exit;
  }
}
?>
<?php
//initialize the session
$colname_usuario = "1";
if (isset($_SESSION['MM_Username'])) {
  $colname_usuario = (get_magic_quotes_gpc()) ? $_SESSION['MM_Username'] : addslashes($_SESSION['MM_Username']);
}
mysql_select_db($database_bd2, $bd2);
$query_usuario = sprintf("SELECT * FROM usuario WHERE usuario = '%s'", $colname_usuario);
$usuario = mysql_query($query_usuario, $bd2) or die(mysql_error());
$row_usuario = mysql_fetch_assoc($usuario);
$totalRows_usuario = mysql_num_rows($usuario);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
.menutitle{
	cursor:pointer;
	margin-bottom: 5px;
	background-color:#205375;
	color:#FFFFFF;
	width:200px;
	padding:2px;
	text-align:center;
	font-weight:bold;
	/*/*/border:1px solid #000000;/* */
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	text-transform: uppercase;
}

.submenu{
	margin-bottom: 0.5em;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	text-transform: capitalize;
	text-align: center;
	letter-spacing: normal;
	word-spacing: 0.05em;
}
body {
	margin-left: 2px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #EEEEEE;
}
.nolink {
	color: #000000;
	background-color: #fae3b7;
	width: 200px;
	text-align: center;
	padding: 2px;
	/*/*/border:1px solid #000000;/* */
}
.Estilo1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 10px;
}
.Estilo2 {
	font-size: 11px;
	font-weight: bold;
}
</style>

<script type="text/javascript">

/***********************************************
* Switch Menu script- by Martial B of http://getElementById.com/
* Modified by Dynamic Drive for format & NS4/IE4 compatibility
* Visit http://www.dynamicdrive.com/ for full source code
***********************************************/

var persistmenu="yes" //"yes" or "no". Make sure each SPAN content contains an incrementing ID starting at 1 (id="sub1", id="sub2", etc)
var persisttype="sitewide" //enter "sitewide" for menu to persist across site, "local" for this page only

if (document.getElementById){ //DynamicDrive.com change
document.write('<style type="text/css">\n')
document.write('.submenu{display: none;}\n')
document.write('</style>\n')
}

function SwitchMenu(obj){
	if(document.getElementById){
	var el = document.getElementById(obj);
	var ar = document.getElementById("masterdiv").getElementsByTagName("span"); //DynamicDrive.com change
		if(el.style.display != "block"){ //DynamicDrive.com change
			for (var i=0; i<ar.length; i++){
				if (ar[i].className=="submenu") //DynamicDrive.com change
				ar[i].style.display = "none";
			}
			el.style.display = "block";
		}else{
			el.style.display = "none";
		}
	}
}

function get_cookie(Name) { 
var search = Name + "="
var returnvalue = "";
if (document.cookie.length > 0) {
offset = document.cookie.indexOf(search)
if (offset != -1) { 
offset += search.length
end = document.cookie.indexOf(";", offset);
if (end == -1) end = document.cookie.length;
returnvalue=unescape(document.cookie.substring(offset, end))
}
}
return returnvalue;
}

function onloadfunction(){
if (persistmenu=="yes"){
var cookiename=(persisttype=="sitewide")? "switchmenu" : window.location.pathname
var cookievalue=get_cookie(cookiename)
if (cookievalue!="")
document.getElementById(cookievalue).style.display="block"
}
}

function savemenustate(){
var inc=1, blockid=""
while (document.getElementById("sub"+inc)){
if (document.getElementById("sub"+inc).style.display=="block"){
blockid="sub"+inc
break
}
inc++
}
var cookiename=(persisttype=="sitewide")? "switchmenu" : window.location.pathname
var cookievalue=(persisttype=="sitewide")? blockid+";path=/" : blockid
document.cookie=cookiename+"="+cookievalue
}

if (window.addEventListener)
window.addEventListener("load", onloadfunction, false)
else if (window.attachEvent)
window.attachEvent("onload", onloadfunction)
else if (document.getElementById)
window.onload=onloadfunction

if (persistmenu=="yes" && document.getElementById)
window.onunload=savemenustate

</script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t&iacute;tulo</title>
</head>

<body>
<!-- Keep all menus within masterdiv-->
<div id="masterdiv">
	<?php
 	if ($row_usuario['admin']==1){
	?>
	<div class="menutitle" onclick="SwitchMenu('sub1')"> <img src="images/vineta_down.gif" width="16" height="16" />&nbsp; Configuraci&oacute;n del Sistema</div>
	<span class="submenu" id="sub1">&curren; <a href="contenido/menu3.php" target="mainFrame">Datos Generales del Ayuntamiento</a><br>
	&curren; 
	<a href="contenido/menu_dep.php" target="mainFrame">Dependencias del Ayuntamiento</a><br>
	&curren; 
	<a href="contenido/usuarios/usuarios.php" target="mainFrame">Administración de Usuarios</a><br>
	<br>
	</span>
	<?php
	}
 	if ($row_usuario['rh']==1){
	?>
	<div class="menutitle" onclick="SwitchMenu('sub2')"> <img src="images/vineta_down.gif" width="16" height="16" /> &nbsp;Recursos Humanos</div>
	<span class="submenu" id="sub2"><div class="nolink"><b>Trabajadores</b></div>
		&curren; <a href="contenido/rec_hum/plazas/puesto1.php" target="mainFrame">Cat&aacute;logo de puestos y Plazas </a><br>
		&curren; <a href="contenido/rec_hum/trabajador1.php" target="mainFrame">Agregar trabajador(es) </a><br>
		&curren; <a href="contenido/rec_hum/busca_trab.php" target="mainFrame">Buscar registro(s) </a><br>
		&curren; <a  href="contenido/rec_hum/comisionado/comision.php" target="mainFrame">personal comisionado </a><br>
		&curren; <a href="faqs.htm">Emitir Gafetes pendientes</a><br>
		&curren; <a href="help.htm">Emitir Nombramientos pendientes</a><br>
		<br>
		<div class="nolink"><b>Personal Sindicalizado</b></div>
		&curren; <a href="faqs.htm">Sindicalizar Personal</a><br>
		&curren; 
		<a href="help.htm">Convenios Sindicales</a><br>
		&curren; 
		<a href="notice.htm">Personal Comisionado</a><br>
		&curren; 
		<a href="faqs.htm">Juicios Laborales</a><br>
		<br>
		<div class="nolink"><b>Reportes</b></div>
		&curren; <a href="help.htm">movimientos de personal</a><br>
		&curren; 
		<a href="faqs.htm">Control de Asistencias</a><br>
		<br>
  </span>
	<?php
	}
	if($row_usuario['bp']==1){
	?>
 	<div class="menutitle" onclick="SwitchMenu('sub3')"> <img src="images/vineta_down.gif" width="16" height="16" />&nbsp;Servicios m&eacute;dicos </div>
	<span class="submenu" id="sub3">&curren; 
	<a href="new.htm">Historial Cl&iacute;nico </a><br>&curren; 
	<a href="hot.htm">Consultas</a><br>	&curren; 
	<a href="revised.htm">Emisi&oacute;n de recetas </a><br>
	&curren;
	<a href="new.htm">Personal M&eacute;dico </a><br>
	&curren; 
	<a href="hot.htm">Agenda de citas </a><br>	
	&curren; 
	<a href="revised.htm">Farmacia</a><br>
	<br>
  </span>
 
	
	<div class="menutitle" onclick="SwitchMenu('sub4')"><img src="images/vineta_down.gif" width="16" height="16" /> &nbsp;patrimonio municipal </div>
    <span class="submenu" id="sub4"><div class="nolink"><b>Bienes Muebles por Dependencia</b></div>
		&curren; <a href="faqs.htm">Control de Bienes Muebles por Dependencia </a><br>
		&curren; 
		<a href="help.htm">Bienes dados de baja </a><br>
		<br>
		<div class="nolink"><b>Bienes Inmuebles</b></div>
	  &curren; <a href="faqs.htm">Control de bienes inmuebles </a><br>
	  <br>
	  <div class="nolink"><b>Relación de Vehículos Oficiales</b></div>
		&curren; <a href="notice.htm">Control de Veh&iacute;culos oficiales </a><br>
		&curren; 
		<a href="faqs.htm">Reparaci&oacute;n de Veh&iacute;culos </a><br>
		<br>
  </span>
	<?php
	}
	 if ($row_usuario['admin_er']==1){
	?>
	<div class="menutitle" onclick="SwitchMenu('sub5')"><img src="images/vineta_down.gif" width="16" height="16" /> &nbsp;entrega - recepci&oacute;n </div>
    <span class="submenu" id="sub5">&curren; <a href="faqs.htm">Configuraci&oacute;n de Formatos por Dependencia </a><br>
    &curren; 
    <a href="help.htm">Modificar la configuraci&oacute;n </a><br>
	<?php
	}
 	if ($row_usuario['er']==1){
	?>  
    &curren; 
    <a href="help.htm">Formatos de Entrega-Receci&oacute;n  </a><br>
    <?php
	}
 	if ($row_usuario['er']==1){
	?>
	&curren;
    <a href="faqs.htm">Acta de Entrega-Recepci&oacute;n</a><br>
    &curren; 
    <a href="notice.htm">Documentaci&oacute;n oficial PERTAM </a><br>
    <br>
  </span>
 
   	<?php
	}
	?>  
  <div class="menutitle" onclick="SwitchMenu('sub7')"><img src="images/vineta_down.gif" width="16" height="16" /> &nbsp;Salir del sistema </div>
  <span class="submenu" id="sub7">&curren; <a href="<?php echo $logoutAction ?>" target="_parent">Salir del sistema  </a><br>
  		<br>
  </span>

</div>

<div align="center">
  <p>&nbsp;</p>
</div>
</body>
</html>
