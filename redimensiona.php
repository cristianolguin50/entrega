<?php

function minis($imagen, $mini, $nom){
//echo $imagen;
$destino=$mini.$nom.".jpg";
$original =imagecreatefromjpeg($imagen);
$ancho_orig=imagesx($original);
$alto_orig=imagesy($original);

if ($ancho_orig==$alto_orig){
	$band=1;
	if($ancho_orig>500){
		$tam1=500;
		$tam2=500;
	}
	else{
		$tam1=$ancho_orig;
		$tam2=$alto_orig;
	}
}
		
else {
	if($ancho_orig<$alto_orig){
		$band=2;
		if($ancho_orig>480)
			$tam1=480;
		else
			$tam1=$ancho_orig;
			
		if($alto_orig>640)
			$tam2=640;
		else
			$tam2=$alto_orig;
			
	}
	else{
		$band=2;
		if($ancho_orig>640)
			$tam1=640;
		else
			$tam1=$ancho_orig;
			
		if($alto_orig>480)
			$tam2=480;
		else
			$tam2=$alto_orig;
			
	}
}

$ancho=$tam1;
$alto=$tam2;

$margenx=2;
$margeny=2;

$ancho_min=$ancho + $margenx;
$alto_min=$alto + $margeny;
$thumb=imagecreatetruecolor($ancho_min, $alto_min);
imagecopyresampled($thumb, $original, 1,1,1,1,$ancho,$alto,$ancho_orig, $alto_orig);
imagejpeg($thumb,$destino,90);

}

function minis2($imagen, $mini, $nom){
//echo $imagen;
$destino=$mini.$nom.".jpg";
$original =imagecreatefromjpeg($imagen);
$ancho_orig=imagesx($original);
$alto_orig=imagesy($original);

if ($ancho_orig==$alto_orig){
	$band=1;
	if($ancho_orig>150){
		$tam1=150;
		$tam2=150;
	}
	else{
		$tam1=$ancho_orig;
		$tam2=$alto_orig;
	}
}
		
else {
	if($ancho_orig<$alto_orig){
		$band=2;
		if($ancho_orig>120)
			$tam1=120;
		else
			$tam1=$ancho_orig;
			
		if($alto_orig>160)
			$tam2=160;
		else
			$tam2=$alto_orig;
			
	}
	else{
		$band=2;
		if($ancho_orig>160)
			$tam1=160;
		else
			$tam1=$ancho_orig;
			
		if($alto_orig>120)
			$tam2=120;
		else
			$tam2=$alto_orig;
			
	}
}

$ancho=$tam1;
$alto=$tam2;

$margenx=2;
$margeny=2;

$ancho_min=$ancho + $margenx;
$alto_min=$alto + $margeny;
$thumb=imagecreatetruecolor($ancho_min, $alto_min);
imagecopyresampled($thumb, $original, 1,1,1,1,$ancho,$alto,$ancho_orig, $alto_orig);
imagejpeg($thumb,$destino,90);

}


?>
