<?php require_once('../../Connections/bd2.php'); ?>
<?php
	
	/*	Modificado por: Marco Robles
	Fecha: 14/09/2018	*/
	
	session_start();
	//die();
	
	if (!isset($_SESSION['MM_Username'])){
		header("Location: ../../adios.php");
	}
	
	mysql_select_db($database_bd2, $bd2);
	$query_depend = "SELECT * FROM dependencia ORDER BY clave";
	$depend = mysql_query($query_depend, $bd2) or die(mysql_error());
	$row_depend = mysql_fetch_assoc($depend);
	$totalRows_depend = mysql_num_rows($depend);
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
	<head>
		<link href="../../css/idots.css" rel="stylesheet" type="text/css">
		<title>Unidades Administrativas</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		
		<link type="text/css" href="../../js/jquery-ui-1.8.23.custom/css/south-street/jquery-ui-1.8.23.custom.css" rel="stylesheet" />	
		<script type="text/javascript" src="../../js/jquery-ui-1.8.23.custom/js/jquery-1.8.0.min.js"></script>
		<script type="text/javascript" src="../../js/jquery-ui-1.8.23.custom/js/jquery-ui-1.8.23.custom.min.js"></script>
		
		<script language="JavaScript" type="text/JavaScript">
			function MM_goToURL() {
				var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
				for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}
		</script>
		
		<script type="text/javascript">
			$( function() {
				$( "#dialog" ).dialog({
					autoOpen: false,
					dialogClass: 'hide-close',
					resizable: false,
					modal: true,
					buttons: {
						"Aceptar": function() {
							$( this ).dialog( "close" );
							setTimeout ('abrirMensajeDos()', 750); 
						}
					}
				});
			});
			
			$( function() {
				$( "#dialogDos" ).dialog({
					autoOpen: false,
					dialogClass: 'hide-close',
					resizable: false,
					modal: true,
					buttons: {
						"Aceptar": function() {
							$( this ).dialog( "close" );
						}
					}
				});
			});	
			
			function abrirMensaje(){
				var e = document.getElementById("dialog");
				e.style.display = 'block';
				e.innerHTML = '<strong>Antes de cargar su nuevo organigrama y de realizar la matriz de equivalencia, se les recomienda hacer un respaldo de informaci�n.</strong>'; 
				$( "#dialog" ).dialog( "open" );
			}
			
			function abrirMensajeDos(){
				var e = document.getElementById("dialogDos");
				e.style.display = 'block';
				e.innerHTML = '<strong>Para usar este apartado, invariablemente se deber� de contar con asesor�a.</strong>'; 
				$( "#dialogDos" ).dialog( "open" );
			}
		</script>
		
		<script type="text/javascript">
			function confirma (){
				
				$(function() {
					$( "#dialog-confirm" ).dialog({
						resizable: false,
						width:400, 
						height:200,
						modal: true,
						buttons: {
							"S�": function() {
								document.getElementById("form1").submit();
							},
							"No": function() {
								$( this ).dialog( "close" );
							},
							Cancel: function() {
								$( this ).dialog( "close" );
							}
						}
					});
				});
			}
		</script>
		
		<script language="JavaScript" src="_scripts.js" type="text/javascript"></script>
		<script language="javascript" src="../../js/validate.js" type="text/javascript"></script>
		<style type="text/css">
			body {
			margin-top: 0px;
			}
			.style1 {
			color: #FF0000;
			font-style: italic;
			}
			.style2 {font-size: 110%;}
			.style3 {color: #FF0000;
			font-style: italic;
			}
			
			.hide-close .ui-dialog-titlebar-close { display: none }
			
			.ui-dialog .ui-dialog-titlebar {
			padding: 0.1em .5em;
			position: relative;
			font-size: 1.2em;
			}
			
			.ui-dialog .ui-dialog-buttonpane {
			padding: 0.1em .5em;
			position: relative;
			font-size: 1.5em;
			font-style: bold;
			}
		</style>
	</head>
	
	<body onLoad="abrirMensaje();">
		<script language="JavaScript" type="text/javascript">
			
			//alert("Antes de cargar su nuevo organigrama y de realizar la matriz de equivalencia, se les recomienda hacer un respaldo de informaci�n.");
			//	alert("Para usar este apartado, invariablemente se deber� de contar con la asesor�a del OSFEM � la empresa desarrolladora del software.");
			
		</script>
		<div align="center"><h2 style="color: #0404B4">Unidades Administrativas</h2></div>
		<form id="form1" name="form1" method="post" action="carga_organigrama.php" enctype="multipart/form-data">
			<div align="center">
				<table width="60%" border="1" cellpadding="3" cellspacing="0">
					
					<tr>
						<td colspan="4"><div align="center"><font size="3"><strong>Matriz de equivalencia</strong></font></div></td>
					</tr>
					
					<tr>
						<td colspan="4"><div align="center" class="divSideboxHeader"><a href="matriz_equi.php"><img src="../../images/datata.png" width="32" height="32" title="Haga clic en este icono para mover la informaci�n de una Unidad Administrativa a otra."></a></div></td>
					</tr>
					
				</table>
				<br /><br />
				<table width="60%" border="1" cellpadding="3" cellspacing="0">
					
					<tr>
						<td colspan="4"><div align="center"><strong>Ingrese los datos de las Unidades Administrativas</strong></div></td>
					</tr>
					
					<tr>
						<td><div align="right"><strong>Entidad Fiscalizable:</strong></div></td>
						<td>
							<div align="left">
								<input type="radio" name="tipo_entidad" id="tipo_entidad" value="1" checked >
								Ayuntamiento<br>
								
								<input type="radio" name="tipo_entidad" id="tipo_entidad" value="2" >
								DIF <br>
								
								<input type="radio" name="tipo_entidad" id="tipo_entidad" value="3" >
								ODAS<br>
								
								<input type="radio" name="tipo_entidad" id="tipo_entidad" value="4" >
								IMCUFIDE<br>
							</div>
						</td>
					</tr>
					
					
					<tr>
						<td><div align="right"><strong>Organigrama Vigente (csv):</strong></div></td>
						<td><div align="left">
							<input type="file" name="anexo" id="anexo" accept=".csv" >
						<a href="#" class="hintanchor" onMouseOver="showhint('<center>Haga clic en el bot�n <b><i>Examinar</i></b> y elija el archivo que desea cargar. <b>(El archivo debe de estar en formato .csv).</b></center>', this, event, '150px')">[?]</a></div></td>
					</tr>
					
					
					<tr>
						<td><div align="right"><strong>Asociaci&oacute;n de Unidades Administrativas (csv):</strong></div></td>
						<td colspan="2"><div align="left">
							<input  type="file" name="anexo2" id="anexo2" accept=".csv">
						<a href="#" class="hintanchor" onMouseover="showhint('<center>Haga clic en el bot�n <b><i>Examinar</i></b> y elija el archivo que desea cargar. <b>(El archivo debe de estar en formato .csv).</b></center>', this, event, '150px')">[?]</a></div></td>
						
					</tr>
					
					<tr>
						<td colspan="4"><div align="center">
							<input name="regresa" type="button" onClick="MM_goToURL('self','depend2.php');return document.MM_returnValue" value="Regresar">
							<input type="button" name="enviar" value="Almacenar Datos" onClick="confirma()">
						</div></td>
					</tr>
					
				</table>
			</div>
			<!--<p align="center">  	
				<input name="regresa" type="button" onClick="MM_goToURL('self','depend2.php');return document.MM_returnValue" value="Regresar">
				<input type="submit" name="enviar" value="Almacenar Datos" >
			</p>-->
			<div align="center">
				<input type="hidden" name="MM_insert" value="form1">
			</div>
		</form>
		
		<div id="dialog" title="Respaldo:"></div>
		<div id="dialogDos" title="Aviso:"></div>
		<div id="dialog-confirm" style='display:none;' title="Aviso">
		<h3><strong><img src="../../images/X.png" style="float:left; margin:10px;" ></span>Si realiza esta acci&oacute;n de forma incorrecta puede perder la informaci&oacute;n almacenada.<br/><br/>�Est&aacute; seguro de continuar?</strong></h3>
	</div>
</body>
</html>			