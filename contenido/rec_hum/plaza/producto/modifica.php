<?php require_once('../../../../Connections/bd2.php'); ?>
<?php

session_start();

if (!isset($_SESSION['MM_Username'])){
	header("Location: ../../adios.php");
}

$clave=$_REQUEST['oculto2'];

mysql_select_db($database_bd2, $bd2);
$query_cat = sprintf("SELECT * FROM `producto` WHERE clave = $clave ORDER BY clave ASC");
$cat = mysql_query($query_cat, $bd2) or die(mysql_error());
$row_cat = mysql_fetch_assoc($cat);
$totalRows_cat = mysql_num_rows($cat);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="../../../../css/idots.css" rel="stylesheet" type="text/css">
<title>Alta de un nuevo registro</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" type="text/javascript">
function MM_goToURL() { //v3.0
	var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
	for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

function pasa(valor, destino){
	destino.value=valor;
}

function validate(){
	var cad1=document.form1.clave_cat.value;
	var cad2=document.form1.nombre.value;
	var lon1;
	var lon2;
	lon1=cad1.length;
	lon2=cad2.length;

	if((lon1>0) && (lon2>0)){
		document.form1.guarda.disabled=false;
	}
	if ((lon1<=0) || (lon2<=0)){
		document.form1.guarda.disabled=true;
	}
}
</script>
<script language="JavaScript" src="../../../producto/_scripts.js" type="text/javascript"></script>
<script language="javascript" src="../../../../js/validate.js" type="text/javascript"></script>

<style type="text/css">
	body {
		margin-top: 0px;
	}
	.style1 {
		font-size: 110%;
		font-weight: bold;
	}
	.style2 {
		color: #FF0000;
		font-style: italic;
	}
	.style3 {color: #FF0000}
</style>
</head>

<body onLoad="document.form1.clave_cat.focus();">
<p align="center" class="divSideboxHeader style1">CAT&Aacute;LOGO DE PRODUCTOS Y SERVICIOS</p>
<form name="form1" method="post" action="../../../producto/modifica2.php" enctype="multipart/form-data"><div align="center">
<table width="95%"  border="1" align="center" cellpadding="3" cellspacing="0">
	<tr>
		<td width="40%"><div align="right"><strong>*Clave:</strong></div></td>
		<td bgcolor="#FFFFFF"><div align="left">
			<input name="clave_cat" type="text" id="clave_cat" size="15" onKeyPress="return pulsar(event)" onKeyUp="return handleEnter(this, event)" value="<?php echo $row_cat['clave_cat']; ?>" onBlur="validate();" tabindex="1">
			<input name="clave" type="hidden" id="clave" value="<?php echo $row_cat['clave']; ?>"></div></td>
	</tr>
	<tr>
		<td><div align="right"><strong>*Nombre:</strong></div></td>
		<td bgcolor="#FFFFFF"><div align="left">
			<input name="nombre" type="text" id="nombre" size="45" onKeyPress="return pulsar(event)" onKeyUp="return handleEnter(this, event)" value="<?php echo $row_cat['nombre']; ?>" onBlur="validate();" tabindex="2"></div></td>
	</tr>
	<tr>
		<td width="40%"><div align="right"><strong>Tipo:</strong></div></td>
		<td bgcolor="#FFFFFF"><div align="left">
		<?php 
		if ($row_cat['tipo']=='P') {
			echo "<input name=\"select\" type=\"radio\" onkeypress=\"return handleEnter(this, event)\"  onClick=\"pasa(this.value, tipo)\" value=\"P\" tabindex=\"3\" checked>";
		}
		else {
			echo "<input name=\"select\" type=\"radio\" onkeypress=\"return handleEnter(this, event)\"  onClick=\"pasa(this.value, tipo)\" value=\"P\" tabindex=\"3\">";
		}
		?> Producto 
		<?php
		if ($row_cat['tipo']=='S') {
			echo "<input name=\"select\" type=\"radio\" value=\"S\" onkeypress=\"return handleEnter(this, event)\" onClick=\"pasa(this.value, tipo)\" tabindex=\"4\" checked>";
		}
		else {
			echo "<input name=\"select\" type=\"radio\" value=\"S\" onkeypress=\"return handleEnter(this, event)\" onClick=\"pasa(this.value, tipo)\" tabindex=\"4\">";
		}
		?> Servicio 
			<input name="tipo" type="hidden" id="tipo" value="<?php echo $row_cat['tipo']; ?>"><a href="#" class="hintanchor" onMouseover="showhint('<center>Indique si es producto o servicio</center>', this, event, '150px')">[?]</a></div></td>
	</tr>
	<tr>
		<td colspan="2"><div align="center"><span class="style3"><strong>NOTA:</strong></span><span class="style2"> Los campos marcados con * son obligatorios</span></div></td>
	</tr>
</table>

<p>&nbsp;</p>
<p align="center">
	<input name="Button" type="button" onClick="window.close()" value="Cerrar">
	<input name="limpia" type="reset" id="limpia" value="Limpiar Formulario">
    <input name="guarda" type="submit" disabled="true" id="guarda" value="Modificar datos" tabindex="3"></p>

</div>
</form>
</body>
</html>

<?php
mysql_free_result($cat);
?>