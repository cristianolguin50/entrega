<?php require_once('../../Connections/bd2.php'); ?>
<?php
session_start();
if (!isset($_SESSION['MM_Username'])){
	header("Location: ../../adios.php");
}


$fallo="../../no_perm.php";
$usuario=$_SESSION['MM_Username'];

$colname_trab = "1";
if (isset($_REQUEST['mod'])) {
  $colname_trab = (get_magic_quotes_gpc()) ? $_REQUEST['mod'] : addslashes($_REQUEST['mod']);
}

mysql_select_db($database_bd2, $bd2);
$query_trab = sprintf("SELECT * FROM trabajador WHERE clave = %s", $colname_trab);
$trab = mysql_query($query_trab, $bd2) or die(mysql_error());
$row_trab = mysql_fetch_assoc($trab);
$totalRows_trab = mysql_num_rows($trab);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script language="javascript" src="../../js/validate.js"></script>
<script language="javascript" src="../../js/cal2.js" type="text/javascript"></script>
<script language="javascript" src="../../js/cal_conf2.js" type="text/javascript"></script>
<link href="../../css/idots.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t&iacute;tulo</title>
<script language="javascript" type="text/javascript">
function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
function validate(valor){
var cad1=document.form1.fech1.value;
var cad2=document.form1.motivo.value;
var lon1;
var lon2;
lon1=cad1.length;
lon2=cad2.length;

if((lon1>0) && (lon2>0)){
	document.form1.guarda.disabled=false;
	}
if ((lon1<=0) || (lon2<=0)){
document.form1.guarda.disabled=true;
}
}

</script>
<style type="text/css">
<!--
.Estilo1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-weight: bold;
	font-size: 11px;
}
.Estilo2 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
}
body {
	margin-top: 0px;
}
.style3 {color: #FF0000;
	font-style: italic;
}
-->
</style></head>

<body>
<p align="center"><img src="../../images/titles/rec_hum.gif" width="167" height="16"></p>
<p align="center"><strong>BAJA DE UN TRABAJADOR</strong></p>
<form id="form1" name="form1" method="post" action="guarda_baja.php">
  <table width="80%"  border="1" align="center" cellspacing="0" cellpadding="3">
    
    <tr>
      <td><div align="right"><strong>Clave:</strong></div></td>
      <td bgcolor="#FFFFFF"><?php echo $row_trab['numero'];  ?>
      <input name="numero" type="hidden" id="numero" value="<?php echo $row_trab['numero'];  ?>" /></td>
    </tr>
    <tr>
      <td width="34%"><div align="right" class="Estilo1">Nombre Trabajador: </div></td>
      <td width="66%" bgcolor="#FFFFFF"><div align="left"><?php echo $row_trab['nombre_comp'];  ?>
              <input name="nombre1" type="hidden" id="nombre1" value="<?php echo $row_trab['nombre_comp'];  ?>" />
      </div></td>
    </tr>
    <tr>
      <td><div align="right" class="Estilo1">RFC:</div></td>
      <td bgcolor="#FFFFFF"><div align="left"><?php echo $row_trab['rfc']; ?>
              <input name="rfc" type="hidden" id="rfc" value="<?php echo $row_trab['rfc']; ?>" />
      </div></td>
    </tr>
    <tr>
      <td><div align="right"><strong><span class="Estilo2">�rea de adscripci�n</span>:</strong></div></td>
      <td bgcolor="#FFFFFF"><div align="left"><?php echo $row_trab['area']; ?>
              <input name="direccion1" type="hidden" id="direccion1" value="<?php echo $row_trab['area']; ?>" />
      </div></td>
    </tr>
    <tr>
      <td><div align="right"><strong>Cargo:</strong></div></td>
      <td bgcolor="#FFFFFF"><div align="left"><?php echo $row_trab['cargo']; ?>
              <input name="tel1" type="hidden" id="tel1" value="<?php echo $row_trab['cargo']; ?> ">
      </div></td>
    </tr>
    <tr>
      <td><div align="right"><strong>* Fecha de baja: </strong></div></td>
      <td bgcolor="#FFFFFF"><div align="left">
          <input name="fech1" type="text" id="fech1" size="10" readonly="true" />
          <a href="javascript:showCal('Calendar1')" tabindex="1" onblur="validate();"><img src="../../images/nuevos/FIREWORK/FORMATOSENTREGARECEPCION.gif" width="16" height="16" border="0" /></a><a href="#" class="hintanchor" onmouseover="showhint('<center>Especifique la fecha de baja del trabajador.</center>', this, event, '150px')"> [?]</a></div></td>
    </tr>
    <tr>
      <td><div align="right"><strong>* Motivo:</strong></div></td>
      <td bgcolor="#FFFFFF"><div align="left">
        <textarea name="motivo" cols="50" id="motivo" onKeyPress="return pulsar(event)" onblur="validate();" tabindex="2"></textarea>
	  <a href="#" class="hintanchor" onMouseOver="showhint('<center> Indique cual fue el motivo de la baja del trabajador. </center>', this, event, '150px')">[?]</a></div></td>
    </tr>
    <tr>
      <td><div align="right"><strong>Observaciones:</strong></div></td>
      <td bgcolor="#FFFFFF"><textarea name="observaciones" cols="50" id="observaciones" onKeyPress="return pulsar(event)" onblur="validate();" tabindex="3"></textarea>
      <a href="#" class="hintanchor" onmouseover="showhint('<center> Escriba si es necesario alguna nota que referencie esta baja.</center>', this, event, '150px')">[?]</a></td>
    </tr>
    <tr bgcolor="#FFFFFF">
      <td colspan="2"><div align="center"><span class="style3"><strong>NOTA</strong>: Los campos marcados con * son obligatorios</span></div></td>
    </tr>
  </table>
  <p align="center">
    <input name="Button" type="button" onclick="MM_goToURL('self','busca_trab.php');return document.MM_returnValue" value="Regresar" />
    <input name="limpia" type="reset" id="limpia" value="Limpiar Formulario" />
    <input name="guarda" type="submit" disabled="true" id="guarda" tabindex="4"  onkeypress="return handleEnter(this, event)" value="Guardar datos" />
  </p>
</form>

</body>
</html>
