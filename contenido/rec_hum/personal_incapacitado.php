<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t&iacute;tulo</title>
<style type="text/css">
<!--
.Estilo1 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-weight: bold;
	font-size: 11px;
}
-->
</style>
</head>
<link href="../../css/idots.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body {
	margin-top: 0px;
}
.style1 {
	font-size: 110%;
	font-weight: bold;
}
.style2 {font-size: 110%}
.style3 {	color: #FF0000;
	font-style: italic;
}
.Estilo1 {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-weight: bold;
	font-size: 11px;
}
-->
</style>
<script language="javascript" src="../../js/validate.js"></script>
<body>
<form id="form1" name="form1" method="post" action="">
  <div align="center"><strong>PERSONAl INCAPACITADO </strong>
      <table width="80%"  border="1" align="center" cellspacing="0" cellpadding="3">
        <tr>
          <td width="32%"><div align="right" class="Estilo1"> *&Aacute;rea: </div></td>
          <td width="68%"><div align="left">
              <select name="area" id="area" tabindex="1">
            </select>
          <a href="#" class="hintanchor" onmouseover="showhint('&lt;center&gt; Nombre del Departamento en el Cual se Encuentra Adscrito el Trabajador. &lt;/center&gt;', this, event, '150px')">[?]</a></div></td>
        </tr>
        <tr>
          <td><div align="right"><strong>*Trabajador: </strong></div></td>
          <td><div align="left">
              <select name="trab" id="trab" tabindex="2">
            </select>
          <a href="#" class="hintanchor" onmouseover="showhint('&lt;center&gt; Nombre del Empleado Incapacitado. &lt;/center&gt;', this, event, '150px')">[?]</a></div></td>
        </tr>
        <tr>
          <td><div align="right" class="Estilo1">Expediente: </div></td>
          <td><div align="left">
              <input type="text" name="textfield" tabindex="3"onKeyPress="return pulsar(event)" onKeyUp="return handleEnter(this, event)">
          <a href="#" class="hintanchor" onmouseover="showhint('&lt;center&gt; N&uacute;mero de Expediente que se le Asigna a Cada Empleado. &lt;/center&gt;', this, event, '150px')">[?]</a></div></td>
        </tr>
        <tr>
          <td><div align="right" class="Estilo1">Cargo: </div></td>
          <td><div align="left">
              <input type="text" name="textfield2" tabindex="4" onKeyPress="return pulsar(event)" onKeyUp="return handleEnter(this, event)">
          <a href="#" class="hintanchor" onmouseover="showhint('&lt;center&gt; Anotar el Puesto Funcional que Desempe&ntilde;a el Empleado.  &lt;/center&gt;', this, event, '150px')">[?]</a></div></td>
        </tr>
        <tr>
          <td><div align="right" class="Estilo1">*Inicio de la Incapcidad: </div></td>
          <td><div align="left">
              <select name="dia1" id="dia1" tabindex="5" onkeypress="return handleEnter(this, event)">
                <option value="01" selected="selected">01</option>
                <option value="02">02</option>
                <option value="03">03</option>
                <option value="04">04</option>
                <option value="05">05</option>
                <option value="06">06</option>
                <option value="07">07</option>
                <option value="08">08</option>
                <option value="09">09</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
                <option value="21">21</option>
                <option value="22">22</option>
                <option value="23">23</option>
                <option value="24">24</option>
                <option value="25">25</option>
                <option value="26">26</option>
                <option value="27">27</option>
                <option value="28">28</option>
                <option value="29">29</option>
                <option value="30">30</option>
                <option value="31">31</option>
              </select>
            /
            <select name="mes1" id="mes1" tabindex="6" onkeypress="return handleEnter(this, event)">
              <option value="01" selected="selected">Enero</option>
              <option value="02">Febrero</option>
              <option value="03">Marzo</option>
              <option value="04">Abril</option>
              <option value="05">Mayo</option>
              <option value="06">Junio</option>
              <option value="07">Julio</option>
              <option value="08">Agosto</option>
              <option value="09">Septiembre</option>
              <option value="10">Octubre</option>
              <option value="11">Noviembre</option>
              <option value="12">Diciembre</option>
            </select>
            /
            <input name="year1" type="text" id="year1" size="4" maxlength="4" tabindex="7" onKeyPress="return handleEnter(this, event)" onblur=" validanumero(this);" />
            (dd/mm/aaaa) <a href="#" class="hintanchor" onmouseover="showhint('&lt;center&gt; D&iacute;a, Mes y A&ntilde;o en el Cual Inicia su Incapcidad.  &lt;/center&gt;', this, event, '150px')">[?]</a></div></td>
        </tr>
        <tr>
          <td><div align="right" class="Estilo1">*Vencimiento: </div></td>
          <td><div align="left">
              <select name="dia2" id="select" tabindex="8" onkeypress="return handleEnter(this, event)">
                <option value="01" selected="selected">01</option>
                <option value="02">02</option>
                <option value="03">03</option>
                <option value="04">04</option>
                <option value="05">05</option>
                <option value="06">06</option>
                <option value="07">07</option>
                <option value="08">08</option>
                <option value="09">09</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
                <option value="21">21</option>
                <option value="22">22</option>
                <option value="23">23</option>
                <option value="24">24</option>
                <option value="25">25</option>
                <option value="26">26</option>
                <option value="27">27</option>
                <option value="28">28</option>
                <option value="29">29</option>
                <option value="30">30</option>
                <option value="31">31</option>
            </select>
            /
            <select name="mes2" id="select2" tabindex="9" onkeypress="return handleEnter(this, event)">
              <option value="01" selected="selected">Enero</option>
              <option value="02">Febrero</option>
              <option value="03">Marzo</option>
              <option value="04">Abril</option>
              <option value="05">Mayo</option>
              <option value="06">Junio</option>
              <option value="07">Julio</option>
              <option value="08">Agosto</option>
              <option value="09">Septiembre</option>
              <option value="10">Octubre</option>
              <option value="11">Noviembre</option>
              <option value="12">Diciembre</option>
            </select>
            /
            <input name="year2" type="text" id="year2" size="4" maxlength="4" tabindex="10" onKeyPress="return handleEnter(this, event)" onblur=" validanumero(this);" />
            (dd/mm/aaaa) <a href="#" class="hintanchor" onmouseover="showhint('&lt;center&gt; D&iacute;a, Mes y A&ntilde;o en el Cual Vence su Incapcidad.  &lt;/center&gt;', this, event, '150px')">[?]</a></div></td>
        </tr>
        <tr>
          <td><div align="right" class="Estilo1">Motivo: </div></td>
          <td><div align="left">
              <textarea name="motivo" id="motivo" tabindex="11" onKeyPress="return pulsar(event)" onKeyUp="return handleEnter(this, event)"></textarea>
          <a href="#" class="hintanchor" onmouseover="showhint('&lt;center&gt; Breve Explicaci&oacute;n de la Raz&oacute;n de la Incapacidad. &lt;/center&gt;', this, event, '150px')">[?]</a></div></td>
        </tr>
        <tr>
          <td><div align="right" class="Estilo1">*Trabajador Suplente: </div></td>
          <td><div align="left">
              <select name="suplente" id="suplente" tabindex="12">
            </select>
          <a href="#" class="hintanchor" onmouseover="showhint('&lt;center&gt; Nombre de la Persona que Suple al Empleado Incapacitado &lt;/center&gt;', this, event, '150px')">[?]</a></div></td>
        </tr>
      </table>
      <p>&nbsp;      </p>
      <p>
        <input name="regresa" type="button" id="regresa" onclick="MM_goToURL('self','busca_trab.php');return document.MM_returnValue" value="Regresar" />
        <input name="limpia" type="reset" id="limpia" value="Limpiar Formulario" />
        <input name="guarda" type="submit" id="guarda" value="Guardar datos" disabled="disabled" onkeypress="return handleEnter(this, event)" tabindex="13" />
          </p>
  </div>
</form>
</body>
</html>
