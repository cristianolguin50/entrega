<?php
//define('FPDF_FONTPATH','font/');
require('../../pdf/fpdf.php');
require('../../pdf/expediente.php');
require('../../Connections/bd2.php');
/*if (!isset($_SESSION['MM_Username'])){
	header("Location: ../../adios.php");
}*/
session_start();
$colname_inventa = "1";
if (isset($_SESSION['clave_dependencia'])) {
  $colname_inventa = (get_magic_quotes_gpc()) ? $_SESSION['clave_dependencia'] : addslashes($_SESSION['clave_dependencia']);
  
 $emp=$_GET['mod'];
}

//Creaci�n del objeto de la clase heredada
$pdf=new PDF_MC_Table('P', 'mm', 'Letter');
$pdf->SetTopMargin(5);
$title='Expediente Laboral';
$pdf->SetTitle($title);

/*$colname_inventa = "1";
if (isset($_SESSION['clave_dependencia'])) {
  $colname_inventa = (get_magic_quotes_gpc()) ? $_SESSION['clave_dependencia'] : addslashes($_SESSION['clave_dependencia']);
}*/
mysql_select_db($database_bd2, $bd2);


/*$query_trab = sprintf("SELECT * FROM trabajador WHERE clave = %s", $emp);
$trab = mysql_query($query_trab, $bd2) or die(mysql_error());
$row_trab = mysql_fetch_assoc($trab);
$totalRows_trab = mysql_num_rows($trab);*/

$query_trab = sprintf("SELECT * FROM trabajador a, dependencia b WHERE a.clave = %s AND a.area=b.clave_dep", $emp);
$trab = mysql_query($query_trab, $bd2) or die(mysql_error());
$row_trab = mysql_fetch_assoc($trab);
$totalRows_trab = mysql_num_rows($trab);
//echo $query_trab;
$dep=$row_trab['area'];

mysql_select_db($database_bd2, $bd2);
$query_depen = "SELECT a.area, b.nombre, b.clave_dep FROM trabajador a, dependencia b WHERE a.area=b.clave_dep";
$depen = mysql_query($query_depen, $bd2) or die(mysql_error());
$row_depen = mysql_fetch_assoc($depen);
$totalRows_depen = mysql_num_rows($depen);


$query_ayunta = "SELECT * FROM ayuntamiento WHERE clave=1"; //sprintf("SELECT * FROM ayuntamiento WHERE clave=1");
$ayunta = mysql_query($query_ayunta, $bd2) or die(mysql_error());
$row_ayunta = mysql_fetch_assoc($ayunta);
$totalRows_ayunta = mysql_num_rows($ayunta);

$ayuntam=$row_ayunta['nombre'];
$adminIni=$row_ayunta['per_ini'];
$adminFin=$row_ayunta['per_fin'];

if ($row_trab['sexo']=='M'){
$sexo='Masculino';
}
else{
$sexo='Femenino';
}

//-----------------------------------
if ($row_trab['forma_pago']==1){
$forma='Semanal';
}
elseif ($row_trab['forma_pago']==2)
{
$forma='Quincenal';
}
elseif ($row_trab['forma_pago']==3)
{
$forma='Mensual';
}



//---------------------------
if ($row_trab['tipo_trab']=='G'){
$categoria='General';
}
else
{
$categoria='De Confianza';
}

$pdf->AliasNbPages();
$pdf->SetFont('Helvetica','',9);
$pdf->AddPage();




//echo $ayuntam;



$y=$pdf->GetY();
/*$pdf->SetY(10);
$pdf->SetX(10);
//$pdf->Image($foto,10,10,20);*/
$pdf->SetFont('Arial','B', 12, 0);
$pdf->Cell(6);
$pdf->MultiCell(210,6,"DATOS PERSONALES",0,'C');
$pdf->SetFont('Helvetica','',9);

if(file_exists("../../fotos/rh/".$emp.'/'.$emp.'.jpg')){
	$pdf->Image("../../fotos/rh/".$emp.'/'.$emp.'.jpg', 160, 50, 25, 25);
}

$pdf->Ln();
 
$pdf->Cell(30);
$pdf->Cell(30,4,"Nombre del Trabajador:",0, 0, 'R');
$pdf->SetFont('Helvetica','B',9);
$pdf->MultiCell(60,4,$row_trab['nombre_comp'],0,'L');
$pdf->SetFont('Helvetica','',9);

$pdf->Cell(30);
$pdf->Cell(30,4,"Sexo:",0, 0, 'R');
$pdf->SetFont('Helvetica','B',9);
$pdf->MultiCell(60,4,$sexo,0,'L');
$pdf->SetFont('Helvetica','',9);

$pdf->Cell(30);
$pdf->Cell(30,4,"Lugar de Nacimiento:",0, 0, 'R');
$pdf->SetFont('Helvetica','B',9);
$pdf->MultiCell(60,4,$row_trab['lugar_nac'],0,'L');
$pdf->SetFont('Helvetica','',9);

$pdf->Cell(30);
$pdf->Cell(30,4,"Fecha de Nacimiento:",0, 0, 'R');
$pdf->SetFont('Helvetica','B',9);
$pdf->MultiCell(60,4,$row_trab['dia_nace']."/".$row_trab['mes_nace']."/" .$row_trab['year_nace'],0,'L');
$pdf->SetFont('Helvetica','',9);

$pdf->Cell(30);
$pdf->Cell(30,4,"Nacionalidad:",0, 0, 'R');
$pdf->SetFont('Helvetica','B',9);
$pdf->MultiCell(60,4,$row_trab['nacionalidad'],0,'L');
$pdf->SetFont('Helvetica','',9);

$pdf->Cell(30);
$pdf->Cell(30,4,"Estado Civil:",0, 0, 'R');
$pdf->SetFont('Helvetica','B',9);
$pdf->MultiCell(60,4,$row_trab['edo_civil'],0,'L');
$pdf->SetFont('Helvetica','',9);

$pdf->Cell(30);
$pdf->Cell(30,4,"RFC:",0, 0, 'R');
$pdf->SetFont('Helvetica','B',9);
$pdf->MultiCell(60,4,$row_trab['rfc'],0,'L');
$pdf->SetFont('Helvetica','',9);

$pdf->Cell(30);
$pdf->Cell(30,4,"CURP:",0, 0, 'R');
$pdf->SetFont('Helvetica','B',9);
$pdf->MultiCell(60,4,$row_trab['curp'],0,'L');
$pdf->Ln();
 
//-------------------------------------
$pdf->SetFont('Arial','B', 12, 0);
$pdf->MultiCell(210,6,"DOMICILIO",0,'C');
$pdf->Ln();
 
$pdf->Cell(30);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(30,6,"Direcci�n:",0, 0, 'R');
$pdf->SetFont('Helvetica','B',9);
$pdf->MultiCell(60,4,$row_trab['calle'].", ".  $row_trab['colonia'] ." ". $row_trab['municipio'] .", " .  $row_trab['estado'],0,'L');

$pdf->Cell(30);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(30,4,"CP:",0, 0, 'R');
$pdf->SetFont('Helvetica','B',9);
$pdf->MultiCell(60,4,$row_trab['cp'],0,'L');

$pdf->Cell(30);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(30,4,"Tel�fono Particfular:",0, 0, 'R');
$pdf->SetFont('Helvetica','B',9);
$pdf->MultiCell(60,4,$row_trab['tel1'],0,'L');

$pdf->Cell(30);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(30,4,"Tel�fono Celular:",0, 0, 'R');
$pdf->SetFont('Helvetica','B',9);
$pdf->MultiCell(60,4,$row_trab['tel2'],0,'L');

$pdf->Cell(30);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(30,4,"Correo Electronico:",0, 0, 'R');
$pdf->SetFont('Helvetica','B',9);
$pdf->MultiCell(60,4,$row_trab['mail'],0,'L');
$pdf->Ln();
//-------------------------
$pdf->SetFont('Arial','B', 12, 0);
$pdf->MultiCell(210,6,"DATOS LABORALES",0,'C');
$pdf->Ln();
 

$pdf->Cell(30);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(30,4,"Grado m�ximo de Estudios:",0, 0, 'R');
$pdf->SetFont('Helvetica','B',9);
$pdf->MultiCell(60,4,$row_trab['gdo_estudios'],0,'L');

$pdf->Cell(30);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(30,4,"Documento que lo acredita:",0, 0, 'R');
$pdf->SetFont('Helvetica','B',9);
$pdf->MultiCell(60,4,$row_trab['doc_acredita'],0,'L');

$pdf->Cell(30);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(30,4,"Fecha de inicio de Servicio:",0, 0, 'R');
$pdf->SetFont('Helvetica','B',9);
$pdf->MultiCell(60,4,$row_trab['dia_servicio']."/".$row_trab['mes_servicio']."/" .$row_trab['year_servicio'],0,'L');

$pdf->Cell(30);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(30,4,"Dependencia en la que labora:",0, 0, 'R');
$pdf->SetFont('Helvetica','B',9);
$pdf->MultiCell(60,4,$row_trab['nombre'],0,'L');

$pdf->Cell(30);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(30,4,"Cargo:",0, 0, 'R');
$pdf->SetFont('Helvetica','B',9);
$pdf->MultiCell(60,4,$row_trab['cargo'],0,'L');

$pdf->Cell(30);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(30,4,"Sueldo Bruto:",0, 0, 'R');
$pdf->SetFont('Helvetica','B',9);
$pdf->MultiCell(60,4,$row_trab['sueldo_bruto'],0,'L');

$pdf->Cell(30);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(30,4,"No. de afiliaci�n al ISSEMYN:",0, 0, 'R');
$pdf->SetFont('Helvetica','B',9);
$pdf->MultiCell(60,4,$row_trab['issem'],0,'L');

$pdf->Cell(30);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(30,4,"Forma de pago:",0, 0, 'R');
$pdf->SetFont('Helvetica','B',9);
$pdf->MultiCell(60,4,$forma,0,'L');

$pdf->Cell(30);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(30,4,"Documento que lo acredita:",0, 0, 'R');
$pdf->SetFont('Helvetica','B',9);
$pdf->MultiCell(60,4,$row_trab['doc_acredita'],0,'L');

$pdf->Cell(30);
$pdf->SetFont('Helvetica','',9);
$pdf->Cell(30,4,"Categoria:",0, 0, 'R');
$pdf->SetFont('Helvetica','B',9);
$pdf->MultiCell(60,4,$categoria,0,'L');

$foto_ruta="../../fotos/rh/" . $row_trab['numero'] . "/trabajador.jpg";

if (file_exists($foto_ruta)){
$pdf->Image($foto_ruta,150,$y,30, 40);
}


$id_ruta1="../../fotos/rh/" . $row_trab['numero'] . "/identifica.jpg";
if (file_exists($id_ruta1)){
$pdf->AddPage();
$pdf->SetFont('Helvetica','I',10);
$pdf->MultiCell(210,6,"IDENTIFICACI�N OFICIAL",0,'L');
$y=$pdf->GetY() + 5;
$pdf->Image($id_ruta1,20,$y, 100);
}

$id_ruta2="../../fotos/rh/" . $row_trab['numero'] . "/nacimiento.jpg";
if (file_exists($id_ruta2)){
$pdf->AddPage();
$pdf->SetFont('Helvetica','I',10);
$pdf->MultiCell(210,6,"ACTA DE NACIMIENTO",0,'L');
$y=$pdf->GetY() + 5;
$pdf->Image($id_ruta2,20,$y, 180, 210);
}

$id_ruta3="../../fotos/rh/" . $row_trab['numero'] . "/domicilio.jpg";
if (file_exists($id_ruta3)){
$pdf->AddPage();
$pdf->SetFont('Helvetica','I',10);
$pdf->MultiCell(210,6,"COMPROBANTE DE DOMICILIO",0,'L');
$y=$pdf->GetY() + 5;
$pdf->Image($id_ruta3,20,$y, 150);
}

$id_ruta4="../../fotos/rh/" . $row_trab['numero'] . "/estudios.jpg";
if (file_exists($id_ruta4)){
$pdf->AddPage();
$pdf->SetFont('Helvetica','I',10);
$pdf->MultiCell(210,6,"COMPROBANTE DE ESTUDIOS",0,'L');
$y=$pdf->GetY() + 5;
$pdf->Image($id_ruta4,20,$y, 150);
}

$id_ruta5="../../fotos/rh/" . $row_trab['numero'] . "/licencia.jpg";
if (file_exists($id_ruta5)){
$pdf->AddPage();
$pdf->SetFont('Helvetica','I',10);
$pdf->MultiCell(210,6,"LICENCIA DE MANEJO",0,'L');
$y=$pdf->GetY() + 5;
$pdf->Image($id_ruta5,20,$y, 100);
}

$id_ruta6="../../fotos/rh/" . $row_trab['numero'] . "/licencia.jpg";
if (file_exists($id_ruta5)){
$pdf->AddPage();
$pdf->SetFont('Helvetica','I',10);
$pdf->MultiCell(210,6,"LICENCIA DE MANEJO",0,'L');
$y=$pdf->GetY() + 5;
$pdf->Image($id_ruta5,20,$y, 100);
}


//echo $adminIni;
//echo $adminFin;
$pdf->Output();
?> 