<?php
	require($_SERVER["DOCUMENT_ROOT"].'/entrega/Connections/bd2.php');
	
	$i = 0;
	$ver = 332;
	$nombre_archivo = "actualizaciones.txt";
	$municipio = utf8_decode('SAN MARTÍN DE LAS PIRAMIDES');
	
	$fp = fopen($nombre_archivo, "r");
	$contents = fread($fp, filesize($nombre_archivo));
	fclose($fp);
	
	if($contents != $ver){
		echo 'entrega';
		mysql_select_db($database_bd2, $bd2);
		
		$query_alter = array("ALTER TABLE `obra` ADD `ejercicio` VARCHAR( 20 ) NULL AFTER `clave`",
		"ALTER TABLE `obra` ADD `programa` VARCHAR( 50 ) NULL AFTER `ejercicio`",
		"ALTER TABLE `ica007` CHANGE `tipo_soporte` `tipo_soporte` VARCHAR(50)",
		"UPDATE `t_municipio_datos` SET numero = '021' WHERE id = '51'",
		"UPDATE `t_municipio_datos` SET numero = '086' WHERE id = '52'",
		"UPDATE `t_municipio_datos` SET numero = '038' WHERE id = '53'",
		"UPDATE `t_municipio_datos` SET numero = '123' WHERE id = '54'",
		"UPDATE `t_municipio_datos` SET numero = '061' WHERE id = '55'",
		"UPDATE `t_municipio_datos` SET numero = '008' WHERE id = '56'",
		"UPDATE `t_municipio_datos` SET numero = '103' WHERE id = '57'",
		"UPDATE `t_municipio_datos` SET numero = '074' WHERE id = '58'",
		"UPDATE `t_municipio_datos` SET numero = '029' WHERE id = '59'",
		"UPDATE `t_municipio_datos` SET numero = '097' WHERE id = '60'",
		"UPDATE `t_municipio_datos` SET numero = '118' WHERE id = '61'",
		"UPDATE `t_municipio_datos` SET numero = '087' WHERE id = '62'",
		"UPDATE `t_municipio_datos` SET numero = '098' WHERE id = '63'",
		"UPDATE `t_municipio_datos` SET numero = '045' WHERE id = '64'",
		"UPDATE `t_municipio_datos` SET numero = '039' WHERE id = '65'",
		"UPDATE `t_municipio_datos` SET numero = '062' WHERE id = '66'",
		"UPDATE `t_municipio_datos` SET numero = '043' WHERE id = '67'",
		"UPDATE `t_municipio_datos` SET numero = '111' WHERE id = '68'",
		"UPDATE `t_municipio_datos` SET numero = '040' WHERE id = '69'",
		"UPDATE `t_municipio_datos` SET numero = '016' WHERE id = '70'",
		"UPDATE `t_municipio_datos` SET numero = '088' WHERE id = '71'",
		"UPDATE `t_municipio_datos` SET numero = '034' WHERE id = '72'",
		"UPDATE `t_municipio_datos` SET numero = '075' WHERE id = '73'",
		"UPDATE `t_municipio_datos` SET numero = '076' WHERE id = '74'",
		"UPDATE `t_municipio_datos` SET numero = '030' WHERE id = '75'",
		"UPDATE `t_municipio_datos` SET numero = '124' WHERE id = '76'",
		"UPDATE `t_municipio_datos` SET numero = '048' WHERE id = '77'",
		"UPDATE `t_municipio_datos` SET numero = '041' WHERE id = '78'",
		"UPDATE `t_municipio_datos` SET numero = '056' WHERE id = '79'",
		"UPDATE `t_municipio_datos` SET numero = '113' WHERE id = '80'",
		"UPDATE `t_municipio_datos` SET numero = '035' WHERE id = '81'",
		"UPDATE `t_municipio_datos` SET numero = '049' WHERE id = '82'",
		"UPDATE `t_municipio_datos` SET numero = '046' WHERE id = '83'",
		"UPDATE `t_municipio_datos` SET numero = '057' WHERE id = '84'",
		"UPDATE `t_municipio_datos` SET numero = '101' WHERE id = '108'",
		"UPDATE `t_municipio_datos` SET numero = '125' WHERE id = '109'",
		"UPDATE `t_municipio_datos` SET prefijo = 'ELO' WHERE id = '36'",
		"UPDATE `t_municipio_datos` SET prefijo = 'LAP' WHERE id = '52'",
		"UPDATE `t_municipio_datos` SET municipio = '$municipio', prefijo = 'SAN' WHERE id = '77'",
		"UPDATE `creg_entrega`.`iep001` SET `anio` = '2015' WHERE `iep001`.`clave` =1 LIMIT 1",
		"UPDATE `creg_entrega`.`iep001` SET `anio` = '2015' WHERE `iep001`.`clave` =2 LIMIT 1",
		"UPDATE `creg_entrega`.`iep001` SET `anio` = '2015' WHERE `iep001`.`clave` =3 LIMIT 1",
		"UPDATE `creg_entrega`.`iep001` SET `anio` = '2015' WHERE `iep001`.`clave` =4 LIMIT 1",
		"INSERT INTO `iep001` (`clave`, `nombre`, `trimestre`, `anio`, `numero_complementaria`, `observacion_complementaria`, `numero_recomendacion`, `observacion_recomendacion`, `numero_promocion`, `observacion_promocion`, `numero_evaluacion`, `observacion_evaluacion`) VALUES
		(null, 'ENE-MAR', 1, 2016, 0, NULL, 0, NULL, 0, NULL, 0, NULL),
		(null, 'ABR-JUN', 2, 2016, 0, NULL, 0, NULL, 0, NULL, 0, NULL),
		(null, 'JUL-SEP', 3, 2016, 0, NULL, 0, NULL, 0, NULL, 0, NULL),
		(null, 'OCT-DIC', 4, 2016, 0, NULL, 0, NULL, 0, NULL, 0, NULL),
		(null, 'ENE-MAR', 1, 2017, 0, NULL, 0, NULL, 0, NULL, 0, NULL),
		(null, 'ABR-JUN', 2, 2017, 0, NULL, 0, NULL, 0, NULL, 0, NULL),
		(null, 'JUL-SEP', 3, 2017, 0, NULL, 0, NULL, 0, NULL, 0, NULL),
		(null, 'OCT-DIC', 4, 2017, 0, NULL, 0, NULL, 0, NULL, 0, NULL),
		(null, 'ENE-MAR', 1, 2018, 0, NULL, 0, NULL, 0, NULL, 0, NULL),
		(null, 'ABR-JUN', 2, 2018, 0, NULL, 0, NULL, 0, NULL, 0, NULL),
		(null, 'JUL-SEP', 3, 2018, 0, NULL, 0, NULL, 0, NULL, 0, NULL),
		(null, 'OCT-DIC', 4, 2018, 0, NULL, 0, NULL, 0, NULL, 0, NULL)");
		
		$maxAlter = count($query_alter) -1;
		for($i=0;$i<=$maxAlter;$i++){
			$dtAlter = mysql_query($query_alter[$i], $bd2);	
		}
		
	}
	
	if($archivo = fopen($nombre_archivo, "w"))
	{
		fwrite($archivo, $ver);		
		fclose($archivo);
	}
	
	echo "<br>Correcto!! 3.3.1<br>";
	
?>