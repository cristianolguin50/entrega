<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t&iacute;tulo</title>
</head>
<link href="../../css/idots.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body {
	margin-top: 0px;
}
.style1 {
	font-size: 110%;
	font-weight: bold;
}
.style2 {font-size: 110%}
.style3 {	color: #FF0000;
	font-style: italic;
}
-->
</style>
<script language="javascript" src="../../js/validate.js"></script>
<body>
<form id="form1" name="form1" method="post" action="">
<div align="center">
  <table width="80%"  border="1" cellspacing="0" cellpadding="3">
    <tr>
      <td colspan="2"><div align="center"><strong>MANUALES DE ORGANIZACIÓN, PROCEDIMIENTOS Y CONTROL INTERNO</strong></div>
          <div align="center"></div></td>
    </tr>
    <tr>
      <td width="38%"><div align="right"><strong>*Tipo de Manual:</strong></div></td>
      <td width="62%" bgcolor="#FFFFFF"><select name="tipo" id="tipo" onkeypress="return pulsar(event);  return handleEnter(this, event)" tabindex="1" >
        <option value="O">ORGANIZACION</option>
        <option value="P" selected="selected">PROCEDIMIENTOS</option>
        <option value="CI">CONTROL INTERNO</option>
      </select>
      <a href="#" class="hintanchor" onmouseover="showhint('&lt;center&gt; Indicar a que Tipo de Manual se Refiere (Organización, Procedimientos o Control Interno).  &lt;/center&gt;', this, event, '150px')">[?]</a></td>
    </tr>
    <tr>
      <td><div align="right"><strong>Objetivos Generales:</strong></div></td>
      <td bgcolor="#FFFFFF"><textarea name="general" id="general" onKeyPress="return pulsar(event)" onKeyUp="return handleEnter(this, event)" tabindex="2" ></textarea>
      <a href="#" class="hintanchor" onmouseover="showhint('&lt;center&gt; Asentar Brevemente los Objetivos Generales y Particulares del Manual  &lt;/center&gt;', this, event, '150px')">[?]</a></td>
    </tr>
    <tr>
      <td><div align="right"><strong>Objetivos Particulares:</strong></div></td>
      <td bgcolor="#FFFFFF"><textarea name="partic." id="partic." tabindex="3" onKeyPress="return pulsar(event)" onKeyUp="return handleEnter(this, event)"></textarea>
      <a href="#" class="hintanchor" onmouseover="showhint('&lt;center&gt; Asentar Brevemente los Objetivos Generales y Particulares del Manual.  &lt;/center&gt;', this, event, '150px')">[?]</a></td>
    </tr>
    <tr>
      <td><div align="right"><strong>*Fecha en el que se Implementó el Manual:</strong></div></td>
      <td bgcolor="#FFFFFF"><select name="dia1" id="dia1" tabindex="4" onkeypress="return handleEnter(this, event)">
        <option value="01" selected="selected">01</option>
        <option value="02">02</option>
        <option value="03">03</option>
        <option value="04">04</option>
        <option value="05">05</option>
        <option value="06">06</option>
        <option value="07">07</option>
        <option value="08">08</option>
        <option value="09">09</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
        <option value="16">16</option>
        <option value="17">17</option>
        <option value="18">18</option>
        <option value="19">19</option>
        <option value="20">20</option>
        <option value="21">21</option>
        <option value="22">22</option>
        <option value="23">23</option>
        <option value="24">24</option>
        <option value="25">25</option>
        <option value="26">26</option>
        <option value="27">27</option>
        <option value="28">28</option>
        <option value="29">29</option>
        <option value="30">30</option>
        <option value="31">31</option>
      </select>
/
<select name="mes1" id="mes1" tabindex="5" onkeypress="return handleEnter(this, event)">
  <option value="01" selected="selected">Enero</option>
  <option value="02">Febrero</option>
  <option value="03">Marzo</option>
  <option value="04">Abril</option>
  <option value="05">Mayo</option>
  <option value="06">Junio</option>
  <option value="07">Julio</option>
  <option value="08">Agosto</option>
  <option value="09">Septiembre</option>
  <option value="10">Octubre</option>
  <option value="11">Noviembre</option>
  <option value="12">Diciembre</option>
</select>
/
<input name="year1" type="text" id="year1" size="4" maxlength="4" tabindex="6" onKeyPress="return handleEnter(this, event)" onblur=" validanumero(this);" />
(dd/mm/aaaa) <a href="#" class="hintanchor" onmouseover="showhint('&lt;center&gt; Fecha en que se Implementó Cada Uno de los Manuales en la Dependencia. &lt;/center&gt;', this, event, '150px')">[?]</a></td>
    </tr>
    <tr>
      <td><div align="right"><strong>Observaciones:</strong></div></td>
      <td bgcolor="#FFFFFF"><textarea name="observaciones" id="observaciones" tabindex="7" onKeyPress="return pulsar(event)"></textarea>
      <a href="#" class="hintanchor" onmouseover="showhint('&lt;center&gt; Comentarios que se Estime Pertinente Asentar en los Formatos. &lt;/center&gt;', this, event, '150px')">[?]</a></td>
    </tr>
  </table>
  <p>&nbsp;</p>
</div>
  <div align="center">
    <input name="regresa" type="button" id="regresa" onclick="MM_goToURL('self','IAD-003.php');return document.MM_returnValue" value="Regresar" />
    <input name="limpia" type="reset" id="limpia" value="Limpiar Formulario" />
    <input name="guarda" type="submit" disabled="disabled" id="guarda" value="guardar datos" tabindex="8" />
  </div>
</form>
</body>
</html>
