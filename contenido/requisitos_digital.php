<?php
	//initialize the session
	session_start();
	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<script language="JavaScript" src="_scripts.js" type="text/javascript"></script>
		<script language="javascript" src="../js/validate.js"  type="text/javascript"></script>
		<script language="javascript" src="../js/cal2.js" type="text/javascript"></script>
		<script language="javascript" src="../js/cal_conf2.js" type="text/javascript"></script>
		<title>Untitled Document</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="../css/idots.css" rel="stylesheet" type="text/css">
		<style type="text/css">
			<!--
				body {
				margin-top: 0px;
				}
			-->
		</style>
		
		<style>
			table, tr, td {
			border: 1px solid black;
			border-collapse: collapse;
			}
			tr, td {
			padding: 150px;
			}
		</style>
	</head>
	
	<body>
		<div align="center"><h2 style="color: #0404B4">Requisitos de Digitalizaci&oacute;n</h2></div>
		
		<div align="center">
			
			<table width="65%" border="1">
				<tr class="divSideboxHeader">
					<td><div align="left"><h3>Los requerimientos para la digitalizaci&oacute;n de la documental que se adjunte deber&aacute;n ser: </h3></div></td>
				</tr>
				<tr>
					<td>
						<span style="font-size:small; text-align: justify;">
							<ol>
								<li value="1">En monocromo (blanco y negro).</li> <br />
								<li value="2">Resoluci&oacute;n de digitalizaci&oacute;n 200 dpi (200 x 200).</li> <br />
								<li value="3">El formato del archivo deber&aacute; ser PDF multip&aacute;gina con una tasa de comprensi&oacute;n alta (establecer valor num&eacute;rico en 4 o 5).</li> <br />
								<li value="4">Para todos los anexos a digitalizar deber&aacute; de activar la opci&oacute;n de OCR (Reconocimiento &Oacute;ptico de Caracteres por sus siglas en ingl&eacute;s) y el idioma a reconocer deber&aacute; ser espa&ntilde;ol. </li> <br />
								<li value="5">El nombre del archivo digitalizado deber&aacute; responder al formato OSFER m&aacute;s su anexo, por ejemplo: el formato OSFER-06a Relaci&oacute;n de asuntos pendientes OSFER-06a-01.pdf donde el OSFER-06a corresponde al n&uacute;mero del formato y el -01 al n&uacute;mero de anexo correspondiente y .pdf a la extensi&oacute;n del archivo. </li> <br />
								
							</ol>
						</span>
					</td>
				</tr>
				
			</table>
			
		</div>
		
		<p align="center"></p>
		
	</body>
</html>



