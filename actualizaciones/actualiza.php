<?php 
require_once('../Connections/bd2.php');
?>
<?php
session_start();

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="../css/idots.css" rel="stylesheet" type="text/css">
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
//-->

function confirma (valor){
	var val=valor;
	if (confirm ("�Seguro desea continuar? La tabla sera borrada y creada nuevamente")){
		if (confirm ("Una vez que haga esto las modificaciones no seran reversibles �Seguro desea continuar?")){
			MM_goToURL('self', 'crear_ioe016.php');	
		}
	}
}

function confirma2 (valor){
	var val=valor;
	if (confirm ("�Seguro desea continuar? La tabla sera borrada y creada nuevamente")){
		if (confirm ("Una vez que haga esto las modificaciones no seran reversibles �Seguro desea continuar?")){
			MM_goToURL('self', 'crear_ial003.php');	
		}
	}
}


function confirma3 (valor){
	var val=valor;
	if (confirm ("�Seguro desea continuar? Se creara una nueva tabla")){
		if (confirm ("Una vez que haga esto las modificaciones no seran reversibles �Seguro desea continuar?")){
			MM_goToURL('self', 'crear_tabla_fecha.php');	
		}
	}
}
</script>

<style type="text/css">
<!--
body {
	margin-top: 0px;
}
.style1 {
	font-size: 110%;
	font-weight: bold;
}
.Estilo1 {
	font-size: 14px;
	font-weight: bold;
}
-->
</style></head>
<body>
<div align="center">
<table border="1" width="70%" cellpadding="3" cellspacing="3">
 <tr>
   <td>
   	<strong>Si los funcionarios no corresponden (Datos generales de la entidad de gobierno) seleccione su organismo su dependencia acontinuacion</strong>   	
	<form name="entidad" id="entidad" action="dependencias.php" method="post" enctype="multipart/form-data">
		<select name="tipo" id="tipo">
			<option value="DIF">DIF</option>
			<option value="ODAS">ODAS</option>
			<option value="IMCUFIDE">IMCUFIDE</option>
			<option value="Ayto">Ayuntamiento</option>
		</select>
		<input name="ok" value="ok" type="submit" />
	</form>
	</td>
 </tr>
 <tr>
 	<td>
 		<strong>*Apliar la longitud de los campos de la base de datos <span class="divLoginboxHeader"><em>(SE RECOMIENDA HACER UN RESPALDO ANTES DE AMPLIAR LA LONGITUD DE COMPOS) haga clic una vez y espere por favor. El tiempo que tarde dependera de la capacidad de procesamiento de su equipo.</em></span></strong>
 		<br>
 		<a href="campos.php">Ampliar campos</a>
	</td>
 </tr>
 <tr>
 	<td>
 		<strong>Crear dependencias 152, 153, 154 y 155 (solo ayuntamientos)</strong>
 		<br>
 		<a href="dep_155.php">Crear dependencias</a>
	</td>
 </tr>
 
 <tr>
 	<td>
 		<strong>Crear tabla de material bibliografico y hemerografico</strong><span class="divLoginboxHeader"><em>(SE RECOMIENDA HACER UN RESPALDO ANTES DE CREAR LA TABLA DE -INVENTARIO DE ACERVO BIBLIOGR�FICO Y/O HEMEROGR�FICO-  OSFER-07a) haga clic una vez y espere por favor. Esta actualizacion debera aplicarse <strong>una sola vez</strong>, si se aplica mas de una ocacion se perdera la informacion capturada previamente en el apartado de OSFER-07a. <strong>La tabla anterior sera borrada y creada nuevamente</strong></em></span>
 		<br>
 		<a href="#" onClick="confirma();">Crear Tabla</a>
	</td>
 </tr> 
 
  <tr>
 	<td>
 		<strong>*MANTENIMIENTO A LA BASE DE DATOS</strong><span class="divLoginboxHeader"><em>(SE RECOMIENDA HACER UN RESPALDO ANTES DE HACER CLICK EN ESTA OPCION) <strong>haga clic una vez y espere por favor.</strong> Esta actualizacion hace un mantenimiento basico a la base de datos. <strong>ES ALTAMENTE RECOMENDABLE APLICARLO</strong> previo a esto debera hacer un respaldo de su base de datos</em></span>
 		<br>
 		<a href="alter_engine.php">Aplicar mantenimiento a la base de datos</a> 	
	</td>
 </tr> 
 
 <tr>
 	<td>
 		<strong>Crear tabla de INTEGRACI�N DE LOS MIEMBROS DEL AYUNTAMIENTO, CONSEJO � JUNTA DE GOBIERNO(OSFER-58)</strong><span class="divLoginboxHeader"><em>(SE RECOMIENDA HACER UN RESPALDO ANTES DE CREAR LA TABLA haga clic una vez y espere por favor. Esta actualizacion debera aplicarse <strong>una sola vez</strong>, si se aplica mas de una ocacion se perdera la informacion capturada previamente en el apartado de OSFER-58. <strong>La tabla anterior sera borrada y creada nuevamente</strong></em></span>
 		<br>
 		<a href="#" onClick="confirma2();">Crear Tabla</a> 	
	</td>
 </tr> 
 
  
 <tr>
 	<td>
 		<strong>*Modificaciones a la base de datos</strong><span class="divLoginboxHeader"><em>(SE RECOMIENDA HACER UN RESPALDO ANTES DE APLICAR LAS MODIFICACIONES. Haga clic una vez y espere por favor. Esta actualizacion debera aplicarse <strong>una sola vez</strong>, estas modificaciones permiten que el OSFER-55 permita ingresar el periodo de la administracion manualmente y el OSFER-44 permita guardar los valores minimos y maximos de suelo</strong></em></span>
 		<br>
 		<a href="act_db.php" >Modificar Base de Datos</a> 	
	</td>
 </tr> 
 
 
 <tr>
 	<td>
 		<strong>Ajustar fechas de ingreso</strong><span class="divLoginboxHeader"><em>(SE RECOMIENDA HACER UN RESPALDO ANTES DE APLICAR LAS MODIFICACIONES. Haga clic una vez y espere por favor. Esta actualizacion ajusta el tiempo total de servicio y las fechas de ingreso de los servidores publicos. <strong>Solo es necesario aplicarla si los datos antes mencionados no se muestran adecuadamente en el sistema</strong></em></span>
 		<br>
	  <a href="fecha_ing.php" >Ajustar Fechas</a> 	
	</td>
 </tr> 
 
 
  <tr>
 	<td>
 		<strong>Reparar Base de Datos</strong><span class="divLoginboxHeader"><em>(SE RECOMIENDA HACER UN RESPALDO ANTES DE APLICAR LAS MODIFICACIONES. Haga clic una vez y espere por favor. Esta actualizaci�n se aplicara si en una o mas tablas aparece la siguiente leyenda: <i><strong>"table 'nombre_de_la_tabla' crashed"</strong></i><strong></strong></em></span>
 		<br>
	  <a href="repair_table.php" >Repara Base de Datos</a>
	 </td>
 </tr>
 
 <tr>
 	<td>
 		<strong>Crear Tabla Respaldos </strong><span class="divLoginboxHeader"><em>(SE RECOMIENDA HACER UN RESPALDO ANTES DE APLICAR LAS MODIFICACIONES. Haga clic una vez y espere por favor. Esta actualizaci�n creara una nueva tabla, en la cual se guardara la �ltima fecha de  sus respaldos. <i><strong></strong></i><strong></strong></em></span>
 		<br>
	  <a href="#" onClick="confirma3();">Crear tabla</a> 	
	</td>
 </tr>
 
 <tr>
 	<td>
 		<strong>Crear Tabla Control de Acceso </strong><span class="divLoginboxHeader"><em>(SE RECOMIENDA HACER UN RESPALDO ANTES DE APLICAR LAS MODIFICACIONES. Haga clic una vez y espere por favor. Esta actualizaci�n creara una nueva tabla, la cual llevara el control de los usuarios que ingresen al sistema. <i><strong></strong></i><strong></strong></em></span>
 		<br>
	  <a href="control_acceso.php" onClick="confirma3();">Crear tabla</a> 	
	</td>
 </tr>
 <tr>	  
 	<td>
 		<strong>Crear Tabla Correspondencia </strong><span class="divLoginboxHeader"><em>(SE RECOMIENDA HACER UN RESPALDO ANTES DE APLICAR LAS MODIFICACIONES. Haga clic una vez y espere por favor. Esta actualizaci�n creara una nueva tabla, la cual llevara el control de correspondencia entre el personal. Si se aplica mas de una ocacion se perdera la informacion capturada previamente. La tabla anterior sera borrada y creada nuevamente. 
. <i><strong></strong></i><strong></strong></em></span>
 		<br>
	  <a href="crea_correspondencia.php" onClick="confirma3();">Crear tabla</a> 	
	</td>  
 </tr>
 
 
  		
</table>
<br>
<table border="1" width="25%" cellpadding="3" cellspacing="3">
	<tr>
		<td>
			<strong>*</strong>
		</td>
		<td><strong>Importancia alta</strong></td>
	</tr>	
</table>
</div>
</body>
</html>