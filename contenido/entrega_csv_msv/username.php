<?php
//palabra aleatoria o creador de nombres aleatorios
function construir_nombre(){

        $vocales = array("a", "e", "i", "o", "u");

        $consonantes = array("b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "y", "z");

        $random_nombre = 6;//rand($min, $max);//largo de la palabra

        $random = rand(0,1);//si empieza por vocal o consonante
		$nombre = '';
		
        for($j=0;$j<$random_nombre;$j++){//palabra
                switch($random){
                        case 0: 
                        	$random_vocales = rand(0, count($vocales)-1); $nombre.= $vocales[$random_vocales]; $random = 1; 
                        break;

                        case 1: 
                        	$random_consonantes = rand(0, count($consonantes)-1); $nombre.= $consonantes[$random_consonantes]; $random = 0; 
                        break;
                }
        }
		
		mt_srand();
		$num = mt_rand(0, 9).mt_rand(0, 9).mt_rand(0, 9).mt_rand(0, 9);	
		
        return $nombre.$num;
}
?>