<?php
require('../../../pdf/fpdf.php');
require('../../../pdf/mc_table.php');
require('../../../Connections/bd2.php');

session_start();
if (!isset($_SESSION['MM_Username'])){
	header("Location: ../../../adios.php");
}


//Creación del objeto de la clase heredada
$pdf=new PDF_MC_Table('L', 'mm', 'Letter');
$pdf->SetTopMargin(0);
$title='INVENTARIO DE MATERIAL BIBLIOGRÁFICO Y HEMEROGRÁFICO';
$pdf->SetTitle($title);

$colname_inventa = "1";
if (isset($_SESSION['clave_dependencia'])) {
  $colname_inventa = (get_magic_quotes_gpc()) ? $_SESSION['clave_dependencia'] : addslashes($_SESSION['clave_dependencia']);
}
mysql_select_db($database_bd2, $bd2);

$query_inventa = sprintf("SELECT * FROM `ido-001` WHERE clave_area='%s' ORDER BY clave ASC ", $colname_inventa);
$inventa = mysql_query($query_inventa, $bd2) or die(mysql_error());
$row_inventa = mysql_fetch_assoc($inventa);
$totalRows_inventa = mysql_num_rows($inventa);

$query_dep = sprintf("SELECT a.clave as clave, a.nombre as nombre, b.nombre_comp as nombre_comp FROM dependencia a, trabajador b WHERE a.clave_dep = '%s' and a.resp_nombre=b.clave", $colname_inventa);
$dep = mysql_query($query_dep, $bd2) or die(mysql_error());
$row_dep = mysql_fetch_assoc($dep);
$totalRows_dep = mysql_num_rows($dep);

$query_ayunta = sprintf("SELECT * FROM ayuntamiento WHERE clave=1");
$ayunta = mysql_query($query_ayunta, $bd2) or die(mysql_error());
$row_ayunta = mysql_fetch_assoc($ayunta);
$totalRows_ayunta = mysql_num_rows($ayunta);

$colname_user = "1";
if (isset($_SESSION['MM_Username'])) {
  $colname_user = (get_magic_quotes_gpc()) ? $_SESSION['MM_Username'] : addslashes($_SESSION['MM_Username']);
}
mysql_select_db($database_bd2, $bd2);

$query_user = sprintf("SELECT a.usuario AS usuario, b.nombre_comp AS nombre FROM `usuario` a, trabajador b WHERE a.usuario = '%s' AND a.nombre_comp = b.numero ", $colname_user);
$user = mysql_query($query_user, $bd2) or die(mysql_error());
$row_user = mysql_fetch_assoc($user);
$totalRows_user = mysql_num_rows($user);


//$pdf=new FPDF('P','mm','A4');
$area=$row_dep['nombre'];
$ayuntam=$row_ayunta['nombre'];
$usr=$row_user['nombre'];
$ent=$row_dep['nombre_comp'];
$forma="OSFER61";
$var="";
$dia=date("d");
$mes=date("m");
$year=date("Y");
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','B',10);
//$pdf->SetTextColor(0, 0, 128);
$pdf->SetWidths(array(27,58,58,50,67));
$encabezado=(array("No.","TÍTULO","AUTOR", "EDITORIAL Y FECHA DE PUBLICACION", "OBSERVACIONES"));
$pdf->Recibe($encabezado);
$y=$pdf->GetY();
$pdf->SetLineWidth(0.5);
$pdf->Line(10, $y, 270, $y);
$pdf->SetLineWidth(0.2);
$pdf->Alinea('C');
$pdf->Row($encabezado);
$y=$pdf->GetY();
$pdf->SetLineWidth(0.5);
$pdf->Line(10, $y, 270, $y);
$pdf->SetLineWidth(0.2);
$pdf->SetFont('Arial','',8);
$i=1; 
do {	
	$pdf->Alinea('L');
	 $pdf->Row(array($i,$row_inventa['titulo'],$row_inventa['autor'], $row_inventa['editorial']." " . $row_inventa['fecha_pub'] ,  $row_inventa['observaciones']));
	$i++; 
} while ($row_inventa = mysql_fetch_assoc($inventa));

$y=$pdf->GetY();
if ($y<140){
	$alto=150-$y;
	$veces=$alto/10;
	//$i=0;
	for ($i=0; $i<=$veces; $i++){
		$pdf->Image('../../../images/jeans.jpg',10,$y+((10*$i)+2),260, 10);
	}
}

$pdf->Output();

?> 