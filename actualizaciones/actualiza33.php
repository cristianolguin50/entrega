<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Actualizaci&oacute;n Entrega Recepci&oacute;n</title>
		
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="../css/bootstrap.min.css">
		
		<!-- Optional theme -->
		<link rel="stylesheet" href="./css/bootstrap-theme.min.css" >
		
		<!-- Latest compiled and minified JavaScript -->
		<script src="../js/bootstrap.min.js" ></script>
	</head>
	
	<body>
		<div class="container">
			<h1>CREG Entrega Recepci&oacute;n &copy; Versi&oacute;n 3.3</h1>
			
			<form class="form-horizontal" method="POST" action="actualiza_v33_patrimonio.php">
				<div class="form-group">
					<label for="base" class="col-sm-3 control-label">Seleccionar base de datos:</label>
					<div class="col-sm-6">
						<select id="base" name="base" class="form-control" >
							<option value="creg">Ayuntamiento</option>
							<option value="creg_odas">ODAS</option>
							<option value="creg_dif">DIF</option>
							<option value="creg_deporte">IMCUFIDE</option>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-9">
						<button type="submit" class="btn btn-success">Actualizar</button>
					</div>
				</div>
				
			</form>
		</div>
	</body>
</html>