<?php
	require_once('../../../Connections/bd2.php'); 
	
	session_start();
	
	if (!isset($_SESSION['MM_Username'])){
		header("Location: ../../../adios.php");
	}
	mysql_select_db($database_bd2, $bd2);
	$query_dep = "SELECT clave, clave_dep, nombre FROM `dependencia` WHERE mostrar='1' AND (tipo='G' OR tipo='P') ORDER BY tipo ASC, clave_dep ASC";
	$dep = mysql_query($query_dep, $bd2) or die(mysql_error());
	
	
	//Recordset de los bienes
	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link href="../../../css/idots.css" rel="stylesheet" type="text/css">
		<title>Alta de un nuevo registro</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<script language="javascript" type="text/javascript">
			<!--
			function cargaImagen(){
				var cadena=document.form1.archivo.value;
				document.form1.foto.src=cadena;
				//document.form1.ruta2.value=cadena;
			}
			
			function MM_goToURL() { //v3.0
				var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
				for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}
			
			function upper(field){
				field.value=field.value.toUpperCase();
			}
			
			function pasa(valor){
				document.form1.uso.value=valor;
				/*if document.form1.opcion.selectedIndex != 0{
					document.form1.criterio.readonly=false;
				}*/
			}
			
			function pasatipo(valor){
				
				if (valor==1){
					document.form1.tipo1.value="A"
				}
				
				else if (valor==2){
					document.form1.tipo1.value="B"
				}
				else if (valor==3){
					document.form1.tipo1.value="C"
				}
				else if (valor==4){
					document.form1.tipo1.value="D"
				}
			}
			//-->
			function validate(){
				var cad1=document.form1.original.selectedIndex;
				var cad2=document.form1.trabajador.selectedIndex;
				var cad3=document.form1.comision.selectedIndex;
				var cad4=document.form1.fech1.value;
				var cad5=document.form1.fech2.value;
				var lon1;
				var lon2;
				var lon3;
				var lon4;
				var lon5;
				
				//lon1=cad1.length;
				//lon2=cad2.length;
				lon4=cad4.length;
				lon5=cad5.length;
				
				if((cad1!=0)  && (cad3!=0) && (lon4>0) && (lon5>0)){
					document.form1.guarda.disabled=false;
				}
				if ((cad1==0)  || (cad3==0) || (lon4<=0) || (lon5<=0)){
					document.form1.guarda.disabled=true;
				}
			}
		</script>
		
		<script language="JavaScript" src="../../../js/cal2.js" type="text/javascript"></script>
		<script language="JavaScript" src="../../../js/cal_conf2.js" type="text/javascript"></script>
		<script language="JavaScript" src="_scripts.js" type="text/javascript"></script>
		<script language="javascript" src="../../../js/validate.js" type="text/javascript"></script>
	</head>
	
	<body>
		
		<p align="center" class="divSideboxHeader style1">PERSONAL COMISIONADO</p>
		<form name="form1"  method="post" action="guarda.php" enctype="multipart/form-data" autocomplete="off">
			<div align="center">
				<table width="90%"  border="1" align="center" cellpadding="3" cellspacing="0">
					<tr>
						<iframe id="iframeCentral" name="iframeCentral" style="width:0px; height:0px; border: 0px;display: hidden;"></iframe>
						<td width="40%"><div align="right"><strong>*Adscripci&oacute;n Original </strong></div></td>
						<td bgcolor="#FFFFFF">
							<div align="left">
								<select name="original" onChange="recargarTrab(this);" onBlur="validate();" tabindex="1">
									<option value="0" selected>Seleccione una Opci&oacute;n</option>
									<?php
										while ($row_dep = mysql_fetch_assoc($dep)) {  
										?>
										<option value="<?php echo $row_dep['clave_dep']; ?>"><?php echo $row_dep['clave_dep'].' - '.$row_dep['nombre']; ?></option>
										<?php } ?>
								</select>          
							<a href="#" class="hintanchor" onMouseover="showhint('<center>Seleccione la adscripci�n original del servidor p�blico.</center>', this, event, '150px')">[?]</a> </div></td>
					</tr>
					<tr>
						<td><div align="right"><strong>*Servidor P�blico: </strong></div></td>
						<td bgcolor="#FFFFFF"><div align="left"><span id="divCentral" name="divCentral" align="left">
							<input name="trabajador" type="text" disabled="true" id="trabajador" size="40">
						</span>
						<a href="#" class="hintanchor" onMouseover="showhint('<center>Seleccione el nombre del servidor p�blico.<i> Este campo es obligatorio.</i></center>', this, event, '150px')">[?]</a></div></td>
					</tr>
					<tr>
						<td><div align="right"><strong>Cargo</strong></div></td>
						<td bgcolor="#FFFFFF">   <div align="left"><span id="divSiguiente" name="divSiguiente" align="left">
							<input name="cargo" type="text" disabled="true" id="cargo" size="40">
						</span>
						<a href="#" class="hintanchor" onMouseover="showhint('<center>Indique el cargo original del servidor p�blico.<i> Este campo es obligatorio.</i></center>', this, event, '150px')">[?]</a></div></td>
					</tr>
					<tr>
						<td><div align="right"><strong>* Comisionado en: </strong></div></td>
						<td bgcolor="#FFFFFF"><div align="left">
							<input type="text" name="comision" id="comision" maxlength="50" size="40" >
							
							<!--  <select name="comision" id="comision" onBlur="validate();" tabindex="4">
								<option value="0" selected>Seleccione una Opci&oacute;n</option>
								<?php
									do {  
									?>
									<option value="<?php echo $row_dep['nombre']?>"><?php echo $row_dep['nombre']?></option>
									<?php
									} while ($row_dep = mysql_fetch_assoc($dep));
									$rows = mysql_num_rows($dep);
									if($rows > 0) {
										mysql_data_seek($dep, 0);
										$row_dep = mysql_fetch_assoc($dep);
									}
								?>
							</select>-->
						<a href="#" class="hintanchor" onMouseover="showhint('<center>Se deber&aacute; especificar, la entidad y/o unidad administrativa donde desempe&ntilde;a la comisi&oacute;n. <i> Este campo es obligatorio.</i></center>', this, event, '150px')">[?]</a></div></td>
					</tr>
					<tr>
						<td colspan="2"><div align="center"><strong>Duraci&oacute;n de la Comisi&oacute;n: </strong></div></td>
					</tr>
					<tr>
						<td><div align="right"><strong>* Inicio: </strong></div></td>
						<td bgcolor="#FFFFFF">
							<div align="left">		
								<input name="fech1" type="text" id="fech1" tabindex="10" size="10" readonly="true" onBlur="validate();" />
								<a href="javascript:showCal('Calendar1')" tabindex="10" onBlur="validate();"><img src="../../../images/nuevos/FIREWORK/FORMATOSENTREGARECEPCION.gif" width="16" height="16" border="0"> </a> <a href="#" class="hintanchor" onMouseOver="showhint('<center> Indique la fecha en el calendario. <i>Este campo es obligatorio. </i></center>', this, event, '150px')">[?]</a>
								
							</div>
						</td>
					</tr>
					
					<tr>
						<td><div align="right"><strong>* Fin: </strong></div></td>
						<td bgcolor="#FFFFFF">
							<div align="left">		
								<input name="fech2" type="text" id="fech2" tabindex="10" size="10" readonly="true" onBlur="validate();" />
								<a href="javascript:showCal('Calendar2')" tabindex="10" onBlur="validate();"><img src="../../../images/nuevos/FIREWORK/FORMATOSENTREGARECEPCION.gif" width="16" height="16" border="0"> </a> <a href="#" class="hintanchor" onMouseOver="showhint('<center> Indique la fecha en el calendario. <i>Este campo es obligatorio. </i></center>', this, event, '150px')">[?]</a>
								
							</div>
						</td>
					</tr>
					
					<tr bgcolor="#FFFFFF">
						<td colspan="2"><div align="center"><span class="style2"><strong>Nota</strong>: Los campos marcados con * son obligatorios</span></div></td>
					</tr>
				</table>
			</div>
			<p align="center">
				<input name="Button" type="button" onClick="MM_goToURL('self','comision.php');return document.MM_returnValue" value="Regresar">
				<input name="limpia" type="reset" id="limpia" value="Limpiar Formulario">
				<input name="guarda" type="submit" id="guarda" value="Guardar datos" disabled="true" onkeypress="return handleEnter(this, event)" tabindex="6">
			</p>
		</form>
		<p align="center">&nbsp; </p>
	</body>
</html>
<?php
	mysql_free_result($dep);
?>
