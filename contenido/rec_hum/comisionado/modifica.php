<?php require_once('../../../Connections/bd2.php'); ?>
<?php
	
	session_start();
	
	if (!isset($_SESSION['MM_Username'])){
		header("Location: ../../../adios.php");
	}
	//Recordset de los bienes
	
	/* echo $area=$_SESSION['clave_dependencia'];
	echo $_REQUEST['oculto2'];	 */
	$clave = $_REQUEST['oculto2'];
	
	mysql_select_db($database_bd2, $bd2);
	//$query_bien = sprintf("SELECT * FROM `irh-002` WHERE clave = $clave ORDER BY clave ASC");
	$query_bien = "SELECT b.nombre_comp, b.cargo, a.original, a.comision, a.clave, a.fecha_ini, a.fecha_fin FROM `irh-002` a INNER JOIN trabajador b ON a.clave_trab=b.curp WHERE a.clave = $clave LIMIT 1";
	$bien = mysql_query($query_bien, $bd2) or die(mysql_error());
	$row_bien = mysql_fetch_assoc($bien);
	
	mysql_select_db($database_bd2, $bd2);
	$query_dep = "SELECT * FROM dependencia";
	$dep = mysql_query($query_dep, $bd2) or die(mysql_error());
	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link href="../../../css/idots.css" rel="stylesheet" type="text/css">
		<title>Untitled Document</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		
		<script language="javascript" type="text/javascript">
			<!--
			function cargaImagen(){
				var cadena=document.form1.archivo.value;
				document.form1.foto.src=cadena;
				//document.form1.ruta2.value=cadena;
			}
			
			function MM_goToURL() { //v3.0
				var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
				for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}
			
			function upper(field){
				field.value=field.value.toUpperCase();
			}
			
			function pasa(valor){
				document.form1.uso.value=valor;
				/*if document.form1.opcion.selectedIndex != 0{
					document.form1.criterio.readonly=false;
				}*/
			}
			//-->
		</script>
		<script language="JavaScript" src="../../../js/cal2.js" type="text/javascript"></script>
		<script language="JavaScript" src="../../../js/cal_conf2.js" type="text/javascript"></script>
		<script language="JavaScript" src="_scripts.js" type="text/javascript"></script>
		<script language="javascript" src="../../../js/validate.js" type="text/javascript"></script>
	</head>
	
	<body>
		<p align="center" class="divSideboxHeader style1">PERSONAL COMISIONADO </p>
		<form name="form1" method="post" action="modifica2.php" enctype="multipart/form-data" autocomplete="off">
			<div align="center">
				<table width="90%"  border="1" align="center" cellpadding="3" cellspacing="0">
					<tr>
						<input type="hidden" id="clave" name="clave" value="<?php echo $clave; ?>" />
						<td width="40%"><div align="right"><strong>*Adscripci&oacute;n Original </strong></div></td>
						<td bgcolor="#FFFFFF"><div align="left">
							<select name="original" onChange="recargarTrab(this);" tabindex="1" disabled>
								<option value="0" selected>Seleccione una Opci&oacute;n</option>
								<?php while ($row_dep = mysql_fetch_assoc($dep)) { ?>
									<option value="<?php echo $row_dep['clave_dep']; ?>" <?php if($row_bien['original']==$row_dep['clave_dep']) { echo 'selected'; } ?>><?php echo $row_dep['clave_dep'].' - '.$row_dep['nombre']?></option>
								<?php } mysql_data_seek($dep, 0); ?>
							</select>
						<a href="#" class="hintanchor" onMouseover="showhint('<center>Seleccione la adscripci&oacute;n original del trabajador.</center>', this, event, '150px')">[?]</a> </div></td>
					</tr>
					<tr>
						<td><div align="right"><strong>*Servidor P&uacute;blico: </strong></div></td>
						<td bgcolor="#FFFFFF"><div align="left"><span id="divCentral" name="divCentral" align="left">
							<input name="trabajador" type="text" id="trabajador" size="40" value="<?php echo $row_bien['nombre_comp']; ?>" readonly >
						</span> <a href="#" class="hintanchor" onMouseover="showhint('<center>Nombre del trabajador.<i> Este campo es obligatorio.</i></center>', this, event, '150px')">[?]</a></div></td>
					</tr>
					<tr>
						<td><div align="right"><strong>Cargo</strong></div></td>
						<td bgcolor="#FFFFFF"><div align="left"><span id="divSiguiente" name="divSiguiente" align="left">
							<input name="cargo" type="text" id="cargo" size="40" value="<?php echo $row_bien['cargo']; ?>" readonly>
						</span> <a href="#" class="hintanchor" onMouseover="showhint('<center>Puesto Original del tranajador.<i> Este campo es obligatorio.</i></center>', this, event, '150px')">[?]</a></div></td>
					</tr>
					
					<tr>
						<td><div align="right"><strong>* Comisionado en: </strong></div></td>
						<td bgcolor="#FFFFFF"><div align="left">
							<input name="comision" type="text" id="comision" size="40" value="<?php echo $row_bien['comision']; ?>">
						<a href="#" class="hintanchor" onMouseover="showhint('<center>Se deber&aacute; especificar, la entidad y/o unidad administrativa donde desempe&ntilde;a la comisi&oacute;n. <i> Este campo es obligatorio.</i></center>', this, event, '150px')">[?]</a></div></td>
					</tr>
					
					<tr>
						<td colspan="2"><div align="center"><strong>Duraci&oacute;n de la Comisi&oacute;n: </strong></div></td>
					</tr>
					
					<tr>
						<td><div align="right"><strong>* Inicio: </strong></div></td>
						<td bgcolor="#FFFFFF">
							<div align="left">		
								<input name="fech1" type="text" id="fech1" tabindex="10" size="10" value="<?php echo $row_bien['fecha_ini']; ?>" readonly="true" />
								<a href="javascript:showCal('Calendar1')" tabindex="10""><img src="../../../images/nuevos/FIREWORK/FORMATOSENTREGARECEPCION.gif" width="16" height="16" border="0"> </a> <a href="#" class="hintanchor" onMouseOver="showhint('<center> Indique la fecha en el calendario. <i>Este campo es obligatorio. </i></center>', this, event, '150px')">[?]</a>
								
							</div>
						</td>
					</tr>
					
					<tr>
						<td><div align="right"><strong>* Fin: </strong></div></td>
						<td bgcolor="#FFFFFF">
							<div align="left">		
								<input name="fech2" type="text" id="fech2" tabindex="10" size="10" value="<?php echo $row_bien['fecha_fin']; ?>" readonly="true" />
								<a href="javascript:showCal('Calendar2')" tabindex="10"><img src="../../../images/nuevos/FIREWORK/FORMATOSENTREGARECEPCION.gif" width="16" height="16" border="0"> </a> <a href="#" class="hintanchor" onMouseOver="showhint('<center> Indique la fecha en el calendario. <i>Este campo es obligatorio. </i></center>', this, event, '150px')">[?]</a>
								
							</div>
						</td>
					</tr>
					<tr bgcolor="#FFFFFF">
						<td colspan="2"><div align="center"><span class="style2"><strong>Nota</strong>: Los campos marcados con * son obligatorios</span></div></td>
					</tr>
				</table>
			</div>
			<p align="center">
				<input name="Button" type="button" onClick="MM_goToURL('self','comision.php');return document.MM_returnValue" value="Regresar">
				<input name="limpia" type="reset" id="limpia" value="Limpiar Formulario">
				<input name="guarda" type="submit" id="guarda" value="Modificar Registro">
			</p>
		</form>
		
	</body>
</html>
<?php
	mysql_free_result($bien);
?>
