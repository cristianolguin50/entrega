<?php require_once('../../../Connections/bd2.php'); ?>
<?php
/*	Elaborado por: Alma Leticia Pinacho
	Fecha: 22/12/2006
	Modificado por: Wenceslao Arroyo
	Fecha: 16/01/2007  */
session_start();
if (!isset($_SESSION['MM_Username'])){
	header("Location: ../../../adios.php");
}

//Recordset de los bienes

$clavecita=$_SESSION['clave_dependencia'];
$clave=$_REQUEST['oculto2'];

mysql_select_db($database_bd2, $bd2);
$query_bien = sprintf("SELECT * FROM `puesto` WHERE clave = $clave ORDER BY clave ASC");
$bien = mysql_query($query_bien, $bd2) or die(mysql_error());
$row_bien = mysql_fetch_assoc($bien);
$totalRows_bien = mysql_num_rows($bien);

/*$dia1=substr($row_bien['fecha_pub'],8 ,2);
$mes1=substr($row_bien['fecha_pub'],5 ,2);
echo $mes1;
$year1=substr($row_bien['fecha_pub'],0 ,4);*/
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="../../../css/idots.css" rel="stylesheet" type="text/css">
<title>Modificar Datos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
body {
	margin-top: 0px;
}
.style1 {
	font-size: 110%;
	font-weight: bold;
}
.style2 {	color: #FF0000;
	font-style: italic;
}
-->
</style>
<script language="javascript" src="../../../js/validate.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">


function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}


//-->
function valida(){
var cad1=document.form1.clave_puesto.value;
var cad2=document.form1.nombre.value;
var cad3=document.form1.sueldo.value;
var cad4=document.form1.tipo.value;
var lon1;
var lon2;
var lon3;
var lon4;
lon1=cad1.length;
lon2=cad2.length;
lon3=cad3.length;
lon4=cad4.length;
if((lon1>0) && (lon2>0) && (lon3>0)){
	document.form1.guarda.disabled=false;
	}
if ((lon1<=0) || (lon2<=0) || (lon3<=0)){
document.form1.guarda.disabled=true;
}
}

function numer(valor){

if ((isNaN(valor.value)) || (valor.value=="")){
	valor.value=0;
}

}


</script>

</head>

<body onLoad="document.form1.clave_puesto.focus();">
<p align="center" class="divSideboxHeader style1">CAT&Aacute;LOGO DE PUESTO</p>
<form name="form1" method="post" action="modifica2.php" enctype="multipart/form-data">
  <div align="center">
    <table width="90%"  border="1" align="center" cellpadding="3" cellspacing="0">
      <tr>
        <td width="40%"><div align="right"><strong>
          <input name="clave" type="hidden" id="clave" value="<?php echo $clave; ?>">
          *Clave:</strong></div></td>
        <td bgcolor="#FFFFFF"><div align="left">
          <input name="clave_puesto" type="text" id="clave_puesto" tabindex="1" onBlur="valida();" onKeyPress="return	 pulsar(event)" onKeyUp="return handleEnter(this, event)" size="35" value="<?php echo $row_bien['clave_puesto']; ?>">
          <a href="#" class="hintanchor" onMouseOver="showhint('<center> T&iacute;tulo de la obra <i>Este campo es obligatorio</i> </center>', this, event, '150px')">[?]</a> </div></td>
      </tr>
      <tr>
        <td><div align="right"><strong>*Nombre de la Plaza:</strong></div></td>
        <td bgcolor="#FFFFFF"><div align="left">
          <input name="nombre" type="text" id="nombre" tabindex="2" onBlur="" onKeyPress="return pulsar(event)" onKeyUp="return handleEnter(this, event)" value="<?php echo $row_bien['nombre']; ?>" size="35">
          <a href="#" class="hintanchor" onMouseOver="showhint('<center> Autor o autores <i>Este campo es obligatorio</i>  </center>', this, event, '150px')">[?]</a> </div></td>
      </tr>
      <tr>
        <td><div align="right"><strong>*Sueldo Minimo:</strong></div></td>
        <td bgcolor="#FFFFFF"><div align="left">
          <input name="sueldo" type="text" id="sueldo" tabindex="3" onBlur="numer(this); " onKeyPress="return pulsar(event)" onKeyUp="return handleEnter(this, event)" value="<?php echo $row_bien['sueldo']; ?>" size="35">
          <a href="#" class="hintanchor" onMouseOver="showhint('<center> Editorial que publica la obra <i>Este campo es obligatorio</i> </center>', this, event, '150px')">[?]</a> </div></td>
      </tr>
      <tr>
        <td><p align="right"><strong>Tipo:</strong></p></td>
        <td bgcolor="#FFFFFF"><div align="left">
          <label>
            <select name="tipo" onKeyPress="return handleEnter(this, event)" onBlur="" tabindex="4" >
              <option value="ADM" selected <?php if (!(strcmp("ADM", $row_bien['tipo']))) {echo "SELECTED";} ?>>Administrativo</option>
              <option value="OPE"<?php if (!(strcmp("OPE", $row_bien['tipo']))) {echo "SELECTED";} ?>>Operativo</option>
              <option value="SPE"<?php if (!(strcmp("SPE", $row_bien['tipo']))) {echo "SELECTED";} ?>>Seguridad Publica</option>
            </select>
            </label>
        </div></td>
      </tr>
      <tr>
        <td colspan="2" valign="top"><div align="center"><span class="style2"><strong>Nota</strong>: Los campos marcados con * son obligatorios</span></div></td>
      </tr>
    </table>
    <p>&nbsp;</p>
  </div>
  <p align="center">
    <input name="Button" type="button" onClick="window.close()" value="Cerrar">
    <input name="limpia" type="reset" id="limpia" value="Limpiar Formulario">
    <input name="guarda" type="submit" id="guarda" value="Modificar Registro" disabled="true" tabindex="5">
  </p>
</form>
</body>
</html>
<?php
mysql_free_result($bien);
?>
