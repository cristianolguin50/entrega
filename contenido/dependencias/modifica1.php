<?php
	
	require_once('../../Connections/bd2.php'); 
	
	$colname_dta = "1";
	if (isset($_REQUEST['pasa'])) {
		$colname_dta = (get_magic_quotes_gpc()) ? $_REQUEST['pasa'] : addslashes($_REQUEST['pasa']);
	}
	/*	Modificado por: Marco Huerta
	Fecha: 11/01/2007	*/
	
	session_start();
	//die();
	
	if (!isset($_SESSION['MM_Username'])){
		header("Location: ../../adios.php");
	}
	$clave = $_GET['oculto2'];
	
	mysql_select_db($database_bd2, $bd2);
	$query_depend = "SELECT * FROM dependencia ORDER BY clave";
	$depend = mysql_query($query_depend, $bd2) or die(mysql_error());
	$totalRows_depend = mysql_num_rows($depend);
	
	
	mysql_select_db($database_bd2, $bd2);
	$query_depend = "SELECT * FROM dependencia WHERE clave = '$clave' ORDER BY clave";
	$depend2 = mysql_query($query_depend, $bd2) or die(mysql_error());
	$row_depend2 = mysql_fetch_array($depend2);
	$totalRows_depend2 = mysql_num_rows($depend2);
	
	mysql_select_db($database_bd2, $bd2);
	$sqlDepOficial = "SELECT clave_dep, nombre FROM `dependencia` WHERE tipo='G' OR tipo='A' ORDER BY clave";
	$dtDepOficial = mysql_query($sqlDepOficial, $bd2) or die(mysql_error());
	
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link href="../../css/idots.css" rel="stylesheet" type="text/css">
		<title>Unidades Administrativas</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		
		<script type="text/javascript" src="../../js/jquery-1.7.1.js"></script>
		<link rel="stylesheet" type="text/css" media="screen" href="../../js/jquery.ketchup.0.3.2/css/jquery.ketchup.css" />
		<script type="text/javascript" src="../../js/jquery.ketchup.0.3.2/js/jquery.ketchup.all.min.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function() {
				$("#form1").ketchup(); 
			});
		</script>
		
		<script language="JavaScript" type="text/JavaScript">
			function MM_goToURL() {
				var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
				for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}
			
			function pasa(valor){
				document.form1.tipo.value=valor;
			}
		</script>
		<script language="JavaScript" src="_scripts.js" type="text/javascript"></script>
		<script language="javascript" src="../../js/validate.js" type="text/javascript"></script>
		<style type="text/css">
			body {
			margin-top: 0px;
			}
			.style1 {
			color: #FF0000;
			font-style: italic;
			}
			.style2 {font-size: 110%;}
			.style3 {color: #FF0000;
			font-style: italic;
			}
		</style>
	</head>
	
	<body onLoad="document.form1.expediente.focus();">
		<div align="center"><h2 style="color: #0404B4">Unidades Administrativas</h2></div>
		<form id="form1" name="form1" method="post" action="modif_depend1.php" enctype="multipart/form-data" autocomplete="off">
			<div align="center">
				<table width="56%" border="1" cellpadding="3" cellspacing="0">
					<tr>
						<td colspan="4"><div align="center"><strong>Datos de la Unidad Administrativa</strong></div></td>
					</tr>
					<tr>
						<td width="23%"><div align="right"><strong>*Clave:</strong><input type="hidden" name="clave" id="clave" value="<?php echo $clave; ?>"> </div></td>
						
						<td colspan="2" bgcolor="#FFFFFF"><div align="left">
							<input type="text" maxlength="30" readonly="true" name="clave_dep_nueva" id="clave_dep_nueva" value="<?php echo $row_depend2['clave_dep']; ?>">
							
							<input type="hidden" name="clave_dep_ant" id="clave_dep_ant" value="<?php echo $row_depend2['clave_dep']; ?>">
						</div></td>
						
					</tr>
					<tr>
						<td><div align="right"><strong>*Nombre:</strong></div></td>
						<td colspan="2" bgcolor="#FFFFFF"><div align="left">
						<input name="nombre" type="text" readonly="true" size="40" id="nombre2" onKeyPress="return pulsar(event)" onKeyUp="return handleEnter(this, event)" tabindex="2" value="<?php echo $row_depend2['nombre']; ?>"  data-validate="validate(required);"></div></td>						
					</tr>
					
					<!---
					<tr>
						<td><div align="right"><strong>Acta E-R:</strong></div></td>
						<td colspan="2" bgcolor="#FFFFFF"><div align="left">
							<select name="actaer" id="actaer" tabindex="3">     								
								<option value="1" <?php //if($row_depend2['tipo_actaer'] == 1){ echo 'selected="selected"'; } ?>>AER-1</option>
								<option value="2" <?php //if($row_depend2['tipo_actaer'] == 2){ echo 'selected="selected"'; } ?>>AER-2</option>
								<option value="3" <?php //if($row_depend2['tipo_actaer'] == 3){ echo 'selected="selected"'; } ?>>AER-3</option>
								<option value="4" <?php //if($row_depend2['tipo_actaer'] == 4){ echo 'selected="selected"'; } ?>>AER-4</option>
							</select>								
						<a href="#" class="hintanchor" onMouseOver="showhint('<center>Acta de entrega recepci&oacute;n que corresponde a la Unidad Administrativa. Si tiene duda sobre que acta le corresponde por favor consulte con el <b>OSFEM</b> quien le indicara el acta correcta</center>', this, event, '150px')">[?]</a></div></td>			
					</tr>
					--->
					
				</table>
			</div>
			<p align="center">  	
				<input name="regresa" type="button" onClick="MM_goToURL('self','depend2.php');return document.MM_returnValue" value="Regresar">
				<input type="submit" name="enviar" value="Almacenar Datos">
			</p>
			<div align="center">
				<input type="hidden" name="MM_insert" value="form1">
			</div>
		</form>
	</body>
</html>