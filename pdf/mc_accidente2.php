<?php
require('fpdf.php');

class PDF_MC_Table extends FPDF
{
var $widths;
var $aligns;
var $encabezado;
var $alinea;

function Header()
{
global $title;
global $area;
global $ayuntam;
//global $encabezado;
    //Logo
	//$this->SetTopMargin(2);
  //  $this->Image('../../images/azul2.jpg',10,10,20);
    //Arial bold 15
    $this->SetFont('Arial','B',10);
    //Movernos a la derecha
   // $this->Image('../../images/image002.jpg', 165, 8, 20);
	$this->SetY(11);
	$this->Cell(25);
	$this->Cell(75,1,'H. AYUNTAMIENTO CONSTITUCIONAL',0,0,'L');
	$this->Cell(80);
    $this->Cell(75,1,'�RGANO SUPERIOR DE FISCALIZACI�N DEL',0,0,'L');
	$this->Ln(3.5);
	$this->Cell(25);
	$this->Cell(75,1,'DE ' .$ayuntam .', ESTADO DE M�XICO',0,0,'L');
	$this->Cell(80);
	$this->Cell(75,1,'ESTADO DE M�XICO',0,0,'L');
	$this->Ln(3.5);
	$this->Cell(25);
	$this->Cell(75,1,'XXXX-XXXX',0,0,'L');
	$this->Cell(80);
	$this->Cell(75,1,'ENTREGA-RECEPCI�N DE LA ADMINISTRACI�N ',0,0,'L');
	$this->Ln(3.5);
	$this->Cell(180);
	$this->Cell(75,1,'P�BLICA MUNICIPAL',0,0,'L');
	$this->Ln(3.5);
	$this->SetLineWidth(0.5);
	$this->Line(10, 30, 270, 30);
	$this->Ln(6);
	$this->SetFont('Arial','B',15);
    //Calculamos ancho y posici�n del t�tulo.
    $w=$this->GetStringWidth($title)+6;
	$this->SetX((275-$w)/2);
	$this->Cell($w,9,$title,0,1,'C');
    //Salto de l�nea
//	$this->Ln(3.5);
	$this->SetLineWidth(0.2);
	$this->SetFont('Arial','',10);
	$this->Cell(4,4,'X',1,0,'C');
	$this->Cell(30,4,'AYUNTAMIENTO',0,0,'L');
	$this->Cell(4,4,'',1,0,'C');
	$this->Cell(15,4,'ODAS',0,0,'L');
	$this->Cell(4,4,'',1,0,'L');
	$this->Cell(10,4,'DIF',0,0,'L');
	$this->Cell(4,4,'',1,0,'L');
	$this->Cell(15,4,'OTROS',0,0,'L');
	$this->Cell(10);
	$this->Cell(15,4,'MUNICIPIO:',0,0,'L');
	$this->Cell(10);
	$this->SetFont('Arial','B',10);
	$this->Cell(40,4, $ayuntam, 0,0, 'L');
	$this->SetFont('Arial','',10);
	$this->Cell(15,4,'�REA:',0,0,'L');
	$this->SetFont('Arial','B',10);
	$this->Cell(15,4,$area,0,0,'L');
	$this->SetFont('Arial','',10);
	
	
	
    $this->Ln(5);
}

//Pie de p�gina
function Footer()
{
	global $usr;
	global $forma;
	global $var;
	global $dia;
	global $mes;
	global $year;
	global $ent;
    //Posici�n: a 1,5 cm del final
    $this->SetY(-55);	
	$this->SetFont('Arial','B',10);
    //Arial italic 8
	$this->Cell(50, 0, $var, 0, 0, 'L');
	$this->Ln(12);
	$this->Cell(30);
	$this->Cell(80, 0, $usr, 0, 0, 'C');
	$this->Cell(26);
	$this->Cell(80, 0, $ent, 0, 0, 'C');
	$this->Line(40, 175, 120, 175);
	$this->Line(145, 175, 225, 175);
	$this->Ln(5);
	$this->Cell(65);
	$this->Cell(10, 1, 'ELABOR�', 0,0, 'C');
	$this->Cell(95);
	$this->Cell(10, 1, 'ENTREGA', 0,0, 'C');
	$this->SetLineWidth(0.5);
	$this->Line(10, 180, 270, 180);
	$this->Ln(11);
	$this->SetFont('Arial','',8);
	$this->Cell(230);
	$this->Cell(30, 1, 'FECHA DE ELABORACI�N', 0,0, 'C');
	$this->Ln(2);
	$this->Cell(230);
	$this->Cell(10, 4, 'D�A', 1,0, 'C');
	$this->Cell(10, 4, 'MES', 1,0, 'C');
	$this->Cell(10, 4, 'A�O', 1,0, 'C');
	$this->Ln(4);
	$this->Cell(230);
	$this->Cell(10, 4, $dia, 1,0, 'C');
	$this->Cell(10, 4, $mes, 1,0, 'C');
	$this->Cell(10, 4, $year, 1,0, 'C');
	$this->Ln(5);
	$this->Cell(10, 4, $forma, 0,0, 'C');
	//$this->Cell(70
	$this->Ln(5);
    $this->SetFont('Arial','I',8);
    //N�mero de p�gina
    $this->Cell(0,10,'P�gina '.$this->PageNo().'/{nb}',0,0,'C');
}


function SetWidths($w)
{
    //Set the array of column widths
    $this->widths=$w;
}

function SetAligns($a)
{
    //Set the array of column alignments
    $this->aligns=$a;
}
function Recibe($rec)
{
	//global $encabezado;
	$this->encabezado= $rec;
	//$this->Row($encabezado);
    //Set the array of column widths
    //$this->widths=$w;
}
function Alinea($alig)
{
	//global $encabezado;
	$this->alinea= $alig;
	//$this->Row($encabezado);
    //Set the array of column widths
    //$this->widths=$w;
}

function Row($data)
{
    //Calculate the height of the row
    $nb=0;
    for($i=0;$i<count($data);$i++)

        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
    $h=5*$nb;
    //Issue a page break first if needed
    $this->CheckPageBreak($h);
    //Draw the cells of the row
    for($i=0;$i<count($data);$i++)
    {
        $w=$this->widths[$i];
        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : $this->alinea;
        //Save the current position
        $x=$this->GetX();
        $y=$this->GetY();
        //Draw the border
        $this->Rect($x,$y,$w,$h);
        //Print the text
        $this->MultiCell($w,5,$data[$i],0,$a);
        //Put the position to the right of the cell
        $this->SetXY($x+$w,$y);
    }
    //Go to the next line
    $this->Ln($h);
	$this->SetDrawColor(255,255,255);
	$this->SetLineWidth(1);
	$this->Line(10, 45, 10, 200);
	$this->Line(270, 45, 270, 250);
	$this->SetDrawColor(1,1,1);
	$this->SetLineWidth(0.2);
}

function CheckPageBreak($h)
{
    //If the height h would cause an overflow, add a new page immediately
    //if($this->GetY()+$h>$this->PageBreakTrigger){
	if($this->GetY()+$h>160){
        $this->AddPage($this->CurOrientation);
		$this->SetFont('Arial','B',8);
//$this->SetTextColor(0, 0, 128);
$y=$this->GetY();
$this->SetLineWidth(0.5);
$this->Line(10, $y, 270, $y);
$this->SetLineWidth(0.2);
$this->MultiCell(16,5,"No. CONSE- CUTIVO",1,'C');
	$this->SetXY(26,$this->GetY()-15);
	$this->MultiCell(18,15,"MARCA",1,'C');
	$this->SetXY(44,$this->GetY()-15);
	$this->MultiCell(18,5,"MODELO Y N�MERO DE PLACA",1,'C');
	$this->SetXY(62,$this->GetY()-15);
	$this->MultiCell(16,15,"COLOR",1,'C');
	$this->SetXY(78,$this->GetY()-15);
	$this->MultiCell(22,15,"No. DE SERIE",1,'C');
	$this->SetXY(100,$this->GetY()-15);
	$this->MultiCell(20,7.5,"No. DE FACTURA",1,'C');
	$this->SetXY(120,$this->GetY()-15);
	$this->MultiCell(20,15,"COSTO",1,'C');
	$this->SetXY(140,$this->GetY()-15);
	$this->MultiCell(18,5,"FECHA DE ADQUISI- CI�N",1,'C');
	$this->SetXY(158,$this->GetY()-15);
	$this->MultiCell(30,7.5,"NOMBRE DE RESPONSABLE",1,'C');
	$this->SetXY(188,$this->GetY()-15);
	$this->MultiCell(32,7.5,"SITUACI�N ACTUAL",1,'C');
	$this->SetX(188);
	$this->MultiCell(16,3.75,"ACCIDEN- TADO",1,'C');
	$this->SetXY(204,$this->GetY()-7.5);
	$this->MultiCell(16,3.75,"REPARA- CI�N",1,'C');
	$this->SetXY(220,$this->GetY()-15);
	$this->MultiCell(20,7.5,"LOCALIZACI�N F�SICA",1,'C');
	$this->SetXY(240,$this->GetY()-15);
	$this->MultiCell(30,15,"OBSERVACIONES",1,'C');
$y=$this->GetY();
$this->SetLineWidth(0.5);
$this->Line(10, $y, 270, $y);
$this->SetLineWidth(0.2);
$this->SetFont('Arial','',7);

	}
}

function NbLines($w,$txt)
{
    //Computes the number of lines a MultiCell of width w will take
    $cw=&$this->CurrentFont['cw'];
    if($w==0)
        $w=$this->w-$this->rMargin-$this->x;
    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
    $s=str_replace("\r",'',$txt);
    $nb=strlen($s);
    if($nb>0 and $s[$nb-1]=="\n")
        $nb--;
    $sep=-1;
    $i=0;
    $j=0;
    $l=0;
    $nl=1;
    while($i<$nb)
    {
        $c=$s[$i];
        if($c=="\n")
        {
            $i++;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
            continue;
        }
        if($c==' ')
            $sep=$i;
        $l+=$cw[$c];
        if($l>$wmax)
        {
            if($sep==-1)
            {
                if($i==$j)
                    $i++;
            }
            else
                $i=$sep+1;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
        }
        else
            $i++;
    }
    return $nl;
}
}
?>