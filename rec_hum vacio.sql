-- phpMyAdmin SQL Dump
-- version 2.6.0-pl2
-- http://www.phpmyadmin.net
-- 
-- Servidor: localhost
-- Tiempo de generaci�n: 22-02-2006 a las 11:17:39
-- Versi�n del servidor: 4.0.17
-- Versi�n de PHP: 4.3.9
-- 
-- Base de datos: `rec_hum`
-- 

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ayuntamiento`
-- 

CREATE TABLE `ayuntamiento` (
  `clave` int(11) NOT NULL default '0',
  `nombre` varchar(50) NOT NULL default '',
  `numero` varchar(10) NOT NULL default '',
  `direccion` varchar(100) NOT NULL default '',
  `tel1` varchar(30) NOT NULL default '',
  `tel2` varchar(30) NOT NULL default '',
  `fecha_alta` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM;

-- 
-- Volcar la base de datos para la tabla `ayuntamiento`
-- 

INSERT INTO `ayuntamiento` VALUES (1, 'HUEHUETOCA', '123', 'AV. TAL NO.50, COL. FULANA, C.P.57000', '-1505', '01951-3457', '2006-01-22');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `bienes_muebles`
-- 

CREATE TABLE `bienes_muebles` (
  `clave` bigint(20) NOT NULL auto_increment,
  `numero` bigint(20) NOT NULL default '0',
  `nombre` varchar(75) NOT NULL default '',
  `inventario` varchar(30) NOT NULL default '',
  `marca` varchar(30) NOT NULL default '',
  `modelo` varchar(30) NOT NULL default '',
  `serie` varchar(30) NOT NULL default '',
  `uso` varchar(30) NOT NULL default '',
  `observaciones` mediumblob NOT NULL,
  `clave_dependencia` varchar(30) NOT NULL default '',
  `ubicacion` varchar(30) NOT NULL default '',
  `ruta_foto` varchar(100) NOT NULL default '',
  `activo` int(11) NOT NULL default '0',
  `usuario` varchar(30) NOT NULL default '',
  `fecha_alta` datetime NOT NULL default '0000-00-00 00:00:00',
  `fecha_mod` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `bienes_muebles`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `configuracion`
-- 

CREATE TABLE `configuracion` (
  `clave` int(11) NOT NULL auto_increment,
  `datos` int(11) NOT NULL default '0',
  `area` int(11) NOT NULL default '0',
  `user` int(11) NOT NULL default '0',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=2 ;

-- 
-- Volcar la base de datos para la tabla `configuracion`
-- 

INSERT INTO `configuracion` VALUES (1, 1, 0, 0);

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `depen_form`
-- 

CREATE TABLE `depen_form` (
  `clave` int(11) NOT NULL auto_increment,
  `clave_area` varchar(15) NOT NULL default '',
  `clave_formato` varchar(20) NOT NULL default '',
  `fecha_inicio` datetime NOT NULL default '0000-00-00 00:00:00',
  `fecha_termino` datetime NOT NULL default '0000-00-00 00:00:00',
  `iniciado` int(11) NOT NULL default '0',
  `terminado` int(11) NOT NULL default '0',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `depen_form`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `dependencia`
-- 

CREATE TABLE `dependencia` (
  `clave` int(15) NOT NULL auto_increment,
  `nombre` varchar(50) NOT NULL default '',
  `depende` varchar(15) NOT NULL default '',
  `resp_nombre` varchar(50) NOT NULL default '',
  `puesto` varchar(50) NOT NULL default '',
  `configurada` int(11) NOT NULL default '0',
  `ICO-001` int(11) NOT NULL default '0',
  `ICO-002` int(11) NOT NULL default '0',
  `ICO-003` int(11) NOT NULL default '0',
  `ICO-004` int(11) NOT NULL default '0',
  `ICO-005` int(11) NOT NULL default '0',
  `ICO-006` int(11) NOT NULL default '0',
  `IFF-001` int(11) NOT NULL default '0',
  `IFF-002` int(11) NOT NULL default '0',
  `IFF-003` int(11) NOT NULL default '0',
  `IFF-004` int(11) NOT NULL default '0',
  `IFF-005` int(11) NOT NULL default '0',
  `IFF-006` int(11) NOT NULL default '0',
  `IFF-007` int(11) NOT NULL default '0',
  `IFF-008` int(11) NOT NULL default '0',
  `IFF-009` int(11) NOT NULL default '0',
  `IFF-010` int(11) NOT NULL default '0',
  `IFF-011` int(11) NOT NULL default '0',
  `IFF-012` int(11) NOT NULL default '0',
  `IFF-013` int(11) NOT NULL default '0',
  `IFF-014` int(11) NOT NULL default '0',
  `IFF-015` int(11) NOT NULL default '0',
  `IFF-016` int(11) NOT NULL default '0',
  `IFF-017` int(11) NOT NULL default '0',
  `IFF-018` int(11) NOT NULL default '0',
  `IFF-019` int(11) NOT NULL default '0',
  `IFF-020` int(11) NOT NULL default '0',
  `IFF-021` int(11) NOT NULL default '0',
  `IFF-022` int(11) NOT NULL default '0',
  `IFF-023` int(11) NOT NULL default '0',
  `IFF-024` int(11) NOT NULL default '0',
  `IFF-025` int(11) NOT NULL default '0',
  `IFF-026` int(11) NOT NULL default '0',
  `IFF-027` int(11) NOT NULL default '0',
  `IFF-028` int(11) NOT NULL default '0',
  `ICA-001` int(11) NOT NULL default '0',
  `ICA-002` int(11) NOT NULL default '0',
  `ICA-003` int(11) NOT NULL default '0',
  `ICA-004` int(11) NOT NULL default '0',
  `ICA-005` int(11) NOT NULL default '0',
  `ICA-006` int(11) NOT NULL default '0',
  `ICA-007` int(11) NOT NULL default '0',
  `ICA-008` int(11) NOT NULL default '0',
  `ICA-009` int(11) NOT NULL default '0',
  `IOP-001` int(11) NOT NULL default '0',
  `IOP-002` int(11) NOT NULL default '0',
  `IOP-003` int(11) NOT NULL default '0',
  `IOP-004` int(11) NOT NULL default '0',
  `IOP-005` int(11) NOT NULL default '0',
  `IOP-006` int(11) NOT NULL default '0',
  `IPE-001` int(11) NOT NULL default '0',
  `IPE-002` int(11) NOT NULL default '0',
  `IPE-003` int(11) NOT NULL default '0',
  `IPE-004` int(11) NOT NULL default '0',
  `IPE-005` int(11) NOT NULL default '0',
  `IPE-006` int(11) NOT NULL default '0',
  `IPE-007` int(11) NOT NULL default '0',
  `IPE-008` int(11) NOT NULL default '0',
  `IAD-001` int(11) NOT NULL default '0',
  `IAD-002` int(11) NOT NULL default '0',
  `IAD-003` int(11) NOT NULL default '0',
  `IDO-001` int(11) NOT NULL default '0',
  `IDO-002` int(11) NOT NULL default '0',
  `IDO-003` int(11) NOT NULL default '0',
  `IDO-004` int(11) NOT NULL default '0',
  `IDO-005` int(11) NOT NULL default '0',
  `IDO-006` int(11) NOT NULL default '0',
  `IDO-007` int(11) NOT NULL default '0',
  `IDO-008` int(11) NOT NULL default '0',
  `IDO-009` int(11) NOT NULL default '0',
  `IRH-001` int(11) NOT NULL default '0',
  `IRH-002` int(11) NOT NULL default '0',
  `IRM-001` int(11) NOT NULL default '0',
  `IRM-002` int(11) NOT NULL default '0',
  `IRM-003` int(11) NOT NULL default '0',
  `IPO-001` int(11) NOT NULL default '0',
  `IPO-002` int(11) NOT NULL default '0',
  `IPO-003` int(11) NOT NULL default '0',
  `IPO-004` int(11) NOT NULL default '0',
  `IPO-005` int(11) NOT NULL default '0',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `dependencia`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `formato`
-- 

CREATE TABLE `formato` (
  `clave` varchar(20) NOT NULL default '',
  `nombre` varchar(150) NOT NULL default '',
  `clave_tipo` varchar(10) NOT NULL default '0',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM;

-- 
-- Volcar la base de datos para la tabla `formato`
-- 

INSERT INTO `formato` VALUES ('ICO-001', 'RELACI�N DE CUENTAS BANCARIAS DE RECURSOS PROPIOS Y PROGRAMAS ESPECIALES', 'ICO', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('ICO-002', 'CANCELACI�N DE CUENTAS BANCARIAS O CAMBIO DE FIRMAS', 'ICO', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('ICO-003', 'RELACI�N DE CHEQUES DE CAJA', 'ICO', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('ICO-004', 'CORTE DE CHEQUERAS', 'ICO', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('ICO-005', 'RELACI�N DE CR�DITOS CONTRATADOS CUYO PLAZO DE AMORTIZACI�N REBASE EL T�RMINO DE LA GESTI�N MUNICIPAL CONTRATANTE', 'ICO', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IFF-001', 'ESTADO DE DEUDA P�BLICA', 'IFF', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IFF-002', 'RELACI�N DE RECIBOS OFICIALES DE INGRESOS Y OTRAS FORMAS VALORABLES', 'IFF', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IFF-003', 'SISTEMA DE CONTABILIDAD', 'IFF', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IFF-004', 'RELACI�N DE PROGRAMAS TRANSFERIDOS EN ADMINISTRACI�N', 'IFF', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IFF-005', 'RELACI�N DE INFORMES MENSUALES Y CUANTAS P�BLICAS ANUALES ENTREGADOS AL �RGANO SUPERIOR DE FISCALIZACI�N DEL ESTADO DE M�XICO', 'IFF', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('ICA-001', 'VALIDACI�N DE LA INFORMACI�N Y DOCUMENTACI�N EN MATERIA CATASTRAL', 'ICA', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('ICA-002', 'REPORTE DE AVANCE: PRIMER CORTE', 'ICA', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('ICA-003', 'REPORTE DE AVANCE: SEGUNDO CORTE', 'ICA', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('ICA-004', 'REPORTE DE AVANCE: CORTE DEFINITIVO', 'ICA', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('ICA-005', 'CONCILIACI�N DE LA INFORMACI�N Y DOCUMENTACI�N EN MATERIA CATASTRAL', 'ICA', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IPE-001', 'INVENTARIO GENERAL DE BIENES INMUEBLES', 'IPE', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IPE-002', 'C�DULA DE BIENES INMUEBLES DE LA ENTIDAD MUNICIPAL', 'IPE', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IPE-003', 'RELACI�N DE BIENES INMUEBLES EN SITUACI�N DE ARRENDAMIENTO, COMODATO O USUFRUCTO', 'IPE', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IPE-004', 'RELACI�N DE BIENES INMUEBLES ADQUIRIDOS', 'IPE', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IPE-005', 'RELACI�N DE BIENES INMUEBLES ENAJENADOS', 'IPE', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IAD-001', 'SERVICIOS DE INTERNET', 'IAD', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IAD-002', 'INTEGRACI�N DE LOS MIEMBROS DEL CABILDO, CONSEJO, JUNTA DE GOBIERNO SALIENTE Y ENTRANTE', 'IAD', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IOP-001', 'RELACI�N DE OBRAS EN PROCESO', 'IOP', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IOP-002', 'RELACI�N DE EXPEDIENTES T�CNICOS DE OBRAS EN PROCESO', 'IOP', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IOP-003', 'RELACI�N DE OBRAS TERMINADAS', 'IOP', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IOP-004', 'RELACI�N DE EXPEDIENTES T�CNICOS DE OBRAS TERMINADAS', 'IOP', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IOP-005', 'RELACI�N DE ACTAS DE ENTREGA-RECEPCI�N DE OBRAS TERMINADAS', 'IOP', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IDO-001', 'INVENTARIO DE MATERIAL BIBLIOGR�FICO Y HEMEROGR�FICO', 'IDO', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IDO-002', 'INVENTARIO DE ARCHIVO EN TR�MITE', 'IDO', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IDO-003', 'INVENTARIO DE ARCHIVO DE CONCENTRACI�N', 'IDO', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IDO-004', 'INVENTARIO DE DOCUMENTACI�N NO CONVENCIONAL', 'IDO', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IDO-005', 'ACUERDOS DE CABILDO, CONSEJO O JUNTA DE GOBIERNO PENDIENTES POR CUMPLIR', 'IDO', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IRH-001', 'RELACI�N DEL PERSONAL QUE LABORA EN EL �REA QUE SE ENTREGA', 'IRH', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IRH-002', 'RELACI�N DE PERSONAL COMISIONADO', 'IRH', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IRM-001', 'INVENTARIO DE BIENES DE CONSUMO/PAPELER�A', 'IRM', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IRM-002', 'INVENTARIOS VARIOS (NO CONSIDERADOS EN EL ACTIVO FIJO)', 'IRM', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IRM-003', 'RELACI�N DE SELLOS OFICIALES', 'IRM', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IPO-001', 'INVENTARIO DE BIENES MUEBLES (�NICAMENTE DE LA OFICINA QUE SE ENTREGA)', 'IPO', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IPO-002', 'RELACI�N DE EQUIPO DE COMUNICACI�N', 'IPO', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IPO-003', 'RELACI�N DE LLAVES', 'IPO', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IPO-004', 'RELACI�N DE VEH�CULOS OFICIALES ASIGNADOS AL �REA', 'IPO', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('IPO-005', 'RELACI�N DE GAFETES Y/O CREDENCIALES OFICIALES', 'IPO', '2006-02-02 00:00:00');
INSERT INTO `formato` VALUES ('ICO-006', 'RELACI�N DE CONTRATOS DE PRESTACI�N DE SERVICIOS CUYO T�RMINO EXCEDA EL DE LA GESTI�N MUNICIPAL', 'ICO', '2006-02-13 00:00:00');
INSERT INTO `formato` VALUES ('IFF-006', 'RELACI�N DE INFORMES MENSUALES Y CUENTAS P�BLICAS ANUALES DEVUELTOS A LA ENTIDAD MUNICIPAL REVISADOS POR EL �RGANO SUPERIOR DE FISCALIZACI�N', 'IFF', '2006-02-13 00:00:00');
INSERT INTO `formato` VALUES ('IFF-007', 'RELACI�N DE OBSERVACIONES EMITIDAS Y AUDITOR�AS PRACTICADAS POR EL �RGANO SUPERIOR DE FISCALIZACI�N DEL ESTADO DE M�XICO', 'IFF', '2006-02-13 00:00:00');
INSERT INTO `formato` VALUES ('IFF-008', 'RELACI�N DE PAGOS PROVISIONALES Y DECLARACIONES ANUALES ENTERADAS A LA SECRETAR�A DE HACIENDA Y CR�DITO P�BLICO', 'IFF', '2006-02-13 00:00:00');
INSERT INTO `formato` VALUES ('IFF-009', 'RELACI�N DE EXPEDIENTES DE DECLARACIONES DE IMPUESTOS', 'IFF', '2006-02-13 00:00:00');
INSERT INTO `formato` VALUES ('IFF-010', 'REFERENCIA DE ADEUDO Y NEGOCIACI�N CON LA COMPA��A DE LUZ Y FUERZA', 'IFF', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('IFF-011', 'RELACI�N DE EXPEDIENTES RELATIVOS A LUZ Y FUERZA', 'IFF', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('IFF-012', 'REFERENCIA DE RETENCIONES Y ENTEROS AL ISSEMYM', 'IFF', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('IFF-013', 'RELACI�N DE EXPEDIENTES RELATIVOS AL ISSEMYM', 'IFF', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('IFF-014', 'REFERENCIA DE ADEUDO Y NEGOCIACI�N CON LA COMISI�N ESTATAL DE AGUA DEL ESTADO DE M�XICO (CAEM)', 'IFF', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('IFF-015', 'RELACI�N DE EXPEDIENTES RELATIVOS A CAEM', 'IFF', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('IFF-016', 'PADR�N DE CONTRIBUYENTES', 'IFF', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('IFF-017', 'RELACI�N DE CONTRIBUYENTES EN SITUACI�N DE REZAGO', 'IFF', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('IFF-018', 'PADR�N DE PROVEEDORES', 'IFF', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('IFF-019', 'RELACI�N DE MULTAS IMPUESTAS POR AUTORIDADES FEDERALES NO FISCALES', 'IFF', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('IFF-020', 'RELACI�N DE PROYECTOS PRODUCTIVOS', 'IFF', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('IFF-021', 'REPORTE DE REMUNERACIONES AL PERSONAL DE MANDOS Y MEDIOS SUPERIORES', 'IFF', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('IFF-022', 'REPORTE DE N�MINA', 'IFF', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('IFF-023', 'RELACI�N DE INCREMENTOS SALARIALES Y CAMBISO DE CATEGOR�AS', 'IFF', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('IFF-024', 'RELACI�N DE MOVIMIENTOS LABORALES (DESPIDOS - CONTRATACIONES)', 'IFF', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('IFF-025', 'RELACI�N DE LIQUIDACIONES LABORALES', 'IFF', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('IFF-026', 'RELACI�N DE PERSONAL SINDICALIZADO', 'IFF', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('IFF-027', 'RELACI�N DE CONVENIOS SINDICALES', 'IFF', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('IFF-028', 'RELACI�N DE JUICIOS LABORALES (VIGENTES)', 'IFF', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('ICA-006', 'INVENTARIO DE ARCHIVO EN TR�MITE EN MATERIA CATASTRAL', 'ICA', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('ICA-007', 'INVENTARIO DE DOCUMENTACI�N NO CONVENCIONAL EN MATERIA CATASTRAL', 'ICA', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('ICA-008', 'INVENTARIO DE ACERVO BIBLIOGR�FICO Y HEMEROGR�FICO EN MATERIA CATASTRAL', 'ICA', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('ICA-009', 'INVENTARIO DE ARCHIVO DE CONCENTRACI�N EN MATERIA CATASTRAL', 'ICA', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('IOP-006', 'RELACI�N DE CONTRATOS DE OBRA CUYO T�RMINO EXCEDA DE LA GESTI�N MUNICIPAL CONTRATANTE', 'IOP', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('IPE-006', 'RELACI�N DE VEH�CULOS ACCIDENTADOS O EN REPARACI�N', 'IPE', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('IPE-007', 'RELACI�N DE LIBROS DE ACTAS DE CABILDO, CONSEJO O JUNTA DE GOBIERNO', 'IPE', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('IPE-008', 'ARCHIVO MINICIPAL', 'IPE', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('IDO-006', 'ACUERDOS DE CABILDO, CONSEJO O JUNTA DE GOBIERNO PARA LA REMOCI�N O NOMBRAMIENTO DEL NUEVO TITULAR DEL �REA QUE SE ENTREGA', 'IDO', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('IDO-007', 'RELACI�N DE ACUERDOS Y CONVENIOS CON LA FEDERACI�N, EL ESTADO, LOS MUNICIPIOS Y PARTICULARES (VIGENTES)', 'IDO', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('IDO-008', 'ASUNTOS JUR�DICOS', 'IDO', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('IDO-009', 'RELACI�N DE ASUNTOS PENDIENTES', 'IDO', '0000-00-00 00:00:00');
INSERT INTO `formato` VALUES ('IAD-003', 'OTROS', 'IAD', '0000-00-00 00:00:00');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `iad-001`
-- 

CREATE TABLE `iad-001` (
  `clave` bigint(20) NOT NULL auto_increment,
  `pagina` varchar(50) NOT NULL default '',
  `fecha_alta` date NOT NULL default '0000-00-00',
  `mail` varchar(50) NOT NULL default '',
  `fecha_alta3` date NOT NULL default '0000-00-00',
  `administrador` varchar(75) NOT NULL default '',
  `webmaster` varchar(30) NOT NULL default '',
  `boton` int(11) NOT NULL default '0',
  `fecha_alta2` date NOT NULL default '0000-00-00',
  `observaciones` tinyblob NOT NULL,
  `comentarios` tinyblob NOT NULL,
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=2 ;

-- 
-- Volcar la base de datos para la tabla `iad-001`
-- 

INSERT INTO `iad-001` VALUES (1, 'www.iidesoft.com', '2006-02-20', 'iidesoft@iidesoft.com', '2006-10-12', 'IIDESOFT | M�xico', 'Javier L�pez Flores', 1, '2006-10-12', 0x6e696e67756e61, 0x6573746f79206465736573706572616461, '2006-02-21 19:02:44', 'alejandro');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `iad-002`
-- 

CREATE TABLE `iad-002` (
  `clave` bigint(20) NOT NULL auto_increment,
  `clave_trab` varchar(30) NOT NULL default '',
  `cargo` varchar(50) NOT NULL default '',
  `nombre_entra` varchar(75) NOT NULL default '',
  `dom_entra` varchar(100) NOT NULL default '',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `iad-002`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `iad-003`
-- 

CREATE TABLE `iad-003` (
  `clave` bigint(20) NOT NULL auto_increment,
  `numero` varchar(50) NOT NULL default '',
  `descripcion` varchar(100) NOT NULL default '',
  `observaciones` tinyblob NOT NULL,
  `clave_area` varchar(30) NOT NULL default '',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `iad-003`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ica-001`
-- 

CREATE TABLE `ica-001` (
  `clave` bigint(20) NOT NULL auto_increment,
  `concepto` varchar(100) NOT NULL default '',
  `no_hojas` int(11) NOT NULL default '0',
  `observaciones` tinyblob NOT NULL,
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=6 ;

-- 
-- Volcar la base de datos para la tabla `ica-001`
-- 

INSERT INTO `ica-001` VALUES (1, 'Conciliaci�n de la Informaci�n y documentaci�n en materia catastral', 0, 0x636f6e63696c696163696f6e, '2006-02-20 14:12:28', 'alejandro');
INSERT INTO `ica-001` VALUES (2, 'Inventario de Archivo en Tr�mite en materia catastral', 0, 0x6172636869766f, '2006-02-20 14:12:28', 'alejandro');
INSERT INTO `ica-001` VALUES (3, 'Inventario de Documentaci�n no Convencional en materia catastral', 0, 0x646f63756d656e7461, '2006-02-20 14:12:28', 'alejandro');
INSERT INTO `ica-001` VALUES (4, 'Inventario de Acervo Bibliogr�fico y Hemerogr�fico en Materia Catastral', 0, 0x61636572766f, '2006-02-20 14:12:28', 'alejandro');
INSERT INTO `ica-001` VALUES (5, 'Inventario Archivo de Concentraci�n en materia catastral', 0, 0x6d6f677565, '2006-02-20 14:12:28', 'alejandro');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ica-002`
-- 

CREATE TABLE `ica-002` (
  `clave` bigint(20) NOT NULL auto_increment,
  `num_consecutivo` int(11) NOT NULL default '0',
  `actividad` varchar(150) NOT NULL default '',
  `num_corte` int(11) NOT NULL default '0',
  `estado` int(11) NOT NULL default '0',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=24 ;

-- 
-- Volcar la base de datos para la tabla `ica-002`
-- 

INSERT INTO `ica-002` VALUES (1, 1, 'Designaci�n del Coordinador del programa para la Unidad Administrativa de Catastro Municipal', 1, 0, '2006-02-20 17:41:52', 'alejandro');
INSERT INTO `ica-002` VALUES (2, 2, 'Designaci�n de las personas responsables por �rea', 1, 0, '2006-02-20 17:41:52', 'alejandro');
INSERT INTO `ica-002` VALUES (3, 3, 'An�lisis del documento normativo del programa, a cargo del responsable de la Unidad y personal operativo', 1, 0, '2006-02-20 17:41:52', 'alejandro');
INSERT INTO `ica-002` VALUES (4, 4, 'Determinar las caracter�sticas de presentaci�n de la informaci�n y documentaci�n', 1, 0, '2006-02-20 17:41:52', 'alejandro');
INSERT INTO `ica-002` VALUES (5, 5, 'Definici�n de los formatos a requisitar por �rea', 1, 0, '2006-02-20 17:41:52', 'alejandro');
INSERT INTO `ica-002` VALUES (6, 6, 'Organizaci�n de la Informaci�n por �rea', 1, 0, '2006-02-20 17:41:52', 'alejandro');
INSERT INTO `ica-002` VALUES (7, 7, 'Requisitado de los formatos (avance)', 1, 0, '2006-02-20 17:41:52', 'alejandro');
INSERT INTO `ica-002` VALUES (8, 8, 'Entrega de avances al coordinador para su revisi�n', 1, 0, '2006-02-20 17:41:53', 'alejandro');
INSERT INTO `ica-002` VALUES (9, 9, 'Devoluci�n de formatos revisados por el coordinador, con observaciones y recomendaciones', 1, 0, '2006-02-20 17:41:53', 'alejandro');
INSERT INTO `ica-002` VALUES (10, 1, 'Atenci�n a las observaciones y recomendaciones del coordinador del programa, efectuadas en el primer corte', 2, 0, '2006-02-20 17:48:29', 'alejandro');
INSERT INTO `ica-002` VALUES (11, 2, 'Continuaci�n del requisitado de avance', 2, 0, '2006-02-20 17:48:29', 'alejandro');
INSERT INTO `ica-002` VALUES (12, 3, 'Validaci�n y congruencia de la informaci�n (documentos - existencia f�sica', 2, 0, '2006-02-20 17:48:29', 'alejandro');
INSERT INTO `ica-002` VALUES (13, 4, 'Revisi�n de la calidad de la informaci�n', 2, 0, '2006-02-20 17:48:29', 'alejandro');
INSERT INTO `ica-002` VALUES (14, 5, 'Entrega de avances al coordinador para su revisi�n', 2, 0, '2006-02-20 17:48:29', 'alejandro');
INSERT INTO `ica-002` VALUES (15, 6, 'Devoluci�n de formatos revisados por el coordinador, con observaciones y recomendaciones', 2, 0, '2006-02-20 17:48:29', 'alejandro');
INSERT INTO `ica-002` VALUES (16, 1, 'Atenci�n a las observaciones y recomendaciones del coordinador de programa, efectuadas en el segundo corte', 3, 0, '2006-02-20 17:53:28', 'alejandro');
INSERT INTO `ica-002` VALUES (17, 2, 'Actualizaci�n de la informaci�n al 31 de julio de 2006 y de ser posible al 15 de agosto de 2006', 3, 0, '2006-02-20 17:53:28', 'alejandro');
INSERT INTO `ica-002` VALUES (18, 3, '�ltima revisi�n de la calidad de la informaci�n, del sustento y congruencia de la misma', 3, 0, '2006-02-20 17:53:28', 'alejandro');
INSERT INTO `ica-002` VALUES (19, 4, 'Autorizaci�n de los formatos con la firma del Responsable de la Unidad Administrativa de Catastro', 3, 0, '2006-02-20 17:53:28', 'alejandro');
INSERT INTO `ica-002` VALUES (20, 5, 'Requisitado del documento de validaci�n por parte del Responsable de la Unidad Administrativa de Catastro', 3, 0, '2006-02-20 17:53:28', 'alejandro');
INSERT INTO `ica-002` VALUES (21, 6, 'Fotocopia del documento definitivo con los tantos necesarios para entrega a las dependencias correspondientes', 3, 0, '2006-02-20 17:53:28', 'alejandro');
INSERT INTO `ica-002` VALUES (22, 7, 'Reuni�n con el titular entrante de la Unidad Administrativa de Catastro', 3, 0, '2006-02-20 17:53:28', 'alejandro');
INSERT INTO `ica-002` VALUES (23, 8, 'Acto de entrega recepci�n (18 de agosto de 2006)', 3, 0, '2006-02-20 17:53:28', 'alejandro');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ica-005`
-- 

CREATE TABLE `ica-005` (
  `clave` bigint(20) NOT NULL auto_increment,
  `concepto` varchar(100) NOT NULL default '',
  `tipo` varchar(5) NOT NULL default '',
  `per1` double NOT NULL default '0',
  `per2` double NOT NULL default '0',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=23 ;

-- 
-- Volcar la base de datos para la tabla `ica-005`
-- 

INSERT INTO `ica-005` VALUES (1, '�rea de inter�s catastral', 'I', 0, 0, '2006-02-20 18:45:00', 'alejandro');
INSERT INTO `ica-005` VALUES (2, 'Predios en el Padr�n Catastral', 'I', 0, 0, '2006-02-20 18:45:00', 'alejandro');
INSERT INTO `ica-005` VALUES (3, '�reas Homog�neas en el Padr�n Catastral', 'I', 0, 0, '2006-02-20 18:45:00', 'alejandro');
INSERT INTO `ica-005` VALUES (4, 'Manzanas en el Padr�n Catastral', 'I', 0, 0, '2006-02-20 18:45:00', 'alejandro');
INSERT INTO `ica-005` VALUES (5, 'Bandas de Valor en el Padr�n Catastral', 'I', 0, 0, '2006-02-20 18:45:00', 'alejandro');
INSERT INTO `ica-005` VALUES (6, 'C�digos de Clave de Calle y Nomenclatura', 'I', 0, 0, '2006-02-20 18:45:00', 'alejandro');
INSERT INTO `ica-005` VALUES (7, 'Rango de Valores Unitarios de Suelo aprobados por la Legislatura', 'I', 0, 0, '2006-02-20 18:45:00', 'alejandro');
INSERT INTO `ica-005` VALUES (8, 'Expedientes de antecedentes documentales de los servicios catastrales', 'D', 0, 0, '2006-02-20 18:45:00', 'alejandro');
INSERT INTO `ica-005` VALUES (9, 'Ortofotos esc. 1:10,000', 'D', 0, 0, '2006-02-20 18:45:00', 'alejandro');
INSERT INTO `ica-005` VALUES (10, 'Ortofotos esc. 1:5,000', 'D', 0, 0, '2006-02-20 18:45:00', 'alejandro');
INSERT INTO `ica-005` VALUES (11, 'Amplificaciones Fotogr�ficas', 'D', 0, 0, '2006-02-20 18:45:00', 'alejandro');
INSERT INTO `ica-005` VALUES (12, 'Fotograf�as de Contacto', 'D', 0, 0, '2006-02-20 18:45:00', 'alejandro');
INSERT INTO `ica-005` VALUES (13, 'Fotograf�as de contacto', 'D', 0, 0, '2006-02-20 18:45:00', 'alejandro');
INSERT INTO `ica-005` VALUES (14, 'Gr�ficos digitalizados esc. Aprox. 1:5,000', 'D', 0, 0, '2006-02-20 18:45:00', 'alejandro');
INSERT INTO `ica-005` VALUES (15, 'Planos catastrales esc. 1:1,000', 'D', 0, 0, '2006-02-20 18:45:00', 'alejandro');
INSERT INTO `ica-005` VALUES (16, 'Planos catastrales esc. 1:500', 'D', 0, 0, '2006-02-20 18:45:00', 'alejandro');
INSERT INTO `ica-005` VALUES (17, 'Planos autorizados por Desarrollo Urbano', 'D', 0, 0, '2006-02-20 18:45:00', 'alejandro');
INSERT INTO `ica-005` VALUES (18, 'Levantamientos topogr�ficos', 'D', 0, 0, '2006-02-20 18:45:00', 'alejandro');
INSERT INTO `ica-005` VALUES (19, 'Aval�os catastrales o comerciales realizados por el IGECEM', 'D', 0, 0, '2006-02-20 18:45:00', 'alejandro');
INSERT INTO `ica-005` VALUES (20, 'Carpetas manzaneras y documentaci�n', 'D', 0, 0, '2006-02-20 18:45:00', 'alejandro');
INSERT INTO `ica-005` VALUES (21, 'Memorias de c�lculo y condominios', 'D', 0, 0, '2006-02-20 18:45:00', 'alejandro');
INSERT INTO `ica-005` VALUES (22, 'Expedientes de Predios Industriales', 'D', 0, 0, '2006-02-20 18:45:00', 'alejandro');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ica-006`
-- 

CREATE TABLE `ica-006` (
  `clave` bigint(20) NOT NULL auto_increment,
  `clave_exp` varchar(50) NOT NULL default '',
  `nombre_exp` varchar(100) NOT NULL default '',
  `no_legajos` float NOT NULL default '0',
  `no_fojas` float NOT NULL default '0',
  `fecha1` date NOT NULL default '0000-00-00',
  `fecha2` date NOT NULL default '0000-00-00',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=4 ;

-- 
-- Volcar la base de datos para la tabla `ica-006`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ica-007`
-- 

CREATE TABLE `ica-007` (
  `clave` bigint(20) NOT NULL auto_increment,
  `clave_tipo` varchar(50) NOT NULL default '',
  `clave_int` varchar(30) NOT NULL default '',
  `no_partes` varchar(30) NOT NULL default '',
  `descripcion` varchar(150) NOT NULL default '',
  `fecha_elab` date NOT NULL default '0000-00-00',
  `observaciones` tinyblob NOT NULL,
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=3 ;

-- 
-- Volcar la base de datos para la tabla `ica-007`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ica-008`
-- 

CREATE TABLE `ica-008` (
  `clave` bigint(20) NOT NULL auto_increment,
  `no_clasif` varchar(30) NOT NULL default '',
  `titulo` varchar(150) NOT NULL default '',
  `autor` varchar(100) NOT NULL default '',
  `editorial` varchar(50) NOT NULL default '',
  `fecha_pub` date NOT NULL default '0000-00-00',
  `observaciones` tinyblob NOT NULL,
  `fecha` date NOT NULL default '0000-00-00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=3 ;

-- 
-- Volcar la base de datos para la tabla `ica-008`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ica-009`
-- 

CREATE TABLE `ica-009` (
  `clave` bigint(20) NOT NULL auto_increment,
  `clave_exp` varchar(50) NOT NULL default '',
  `nombre_exp` varchar(150) NOT NULL default '',
  `no_legajos` varchar(30) NOT NULL default '',
  `periodo` varchar(50) NOT NULL default '',
  `observaciones` tinyblob NOT NULL,
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=3 ;

-- 
-- Volcar la base de datos para la tabla `ica-009`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ido-001`
-- 

CREATE TABLE `ido-001` (
  `clave` bigint(20) NOT NULL auto_increment,
  `numero` varchar(30) NOT NULL default '',
  `titulo` varchar(75) NOT NULL default '',
  `autor` varchar(75) NOT NULL default '',
  `editorial` varchar(50) NOT NULL default '',
  `fecha_pub` date NOT NULL default '0000-00-00',
  `observaciones` tinyblob NOT NULL,
  `clave_area` varchar(30) NOT NULL default '',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=3 ;

-- 
-- Volcar la base de datos para la tabla `ido-001`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ido-002`
-- 

CREATE TABLE `ido-002` (
  `clave` bigint(20) NOT NULL auto_increment,
  `no_clave` varchar(30) NOT NULL default '',
  `nombre` varchar(100) NOT NULL default '',
  `total_legajos` double NOT NULL default '0',
  `fecha_ini` date NOT NULL default '0000-00-00',
  `fecha_fin` date NOT NULL default '0000-00-00',
  `clave_area` varchar(30) NOT NULL default '',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=2 ;

-- 
-- Volcar la base de datos para la tabla `ido-002`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ido-003`
-- 

CREATE TABLE `ido-003` (
  `clave` bigint(20) NOT NULL auto_increment,
  `no_caja` varchar(50) NOT NULL default '',
  `no_exps` varchar(50) NOT NULL default '',
  `doctos` varchar(200) NOT NULL default '',
  `periodo` varchar(50) NOT NULL default '',
  `tiempo` varchar(50) NOT NULL default '',
  `observaciones` tinyblob NOT NULL,
  `clave_area` varchar(30) NOT NULL default '',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `ido-003`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ido-004`
-- 

CREATE TABLE `ido-004` (
  `clave` bigint(20) NOT NULL auto_increment,
  `no_unidades` varchar(50) NOT NULL default '',
  `clave_tipo` varchar(50) NOT NULL default '',
  `clave_inv` varchar(50) NOT NULL default '',
  `tipo_doc` varchar(150) NOT NULL default '',
  `observaciones` tinyblob NOT NULL,
  `clave_area` varchar(30) NOT NULL default '',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `ido-004`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ido-005`
-- 

CREATE TABLE `ido-005` (
  `clave` bigint(20) NOT NULL auto_increment,
  `acta_no` varchar(50) NOT NULL default '',
  `fecha_act` date NOT NULL default '0000-00-00',
  `acuerdo` varchar(200) NOT NULL default '',
  `obsevaciones` tinyblob NOT NULL,
  `clave_area` varchar(30) NOT NULL default '',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `ido-005`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ido-006`
-- 

CREATE TABLE `ido-006` (
  `clave` bigint(20) NOT NULL auto_increment,
  `fecha_ac` date NOT NULL default '0000-00-00',
  `clave_ex` varchar(30) NOT NULL default '',
  `cargo_ex` varchar(50) NOT NULL default '',
  `baja_por` varchar(100) NOT NULL default '',
  `nombre_new` varchar(150) NOT NULL default '',
  `cargo_new` varchar(50) NOT NULL default '',
  `observaciones` tinyblob NOT NULL,
  `clave_area` varchar(30) NOT NULL default '',
  `fecha` date NOT NULL default '0000-00-00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `ido-006`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ido-007`
-- 

CREATE TABLE `ido-007` (
  `clave` bigint(20) NOT NULL auto_increment,
  `numero` varchar(50) NOT NULL default '',
  `acuerdo` varchar(50) NOT NULL default '',
  `fecha_vig` date NOT NULL default '0000-00-00',
  `autoridad` varchar(100) NOT NULL default '',
  `observaciones` tinyblob NOT NULL,
  `clave_area` varchar(30) NOT NULL default '',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `ido-007`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ido-008`
-- 

CREATE TABLE `ido-008` (
  `clave` bigint(20) NOT NULL auto_increment,
  `datos_id` varchar(50) NOT NULL default '',
  `asunto` varchar(200) NOT NULL default '',
  `responsable` varchar(30) NOT NULL default '',
  `observaciones` tinyblob NOT NULL,
  `clave_area` varchar(30) NOT NULL default '',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `ido-008`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ido-009`
-- 

CREATE TABLE `ido-009` (
  `clave` bigint(20) NOT NULL auto_increment,
  `numero` varchar(50) NOT NULL default '',
  `descripcion` varchar(200) NOT NULL default '',
  `observaciones` tinyblob NOT NULL,
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `ido-009`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `iop-001`
-- 

CREATE TABLE `iop-001` (
  `clave` bigint(20) NOT NULL auto_increment,
  `no_obra` varchar(50) NOT NULL default '',
  `nombre` varchar(150) NOT NULL default '',
  `ubicacion` varchar(200) NOT NULL default '',
  `fecha_ini` date NOT NULL default '0000-00-00',
  `avance_fis` int(11) NOT NULL default '0',
  `avance_fin` int(11) NOT NULL default '0',
  `rec_prop` double NOT NULL default '0',
  `ramo33` double NOT NULL default '0',
  `codem` double NOT NULL default '0',
  `diputados` double NOT NULL default '0',
  `ciudadanos` double NOT NULL default '0',
  `otros` double NOT NULL default '0',
  `total` double NOT NULL default '0',
  `fecha_term` date NOT NULL default '0000-00-00',
  `tipo` varchar(10) NOT NULL default '',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `iop-001`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `iop-002`
-- 

CREATE TABLE `iop-002` (
  `clave` bigint(20) NOT NULL auto_increment,
  `no_exp` varchar(50) NOT NULL default '',
  `nombre_obra` varchar(150) NOT NULL default '',
  `observaciones` tinyblob NOT NULL,
  `tipo` varchar(10) NOT NULL default '',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `iop-002`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `iop-005`
-- 

CREATE TABLE `iop-005` (
  `clave` bigint(20) NOT NULL auto_increment,
  `no_obra` varchar(50) NOT NULL default '',
  `nombre` varchar(150) NOT NULL default '',
  `ubicaci�n` varchar(200) NOT NULL default '',
  `no_exp` varchar(50) NOT NULL default '',
  `no_acta` varchar(50) NOT NULL default '',
  `fecha1` date NOT NULL default '0000-00-00',
  `observaciones` tinyblob NOT NULL,
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `iop-005`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `iop-006`
-- 

CREATE TABLE `iop-006` (
  `clave` bigint(20) NOT NULL auto_increment,
  `no_contrato` varchar(50) NOT NULL default '',
  `empresa` varchar(150) NOT NULL default '',
  `obra` varchar(150) NOT NULL default '',
  `ubicacion` varchar(200) NOT NULL default '',
  `vigencia` varchar(50) NOT NULL default '',
  `monto_orig` varchar(50) NOT NULL default '',
  `monto_pend` varchar(50) NOT NULL default '',
  `autoriza` varchar(150) NOT NULL default '',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `iop-006`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ipe-002`
-- 

CREATE TABLE `ipe-002` (
  `clave` bigint(20) NOT NULL auto_increment,
  `no_inventario` varchar(50) NOT NULL default '',
  `nombre` varchar(150) NOT NULL default '',
  `calle_no` varchar(100) NOT NULL default '',
  `localidad` varchar(100) NOT NULL default '',
  `norte` varchar(100) NOT NULL default '',
  `prop_norte` varchar(100) NOT NULL default '',
  `sur` varchar(100) NOT NULL default '',
  `prop_sur` varchar(100) NOT NULL default '',
  `este` varchar(100) NOT NULL default '',
  `prop_este` varchar(100) NOT NULL default '',
  `oeste` varchar(100) NOT NULL default '',
  `prop_oeste` varchar(100) NOT NULL default '',
  `tipo` varchar(10) NOT NULL default '',
  `detalle_tipo` varchar(50) NOT NULL default '',
  `zona` varchar(10) NOT NULL default '',
  `uso` varchar(10) NOT NULL default '',
  `detalle_uso` varchar(50) NOT NULL default '',
  `total_super` double NOT NULL default '0',
  `constr_super` double NOT NULL default '0',
  `agua` int(11) NOT NULL default '0',
  `telefono` int(11) NOT NULL default '0',
  `luz` int(11) NOT NULL default '0',
  `alumbrado` int(11) NOT NULL default '0',
  `alcantarilla` int(11) NOT NULL default '0',
  `pavimenta` int(11) NOT NULL default '0',
  `otro` int(11) NOT NULL default '0',
  `detalle_serv` varchar(150) NOT NULL default '',
  `adquisicion` varchar(10) NOT NULL default '',
  `detalle_adq` varchar(100) NOT NULL default '',
  `usuario_inm` varchar(50) NOT NULL default '',
  `ocupante` varchar(50) NOT NULL default '',
  `aprovechamiento` varchar(10) NOT NULL default '',
  `no_escritura` varchar(100) NOT NULL default '',
  `clave_catastro` varchar(100) NOT NULL default '',
  `no_registro` varchar(100) NOT NULL default '',
  `valor_catastro` double NOT NULL default '0',
  `edo_legal` varchar(150) NOT NULL default '',
  `ult_avaluo` date NOT NULL default '0000-00-00',
  `gravamen` int(11) NOT NULL default '0',
  `observaciones` tinyblob NOT NULL,
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `ipe-002`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ipe-003`
-- 

CREATE TABLE `ipe-003` (
  `clave` bigint(20) NOT NULL auto_increment,
  `nombre` varchar(100) NOT NULL default '',
  `situacion` varchar(10) NOT NULL default '',
  `uso` varchar(50) NOT NULL default '',
  `area_resp` varchar(30) NOT NULL default '',
  `contrato` varchar(50) NOT NULL default '',
  `ubicacion` varchar(100) NOT NULL default '',
  `observaciones` tinyblob NOT NULL,
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `ipe-003`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ipe-004`
-- 

CREATE TABLE `ipe-004` (
  `clave` bigint(20) NOT NULL auto_increment,
  `nombre` varchar(100) NOT NULL default '',
  `motivo` varchar(100) NOT NULL default '',
  `ubicacion` varchar(100) NOT NULL default '',
  `monto` double NOT NULL default '0',
  `fuente` varchar(75) NOT NULL default '',
  `autorizacion` varchar(75) NOT NULL default '',
  `tipo` varchar(10) NOT NULL default '',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `ipe-004`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ipe-006`
-- 

CREATE TABLE `ipe-006` (
  `clave` bigint(20) NOT NULL auto_increment,
  `marca` varchar(50) NOT NULL default '',
  `modelo` varchar(50) NOT NULL default '',
  `placa` varchar(20) NOT NULL default '',
  `color` varchar(20) NOT NULL default '',
  `serie` varchar(50) NOT NULL default '',
  `factura` varchar(50) NOT NULL default '',
  `costo` double NOT NULL default '0',
  `fecha_adq` date NOT NULL default '0000-00-00',
  `clave_area` varchar(30) NOT NULL default '',
  `responsable` varchar(50) NOT NULL default '',
  `situacion` varchar(10) NOT NULL default '',
  `localizacion` varchar(50) NOT NULL default '',
  `observaciones` tinyblob NOT NULL,
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `ipe-006`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ipe-007`
-- 

CREATE TABLE `ipe-007` (
  `clave` bigint(20) NOT NULL auto_increment,
  `no_libro` varchar(50) NOT NULL default '',
  `fecha_ini` date NOT NULL default '0000-00-00',
  `fecha_fin` date NOT NULL default '0000-00-00',
  `foja_ini` int(11) NOT NULL default '0',
  `foja_fin` int(11) NOT NULL default '0',
  `observaciones` tinyblob NOT NULL,
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `ipe-007`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ipo-002`
-- 

CREATE TABLE `ipo-002` (
  `clave` bigint(20) NOT NULL auto_increment,
  `numero` varchar(50) NOT NULL default '',
  `descripcion` varchar(100) NOT NULL default '',
  `observaciones` tinyblob NOT NULL,
  `clave_area` varchar(30) NOT NULL default '',
  `fecha` date NOT NULL default '0000-00-00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=3 ;

-- 
-- Volcar la base de datos para la tabla `ipo-002`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ipo-003`
-- 

CREATE TABLE `ipo-003` (
  `clave` bigint(20) NOT NULL auto_increment,
  `clave_llave` varchar(50) NOT NULL default '',
  `corresponde` varchar(100) NOT NULL default '',
  `responsable` varchar(30) NOT NULL default '',
  `observaciones` tinyblob NOT NULL,
  `clave_area` varchar(30) NOT NULL default '',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=2 ;

-- 
-- Volcar la base de datos para la tabla `ipo-003`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ipo-004`
-- 

CREATE TABLE `ipo-004` (
  `clave` bigint(20) NOT NULL auto_increment,
  `numero` varchar(50) NOT NULL default '',
  `marca` varchar(50) NOT NULL default '',
  `modelo` varchar(50) NOT NULL default '',
  `color` varchar(50) NOT NULL default '',
  `serie` varchar(50) NOT NULL default '',
  `placas` varchar(30) NOT NULL default '',
  `uso` varchar(10) NOT NULL default '',
  `observaciones` tinyblob NOT NULL,
  `clave_area` varchar(30) NOT NULL default '',
  `ruta` varchar(50) NOT NULL default '',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=5 ;

-- 
-- Volcar la base de datos para la tabla `ipo-004`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `ipo-005`
-- 

CREATE TABLE `ipo-005` (
  `clave` bigint(20) NOT NULL auto_increment,
  `tipo` varchar(50) NOT NULL default '',
  `folio` varchar(50) NOT NULL default '',
  `clave_trab` varchar(30) NOT NULL default '',
  `observaciones` tinyblob NOT NULL,
  `clave_area` varchar(30) NOT NULL default '',
  `ruta` varchar(30) NOT NULL default '',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=5 ;

-- 
-- Volcar la base de datos para la tabla `ipo-005`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `irh-002`
-- 

CREATE TABLE `irh-002` (
  `clave` bigint(20) NOT NULL auto_increment,
  `clave_trab` varchar(30) NOT NULL default '',
  `original` varchar(50) NOT NULL default '',
  `comision` varchar(50) NOT NULL default '',
  `duracion` varchar(30) NOT NULL default '',
  `clave_area` varchar(30) NOT NULL default '',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `irh-002`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `irm-001`
-- 

CREATE TABLE `irm-001` (
  `clave` bigint(20) NOT NULL auto_increment,
  `numero` varchar(50) NOT NULL default '',
  `descripcion` varchar(100) NOT NULL default '',
  `unidad` varchar(30) NOT NULL default '',
  `existencia` float NOT NULL default '0',
  `clave_area` varchar(30) NOT NULL default '',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `irm-001`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `irm-002`
-- 

CREATE TABLE `irm-002` (
  `clave` bigint(20) NOT NULL auto_increment,
  `numero` varchar(50) NOT NULL default '',
  `descripcion` varchar(100) NOT NULL default '',
  `unidades` float NOT NULL default '0',
  `estado` varchar(10) NOT NULL default '',
  `observaciones` tinyblob NOT NULL,
  `clave_area` varchar(30) NOT NULL default '',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `irm-002`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `irm-003`
-- 

CREATE TABLE `irm-003` (
  `clave` bigint(20) NOT NULL auto_increment,
  `leyenda` varchar(50) NOT NULL default '',
  `observaciones` tinyblob NOT NULL,
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `irm-003`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `personal`
-- 

CREATE TABLE `personal` (
  `clave` varchar(20) NOT NULL default '',
  `nombre` varchar(150) NOT NULL default '',
  `rfc` varchar(30) NOT NULL default '',
  `departamento` varchar(50) NOT NULL default '',
  `categoria` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM;

-- 
-- Volcar la base de datos para la tabla `personal`
-- 

INSERT INTO `personal` VALUES ('PC1', 'JUAREZ SANTILLAN CELEDONIO', ' JUSC660303', '19', 'RADIO OPERADOR');
INSERT INTO `personal` VALUES ('PC2', 'PEREZ VILLEGAS GONZALO', ' PEVG790917', '19', 'AUXILAR DE COORDINADOR');
INSERT INTO `personal` VALUES ('PC4', 'GARCIA GUADARRAMA JORGE', ' GAGJ610114', '19', 'CHOFER DE AMBULANCIA');
INSERT INTO `personal` VALUES ('PC6', 'GARCIA MORALES ALMA', ' GAMA810101', '19', 'PARAMEDICO');
INSERT INTO `personal` VALUES ('PC7', 'VARGAS BALDERAS EDWIN GIBRAN', ' VABE', '19', 'BOMBERO');
INSERT INTO `personal` VALUES ('PC8', 'TERRAZAS MORALES JORGE', ' TEMJ581113', '19', 'CHOFER DE AMBULANCIA');
INSERT INTO `personal` VALUES ('PC10', 'RODRIGUEZ GALLEGOS JESUS', ' ROGJ661126', '19', 'JEFE DE SERVICIO');
INSERT INTO `personal` VALUES ('PC11', 'REYES CALERO JORGE', ' RECJ720423', '19', 'BOMBERO');
INSERT INTO `personal` VALUES ('SA1', 'BERNAL GARCIA GREGORIO', ' BEGG660331', '34', 'AGENTE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA2', 'HERRERA ZAMORA MANOLO', ' HEZM731012', '34', 'AGENTE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA4', 'MORAN GALAN ANTONIO', ' MOGA770109', '34', 'AGENTE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA7', 'VALENTIN PINEDA PEDRO', ' VAPP551023', '34', 'AGENTE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA8', 'PEREZ NAVA JOSE ADRIAN', ' PENA770621', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA9', 'PEREZ VILCHIS LUIS ANTONIO', ' PEVL650718', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA13', 'REYES MORALES CANDELARIA A.', ' REMC690202', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA14', 'PEREZ VELASCO MARIO', ' PEVM610119', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA16', 'ZACARIAS LOPEZ JESUS', ' ZALJ660914', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA17', 'REYNA MENDEZ ALEJANDRO', ' REMA760705', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA20', 'COCA SANCHEZ ISAAC', ' COSI760619', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA23', 'HERNANDEZ ZAMORA ROBERTO', ' HEZR660104', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA26', 'GARCIA ARREGUIN MARIANO    VALENTIN', ' GAAM690213', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA27', 'SANCHEZ HERNANDEZ RAFAEL', ' SAHR631202', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA28', 'VELAZQUEZ ISLAS MAXIMINO', ' VEIM710108', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA29', 'TAPIA OMA�A FERNANDO', ' TAOF720603', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA31', 'SANTILLAN VELAZQUEZ ROBERTO', ' SAVR', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA34', 'HERNANDEZ ORTIZ JULIO CESAR DEL CARMEN', ' HEOJ760714', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA37', 'ROMERO TREJO JAIME', ' ROTJ751006', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA40', 'FLORES LARA CIRO', ' FOLC720714', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA41', 'CRUZ RIVERA ALEJANDRO', ' CURA830707', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA44', 'GARCIA ROSAS CRUZ JAVIER', ' GARC820726', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA45', 'OLGUIN ORTIZ RAUL', ' OUOR790217', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA49', 'SALAZAR HERNANDEZ EDUARDO', ' SAHE751013', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA51', 'MEDINA GARCIA AMADO', ' MEGA741013', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA55', 'LOPEZ MENDOZA CARLOS', ' LOMC690414', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA56', 'HERNANDEZ JIMENEZ JORGE', ' HEJJ621115', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA57', 'NAJERA FLORES MELESIO', ' NAFM771204', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA58', 'BENITEZ PANIAGUA ERICK', ' BEPE810419', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA60', 'ALVAREZ SOLORZANO OSCAR', ' AASO760122', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA62', 'GUZMAN MOYA JULIO ALBERTO', ' GUMJ711115', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA64', 'SERRANO NAVA MOISES', ' SENM710512', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA66', 'GONZALEZ RAMOS FREDY', ' GORF831017', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA67', 'MARTINEZ ZU�IGA ANASTACIO', ' MAZA670723', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA68', 'SIERRA CASTILLO RAYMUNDO', ' SICR750106', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA69', 'HERNANDEZ SANCHEZ MACARIO', ' HESM721231', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA70', 'PEREZ TOVAR JAVIER', ' PETJ601120', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA73', 'PEREZ FLORES SALVADOR', ' PEFS680214', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA75', 'SANCHEZ RUIZ FELIX', ' SARF681220', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA76', 'MEDINA MORALES GUSTAVO', ' MEMG831004', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA77', 'GALINDO LOPEZ UBALDO', ' GALU750522', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA79', 'MONDRAGON FRANCO RICARDO', ' MOFR750817', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA80', 'VERGARA PARIENTE EDGARDO', ' VEPE690110', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA83', 'GONZALEZ GARFIAS DIVANY ELID', ' GOGD820414', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA84', 'FLORES ROMAN NOE', ' FORN681203', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA86', 'CRUZ ESPINOSA GUSTAVO ADOLFO', ' CUEG800818', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA87', 'BAZAN GUTIERREZ MARIO', ' BAGM650122', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA92', 'CRUZ MELENDEZ BEATRIZ', ' CUMB810528', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA97', 'ELEUTERIO CRUZ JOSE', ' EECJ801124', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA100', 'VALENCIA CRUZ SANDRA', ' VACS830602', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA101', 'GARCIA VAZQUEZ JOSE ANTONIO', ' GAVA620209', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA106', 'HERNANDEZ MENDOZA FRANCISCO MARIO', ' HEMF711004', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA108', 'AGUILAR LOPEZ JORGE ALBERTO', ' AULJ820419', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA109', 'MEDINA HERNANDEZ VIRGILIO', ' MEHV860717', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA111', 'RODRIGUEZ REYES VICTOR MANUEL', ' RORV710119', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA112', 'BARRAGAN MAGA�A RODOLFO', ' BAMR680220', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA113', 'MENDEZ MIGUEL ROBERTO', ' MEMR790321', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA114', 'ITURBE FERRER DIEGO ARMANDO', ' IUFD840915', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA115', 'CERON MU�OZ ISAAC', ' CEMI700602', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('SA116', 'MARTINEZ CASTRO SERGIO', ' MACS680506', '34', 'AGENTE DE SEGURIDAD');
INSERT INTO `personal` VALUES ('CS1', 'GARFIAS VELAZQUEZ LETICIA', ' GAVL740512', '7', 'AUXILIAR');
INSERT INTO `personal` VALUES ('CS3', 'GONZALEZ ROJAS ARLET GUADALUPE', ' GORA691026', '7', 'DIRECTORA');
INSERT INTO `personal` VALUES ('ED1', 'LEON AVILES CYNTHIA KARINA', ' LEAC820808', '13', 'DIRECTORA');
INSERT INTO `personal` VALUES ('PM1', 'HERNANDEZ VELAZQUEZ SILVIA YASMIN', ' HEVS820116', '18', 'RECEPCIONISTA');
INSERT INTO `personal` VALUES ('PM2', 'REYNA CORONA IGNACIO', ' RECI540702', '18', 'PRESIDENTE');
INSERT INTO `personal` VALUES ('PM3', 'HERNANDEZ RAMIREZ BERNARDO', ' HERB660211', '18', 'CHOFER');
INSERT INTO `personal` VALUES ('PM4', 'RAMIREZ GIL ILDEFONSO', ' RAGI410412', '18', 'CRONISTA');
INSERT INTO `personal` VALUES ('PM6', 'LUGO ALVAREZ LUIS', ' LUAL601007', '18', 'CENTRO DE PREVENCION  ATENCION CONTRA LAS ADICCION');
INSERT INTO `personal` VALUES ('PM8', 'SAAVEDRA HERNANDEZ LAURA', ' SAHL830602', '18', 'RECEPCIONISTA');
INSERT INTO `personal` VALUES ('PM9', 'MONTIEL OLVERA MARTIN', ' MOOM691128', '18', 'VIGILANTE DEL AUDITORIO');
INSERT INTO `personal` VALUES ('PM10', 'MIRANDA NOGUEZ MARIA DE LOS ANGELES', ' MINA820514', '18', 'TERAPEUTA');
INSERT INTO `personal` VALUES ('PM11', 'HERNANDEZ CERVANTES GUADALUPE BERENICE', ' HECG870824', '18', 'SECRETARIA');
INSERT INTO `personal` VALUES ('PM12', 'REYES MARTINEZ JUAN PABLO', ' REMJ790126', '18', 'FACILITADOR');
INSERT INTO `personal` VALUES ('OC1', 'ORTEGA ORTIZ OSCAR', ' OEOO721118', '17', 'OFICIAL CONCILIADOR');
INSERT INTO `personal` VALUES ('SM1', 'RAMIREZ LOPEZ GABRIEL', ' RALG711112', '37', 'ASISTENTE');
INSERT INTO `personal` VALUES ('SM2', 'GUERRERO HERNANDEZ MARIA GUADALUPE', ' GUHG860809', '37', 'SECRETARIA');
INSERT INTO `personal` VALUES ('SM3', 'HERNANDEZ ANGELES VALENTIN', ' HEAV690214', '37', 'SINDICO');
INSERT INTO `personal` VALUES ('SR1', 'CANO CORTEZ JAIME', ' CACJ510430', '21', 'PRIMER REGIDOR');
INSERT INTO `personal` VALUES ('SR2', 'REYES TERRAZAS MARIA JUANA', ' RETJ771016', '23', 'SEGUNDO REGIDOR');
INSERT INTO `personal` VALUES ('SR3', 'PEREZ MORALES JUAN ELEUTERIO', ' PEMJ600220', '24', 'TERCER REGIDOR');
INSERT INTO `personal` VALUES ('SR4', 'MARTINEZ GARCIA FELIPE', ' MAGF711221', '25', 'CUARTO REGIDOR');
INSERT INTO `personal` VALUES ('SR5', 'AGUILAR SANCHEZ MARIA LUISA', ' AUSL600621', '26', 'QUINTO REGIDOR');
INSERT INTO `personal` VALUES ('SR6', 'HERNANDEZ NARVAEZ ROBERTO', 'HENR640807', '27', 'SEXTO REGIDOR');
INSERT INTO `personal` VALUES ('SR7', 'PINEDA  YA�EZ DAVID', ' PIYD520626', '28', 'SEPTIMO REGIDOR');
INSERT INTO `personal` VALUES ('SR8', 'VELAZQUEZ LOPEZ ANDRES', ' VELA711210', '29', 'OCTAVO REGIDOR');
INSERT INTO `personal` VALUES ('SR9', 'ISLAS GODINEZ ABEL', ' IAGA391210', '30', 'NOVENO REGIDOR');
INSERT INTO `personal` VALUES ('SR10', 'OVIEDO RAMIREZ MARTHA PATRICIA', ' OIRM740829', '22', 'DECIMO REGIDOR');
INSERT INTO `personal` VALUES ('SR12', 'RAMIREZ SANCHEZ DORA ALICIA', ' RASD671009', '21', 'SECRETARIA');
INSERT INTO `personal` VALUES ('SR13', 'FIGUEROA GARCIA LILIANA', ' FIGL830603', '25', 'SECRETARIA');
INSERT INTO `personal` VALUES ('SR14', 'FLORES COLIN EVA', ' FOCE771123', '25', 'SECRETARIA');
INSERT INTO `personal` VALUES ('RC1', 'ORTIZ VILLEGAS MARIA ISABEL', ' OIVI810620', '31', 'DIRECTORA');
INSERT INTO `personal` VALUES ('RC2', 'LARA MARTINEZ DULCE GABRIELA', ' LAMD840326', '31', 'SECRETARIA');
INSERT INTO `personal` VALUES ('RC3', 'PEREZ AVILA SANDRA SUSANA', ' PEAS840916', '31', 'SECRETARIA');
INSERT INTO `personal` VALUES ('RC4', 'MATIAS HERNANDEZ ANGELICA', ' MAHA850623', '31', 'SECRETARIA');
INSERT INTO `personal` VALUES ('RC5', 'SANMIGUEL KU KARLA EDITH', ' SAKK831007', '31', 'SECRETARIA');
INSERT INTO `personal` VALUES ('SME1', 'FORTANEL HERRERA ELIZABETH', ' FOHE791109', '35', 'DIRECTORA');
INSERT INTO `personal` VALUES ('FD1', 'GARFIAS VELAZQUEZ ENRIQUE A', ' GAVE710907', '14', 'COORDINADOR');
INSERT INTO `personal` VALUES ('SAH1', 'SOLIS PANIAGUA JESUS ANTONIO', ' SOPJ630708', '33', 'AUXILIAR');
INSERT INTO `personal` VALUES ('SAH2', 'VILCHIS PE�ALOZA CESAR', ' VIPC770321', '33', 'COORDINADOR CURP');
INSERT INTO `personal` VALUES ('SAH3', 'CIRILO YA�EZ SANDRA', ' CIYS811124', '33', 'SECRETARIA');
INSERT INTO `personal` VALUES ('SAH4', 'LEON YA�EZ PROSPERO', ' LEYP580729', '33', 'SECRETARIO');
INSERT INTO `personal` VALUES ('SAH6', 'GONZALEZ CARBAJAL KARLA PAOLA', ' GOCK830903', '33', 'SECRETARIA');
INSERT INTO `personal` VALUES ('SAH8', 'RAMIREZ LOPEZ MARIO', ' RALM700407', '33', 'COORDINADOR EVENTOS');
INSERT INTO `personal` VALUES ('SAH9', 'AGUILAR GARCIA VICTORIA', ' AUGV730104', '33', 'SECRETARIA');
INSERT INTO `personal` VALUES ('SAH10', 'NARVAEZ ORTEGA ALEJANDRINA', ' NAOA851117', '33', 'SECRETARIA');
INSERT INTO `personal` VALUES ('SAH11', 'GONZALEZ MONTALVO JORGE', ' GOMJ720313', '33', 'AUXILIAR DE EVENTOS');
INSERT INTO `personal` VALUES ('SAH12', 'CABRERA PACHECO MARIA DEL CARMEN', ' CAPC580313', '33', 'ENC. FUNERARIA');
INSERT INTO `personal` VALUES ('RH1', 'BARRAGAN LEPE SALVADOR', ' BALS441226', '20', 'DIRECTOR');
INSERT INTO `personal` VALUES ('RH2', 'BENITEZ HERNANDEZ MIRAM G.', ' BEHM841124', '20', 'SECRETARIA');
INSERT INTO `personal` VALUES ('OP1', 'CANO MARTINEZ GILBERTO', ' CAMG650204', '16', 'CHOFER');
INSERT INTO `personal` VALUES ('OP2', 'CHIMAL COLIN MIGUEL', ' CICM520929', '16', 'SUPERVISOR');
INSERT INTO `personal` VALUES ('OP3', 'MARTINEZ MONTOYA RAYMUNDO', ' MAMR490125', '16', 'SUPERVISOR');
INSERT INTO `personal` VALUES ('OP4', 'REGINO CALZADA JUAN MANUEL', ' RECJ770824', '16', 'SUPERVISOR');
INSERT INTO `personal` VALUES ('OP6', 'MIRANDA PEREZ RAMON', ' MIPR750831', '16', 'COORDINADOR DE OBRA');
INSERT INTO `personal` VALUES ('OP7', 'COLIN VAZQUEZ MARCO ANTONIO', ' COVM721009', '16', 'DIRECTOR');
INSERT INTO `personal` VALUES ('OP8', 'VALENCIA BARRON MART�N', ' VABM661111', '16', 'FONTANERO');
INSERT INTO `personal` VALUES ('OP9', 'LOPEZ HERNANDEZ GUADALUPE', ' LOHG731210', '16', 'SECRETARIA');
INSERT INTO `personal` VALUES ('OP11', 'OLIVARES ESPINOZA JOSE MIGUEL', ' OIEM800109', '16', 'SUPERVISOR DE OBRA');
INSERT INTO `personal` VALUES ('OP12', 'GARFIAS PINEDA ALONSO', ' GAPA851229', '16', 'NOTIFICADOR');
INSERT INTO `personal` VALUES ('OP13', 'LEON YA�EZ APOLINAR', ' LEYA600108', '16', 'TECNICO DE RAMO 33');
INSERT INTO `personal` VALUES ('OP15', 'LOPEZ REYNA MARIA ELENA', ' LORE700915', '16', 'SECRETARIA');
INSERT INTO `personal` VALUES ('OP16', 'GUTIERREZ LADRILLERO FIDEL', ' GULF680224', '16', 'SUB-DIRECTOR');
INSERT INTO `personal` VALUES ('OP17', 'AGUILAR RUEDA ADAN', ' AURA-781010', '16', 'NOTIFICADOR');
INSERT INTO `personal` VALUES ('AP1', 'CASTILLO ALONSO JES�S', ' CAAJ661205', '2', 'COORDINADOR DE PIPA');
INSERT INTO `personal` VALUES ('AP2', 'GARFIAS PINEDA VICENTE', ' GAPV771026', '2', 'DIRECTOR');
INSERT INTO `personal` VALUES ('AP3', 'LUNA CAMACHO ROBERTO', ' LUCR671116', '2', 'SUB-DIRECTOR');
INSERT INTO `personal` VALUES ('AP4', 'MORALES ROJAS IRENE MIREYA', ' MOI641020', '2', 'SECRETARIA');
INSERT INTO `personal` VALUES ('EC1', 'RAMIREZ ZENOS MARIA GUADALUPE', ' RAZG811207', '12', 'DIRECTORA');
INSERT INTO `personal` VALUES ('EC2', 'PASTRANA CORONA CATALINA ASUSENA', ' PACC820429', '12', 'AUXILIAR');
INSERT INTO `personal` VALUES ('SP2', 'REYNA CASTILLO INOCENTE', ' RECI631228', '36', 'JEFE DE ALMACEN');
INSERT INTO `personal` VALUES ('SP3', 'ASTORGA L�PEZ E. CARLOS', ' AOLE510305', '36', 'JEFE DE SERVICIOS P�BLICOS');
INSERT INTO `personal` VALUES ('SP5', 'SUAREZ CONTRERAS MARTIN', ' SUCM750104', '36', 'MECANICO');
INSERT INTO `personal` VALUES ('DS1', 'ESCOBAR BARRERA ROBERTA', ' EOBR640607', '11', 'ASISTENTE');
INSERT INTO `personal` VALUES ('DS2', 'SALGADO MARTINEZ NORMA', ' SAMN700806', '11', 'ASISTENTE');
INSERT INTO `personal` VALUES ('DS3', 'BUENROSTRO ORTEGA NATIVIDAD', ' BUON790417', '11', 'DIRECTORA');
INSERT INTO `personal` VALUES ('CI1', 'TORRES LOPEZ ARACELI', ' TOLA750416', '8', 'CONTRALOR INTERNO');
INSERT INTO `personal` VALUES ('CI3', 'SILVESTRE CHAVEZ NICASIO', ' SICN601011', '8', 'ASISTENTE');
INSERT INTO `personal` VALUES ('CI4', 'SAAVEDRA PEREZ NIDIA', ' SAPN831015', '8', 'ASISTENTE');
INSERT INTO `personal` VALUES ('CI5', 'VALDEZ MARTINEZ ALEJANDRO', ' VAMA761011', '8', 'TITUAR DE UNIDAD DE INFORMACION');
INSERT INTO `personal` VALUES ('CI6', 'CRUZ GUTIERREZ MONICA MARIA DEL CARMEN', ' SUGM830504', '8', 'SECRETARIA');
INSERT INTO `personal` VALUES ('CI7', 'MARTINEZ CASTILLO NATALIA', '', '8', 'SECRETARIA');
INSERT INTO `personal` VALUES ('CI8', 'GARCIA CASARES CARLOS ALBERTO', ' GACC820413', '8', 'ENCARGADO DE PLANEACION');
INSERT INTO `personal` VALUES ('CT1', 'HERNANDEZ SANCHEZ ANTONIO DE JESUS', 'HESA850605', '6', 'AUXILIAR');
INSERT INTO `personal` VALUES ('CT2', 'LARA MARTINEZ GUSTAVO', ' LAMG800721', '6', 'JEFE DE CATASTRO');
INSERT INTO `personal` VALUES ('CT3', 'RICA�O CASTILLO MARTHA', ' RICM850223', '6', 'AUXILIAR');
INSERT INTO `personal` VALUES ('RG1', 'GALINDO OLVERA ALBERTO', ' GAOA691001', '32', 'COORDINADOR');
INSERT INTO `personal` VALUES ('RG2', 'HERNANDEZ CALZADILLA MARIA DE LOS ANGELES', ' HECA840804', '32', 'SECRETARIA');
INSERT INTO `personal` VALUES ('RG3', 'GONZALEZ SANTILLAN VICENTE', ' GOSV720814', '32', 'INSPECTOR');
INSERT INTO `personal` VALUES ('RG4', 'CRUZ VAZQUEZ GERARDO', ' CUVG700727', '32', 'INSPECTOR');
INSERT INTO `personal` VALUES ('TM1', 'TERRAZAS MORALES MARIA ISABEL', ' TEMI720813', '38', 'AUXILIAR DE INGRESOS');
INSERT INTO `personal` VALUES ('TM2', 'ZURITA CASTRO LIDIA', ' ZUCL860609', '38', 'CAJERA');
INSERT INTO `personal` VALUES ('TM3', 'SANCHEZ VELAZQUEZ JUAN ALBERTO', ' SAVJ820308', '38', 'CAJERO');
INSERT INTO `personal` VALUES ('TM4', 'REYES GUERRERO ERIC', 'REGE810227', '38', 'AUX');
INSERT INTO `personal` VALUES ('TM5', 'HERNANDEZ LOPEZ HUMBERTO ALEJANDRO', ' HELH730917', '38', 'CAJERO');
INSERT INTO `personal` VALUES ('TM6', 'MENDOZA CHORE�O NELLY', ' MECN790220', '38', 'ENC. DE CTAS. POR PAGAR');
INSERT INTO `personal` VALUES ('TM7', 'FUERTE GUTIERREZ ADRIANA', ' FUGA820916', '38', 'SECRETARIA');
INSERT INTO `personal` VALUES ('TM8', 'FIESCO ROJAS PORFIRIO', ' FIRP650915', '38', 'CONTADOR GENERAL');
INSERT INTO `personal` VALUES ('TM9', 'SANCHEZ MENDOZA ALMA DELIA', ' SAMA801211', '38', 'JEFE EGRESOS');
INSERT INTO `personal` VALUES ('TM10', 'LOPEZ MORALES ANTONIA ISABEL', ' LOMA781024', '38', 'JEFE INGRESOS');
INSERT INTO `personal` VALUES ('TM11', 'LADINO SOLIS MA DEL CARMEN', ' LASC750916', '38', 'ENC. DE NOMINA');
INSERT INTO `personal` VALUES ('TM12', 'REYNA LOPEZ BLANCA JAQUELINE', ' RELB840401', '38', 'AUXILIAR');
INSERT INTO `personal` VALUES ('TM13', 'GONZALEZ CABRERA SELINA', ' GOCS790821', '38', 'AUXILIAR');
INSERT INTO `personal` VALUES ('TM16', 'ROSEY LEON MIRNA LUCERO', ' ROLM811110', '38', 'JURIDICO');
INSERT INTO `personal` VALUES ('TM17', 'PEREZ MENDOZA EFRAIN', ' -', '38', 'AUXILIAR');
INSERT INTO `personal` VALUES ('TM18', 'GONZALEZ CRUZ BEATRIZ ADRIANA', ' GOCB810814', '38', 'AUXILIAR');
INSERT INTO `personal` VALUES ('TM19', 'LADINO SOLIS JHOVANY E', ' LASJ840220', '38', 'AUXILIAR');
INSERT INTO `personal` VALUES ('TM20', 'COLIN HERNANDEZ HILDA', ' COHH871229', '38', 'AUXILIAR');
INSERT INTO `personal` VALUES ('TM21', 'RUIZ CAZARES DANTE ISMAEL', ' RUCD820926', '38', 'ASESOR');
INSERT INTO `personal` VALUES ('JG1', 'PINEDA GONZALEZ ARMANDO', ' PIGA790827', '15', 'DIRECTOR');
INSERT INTO `personal` VALUES ('JG2', 'MARTINEZ HERNANDEZ ANABEL', ' MAHA861009', '15', 'SECRETARIA');
INSERT INTO `personal` VALUES ('JG3', 'REYES MORENO ADRIAN', ' REMA650205', '15', 'LIC. PENALISTA');
INSERT INTO `personal` VALUES ('DA1', 'REYES COLIN ROBERTO BELARMINO', ' RECB620513', '10', 'COORDINADOR');
INSERT INTO `personal` VALUES ('DA2', 'CORTES ALVAREZ CUAUHTEMOC', ' COAC801214', '10', 'AUXILIAR');
INSERT INTO `personal` VALUES ('SE1', 'CASTRO CRUZ CLAUDIA', ' CACC710302', '34', 'SECRETARIA');
INSERT INTO `personal` VALUES ('SE2', 'PEREZ JIMENEZ EDILBERTO', ' PEJE660224', '34', 'JEFE DE TURNO');
INSERT INTO `personal` VALUES ('SE5', 'ISLAS RAFAEL JOSE', ' IARJ691205', '34', 'JEFE DE COORDINACI�N');
INSERT INTO `personal` VALUES ('SE6', 'CRUZ MAGALLON AMANDO', ' CUMA620418', '34', 'JEFE DE TURNO');
INSERT INTO `personal` VALUES ('CC1', 'VELASCO TORRES MARICELA', ' VETM710116', '5', 'AUXILIAR');
INSERT INTO `personal` VALUES ('CC2', 'HERNANDEZ AGUILAR MARTIN', ' HEAM640906', '5', 'INSTRUCTOR');
INSERT INTO `personal` VALUES ('CC3', 'OROZCO RINCON INDIRA YADIRA', ' OORI840117', '5', 'INSTRUCTOR');
INSERT INTO `personal` VALUES ('CC4', 'GUTIERREZ GARCIA ALMA EVELIA', ' GUGA730118', '5', 'PROF. DE DANZA');
INSERT INTO `personal` VALUES ('CC5', 'QUIJANO JACINTO JOSE FAUSTINO', ' QUJF770215', '5', 'INSTRUCTOR');
INSERT INTO `personal` VALUES ('CC6', 'VELAZQUEZ PEREZ JOSE LUIS', ' VEPL800822', '5', 'COORDINADOR');
INSERT INTO `personal` VALUES ('AD1', 'PEREZ CASTILLO JOSE ANTONIO', 'PECA520613', '1', 'CARTERO');
INSERT INTO `personal` VALUES ('AD2', 'ALVARADO GARCIA SOLEDAD', 'AAGS520814', '1', 'INTENDENCIA');
INSERT INTO `personal` VALUES ('AD3', 'CANO CORTEZ INES', 'CACI560302', '1', 'INTENDENCIA');
INSERT INTO `personal` VALUES ('AD4', 'GUTIERREZ MARQUEZ MARIA DEL CARMEN', 'GUMC510403', '1', 'INTENDENCIA');
INSERT INTO `personal` VALUES ('AD5', 'MONZON RODRIGUEZ SARA', 'MORS470506', '1', 'INTENDENCIA');
INSERT INTO `personal` VALUES ('AD7', 'PADILLA CONTRERAS GUADALUPE', 'PACG550608', '1', 'INTENDENCIA');
INSERT INTO `personal` VALUES ('AD8', 'TORRES FLORES ANTONIA', 'TOFA530526', '1', 'INTENDENCIA');
INSERT INTO `personal` VALUES ('AD9', 'ALEGRIA HERNANDEZ GERARDO', 'AEHG371003', '1', 'PANTEONERO');
INSERT INTO `personal` VALUES ('AD12', 'VARGAS ZAVALA J. GUADALUPE JAVIER', 'VAZG691222', '1', 'NOTIFICADOR');
INSERT INTO `personal` VALUES ('AD13', 'VALERIO VAZQUEZ MARIA ESTHER', 'VAVE700913', '1', 'INTENDENCIA');
INSERT INTO `personal` VALUES ('AD14', 'TALAVERA DUARTE MA. DOLORES', 'TADD550702', '1', 'INTENDENCIA');
INSERT INTO `personal` VALUES ('AD15', 'ALONSO SALINAS ANA GUADALUPE', 'AOSA700516', '1', 'INTENDENCIA');
INSERT INTO `personal` VALUES ('AD17', 'BARAJAS MORA MA. DE LA CRUZ', 'BAMC540508', '1', 'INTENDENCIA');
INSERT INTO `personal` VALUES ('AD18', 'ALFARO AGUILAR ANA MARIA', 'AAAA550511', '1', 'INTENDENCIA');
INSERT INTO `personal` VALUES ('AP5', 'JUAREZ SOTO HIGINIO', 'JUSH600111', '2', 'FONTANERO');
INSERT INTO `personal` VALUES ('AP6', 'MARQUEZ UGALDE DELFINO', 'MAUD681224', '2', 'FONTANERO');
INSERT INTO `personal` VALUES ('AP7', 'MARTINEZ DUQUE SEBASTIAN', 'MADS540119', '2', 'FONTANERO');
INSERT INTO `personal` VALUES ('AP8', 'MARTINEZ GALLEGOS JAVIER', 'MAGJ671020', '2', 'FONTANERO');
INSERT INTO `personal` VALUES ('AP9', 'MARTINEZ GALLEGOS RAUL', 'MAGR681217', '2', 'FONTANERO');
INSERT INTO `personal` VALUES ('AP11', 'MORALES GONZALEZ DIEGO', 'MOGD471028', '2', 'FONTANERO');
INSERT INTO `personal` VALUES ('AP12', 'SANTILLAN TOVAR ELEAZAR', 'SATE700823', '2', 'FONTANERO');
INSERT INTO `personal` VALUES ('AP14', 'SOTO ESCOBEDO MIGUEL', 'SOEM430929', '2', 'FONTANERO');
INSERT INTO `personal` VALUES ('AP15', 'VELASCO SALINAS CESAR', 'VESC740801', '2', 'FONTANERO');
INSERT INTO `personal` VALUES ('AP17', 'MORALES SOTO CARLOS', 'MOXC511010', '2', 'POZERO');
INSERT INTO `personal` VALUES ('AP20', 'ALBA ORTIZ LUCIO ARTURO', 'AAOL561215', '2', 'REGULADOR DE TANQUE');
INSERT INTO `personal` VALUES ('AP21', 'ANAYA GONZALEZ GUADALUPE', 'AAGG331202', '2', 'REGULADOR DE TANQUE');
INSERT INTO `personal` VALUES ('AP22', 'ARENAS IBARRA ROBERTO', 'AEIR490429', '2', 'REGULADOR DE TANQUE');
INSERT INTO `personal` VALUES ('AP23', 'ESTRADA ORDOZ JULIAN', 'EAOJ810130', '2', 'FONTANERO');
INSERT INTO `personal` VALUES ('AP24', 'GARRIDO HERNANDEZ PRISCILIANO', 'GAHP400330', '2', 'REGULADOR DE TANQUE');
INSERT INTO `personal` VALUES ('AP26', 'HERNANDEZ DAMIAN JOSE', 'HEDJ261217', '2', 'REGULADOR DE TANQUE');
INSERT INTO `personal` VALUES ('AP27', 'MEDINA LUZ SALVADOR', 'MELS420923', '2', 'REGULADOR DE TANQUE');
INSERT INTO `personal` VALUES ('AP29', 'ALBA SIFUENTES ANTONIO ARTURO', '', '2', '');
INSERT INTO `personal` VALUES ('AP30', 'REYES GUERRA FRANCISCO', 'REGF610129', '2', 'POZERO');
INSERT INTO `personal` VALUES ('AP32', 'HERNANDEZ FUENTES GONZALO', ' HEFG730928', '2', 'AYUDANTE PLANTA SAFE JET VAC');
INSERT INTO `personal` VALUES ('AP33', 'VILLEGAS MALDONADO NOE', ' VIMN701110', '2', 'AYUDANTE DE PIPA');
INSERT INTO `personal` VALUES ('AP34', 'AREVALO BENITEZ LUIS ANSELMO', ' AEBL530421', '2', 'AYUDANTE PLANTA TRATADORA');
INSERT INTO `personal` VALUES ('AP35', 'CRUZ RAMIREZ ISIDORO', 'CURI570404', '2', 'ENC. PLANTA STA. TERESA');
INSERT INTO `personal` VALUES ('AP37', 'GARCIA LEZAMA RODOLFO', ' GALR560515', '2', 'POZERO');
INSERT INTO `personal` VALUES ('AP38', 'HERNANDEZ VILLEGAS MARIO', 'HEVM540119', '2', 'REGULADOR DE VALVULAS');
INSERT INTO `personal` VALUES ('AP40', 'PORTILLO BOCANEGRA JAVIER', 'POBJ780820', '2', 'FONTANERO');
INSERT INTO `personal` VALUES ('AP41', 'MARTINEZ ALVAREZ NABOR', 'MAAN600712', '2', 'AYUDANTE');
INSERT INTO `personal` VALUES ('AP44', 'OROPEZA LUNA CARLOS ANDRES', 'OOLC841119', '2', '');
INSERT INTO `personal` VALUES ('AP45', 'ALBA SIFUENTES DANIEL', 'AASD861121', '2', '');
INSERT INTO `personal` VALUES ('AP47', 'VEGA LEON PABLO', ' VELP560517', '2', 'POZERO');
INSERT INTO `personal` VALUES ('AP48', 'BARRON JUAREZ PONCEANO', 'BAJP591119', '2', 'OP. RETROEXCAVADORA');
INSERT INTO `personal` VALUES ('AP49', 'PEREZ CETINA FAUSTINO', 'PECF721028', '2', 'FONTANERO');
INSERT INTO `personal` VALUES ('AP50', 'ARIAS MARTINEZ GERARDO', 'AIMG791003', '2', 'AYUDANTE');
INSERT INTO `personal` VALUES ('AP52', 'BOTELLO REYES MARGARITO', 'BORM640816', '2', 'AUXILIAR PLANTA TRAT');
INSERT INTO `personal` VALUES ('DE1', 'PAREDES TREJO AGUSTIN', 'PATA510828', '9', 'INSTRUCTOR');
INSERT INTO `personal` VALUES ('EC3', 'VELAZQUEZ MENDOZA J. GUADALUPE', 'VEMG521208', '2', 'CHOFER DE PIPA');
INSERT INTO `personal` VALUES ('EC4', 'VAZQUEZ ALCANTARA PABLO', 'VAAP770428', '12', 'CHOFER');
INSERT INTO `personal` VALUES ('EC5', 'LOPEZ ALCANTARA GABRIEL', '', '12', 'AYUDANTE');
INSERT INTO `personal` VALUES ('EC6', 'RODRIGUEZ PEREZ JERONIMO', 'ROPJ570930', '12', 'ENCARG. DE PARQUE');
INSERT INTO `personal` VALUES ('EC7', 'SOTO ESCOBEDO BERNARDINO', 'SOEB400520', '2', 'FONTANERO');
INSERT INTO `personal` VALUES ('EC8', 'FRANCO GUTIERREZ ADOLFO', 'FAGA320927', '12', 'JARDINERO');
INSERT INTO `personal` VALUES ('BL1', 'ISLAS TERRAZAS CARMEN', ' IATC790630', '3', 'BIBLIOTECARIA');
INSERT INTO `personal` VALUES ('BL2', 'CANO LOPEZ MARLEN', ' CALM750313', '4', 'BIBLIOTECARIA');
INSERT INTO `personal` VALUES ('BL4', 'RAMIREZ HERNANDEZ GLORIA', ' RAHG760609', '3', 'BIBLIOTECARIA');
INSERT INTO `personal` VALUES ('OP5', 'MORALES ARIAS CARLOS', 'MOAC751104', '16', 'ALBA�IL');
INSERT INTO `personal` VALUES ('OP10', 'REYNA HERNANDEZ ROGELIO RUBEN', 'REHR550407', '16', 'ALBA�IL');
INSERT INTO `personal` VALUES ('OP14', 'TERRAZAS REYNA JESUS HUMBERTO', 'TERH710309', '16', 'ALBA�IL');
INSERT INTO `personal` VALUES ('OP18', 'CRESPO MARTINEZ CANDELARIO', 'CEMC480101', '16', 'AYUDANTE EN GRAL.');
INSERT INTO `personal` VALUES ('OP19', 'PACHECO PADILLA JOSE MARIA CARMELO', 'PAPC461026', '16', 'BARRENDERO');
INSERT INTO `personal` VALUES ('OP20', 'MORALES MELCHOR ALBERTO', 'MOMA640408', '16', 'OFICIAL');
INSERT INTO `personal` VALUES ('OP21', 'LOPEZ HERNANDEZ LUCIANO', 'LOHL380107', '16', 'ELECTRICISTA');
INSERT INTO `personal` VALUES ('OP22', 'PEREZ ALEGRIA PATRICIO', 'PEAP560317', '16', 'AYUDANTE EN GRAL.');
INSERT INTO `personal` VALUES ('OP25', 'ARIAS SANTILLAN VICTOR', 'AISV490728', '16', 'OFICIAL DE ALBA�ILERIA');
INSERT INTO `personal` VALUES ('OP26', 'FLORES ABREGO CRISTOBAL FEDERICO', 'FOAC690301', '16', 'OFICIAL DE ALBA�ILERIA');
INSERT INTO `personal` VALUES ('OP28', 'PEREZ TORRES AURELIO ARTURO', 'PETA590925', '16', 'OFICIAL DE ALBA�ILERIA');
INSERT INTO `personal` VALUES ('OP29', 'ROMERO GUEVARA RAMIRO', 'ROGR710930', '16', 'OFICIAL DE ALBA�ILERIA');
INSERT INTO `personal` VALUES ('OP32', 'SANTILLAN ORTEGA TOMAS', 'SAOT631221', '16', 'OP. DE MOTOCONFORMADORA');
INSERT INTO `personal` VALUES ('OP35', 'REYNA REYNA PANFILO', 'RERP230601', '16', 'VELADOR');
INSERT INTO `personal` VALUES ('OP36', 'SOTO LOPEZ AMANDO', 'SOLA410126', '16', 'VELADOR');
INSERT INTO `personal` VALUES ('OP37', 'TORRES HERNANDEZ RAYMUNDO', 'TOHR541005', '16', 'PLOMERO');
INSERT INTO `personal` VALUES ('OP38', 'DELGADO PAREDES JOSE LUIS', 'DEPL740731', '16', 'OFICIAL');
INSERT INTO `personal` VALUES ('OP40', 'ESCAMILLA PEREZ CESAR', 'EAPC780215', '16', 'OP. RETROEXCAVADORA');
INSERT INTO `personal` VALUES ('OP41', 'ESCAMILLA GARCIA JORGE', 'EAGG550423', '16', 'OP. MOTOCONFORMADORA');
INSERT INTO `personal` VALUES ('OP42', 'VALENCIA BARRON DANIEL', 'VABD680710', '16', 'OFICIAL DE ALBA�ILERIA');
INSERT INTO `personal` VALUES ('OP43', 'PEREZ PEREZ JAVIER SALVADOR', 'PEPJ691017', '16', 'OFICIAL DE ALBA�ILERIA');
INSERT INTO `personal` VALUES ('OP44', 'ALEGRIA GODINEZ DAVID', 'AED651229', '16', 'PANTEONERO');
INSERT INTO `personal` VALUES ('OP45', 'CIRILO GONZALEZ ELEUTERIO', 'CIGE550220', '16', 'OPERADOR');
INSERT INTO `personal` VALUES ('OP46', 'REYNA CASTILLO JOSE GUADALUPE', 'RECG520317', '16', 'OFICIAL DE ALBA�IL');
INSERT INTO `personal` VALUES ('OP47', 'GUTIERREZ ROMERO ERASMO', 'GURE830522', '16', 'OFICIAL DE ALBA�IL');
INSERT INTO `personal` VALUES ('SA3', 'SOLIS VELAZQUEZ RICARDO', ' SOVR590403', '33', 'AUX. DE EVENTOS');
INSERT INTO `personal` VALUES ('SP1', 'GUTIERREZ MENDOZA BERNABE', 'GUMB670611', '36', 'ALBA�IL');
INSERT INTO `personal` VALUES ('SP4', 'GARCIA GODINEZ JOSE DE JESUS', 'GAGJ691013', '36', 'AYUDANTE DE ALBA�IL');
INSERT INTO `personal` VALUES ('SP6', 'GARCIA HERNANDEZ JOSE CRUZ', 'GAHC660503', '36', 'AYUDANTE EN GRAL.');
INSERT INTO `personal` VALUES ('SP8', 'RIOS RAMIREZ EFRAIN', 'RIRE491230', '36', 'ELECTRICISTA');
INSERT INTO `personal` VALUES ('SP9', 'ZAVALA RAMIREZ JAVIER', 'ZARJ560512', '36', 'CHOFER');
INSERT INTO `personal` VALUES ('SP10', 'FLORES ABREGO JUAN ADRIAN', 'FOAJ570901', '36', 'AYUDANTE GRAL.');
INSERT INTO `personal` VALUES ('SP13', 'CHIMAL CANO BENJAMIN', 'CICB350407', '36', 'BARRENDERO');
INSERT INTO `personal` VALUES ('SP14', 'FLORES CORONA FILEMON', 'FOCF640604', '36', 'BARRENDERO');
INSERT INTO `personal` VALUES ('SP15', 'GARCIA GONZALEZ ALFONSO', 'GAGA331120', '36', 'BARRENDERO');
INSERT INTO `personal` VALUES ('SP16', 'HERRERA YA�EZ JUAN', 'HEYJ710719', '36', 'BARRENDERO');
INSERT INTO `personal` VALUES ('SP17', 'JUAREZ VALENCIA PATRICIO', 'JUVP350317', '36', 'BARRENDERO');
INSERT INTO `personal` VALUES ('SP18', 'MORALES LOPEZ EMILIANO', 'MOLE510720', '36', 'BARRENDERO');
INSERT INTO `personal` VALUES ('SP20', 'SOTO MARTINEZ JUAN', 'SOMJ400626', '36', 'BARRENDERO');
INSERT INTO `personal` VALUES ('SP21', 'SOTO MARTINEZ LORENZO', 'SOML450817', '36', 'BARRENDERO');
INSERT INTO `personal` VALUES ('SP22', 'SOTO MARTINEZ VICENTE', 'SOMV350920', '36', 'BARRENDERO');
INSERT INTO `personal` VALUES ('SP24', 'CHAVEZ GUERRERO AARON', 'CAGA720701', '36', 'CHOFER');
INSERT INTO `personal` VALUES ('SP25', 'FLORES LOPEZ ESTEBAN', 'FOLE641227', '36', 'CHOFER');
INSERT INTO `personal` VALUES ('SP28', 'LEON YA�EZ JOSE', 'LEYJ670604', '36', 'CHOFER');
INSERT INTO `personal` VALUES ('SP29', 'NICOLAS CASTA�EDA ALBERTO FEDERICO', 'NICA700611', '36', 'CHOFER');
INSERT INTO `personal` VALUES ('SP30', 'REYNA REYNA IGNACIO', 'RERI430729', '36', 'CHOFER');
INSERT INTO `personal` VALUES ('SP31', 'SOTO VALDES RAUL CLEMENTE', 'SOVC611123', '36', 'CHOFER');
INSERT INTO `personal` VALUES ('SP32', 'TERRAZAS MIRANDA PEDRO', 'TEMP590220', '36', 'CHOFER');
INSERT INTO `personal` VALUES ('SP33', 'YA�EZ DIAZ JUAN', 'YADJ831109', '36', 'CHOFER');
INSERT INTO `personal` VALUES ('SP35', 'MORALES PEREZ PEDRO', 'MOPP420118', '36', 'LIMPIA');
INSERT INTO `personal` VALUES ('SP36', 'CRUCES QUINTERO JESUS', 'CUQJ690103', '36', 'MECANICO');
INSERT INTO `personal` VALUES ('SP37', 'VARGAS LOPEZ ALEJANDRO', 'VALA690421', '36', 'MECANICO');
INSERT INTO `personal` VALUES ('SP38', 'MENDEZ RAMIREZ TOMAS', 'MERT520307', '36', 'VELADOR');
INSERT INTO `personal` VALUES ('SP39', 'MAZA MORALES ELPIDIO', 'MAME491116', '36', 'BARRENDERO');
INSERT INTO `personal` VALUES ('SP40', 'BARRON BASURTO ANA', 'BABA660726', '36', 'BARRENDERAS');
INSERT INTO `personal` VALUES ('SP43', 'CORONA SANDOVAL CONCEPCION', 'COSC581208', '36', 'BARRENDERA');
INSERT INTO `personal` VALUES ('SP44', 'MARTINEZ GALLEGOS ARTURO', 'MAGA770421', '36', 'AYUDANTE EN GRAL.');
INSERT INTO `personal` VALUES ('SP45', 'VELASQUEZ MORALES JAVIER', 'VEMJ680405', '36', 'OP. SAFE JET VAC');
INSERT INTO `personal` VALUES ('SP49', 'SANTILLAN HERNANDEZ CLEMENTE', 'SAHC511123', '36', 'ELECTRICISTA');
INSERT INTO `personal` VALUES ('SP50', 'LOPEZ ORTIZ GUADALUPE', 'LOOG461212', '36', 'INTENDENCIA');
INSERT INTO `personal` VALUES ('SP51', 'MARTINEZ VILLEGAS MELINA', 'MAVM760101', '36', 'BARRENDERA');
INSERT INTO `personal` VALUES ('SP53', 'TORRES CARRANZA PABLO', 'TOCP770629', '36', 'BARRENDERO');
INSERT INTO `personal` VALUES ('SP54', 'FLORES CALZADILLA ROBERTO CARLOS', 'FOCR710406', '36', 'BARRENDERO');
INSERT INTO `personal` VALUES ('SP55', 'HERNANDEZ RAMIREZ GERARDO', 'HERG831003', '36', 'CHOFER');
INSERT INTO `personal` VALUES ('SP56', 'GODINEZ MONTOYA EUSEBIO', 'GOME541216', '36', 'CHOFER');
INSERT INTO `personal` VALUES ('SP57', 'GONZALEZ GASPAR TERESA', 'GOGT751201', '36', 'BARRENDERA');
INSERT INTO `personal` VALUES ('SP58', 'MATIAS HERNANDEZ ANGELICA', 'MAHA850623', '36', 'BARRENDERA');
INSERT INTO `personal` VALUES ('SP59', 'SALCEDO CAMACHO JOSE DE JESUS', 'SACJ621222', '36', 'BARRENDERO');
INSERT INTO `personal` VALUES ('SP60', 'GARCIA CHORE�O LEONEL', 'GACL740309', '36', 'ELECTRICISTA');
INSERT INTO `personal` VALUES ('SP61', 'RODRIGUEZ CARRILLO ROBERTO', 'ROCR660822', '36', 'CHOFER');
INSERT INTO `personal` VALUES ('SP62', 'HERNANDEZ PEREZ JESUS', 'HEPJ460806', '36', 'VELADOR');
INSERT INTO `personal` VALUES ('SP63', 'JUAREZ GOMEZ LUCIO', 'JUGL211205', '36', 'COORD. DE BARRENDEROS');
INSERT INTO `personal` VALUES ('SP64', 'RANGEL BERMEO AGUSTIN', 'RABA500629', '36', 'BARRENDERO');
INSERT INTO `personal` VALUES ('SP65', 'GARFIAS COLIN ROSALBA', 'GACR630904', '36', 'BARRENDERA');
INSERT INTO `personal` VALUES ('SP66', 'CORONA CARRANZA PEDRO', 'COCP800205', '36', 'CHOFER');
INSERT INTO `personal` VALUES ('SP67', 'HERRERA AVILA JULIO CESAR', 'HEAJ740207', '36', 'CHOFER');
INSERT INTO `personal` VALUES ('SP68', 'MONROY VAZQUEZ LEONEL', 'MOVL680709', '36', 'AYUDANTE');
INSERT INTO `personal` VALUES ('SP69', 'GLORIA VELASQUEZ JULIO CESAR', 'GOVM880213', '36', 'AYUDANTE');
INSERT INTO `personal` VALUES ('SP70', 'GUERRERO MORALES ALBERTO', 'GUMA660408', '36', 'CHOFER');
INSERT INTO `personal` VALUES ('SP71', 'REYNA RODRIGUEZ EFRAIN RAFAEL', '25/07/05', '36', 'BARRENDERO');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `tipo_formato`
-- 

CREATE TABLE `tipo_formato` (
  `clave` varchar(10) NOT NULL default '',
  `nombre` varchar(60) NOT NULL default '',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM;

-- 
-- Volcar la base de datos para la tabla `tipo_formato`
-- 

INSERT INTO `tipo_formato` VALUES ('ICO', 'INFORMACI�N CONTABLE', '2006-02-01 00:00:00');
INSERT INTO `tipo_formato` VALUES ('IFF', 'INFORMACI�N FISCAL Y FINANCIERA', '0000-00-00 00:00:00');
INSERT INTO `tipo_formato` VALUES ('ICA', 'INFORMACI�N CATASTRAL', '2006-02-01 00:00:00');
INSERT INTO `tipo_formato` VALUES ('IOP', 'INFORMACI�N DE OBRAS P�BLICAS', '2006-02-01 00:00:00');
INSERT INTO `tipo_formato` VALUES ('IPE', 'INFORMACI�N PATRIMONIAL DE LA ENTIDAD MUNICIPAL', '2006-02-01 00:00:00');
INSERT INTO `tipo_formato` VALUES ('IAD', 'INFORMACI�N ADICIONAL', '2006-02-01 00:00:00');
INSERT INTO `tipo_formato` VALUES ('IDO', 'INFORMACI�N DOCUMENTAL', '2006-02-01 00:00:00');
INSERT INTO `tipo_formato` VALUES ('IRH', 'INFORMACI�N DE RECURSOS HUMANOS', '2006-02-01 00:00:00');
INSERT INTO `tipo_formato` VALUES ('IRM', 'INFORMACI�N DE RECURSOS MATERIALES', '2006-02-01 00:00:00');
INSERT INTO `tipo_formato` VALUES ('IPO', 'INFORMACI�N PATRIMONIAL DE LA OFICINA QUE SE ENTREGA', '2006-02-01 00:00:00');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `trabajador`
-- 

CREATE TABLE `trabajador` (
  `clave` int(11) NOT NULL auto_increment,
  `numero` varchar(30) NOT NULL default '',
  `rfc` varchar(20) NOT NULL default '',
  `curp` varchar(30) NOT NULL default '',
  `ape_pat` varchar(50) NOT NULL default '',
  `ape_mat` varchar(50) NOT NULL default '',
  `nombre` varchar(50) NOT NULL default '',
  `nombre_comp` varchar(150) NOT NULL default '',
  `sexo` varchar(5) NOT NULL default '',
  `calle` varchar(50) NOT NULL default '',
  `colonia` varchar(30) NOT NULL default '',
  `municipio` varchar(30) NOT NULL default '',
  `estado` varchar(50) NOT NULL default '',
  `tel1` varchar(20) NOT NULL default '',
  `tel2` varchar(20) NOT NULL default '',
  `mail` varchar(20) NOT NULL default '',
  `cargo` varchar(50) NOT NULL default '',
  `gdo_estudios` varchar(50) NOT NULL default '',
  `doc_acredita` varchar(50) NOT NULL default '',
  `dia_nace` int(11) NOT NULL default '0',
  `mes_nace` int(11) NOT NULL default '0',
  `year_nace` int(11) NOT NULL default '0',
  `mes_servicio` int(11) NOT NULL default '0',
  `year_servicio` int(11) NOT NULL default '0',
  `area` int(11) NOT NULL default '0',
  `ruta` varchar(100) NOT NULL default '',
  `edo_civil` varchar(20) NOT NULL default '',
  `edad` int(11) NOT NULL default '0',
  `fecha_alta` datetime NOT NULL default '0000-00-00 00:00:00',
  `fecha_mod` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(15) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `trabajador`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `usuario`
-- 

CREATE TABLE `usuario` (
  `clave` int(11) NOT NULL auto_increment,
  `usuario` varchar(15) NOT NULL default '',
  `passwd` varchar(35) NOT NULL default '',
  `nombre_comp` varchar(30) NOT NULL default '',
  `puesto` varchar(50) NOT NULL default '',
  `clave_area` int(11) NOT NULL default '0',
  `rh` int(11) NOT NULL default '0',
  `admin_rh` int(11) NOT NULL default '0',
  `captura_rh` int(11) NOT NULL default '0',
  `modifica_rh` int(11) NOT NULL default '0',
  `lectura_rh` int(11) NOT NULL default '0',
  `bp` int(11) NOT NULL default '0',
  `admin_bp` int(11) NOT NULL default '0',
  `captura_bp` int(11) NOT NULL default '0',
  `modifica_bp` int(11) NOT NULL default '0',
  `lectura_bp` int(11) NOT NULL default '0',
  `er` int(11) NOT NULL default '0',
  `admin_er` int(11) NOT NULL default '0',
  `captura_er` int(11) NOT NULL default '0',
  `modifica_er` int(11) NOT NULL default '0',
  `genera_er` int(11) NOT NULL default '0',
  `acta_er` int(11) NOT NULL default '0',
  `ver_er` int(11) NOT NULL default '0',
  `admin` int(11) NOT NULL default '0',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=6 ;

-- 
-- Volcar la base de datos para la tabla `usuario`
-- 

INSERT INTO `usuario` VALUES (1, 'administrador', 'e80367561ef7bf8928b987527f63d0c4', '', '', 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '0000-00-00 00:00:00');
