<?php
define('FPDF_FONTPATH','font/');
require('fpdf.php');

class PDF_MC_Table extends FPDF
{
var $widths;
var $aligns;
var $encabezado;
var $alinea;

function Header()
{
global $title;
global $area;
global $ayuntam;

//global $encabezado;
    //Logo
	//$this->SetTopMargin(2);
   // $this->Image('../../../images/azul2.jpg',10,10,20);
    //Arial bold 15
    /*$this->SetFont('Arial','B',10);
    //Movernos a la derecha
  //  $this->Image('../../../images/image002.jpg', 165, 8, 20);
	$this->SetY(11);
	$this->Cell(25);*/
	$this->Cell(75,1,'P�LIZA DE SERVICIOS M�DICOS',0,0,'L');
	$this->Ln(3.5);
	/*$this->Cell(80);
    $this->Cell(75,1,'�RGANO SUPERIOR DE FISCALIZACI�N DEL',0,0,'L');
	$this->Ln(3.5);
	$this->Cell(25);
	$this->Cell(75,1,'DE ' .$ayuntam .', ESTADO DE M�XICO',0,0,'L');
	$this->Cell(80);
	$this->Cell(75,1,'ESTADO DE M�XICO',0,0,'L');
	$this->Ln(3.5);
	$this->Cell(25);
	$this->Cell(75,1,'XXXX-XXXX',0,0,'L');
	$this->Cell(80);
	$this->Cell(75,1,'ENTREGA-RECEPCI�N DE LA ADMINISTRACI�N ',0,0,'L');
	$this->Ln(3.5);
	$this->Cell(180);
	$this->Cell(75,1,'P�BLICA MUNICIPAL',0,0,'L');
	$this->Ln(3.5);
	$this->SetLineWidth(0.5);
	$this->Line(10, 30, 270, 30);
	$this->Ln(6);
	$this->SetFont('Arial','B',15);
    //Calculamos ancho y posici�n del t�tulo.
  /*   $w=$this->GetStringWidth($title)+6;
	$this->SetX((275-$w)/2);
	$this->Cell($w,9,$title,0,1,'C'); */
	/*$this->MultiCell(260,6,$title,0,'C');
    //Salto de l�nea
//	$this->Ln(3.5);
	$this->SetLineWidth(0.2);
	$this->SetFont('Arial','',10);
	$this->Cell(4,4,'X',1,0,'C');
	$this->Cell(30,4,'AYUNTAMIENTO',0,0,'L');
	$this->Cell(4,4,'',1,0,'C');
	$this->Cell(15,4,'ODAS',0,0,'L');
	$this->Cell(4,4,'',1,0,'L');
	$this->Cell(10,4,'DIF',0,0,'L');
	$this->Cell(4,4,'',1,0,'L');
	$this->Cell(15,4,'OTROS',0,0,'L');
	$this->Cell(10);
	$this->Cell(15,4,'MUNICIPIO:',0,0,'L');
	$this->Cell(10);
	$this->SetFont('Arial','B',10);
	$this->Cell(40,4, $ayuntam, 0,0, 'L');
	$this->SetFont('Arial','',10);
	$this->Cell(15,4,'�REA:',0,0,'L');
	$this->SetFont('Arial','B',10);
	$this->MultiCell(70,4,$area,0,'L');
	$this->SetFont('Arial','',10);*/

}

//Pie de p�gina
function Footer()
{
	global $usr;
	global $forma;
	global $var;
	global $dia;
	global $mes;
	global $year;
	global $ent;
    //Posici�n: a 1,5 cm del final
   $this->SetY(-10);	/*
	$this->SetFont('Arial','B',10);
    //Arial italic 8
	$this->Cell(50, 0, $var, 0, 0, 'L');
	$this->Ln(12);
	$this->Cell(30);
	$this->Cell(80, 0, $usr, 0, 0, 'C');
	$this->Cell(26);
	$this->Cell(80, 0, $ent, 0, 0, 'C');
	$this->Line(40, 175, 120, 175);
	$this->Line(145, 175, 225, 175);
	$this->Ln(5);
	$this->Cell(65);
	$this->Cell(10, 1, 'ELABOR�', 0,0, 'C');
	$this->Cell(95);
	$this->Cell(10, 1, 'ENTREGA', 0,0, 'C');
	$this->SetLineWidth(0.5);
	$this->Line(10, 180, 270, 180);
	$this->Ln(11);
	$this->SetFont('Arial','',8);
	$this->Cell(230);
	$this->Cell(30, 1, 'FECHA DE ELABORACI�N', 0,0, 'C');
	$this->Ln(2);
	$this->Cell(230);
	$this->Cell(10, 4, 'D�A', 1,0, 'C');
	$this->Cell(10, 4, 'MES', 1,0, 'C');
	$this->Cell(10, 4, 'A�O', 1,0, 'C');
	$this->Ln(4);
	$this->Cell(230);
	$this->Cell(10, 4, $dia, 1,0, 'C');
	$this->Cell(10, 4, $mes, 1,0, 'C');
	$this->Cell(10, 4, $year, 1,0, 'C');
	$this->Ln(5);
	$this->Cell(10, 4, $forma, 0,0, 'C');
	//$this->Cell(70
	$this->Ln(5);*/
    $this->SetFont('Arial','I',5);
    //N�mero de p�gina
    $this->Cell(0,10,'P�gina '.$this->PageNo().'/{nb}',0,0,'C');
}

function DashedRect($x1,$y1,$x2,$y2,$width=0.5,$nb=15)
    {
        $this->SetLineWidth($width);
        $longueur=abs($x1-$x2);
        $hauteur=abs($y1-$y2);
        if($longueur>$hauteur) {
            $Pointilles=($longueur/$nb)/2; // length of dashes
        }
        else {
            $Pointilles=($hauteur/$nb)/2;
        }
        for($i=$x1;$i<=$x2;$i+=$Pointilles+$Pointilles) {
            for($j=$i;$j<=($i+$Pointilles);$j++) {
                if($j<=($x2-1)) {
                    $this->Line($j,$y1,$j+1,$y1); // upper dashes
                    $this->Line($j,$y2,$j+1,$y2); // lower dashes
                }
            }
        }
        for($i=$y1;$i<=$y2;$i+=$Pointilles+$Pointilles) {
            for($j=$i;$j<=($i+$Pointilles);$j++) {
                if($j<=($y2-1)) {
                    $this->Line($x1,$j,$x1,$j+1); // left dashes
                    $this->Line($x2,$j,$x2,$j+1); // right dashes
                }
            }
        }
    }

function SetDash($black=false,$white=false)
    {
        if($black and $white)
            $s=sprintf('[%.3f %.3f] 0 d',$black*$this->k,$white*$this->k);
        else
            $s='[] 0 d';
        $this->_out($s);
    }
	
function SetWidths($w)
{
    //Set the array of column widths
    $this->widths=$w;
}

function SetAligns($a)
{
    //Set the array of column alignments
    $this->aligns=$a;
}
function Recibe($rec)
{
	//global $encabezado;
	$this->encabezado= $rec;
	//$this->Row($encabezado);
    //Set the array of column widths
    //$this->widths=$w;
}
function Alinea($alig)
{
	//global $encabezado;
	$this->alinea= $alig;
	//$this->Row($encabezado);
    //Set the array of column widths
    //$this->widths=$w;
}

function Row($data)
{
    //Calculate the height of the row
    $nb=0;
    for($i=0;$i<count($data);$i++)
        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
    $h=5*$nb;
    //Issue a page break first if needed
    $this->CheckPageBreak($h);
    //Draw the cells of the row
    for($i=0;$i<count($data);$i++)
    {
        $w=$this->widths[$i];
        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : $this->alinea;
        //Save the current position
        $x=$this->GetX();
        $y=$this->GetY();
        //Draw the border
        $this->Rect($x,$y,$w,$h);
        //Print the text
        $this->MultiCell($w,5,$data[$i],0,$a);
        //Put the position to the right of the cell
        $this->SetXY($x+$w,$y);
    }
    //Go to the next line
    $this->Ln($h);
	$this->SetDrawColor(255,255,255);
	$this->SetLineWidth(1);
	$this->Line(10, 45, 10, 200);
	$this->Line(270, 45, 270, 250);
	$this->SetDrawColor(1,1,1);
	$this->SetLineWidth(0.2);
}

function CheckPageBreak($h)
{
    //If the height h would cause an overflow, add a new page immediately
    //if($this->GetY()+$h>$this->PageBreakTrigger){
	if($this->GetY()+$h>155){
        $this->AddPage($this->CurOrientation);
		$y=$this->GetY();
		$alig='C';
		$this->SetLineWidth(0.5);
		$this->Line(10, $y, 270, $y);
		$this->SetLineWidth(0.2);
		$this->SetFont('Arial','B',8);	
		//$this->alinea='C';	
		$this->Row($this->encabezado, $alig);
		$y=$this->GetY();
		//$this->alinea='C';	
		$this->SetLineWidth(0.5);
		$this->Line(10, $y, 270, $y);
		$this->SetLineWidth(0.2);
		$this->SetFont('Arial','',8);
	}
}

function NbLines($w,$txt)
{
    //Computes the number of lines a MultiCell of width w will take
    $cw=&$this->CurrentFont['cw'];
    if($w==0)
        $w=$this->w-$this->rMargin-$this->x;
    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
    $s=str_replace("\r",'',$txt);
    $nb=strlen($s);
    if($nb>0 and $s[$nb-1]=="\n")
        $nb--;
    $sep=-1;
    $i=0;
    $j=0;
    $l=0;
    $nl=1;
    while($i<$nb)
    {
        $c=$s[$i];
        if($c=="\n")
        {
            $i++;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
            continue;
        }
        if($c==' ')
            $sep=$i;
        $l+=$cw[$c];
        if($l>$wmax)
        {
            if($sep==-1)
            {
                if($i==$j)
                    $i++;
            }
            else
                $i=$sep+1;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
        }
        else
            $i++;
    }
    return $nl;
}
function EAN13($x,$y,$barcode,$h=16,$w=.35)
{
    $this->Barcode($x,$y,$barcode,$h,$w,13);
}

function UPC_A($x,$y,$barcode,$h=16,$w=.35)
{
    $this->Barcode($x,$y,$barcode,$h,$w,12);
}

function GetCheckDigit($barcode)
{
    //Compute the check digit
    $sum=0;
    for($i=1;$i<=11;$i+=2)
        $sum+=3*$barcode{$i};
    for($i=0;$i<=10;$i+=2)
        $sum+=$barcode{$i};
    $r=$sum%10;
    if($r>0)
        $r=10-$r;
    return $r;
}

function TestCheckDigit($barcode)
{
    //Test validity of check digit

    $sum=0;
    for($i=1;$i<=11;$i+=2)
        $sum+=3*$barcode{$i};
    for($i=0;$i<=10;$i+=2)
        $sum+=$barcode{$i};
    return ($sum+$barcode{12})%10==0;
}

function Barcode($x,$y,$barcode,$h,$w,$len)
{
    //Padding
    $barcode=str_pad($barcode,$len-1,'0',STR_PAD_LEFT);
    if($len==12)
        $barcode='0'.$barcode;
    //Add or control the check digit
    if(strlen($barcode)==12)
        $barcode.=$this->GetCheckDigit($barcode);
    elseif(!$this->TestCheckDigit($barcode))
        $this->Error('Incorrect check digit');
    //Convert digits to bars
    $codes=array(
        'A'=>array(
            '0'=>'0001101','1'=>'0011001','2'=>'0010011','3'=>'0111101','4'=>'0100011',
            '5'=>'0110001','6'=>'0101111','7'=>'0111011','8'=>'0110111','9'=>'0001011'),
        'B'=>array(
            '0'=>'0100111','1'=>'0110011','2'=>'0011011','3'=>'0100001','4'=>'0011101',
            '5'=>'0111001','6'=>'0000101','7'=>'0010001','8'=>'0001001','9'=>'0010111'),
        'C'=>array(
            '0'=>'1110010','1'=>'1100110','2'=>'1101100','3'=>'1000010','4'=>'1011100',
            '5'=>'1001110','6'=>'1010000','7'=>'1000100','8'=>'1001000','9'=>'1110100')
        );
    $parities=array(
        '0'=>array('A','A','A','A','A','A'),
        '1'=>array('A','A','B','A','B','B'),
        '2'=>array('A','A','B','B','A','B'),
        '3'=>array('A','A','B','B','B','A'),
        '4'=>array('A','B','A','A','B','B'),
        '5'=>array('A','B','B','A','A','B'),
        '6'=>array('A','B','B','B','A','A'),
        '7'=>array('A','B','A','B','A','B'),
        '8'=>array('A','B','A','B','B','A'),
        '9'=>array('A','B','B','A','B','A')
        );
    $code='101';
    $p=$parities[$barcode{0}];
    for($i=1;$i<=6;$i++)
        $code.=$codes[$p[$i-1]][$barcode{$i}];
    $code.='01010';
    for($i=7;$i<=12;$i++)
        $code.=$codes['C'][$barcode{$i}];
    $code.='101';
    //Draw bars
    for($i=0;$i<strlen($code);$i++)
    {
        if($code{$i}=='1')
            $this->Rect($x+$i*$w,$y,$w,$h,'F');
    }
    //Print text uder barcode
    $this->SetFont('Arial','',12);
    $this->Text($x,$y+$h+11/$this->k,substr($barcode,-$len));
}

function Code39($xpos, $ypos, $code, $baseline=0.5, $height=5){

    $wide = $baseline;
    $narrow = $baseline / 3 ;
    $gap = $narrow;

    $barChar['0'] = 'nnnwwnwnn';
    $barChar['1'] = 'wnnwnnnnw';
    $barChar['2'] = 'nnwwnnnnw';
    $barChar['3'] = 'wnwwnnnnn';
    $barChar['4'] = 'nnnwwnnnw';
    $barChar['5'] = 'wnnwwnnnn';
    $barChar['6'] = 'nnwwwnnnn';
    $barChar['7'] = 'nnnwnnwnw';
    $barChar['8'] = 'wnnwnnwnn';
    $barChar['9'] = 'nnwwnnwnn';
    $barChar['A'] = 'wnnnnwnnw';
    $barChar['B'] = 'nnwnnwnnw';
    $barChar['C'] = 'wnwnnwnnn';
    $barChar['D'] = 'nnnnwwnnw';
    $barChar['E'] = 'wnnnwwnnn';
    $barChar['F'] = 'nnwnwwnnn';
    $barChar['G'] = 'nnnnnwwnw';
    $barChar['H'] = 'wnnnnwwnn';
    $barChar['I'] = 'nnwnnwwnn';
    $barChar['J'] = 'nnnnwwwnn';
    $barChar['K'] = 'wnnnnnnww';
    $barChar['L'] = 'nnwnnnnww';
    $barChar['M'] = 'wnwnnnnwn';
    $barChar['N'] = 'nnnnwnnww';
    $barChar['O'] = 'wnnnwnnwn';
    $barChar['P'] = 'nnwnwnnwn';
    $barChar['Q'] = 'nnnnnnwww';
    $barChar['R'] = 'wnnnnnwwn';
    $barChar['S'] = 'nnwnnnwwn';
    $barChar['T'] = 'nnnnwnwwn';
    $barChar['U'] = 'wwnnnnnnw';
    $barChar['V'] = 'nwwnnnnnw';
    $barChar['W'] = 'wwwnnnnnn';
    $barChar['X'] = 'nwnnwnnnw';
    $barChar['Y'] = 'wwnnwnnnn';
    $barChar['Z'] = 'nwwnwnnnn';
    $barChar['-'] = 'nwnnnnwnw';
    $barChar['.'] = 'wwnnnnwnn';
    $barChar[' '] = 'nwwnnnwnn';
    $barChar['*'] = 'nwnnwnwnn';
    $barChar['$'] = 'nwnwnwnnn';
    $barChar['/'] = 'nwnwnnnwn';
    $barChar['+'] = 'nwnnnwnwn';
    $barChar['%'] = 'nnnwnwnwn';

    $this->SetFont('Arial','',10);
    $this->Text($xpos, $ypos + $height + 4, $code);
    $this->SetFillColor(0);

    $code = '*'.strtoupper($code).'*';
    for($i=0; $i<strlen($code); $i++){
        $char = $code{$i};
        if(!isset($barChar[$char])){
            $this->Error('Invalid character in barcode: '.$char);
        }
        $seq = $barChar[$char];
        for($bar=0; $bar<9; $bar++){
            if($seq{$bar} == 'n'){
                $lineWidth = $narrow;
            }else{
                $lineWidth = $wide;
            }
            if($bar % 2 == 0){
                $this->Rect($xpos, $ypos, $lineWidth, $height, 'F');
            }
            $xpos += $lineWidth;
        }
        $xpos += $gap;
    }
}
}
?>