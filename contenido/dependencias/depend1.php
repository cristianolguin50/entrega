<?php
	require_once('../../Connections/bd2.php'); 
	
	
	/*	Modificado por: Marco Robles
	Fecha: 09/01/2018	*/
	
	session_start();
	//die();
	
	if (!isset($_SESSION['MM_Username'])){
		header("Location: ../../adios.php");
	}
	
	mysql_select_db($database_bd2, $bd2);
	$sqlDepOficial = "SELECT clave, clave_dep, nombre FROM `dependencia` WHERE mostrar='1' AND (tipo='G' OR tipo='P') ORDER BY tipo ASC, clave_dep ASC";
	$dtDepOficial = mysql_query($sqlDepOficial, $bd2) or die(mysql_error());
	
	mysql_select_db($database_bd2, $bd2);
	$sqlDepPre = "SELECT clave, clave_dep, nombre FROM `dependencia` WHERE tipo='G' OR tipo='A' ORDER BY clave";
	$dtDepPre = mysql_query($sqlDepPre, $bd2) or die(mysql_error());
	
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link href="../../css/idots.css" rel="stylesheet" type="text/css">
		<title>Unidades Administrativas</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		
		<script type="text/javascript" src="../../js/jquery-1.7.1.js"></script>
		<link rel="stylesheet" type="text/css" media="screen" href="../../js/jquery.ketchup.0.3.2/css/jquery.ketchup.css" />
		<script type="text/javascript" src="../../js/jquery.ketchup.0.3.2/js/jquery.ketchup.all.min.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function() {
				$("#form1").ketchup(); 
			});
		</script>
		
		<script language="JavaScript" type="text/JavaScript">
			
			function MM_goToURL() {
				var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
				for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}
			
			function pasa(valor){
				document.form1.tipo.value=valor;
			}
		</script>
		
		<script language="JavaScript" src="_scripts.js" type="text/javascript"></script>
		<script language="javascript" src="../../js/validate.js" type="text/javascript"></script>
		<style type="text/css">
			body {
			margin-top: 0px;
			}
			.style1 {
			color: #FF0000;
			font-style: italic;
			}
			.style2 {font-size: 110%;}
			.style3 {color: #FF0000;
			font-style: italic;
			}
		</style>
	</head>
	
	<body onLoad="document.form1.expediente.focus();">
		<div align="center"><h2 style="color: #0404B4">Unidad Administrativa</h2></div>
		
		<p align="center"><strong style="color:#21618c">Nota: Para agregar una nueva unidad administrativa, deber&aacute; contar con la asesor&iacute;a.</strong></p>
		
		<form id="form1" name="form1" method="post" action="guarda.php" enctype="multipart/form-data"  autocomplete="off">
			<div align="center">
				<table width="60%" border="1" cellpadding="3" cellspacing="0">
					<tr>
						<td colspan="4"><div align="center"><strong>Ingrese los datos de la Unidad Administrativa</strong></div></td>
					</tr>
					<tr>
						<td width="25%"><div align="right"><strong>*Clave:</strong></div></td>
						<td colspan="2" bgcolor="#FFFFFF"><div align="left">
						<input name="clave" type="text" id="clave" size="20" maxlength="29" onKeyPress="return pulsar(event)" onKeyUp="return handleEnter(this, event)" tabindex="1" data-validate="validate(required);" ><a href="#" class="hintanchor" onMouseover="showhint('<center>Indique la clave que le corresponde a la Unidad Administrativa. <i>Este campo es obligatorio</i></center>', this, event, '150px')">[?]</a></div></td>
					</tr>
					
					<tr>
						<td><div align="right"><strong>*Nombre:</strong></div></td>
						<td colspan="2" bgcolor="#FFFFFF"><div align="left">
						<input name="nombre" type="text" maxlength="249" size="40" id="nombre2" onKeyPress="return pulsar(event)" onKeyUp="return handleEnter(this, event);" tabindex="2" data-validate="validate(required);" ><a href="#" class="hintanchor" onMouseover="showhint('<center>Tecle� el nombre de la Unidad Administrativa. <b>Este nombre no es aleatorio, debe estar contemplado en el Organigrama oficial.</b> <i>Este campo es obligatorio</i></center>', this, event, '150px')">[?]</a></div></td>						
					</tr>
					<!--
					<tr>
						<td><div align="right"><strong>Acta E-R:</strong></div></td>
						<td colspan="2" bgcolor="#FFFFFF"><div align="left">
							<select name="actaer" id="actaer" tabindex="3">     								
								<option value="1">AER-1 Presidencia y Sindicatura</option>
								<option value="2">AER-2 Contraloria y Regidurias</option>
								<option value="3">AER-3 Dem�s unidades administrativas</option>
								<option value="4">AER-4 Creaci�n, Fusion, Escision o Supresion de Unidades Administrativas y/o Municipios, Fallecimiento de titular de una Unidad Administrativa.</option>
							</select>								
						<a href="#" class="hintanchor" onMouseOver="showhint('<center>Acta de entrega recepci&oacute;n que corresponde a la Unidad Administrativa. Si tiene duda sobre que acta le corresponde por favor consulte con el <b>OSFEM</b> quien le indicara el acta correcta</center>', this, event, '150px')">[?]</a></div></td>			
					</tr>
					-->
					
					<tr>
						<td><div align="right"><strong>Depende de:</strong></div></td>
						<td colspan="2" bgcolor="#FFFFFF"><div align="left">
							<select name="depende" id="select" onkeypress="return handleEnter(this.event)" tabindex="4">     
								<?php while ($row_depend = mysql_fetch_assoc($dtDepOficial)) { ?>
									<option value="<?php echo $row_depend['clave']; ?>"><?php echo $row_depend['clave_dep'].' - '.$row_depend['nombre']?></option>
								<?php } ?>
							</select><a href="#" class="hintanchor" onMouseOver="showhint('<center> Indique funcionalmente a quien esta <b>asociada</b> su nueva Unidad Administrativa.</center>', this, event, '150px')">[?]</a></div></td>
							
					</tr>
					<tr>
						<td>
							<div align="right">
								<strong>Asociada Presupuestalmente a:</strong>
							</div>
						</td>
						
						<td bgcolor="#FFFFFF">
							<div align="left">			
								<select name="dep_oficial" id="dep_oficial" tabindex="5">
									<?php
										while($row_depre = mysql_fetch_array($dtDepPre)){
										?>
										<option value="<?php echo $row_depre['clave_dep']; ?>"><?php echo $row_depre['clave_dep'].' - '.$row_depre['nombre']; ?></option>
									<?php } ?>
								</select>
							<a href="#" class="hintanchor" onMouseOver="showhint('<center>Seleccione la asociaci&oacute;n <b>presupuestal</b> de la Unidad Administrativa.</center>', this, event, '150px')">[?]</a></div>			
						</td>
					</tr>
					
				</table>
			</div>
			<p align="center">  	
				<input name="regresa" type="button" onClick="MM_goToURL('self','depend2.php');return document.MM_returnValue" value="Regresar">
				<input type="submit" name="enviar" value="Almacenar Datos">
			</p>
			<div align="center">
				<input type="hidden" name="MM_insert" value="form1" />
			</div>
		</form>
	</body>
</html>