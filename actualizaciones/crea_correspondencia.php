<?php
require_once('../Connections/bd2.php'); 
session_start();
/*
mysql_select_db($database_bd2, $bd2);
$strQuery = "DROP TABLE `ial003`";
$dtAlter = mysql_query($strQuery, $bd2) or die(mysql_error());	
*/
mysql_select_db($database_bd2, $bd2);
$strQuery = "CREATE TABLE IF NOT EXISTS `correspondencia` (
  `id_correspondencia` int(11) NOT NULL AUTO_INCREMENT,
  `id_registro` int(11) NOT NULL,
  `observaciones` blob,
  `activo` int(11) NOT NULL DEFAULT '1',
  `prioridad` int(11) NOT NULL DEFAULT '0',
  `fecha_vencimiento` date NOT NULL DEFAULT '0000-00-00',
  `fecha_ingreso` date NOT NULL,
  `usuario` varchar(30) NOT NULL,
  PRIMARY KEY (`id_correspondencia`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;";

$dtAlter = mysql_query($strQuery, $bd2) or die(mysql_error());	
echo "La tabla de correspondencia fue creada satisfactoriamente<br>";


mysql_select_db($database_bd2, $bd2);
$strQuery = "CREATE TABLE IF NOT EXISTS `correspondencia_ccp` (
   `id_ccp` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_correspondencia` int(11) NOT NULL,
  `curp` varchar(20) NOT NULL,
  `creador` int(11) NOT NULL DEFAULT '0',
  `estado` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_ccp`),
  KEY `id_correspondencia` (`id_correspondencia`),
  CONSTRAINT `correspondencia_ccp_ibfk_1` FOREIGN KEY (`id_correspondencia`) REFERENCES `correspondencia` (`id_correspondencia`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;";

$dtAlter = mysql_query($strQuery, $bd2) or die(mysql_error());	
echo "La tabla de correspondencia ccp fue creada satisfactoriamente<br>";


mysql_select_db($database_bd2, $bd2);
$strQuery = "CREATE TABLE IF NOT EXISTS `correspondencia_nota` (
   `id_nota` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_correspondencia` int(20) NOT NULL,
  `id_ccp` bigint(20) NOT NULL,
  `nota` blob NOT NULL,
  `ext_file` varchar(4) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  PRIMARY KEY (`id_nota`),
  KEY `id_ccp` (`id_ccp`),
  KEY `id_correspondencia` (`id_correspondencia`),
  CONSTRAINT `correspondencia_nota_ibfk_1` FOREIGN KEY (`id_ccp`) REFERENCES `correspondencia_ccp` (`id_ccp`),
  CONSTRAINT `correspondencia_nota_ibfk_2` FOREIGN KEY (`id_ccp`) REFERENCES `correspondencia_ccp` (`id_ccp`),
  CONSTRAINT `correspondencia_nota_ibfk_3` FOREIGN KEY (`id_correspondencia`) REFERENCES `correspondencia` (`id_correspondencia`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;";

$dtAlter = mysql_query($strQuery, $bd2) or die(mysql_error());	
echo "La tabla de correspondencia nota fue creada satisfactoriamente<br>";
?>

