<?php require_once('../Connections/bd2.php'); ?>
<?php
//initialize the session
session_start();

// ** Logout the current user. **
$logoutAction = $_SERVER['PHP_SELF']."?doLogout=true";
if ((isset($_SERVER['QUERY_STRING'])) && ($_SERVER['QUERY_STRING'] != "")){
  $logoutAction .="&". htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_GET['doLogout'])) &&($_GET['doLogout']=="true")){
  //to fully log out a visitor we need to clear the session varialbles
  session_unregister('MM_Username');
  session_unregister('MM_UserGroup');
  session_unregister('clave_dependencia');
	
  $logoutGoTo = "../salida.htm";
  if ($logoutGoTo) {
    header("Location: $logoutGoTo");
    exit;
  }
}
?>
<?php
//initialize the session
$colname_usuario = "1";
if (isset($_SESSION['MM_Username'])) {
  $colname_usuario = (get_magic_quotes_gpc()) ? $_SESSION['MM_Username'] : addslashes($_SESSION['MM_Username']);
}
mysql_select_db($database_bd2, $bd2);
$query_usuario = sprintf("SELECT * FROM usuario WHERE usuario = '%s'", $colname_usuario);
$usuario = mysql_query($query_usuario, $bd2) or die(mysql_error());
$row_usuario = mysql_fetch_assoc($usuario);
$totalRows_usuario = mysql_num_rows($usuario);


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-1905873-2";
urchinTracker();
</script>
<title>CREG Versi&oacute;n 2009 &copy; IIDESOFT M&eacute;xico, S.A. de C.V.</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
	html{
		height:100%;
	}
	body{
		font-family: Trebuchet MS, Lucida Sans Unicode, Arial, sans-serif;
		font-size:0.7em;
		margin:0px;
		padding:0px;

		background-color:#E2EBED;
		height:100%;
		text-align:center;
	}
	.clear{
		clear:both;
	}
	
	#mainContainer{
		width:760px;
		text-align:left;
		margin:0 auto;
		background-color: #FFF;
		border-left:1px solid #000;
		border-right:1px solid #000;
		height:100%;
	}
	
	#topBar{
		width:760px;
		height:100px;
	}
	#leftMenu{
		width:200px;
		padding-left:10px;
		padding-right:10px;
		float:left;
	}
	#mainContent{
		width: 520px;
		padding-right:10px;	
		float:left;
	}
	/*
	General rules
	*/

	#dhtmlgoodies_slidedown_menu li{
		list-style-type:none;
		position:relative;
	}
	#dhtmlgoodies_slidedown_menu ul{
		margin:0px;
		padding:0px;
		position:relative;

	}

	#dhtmlgoodies_slidedown_menu div{
		margin:0px;
		padding:0px;
	}
	/* 	Layout CSS */
#dhtmlgoodies_slidedown_menu{    

  visibility:hidden;
  border:1px solid #000;
  padding:1px;

  width: 182px;  /* IE 5.x */
  width/* */:/**/182px;  /* Other browsers */
  width: /**/182px;  
  
}	

	/* All A tags - i.e menu items. */
	#dhtmlgoodies_slidedown_menu a{
		color: #000;
		text-decoration:none;	
		display:block;
		clear:both;
		width:170px;	
		padding-left:2px;	

	}
	
	/*
	A tags 
	*/
#dhtmlgoodies_slidedown_menu .slMenuItem_depth1{  /* Main menu items */
  margin-top:1px;
  font-weight:bold;
  background-color:#317082;
  color:#FFF;
  background-image:url(../images/bg.gif);
  height:30px;
  line-height:30px;
  vertical-align:middle;
  padding-left:10px;
  border-left:1px solid #000;
  border-right:1px solid #000;
}  
	
#dhtmlgoodies_slidedown_menu .slMenuItem_depth2{  /* Sub menu items */
  margin-top:1px;
  text-align:center;
  font-weight:bold;
}	
	#dhtmlgoodies_slidedown_menu .slMenuItem_depth3{	/* Sub menu items */
		margin-top:1px;
		font-style:italic;
		color:blue;
	}	
	#dhtmlgoodies_slidedown_menu .slMenuItem_depth4{	/* Sub menu items */
		margin-top:1px;
		color:red;
	}	
	#dhtmlgoodies_slidedown_menu .slMenuItem_depth5{	/* Sub menu items */
		margin-top:1px;
	}

	/* UL tags, i.e group of menu utems. 
	It's important to add style to the UL if you're specifying margins. If not, assign the style directly
	to the parent DIV, i.e. 
	
	#dhtmlgoodies_slidedown_menu .slideMenuDiv1
	
	instead of 
	
	#dhtmlgoodies_slidedown_menu .slideMenuDiv1 ul
	*/
	
	#dhtmlgoodies_slidedown_menu .slideMenuDiv1 ul{
		padding:1px;
	}
	#dhtmlgoodies_slidedown_menu .slideMenuDiv2 ul{
		margin-left:5px;
		padding:1px;
	}
	#dhtmlgoodies_slidedown_menu .slideMenuDiv3 ul{
		margin-left:10px;
		padding:1px;
	}
	#dhtmlgoodies_slidedown_menu .slMenuItem_depth4 ul{
		margin-left:15px;
		padding:1px;
	}
	
	</style>
	<script type="text/javascript">
	/************************************************************************************************************
	(C) www.dhtmlgoodies.com, October 2005
	
	This is a script from www.dhtmlgoodies.com. You will find this and a lot of other scripts at our website.	
	
	Terms of use:
	You are free to use this script as long as the copyright message is kept intact. However, you may not
	redistribute, sell or repost it without our permission.
	
	Update log:
	
		March, 15th: Fixed problem with sliding in MSIE
	
	Thank you!
	
	www.dhtmlgoodies.com
	Alf Magne Kalleland
	
	************************************************************************************************************/	
var expandFirstItemAutomatically = false;	// Expand first menu item automatically ?
var initMenuIdToExpand = false;	// Id of menu item that should be initially expanded. the id is defined in the <li> tag.
var expandMenuItemByUrl = true;	// Menu will automatically expand by url - i.e. if the href of the menu item is in the current location, it will expand


var initialMenuItemAlwaysExpanded = true;	// NOT IMPLEMENTED YET

var dhtmlgoodies_slmenuObj;
var divToScroll = false;
var ulToScroll = false;	
var divCounter = 1;
var otherDivsToScroll = new Array();
var divToHide = false;
var parentDivToHide = new Array();
var ulToHide = false;
var offsetOpera = 0;
if(navigator.userAgent.indexOf('Opera')>=0)offsetOpera=1;	
var slideMenuHeightOfCurrentBox = 0;
var objectsToExpand = new Array();
var initExpandIndex = 0;
var alwaysExpanedItems = new Array();
	
function popMenusToShow()
{
	var obj = divToScroll;
	var endArray = new Array();
	while(obj && obj.tagName!='BODY'){
		if(obj.tagName=='DIV' && obj.id.indexOf('slideDiv')>=0){
			var objFound = -1;
			for(var no=0;no<otherDivsToScroll.length;no++){
				if(otherDivsToScroll[no]==obj){
					objFound = no;		
				}					
			}	
			if(objFound>=0){
				otherDivsToScroll.splice(objFound,1);	
			}		
		}	
		obj = obj.parentNode;
	}	
}

function showSubMenu(e,inputObj)
{

	if(this && this.tagName)inputObj = this.parentNode;
	if(inputObj && inputObj.tagName=='LI'){
		divToScroll = inputObj.getElementsByTagName('DIV')[0];
		for(var no=0;no<otherDivsToScroll.length;no++){
			if(otherDivsToScroll[no]==divToScroll)return;
		}			
	}
	hidingInProcess = false;
	if(otherDivsToScroll.length>0){
		if(divToScroll){				
			if(otherDivsToScroll.length>0){
				popMenusToShow();
			}
			if(otherDivsToScroll.length>0){	
				autoHideMenus();
				hidingInProcess = true;
			}
		}	
	}		
	if(divToScroll && !hidingInProcess){
		divToScroll.style.display='';
		otherDivsToScroll.length = 0;
		otherDivToScroll = divToScroll.parentNode;
		otherDivsToScroll.push(divToScroll);	
		while(otherDivToScroll && otherDivToScroll.tagName!='BODY'){
			if(otherDivToScroll.tagName=='DIV' && otherDivToScroll.id.indexOf('slideDiv')>=0){
				otherDivsToScroll.push(otherDivToScroll);
									
			}
			otherDivToScroll = otherDivToScroll.parentNode;
		}			
		ulToScroll = divToScroll.getElementsByTagName('UL')[0];
		if(divToScroll.style.height.replace('px','')/1<=1)scrollDownSub();
	}	
	

}



function autoHideMenus()
{
	if(otherDivsToScroll.length>0){
		divToHide = otherDivsToScroll[otherDivsToScroll.length-1];
		parentDivToHide.length=0;
		var obj = divToHide.parentNode.parentNode.parentNode;
		while(obj && obj.tagName=='DIV'){			
			if(obj.id.indexOf('slideDiv')>=0)parentDivToHide.push(obj);
			obj = obj.parentNode.parentNode.parentNode;
		}
		var tmpHeight = (divToHide.style.height.replace('px','')/1 - slideMenuHeightOfCurrentBox);
		if(tmpHeight<0)tmpHeight=0;
		if(slideMenuHeightOfCurrentBox)divToHide.style.height = tmpHeight  + 'px';
		ulToHide = divToHide.getElementsByTagName('UL')[0];
		slideMenuHeightOfCurrentBox = ulToHide.offsetHeight;
		scrollUpMenu();		
	}else{
		slideMenuHeightOfCurrentBox = 0;
		showSubMenu();			
	}
}


function scrollUpMenu()
{

	var height = divToHide.offsetHeight;
	height-=15;
	if(height<0)height=0;
	divToHide.style.height = height + 'px';

	for(var no=0;no<parentDivToHide.length;no++){	
		parentDivToHide[no].style.height = parentDivToHide[no].getElementsByTagName('UL')[0].offsetHeight + 'px';
	}
	if(height>0){
		setTimeout('scrollUpMenu()',5);
	}else{
		divToHide.style.display='none';
		otherDivsToScroll.length = otherDivsToScroll.length-1;
		autoHideMenus();			
	}
}	

function scrollDownSub()
{
	if(divToScroll){			
		var height = divToScroll.offsetHeight/1;
		var offsetMove =Math.min(15,(ulToScroll.offsetHeight - height));
		height = height +offsetMove ;
		divToScroll.style.height = height + 'px';
		
		for(var no=1;no<otherDivsToScroll.length;no++){
			var tmpHeight = otherDivsToScroll[no].offsetHeight/1 + offsetMove;
			otherDivsToScroll[no].style.height = tmpHeight + 'px';
		}			
		if(height<ulToScroll.offsetHeight)setTimeout('scrollDownSub()',5); else {
			divToScroll = false;
			ulToScroll = false;
			if(objectsToExpand.length>0 && initExpandIndex<(objectsToExpand.length-1)){
				initExpandIndex++;
				
				showSubMenu(false,objectsToExpand[initExpandIndex]);
			}
		}
	}
}
	
function initSubItems(inputObj,currentDepth)
{		
	divCounter++;		
	var div = document.createElement('DIV');	// Creating new div		
	div.style.overflow = 'hidden';	
	div.style.position = 'relative';
	div.style.display='none';
	div.style.height = '1px';
	div.id = 'slideDiv' + divCounter;
	div.className = 'slideMenuDiv' + currentDepth;		
	inputObj.parentNode.appendChild(div);	// Appending DIV as child element of <LI> that is parent of input <UL>		
	div.appendChild(inputObj);	// Appending <UL> to the div
	var menuItem = inputObj.getElementsByTagName('LI')[0];
	while(menuItem){
		if(menuItem.tagName=='LI'){
			var aTag = menuItem.getElementsByTagName('A')[0];
			aTag.className='slMenuItem_depth'+currentDepth;	
			var subUl = menuItem.getElementsByTagName('UL');
			if(subUl.length>0){
				initSubItems(subUl[0],currentDepth+1);					
			}
			aTag.onclick = showSubMenu;				
		}			
		menuItem = menuItem.nextSibling;						
	}		
}

function initSlideDownMenu()
{
	dhtmlgoodies_slmenuObj = document.getElementById('dhtmlgoodies_slidedown_menu');
	dhtmlgoodies_slmenuObj.style.visibility='visible';
	var mainUl = dhtmlgoodies_slmenuObj.getElementsByTagName('UL')[0];		
	var mainMenuItem = mainUl.getElementsByTagName('LI')[0];
	mainItemCounter = 1;
	while(mainMenuItem){			
		if(mainMenuItem.tagName=='LI'){
			var aTag = mainMenuItem.getElementsByTagName('A')[0];
			aTag.className='slMenuItem_depth1';	
			var subUl = mainMenuItem.getElementsByTagName('UL');
			if(subUl.length>0){
				mainMenuItem.id = 'mainMenuItem' + mainItemCounter;
				initSubItems(subUl[0],2);
				aTag.onclick = showSubMenu;
				mainItemCounter++;
			}				
		}			
		mainMenuItem = mainMenuItem.nextSibling;	
	}		
	
	if(location.search.indexOf('mainMenuItemToSlide')>=0){
		var items = location.search.split('&');
		for(var no=0;no<items.length;no++){
			if(items[no].indexOf('mainMenuItemToSlide')>=0){
				values = items[no].split('=');
				showSubMenu(false,document.getElementById('mainMenuItem' + values[1]));	
				initMenuIdToExpand = false;				
			}
		}			
	}else if(expandFirstItemAutomatically>0){
		if(document.getElementById('mainMenuItem' + expandFirstItemAutomatically)){
			showSubMenu(false,document.getElementById('mainMenuItem' + expandFirstItemAutomatically));
			initMenuIdToExpand = false;
		}
	}

	if(expandMenuItemByUrl)
	{
		var aTags = dhtmlgoodies_slmenuObj.getElementsByTagName('A');
		for(var no=0;no<aTags.length;no++){
			var hrefToCheckOn = aTags[no].href;				
			if(location.href.indexOf(hrefToCheckOn)>=0 && hrefToCheckOn.indexOf('#')<hrefToCheckOn.length-1){
				initMenuIdToExpand = false;
				var obj = aTags[no].parentNode;
				while(obj && obj.id!='dhtmlgoodies_slidedown_menu'){
					if(obj.tagName=='LI'){							
						var subUl = obj.getElementsByTagName('UL');
						if(initialMenuItemAlwaysExpanded)alwaysExpanedItems[obj.parentNode] = true;
						if(subUl.length>0){								
							objectsToExpand.unshift(obj);
						}
					}
					obj = obj.parentNode;	
				}
				showSubMenu(false,objectsToExpand[0]);
				break;					
			}			
		}
	}
			
	if(initMenuIdToExpand)
	{
		objectsToExpand = new Array();
		var obj = document.getElementById(initMenuIdToExpand)
		while(obj && obj.id!='dhtmlgoodies_slidedown_menu'){
			if(obj.tagName=='LI'){
				var subUl = obj.getElementsByTagName('UL');
				if(initialMenuItemAlwaysExpanded)alwaysExpanedItems[obj.parentNode] = true;
				if(subUl.length>0){						
					objectsToExpand.unshift(obj);
				}
			}
			obj = obj.parentNode;	
		}
		
		showSubMenu(false,objectsToExpand[0]);

	}
	

		
}
	window.onload = initSlideDownMenu;
	</script>

</head>

<body>


<div id="dhtmlgoodies_slidedown_menu" align="center" style="background-color:#FFFFFF ">
			<ul>
			<!--inicio primer menu-->
	<?php
	if ($row_usuario['admin']==1){
	?>
			<li><a href="#">CONFIGURACI�N DEL SISTEMA </a>
			  <ul>
					<li> <a href="menu3.php" target="mainFrame"> <img src="../images/menu_img/datos_grales.gif" width="30" height="30" border="0"> <br> 
					  DATOS GENERALES DE LA ENTIDAD DE GOBIERNO</a>
				      <hr>
				</li>																					
					<li> <a href="ayuntam3.php" target="mainFrame"> <img src="../images/menu_img/dependencias.gif" width="30" height="30" border="0"> <br> 
					  DEPENDENCIAS DE LA ENTIDAD DE GOBIERNO </a>
					  <hr>
				</li>
				 	<li> <a href="usuarios/usuarios.php" target="mainFrame"> <img src="../images/menu_img/usuarios.gif" width="30" height="30" border="0"> <br> 
					  ADMINISTRACI&Oacute;N DE USUARIOS</a>
				      <hr>
				</li>
			  </ul>
								
			  </li>
			  
<!--------------------------------------------------fin primer menu  inicio segundo menu----------------------------------------------->
	<?php
	}
 	if ($row_usuario['rh']==1){
	?>
			  <li><a href="#">RECURSOS HUMANOS</a> 
			    <ul>
	<?php
	}
 	if ($row_usuario['rh1']==1){
	?>				
					<li> <a href="rec_hum/puesto/puesto.php" target="mainFrame"> <img src="../images/menu_img/cat_puestos.gif" width="30" height="31" border="0"> <br> 
					  CAT&Aacute;LOGO DE PUESTOS </a>
	<?php
	}
 	if ($row_usuario['rh2']==1){
	?>					  
					  <hr>
				</li>																					
					<li> <a href="rec_hum/plaza/plaza.php" target="mainFrame"><img src="../images/menu_img/asig_plazas.gif" width="31" height="31" border="0">  <br> 
					  ASIGNACI&Oacute;N DE PLAZAS POR DEPENDENCIA </a>
	<?php
	}
 	if ($row_usuario['rh5']==1){
	?>					  
				      <hr>
				</li>
				 	<li> <a href="rec_hum/trabajador1.php" target="mainFrame"> <img src="../images/menu_img/agregar_trabajador.gif" width="40" height="31" border="0"> <br> 
					  AGREGAR TRABAJADORES </a>
	<?php
	}
 	if ($row_usuario['rh6']==1){
	?>					  
				      <hr>
				</li>
					<li> <a href="rec_hum/busca_trab.php" target="mainFrame">  <img src="../images/menu_img/buscar_trabajador.gif" width="30" height="30" border="0"><br> 
					  BUSCAR TRABAJADORES </a>
	<?php
	}
 	if ($row_usuario['rh3']==1){
	?>					  
					  <hr>
				</li>
					<li> <a  href="rec_hum/comisionado/comision.php" target="mainFrame"><img src="../images/menu_img/personal_comisionado.gif" width="30" height="30" border="0">  <br> 
					  PERSONAL COMISIONADO </a>
	<?php
	}
 	if ($row_usuario['rh8']==1){
	?>					  
				      <hr>
				</li>
					<li> <a href="#" target="mainFrame"> <img src="../images/menu_img/nomina.gif" width="30" height="29" border="0"> <br> 
					  C&Aacute;LCULO DE N&Oacute;MINA </a>
				      <hr>
				</li>	
	<?php
	}
 	if ($row_usuario['rh7']==1){
	?>						
					<li> <a href="../entrega/IFF/IFF-026/IFF-026.php" target="mainFrame"> <img src="../images/menu_img/suteym.gif" width="30" height="30" border="0"> <br> 
					  RELACI&Oacute;N DE PERSONAL SINDICALIZADO </a>
				      <hr>
				</li>
					<li> <a href="../entrega/IFF/IFF-027/IFF-027.php" target="mainFrame"> <img src="../images/menu_img/convenios_sindicales.gif" width="30" height="31" border="0"> <br> 
					  CONVENIOS SINDICALES </a>
				      <hr>
				</li>					
					<li> <a href="../entrega/IFF/IFF-028/IFF-028.php" target="mainFrame"> <img src="../images/menu_img/juicios_laborales.gif" width="30" height="30" border="0"> <br> 
					  JUICIOS LABORALES </a>
				      <hr>
	<?php
	}
 	if ($row_usuario['rh4']==1){
	?>					  
				</li>									
			  </ul>
								
			  </li>
			  

 	<?php
	}
	if($row_usuario['pm']==1){
	?>
			<li><a href="#">CONTROL PATRIMONIAL</a>
			  <ul>
	<?php
	}
	if($row_usuario['pm1']==1){
	?>
					<li><a href="bienes/bienes_muebles2.php" target="mainFrame"> <img src="../images/menu_img/bienes_muebles.gif" width="30" height="30" border="0"> <br> 
					  RELACI&Oacute;N DE BIENES MUEBLES POR DEPENDENCIA </a>
					  <hr>
				</li>																					
					<li> <a href="bienes/bienes_baja.php" target="mainFrame"> <img src="../images/menu_img/bienes_baja.gif" width="30" height="30" border="0"> <br> 
					  BIENES DADOS DE BAJA </a>
					  <hr>
	<?php
	}
	if($row_usuario['pm3']==1){
	?>					  
				</li>
				 	<li> <a href="inmuebles/IPE-002.php" target="mainFrame"> <img src="../images/menu_img/bienes_inmuebles.gif" width="30" height="30" border="0"> <br> 
					  BIENES INMUEBLES </a>
				      <hr>
				</li>	  
					  <li> <a href="IPE-003/IPE-003.php" target="mainFrame"> <img src="../images/menu_img/bienes_muebles_CUA.gif" width="32" height="32" border="0"><br> 
					  BIENES INMUEBLES EN ARRENDAMIENTO, COMODATO O USUFRUCTO </a>
				      <hr>
	</li>
	<?php
	}
	if($row_usuario['pm2']==1){
	?>					  
				
				 	<li> <a href="vehiculos/IPE-006.php" target="mainFrame"> <img src="../images/menu_img/vehiculos.gif" width="30" height="30" border="0"> <br> 
					  RELACI&Oacute;N DE VEH&Iacute;CULOS OFICIALES </a>
				 	  <hr>
					  </li>		
					  	<?php
	}
	if($row_usuario['pm8']==1){
	?>
				
				
				<li> <a href="armas/armas.php" target="mainFrame"> <img src="../images/minigun.gif" width="36" height="35" border="0"> <br> 
					 CONTROL DE ARMAMENTO Y EQUIPO DE SEGURIDAD </a>
				 	  <hr>
				</li>	
					<?php
	}
	if($row_usuario['pm9']==1){
	?>
				
				<li> <a href="semovientes/animales.php" target="mainFrame"> <img src="../images/semoviente.gif" width="40" height="30" border="0"> <br> 
					  CONTROL DE SEMOVIENTES</a>
				 	  <hr>
				</li>	

<?php
	}
	if($row_usuario['pm6']==1){
	?>	
				<li> <a href="IDO-001/IDO-001.php" target="mainFrame"> <img src="../images/libro_rojo.gif" width="36" height="30" border="0"> <br> 
					  CONTROL BIBLIOTECARIO</a>
				 	  <hr>
				</li>	
													  	<?php
	}
	if($row_usuario['pm7']==1){
	?>		
				<li> <a href="cultural/cultural.php" target="mainFrame"> <img src="../images/historico.gif" width="53" height="45" border="0"> <br> 
					  PATRIMONIO HIST�RICO Y CULTURAL</a>
				 	  <hr>
				</li>	
													  	<?php
	}
	if($row_usuario['pm5']==1){
	?>	
				
								
				<li> <a href="producto/producto.php" target="mainFrame"> <img src="../images/menu_img/productos_servicios.gif" width="30" height="30" border="0"> <br> 
					  CAT&Aacute;LOGO DE PRODUCTOS Y SERVICIOS </a>
				 	  <hr>
				</li>				
				
				<li> <a href="proveedores/IFF-018.php" target="mainFrame"> <img src="../images/menu_img/proveedores.gif" width="30" height="30" border="0"> <br> 
					  PADR&Oacute;N DE PROVEEDORES </a>
				      <hr>
				</li>
				
				<?php
	}
	?>
 				  
			
			  </ul>
								
			  </li>
			  
<!--------------------------------------- Fin del tercer men� inicio del cuarto men� ---------------------------------------------------------->
			 
  	<?php
	if($row_usuario['op']==1){
	?>
			<li><a href="#">OBRAS P&Uacute;BLICAS</a>
			  <ul>
	<?php
	}
	if($row_usuario['op1']==1){
	?>
			  
					<li> <a href="../entrega/IOP/IOP-001/IOP-001.php" target="mainFrame"><img src="../images/menu_img/obras_proceso.gif" width="30" height="30" border="0"> <br> 
					  RELACI&Oacute;N DE OBRAS EN PROCESO </a>
					  <hr>
				</li>																					
					
			  </ul>
			  </li>
			 
<!--------------------------------------- Fin del cuarto men� inicio del quinto men� ---------------------------------------------------------->
			 
    <?php
	}
	if($row_usuario['tr']==1){
	?>
			<li><a href="#">TESORER&Iacute;A</a>
			  <ul>
	<?php
	}
	if($row_usuario['tr4']==1){
	?>			  
					<li> <a href="tesoreria/info_contable.php" target="mainFrame"> <img src="../images/menu_img/contable.gif" width="30" height="30" border="0"> <br> 
					  INFORMACI&Oacute;N CONTABLE</a>
					  <hr>
	<?php
	}
	if($row_usuario['tr2']==1){
	?>					  
				</li>																					
					<li> <a href="tesoreria/info_financiera.php" target="mainFrame"> <img src="../images/menu_img/info_financiera.gif" width="30" height="30" border="0"> <br> 
					  INFORMACI&Oacute;N FINANCIERA </a>
					  <hr>
	<?php
	}
	if($row_usuario['tr1']==1){
	?>					  
				</li>
				 	<li> <a href="tesoreria/info_catastral.php" target="mainFrame"> <img src="../images/menu_img/info_catastral.gif" width="30" height="30" border="0"> <br> 
					  INFORMACI&Oacute;N CATASTRAL </a>
				      <hr>
	<?php
	}
	if($row_usuario['tr5']==1){
	?>
					  
				</li>
				 	<li> <a href="producto/producto.php" target="mainFrame"> <img src="../images/menu_img/productos_servicios.gif" width="30" height="30" border="0"> <br> 
					  CAT&Aacute;LOGO DE PRODUCTOS Y SERVICIOS </a>
				 	  <hr>
				</li>				
				 	<li> <a href="proveedores/IFF-018.php" target="mainFrame"> <img src="../images/menu_img/proveedores.gif" width="30" height="30" border="0"> <br> 
					  PADR&Oacute;N DE PROVEEDORES </a>
				      <hr>
	<?php
	}
	if($row_usuario['tr3']==1){
	?>					  
				</li>
				 	<li> <a href="requisicion/requsicion.php" target="mainFrame"> <img src="../images/menu_img/requisiciones.gif" width="30" height="30" border="0"> <br> 
					  REQUISICIONES</a>
				 	  <hr>
				</li>				
			  </ul>
			  </li>


<!--------------------------------------- Fin del quinto men� inicio del sexto men� ---------------------------------------------------------->
	<?php
	}
	 if ($row_usuario['er']==1){
	?>

			<li><a href="#">ENTREGA-RECEPCI&Oacute;N</a>
			  <ul>
	<?php
	}
	 if ($row_usuario['er1']==1){
	?>			  
					<li> <a href="dependencias/config1.php" target="mainFrame"> <img src="../images/menu_img/er_conf_inicial.gif" width="30" height="30" border="0"> <br> 
					  CONFIGURACI&Oacute;N DE FORMATOS POR DEPENDENCIA (INICIAL)</a>
					  <hr>
				</li>																					
					<li><a href="dependencias/config2.php" target="mainFrame"> <img src="../images/menu_img/er_modif_config.gif" width="30" height="30" border="0"> <br> 
					  MODIFICAR LA CONFIGURACI&Oacute;N INICIAL </a>
					  <hr>
	<?php
	}
 	if ($row_usuario['er5']==1){
	?>					  
				</li>
				 	<li> <a href="../entrega/menu_entrega.php" target="mainFrame"> <img src="../images/menu_img/er_formatos.gif" width="31" height="30" border="0"> <br> 
					  FORMATOS DE ENTREGA RECEPCI&Oacute;N</a>
				      <hr>
    <?php
	}
 	if ($row_usuario['er3']==1){
	?>					  
				</li>
				 	<li> <a href="../entrega/acta.php" target="mainFrame"> <img src="../images/menu_img/er_acta.gif" width="31" height="30" border="0"> <br> 
					  ACTA DE ENTREGA RECEPCI&Oacute;N</a>
				 	  <hr>
	<?php
	}
	 if ($row_usuario['er7']==1){
	?>					  
				</li>	
				 	<li> <a href="../Generalidades y Acta de Entrega Recepcion.pdf" target="_blank"> <img src="../images/menu_img/osfem.gif" border="0"> <br> 
					  DOCUMENTACI&Oacute;N OFICIAL </a>
				 	  <hr>
				</li>							
			  </ul>
			  </li>
	

<!--------------------------------------- Fin del quinto men� inicio del sexto men� ---------------------------------------------------------->

	<?php
	}
	?> 
			<li><a href="#">SALIR DEL SISTEMA</a>
			  <ul>
					<li> <a href="<?php echo $logoutAction ?>" target="_parent"> <img src="../images/menu_img/salir.gif" width="31" height="30" border="0"> <br> 
					  CERRAR SESI&Oacute;N Y SALIR DEL CREG V.2007 </a>
					  <hr>
				</li>																							
			  </ul>
			  </li>


<!--------------------------------------- Fin del quinto men� inicio del sexto men� ---------------------------------------------------------->
  </ul>
</div>
		<!-- END OF MENU -->




<a href="http://www.iidesoft.com.mx" target="_blank">
<img src="../images/LOGO_menu.gif" alt="Inform&aacute;tica, Ingenier&iacute;a y Desarrollo de Software | M&eacute;xico" width="141" height="46" border="0" />
</a>
</body>
</html>
