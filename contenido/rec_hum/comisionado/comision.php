<?php
	require_once('../../../Connections/bd2.php');
	session_start();
	
	if (!isset($_SESSION['MM_Username'])){
		header("Location: ../../../adios.php");
	}
	
	$colname_inventa = "1";
	if (isset($_SESSION['clave_dependencia'])) {
		$colname_inventa = (get_magic_quotes_gpc()) ? $_SESSION['clave_dependencia'] : addslashes($_SESSION['clave_dependencia']);
	}
	
	mysql_select_db($database_bd2, $bd2);
	$query_depend = "SELECT clave, clave_dep, nombre FROM `dependencia` WHERE mostrar='1' AND (tipo='G' OR tipo='P') ORDER BY tipo ASC, clave_dep ASC";
	$depend = mysql_query($query_depend, $bd2) or die(mysql_error());
	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Personal Comisionado</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		
		<script language="javascript" type="text/javascript">
			
			function MM_goToURL() { //v3.0
				var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
				for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}
			
			function pasar(valor){
				document.form1.oculto.value=valor;
				document.form3.oculto2.value=valor
			}
			
			function pasa2(val){
				document.form3.mod.value=val.value;
				document.form2.elim.value=val.value;
			}
			
			function confirma (valor){
				var val=valor;
				if (confirm ("�Desea eliminar este registro?" + val)){
					//cargarPagina('llenacombo.php?tipo=' + codigoTipo,'divCentral','iframeCentral');
					MM_goToURL('self', 'elimina2.php?oculto=' + val);
					//open("elimina2.php?oculto=valor");
				}
			}
			
			function pasa(valor, destino){
				destino.value=valor.value;
				/*if document.form1.opcion.selectedIndex != 0{
					document.form1.criterio.readonly=false;
				}*/
			}
			
			
		</script>
		<script language="javascript" src="../../../js/validate.js"></script>
		
		<link href="../../../css/idots.css" rel="stylesheet" type="text/css">
		
	</head>
	
	<body>
		
		<!--p align="center" class="divSideboxHeader style1">RELACI&Oacute;N DE PERSONAL COMISIONADO </p-->
		<div align="center"><h2 style="color: #0404B4"> RELACI&Oacute;N DE PERSONAL COMISIONADO </h2></div>
		<table width="55%"  border="1" align="center" cellpadding="3" cellspacing="0">
			<tr bgcolor="#FFFFFF">
				<td width="30%"><div align="center"><strong><a href="comision1.php">Agregar un nuevo Registro</a></strong></div></td>
				<!--td>&nbsp;</td-->
			</tr>
		</table>
		
		<table width="80%"  border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
		</table>
		
		<form name="form2" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" autocomplete="off">
			<div align="center">
				<table width="55%"  border="1" align="center" cellpadding="2" cellspacing="0">
					<tr>
						<td width="54%"><div align="right">Seleccione la Unidad Administrativa: </div></td>
						<td width="41%" align="center" valign="middle" bgcolor="#FFFFFF">
							<div align="left">
								<p id="dependencia" name="dependencia" onchange="pasa(this, opt2);">
									<select name="dependencia" id="dependencia" onChange="pasa(this, opt2);">
										<option value="0" selected>Ver Todos</option>
										<?php while ($row_depend = mysql_fetch_assoc($depend)) { ?>
											<option value="<?php echo $row_depend['clave_dep']; ?>"><?php echo $row_depend['clave_dep'].' - '.$row_depend['nombre']; ?></option>
										<?php } ?>
									</select>
								</p>
							</div></td>
							<td width="5%"><div align="center"><a href="#" class="hintanchor" onMouseOver="showhint('<center>Selecciona la Unidad Administrativa a la que pertenece el trabajador.</center>', this, event, '150px')">[?]</a></div></td>
					</tr>
				</table>
			</div>
			<p align="center">
				<input type="submit" name="Submit" value="Buscar">
				<input name="opt2" type="hidden" id="opt2">
			</p>
		</form>
		
		<form name="form1" method="post" action="">
			
			<?php
				if (!isset($_POST['opt2'])){	
					$_POST['opt2']=0;
				}
			?>
			<?php
				if ($_POST['opt2']=='0' || $_POST['opt2']==''){
					//echo $_POST['opt2']." <-- <br>";
				?>
				<!--<p align="center">Seleccione el trabajador: </p>-->
				
				<table width="100%"  border="1" align="center" cellpadding="3" cellspacing="0">
					<tr class="divSideboxHeader style1">
						<td width="5%">No. Prog.</td>
						<td width="21%"><strong>Nombre</strong></td>
						<td width="17%"><strong>Cargo</strong></td>
						<td width="24%"><strong>Adscripci&oacute;n Original </strong></td>
						<td width="21%"><strong>Comisionado en</strong></td>
						<td width="6%"><div align="center"><a href="#" class="hintanchor" onMouseOver="showhint('<center> Presione este �cono para <b>modificar</b> el elemento. </center>', this, event, '150px')">[?]</a></div></td>
						<td width="6%"><div align="center"><a href="#" class="hintanchor" onMouseOver="showhint('<center> Presione este �cono para <b>eliminar</b> el elemento. </center>', this, event, '150px')">[?]</a></div></td>
					</tr>
					<?php 
						
						mysql_select_db($database_bd2, $bd2);
						$query_todos = "SELECT b.nombre_comp as nombre_comp,  b.cargo as cargo, c.nombre as area, a.comision as comision, a.clave as clave  FROM `irh-002` a, trabajador b, dependencia c where a.clave_trab=b.curp and c.clave_dep=a.original ORDER BY a.clave ASC";
						
						//echo $query_todos;
						
						$todos = mysql_query($query_todos, $bd2) or die(mysql_error());
						$row_todos = mysql_fetch_assoc($todos);
						$totalRows_todos = mysql_num_rows($todos);				
						$i=1; 
						
						do { 
							$comis=$row_todos['comision']; 
							
							/*		mysql_select_db($database_bd2, $bd2);
								$query_todos2 = "SELECT nombre as area2 FROM dependencia  where clave_dep='$comis' ORDER BY clave ASC";
								$todos2 = mysql_query($query_todos2, $bd2) or die(mysql_error());
								$row_todos2 = mysql_fetch_assoc($todos2);
							$totalRows_todos2 = mysql_num_rows($todos2);*/
						?>
						<tr bgcolor="#FFFFFF">
							<td><div align="center"><?php echo $i; ?>
							</div></td>
							<td><div align="left"><?php echo $row_todos['nombre_comp']; ?></div></td>
							<td><?php echo $row_todos['cargo']; ?></td>
							<td><div align="left"><?php echo $row_todos['area']; ?></div></td>
							<td><?php echo $row_todos['comision']; ?></td>
							<td><div align="center"><a href="modifica.php?oculto2=<?php echo $row_todos['clave']; ?>" ><img src="../../../images/edit.png" width="16" height="16" border="0"></a></div></td>
							<td><div align="center"><a href="#" onClick="confirma(<?php echo $row_todos['clave']; ?>);"><img src="../../../images/delete.png" width="16" height="16" border="0"></a></div></td>
						</tr>
					<?php $i++; } while ($row_todos = mysql_fetch_assoc($todos)); ?>
				</table>
				
				<?php
					mysql_free_result($todos);
				}
				//if ($_POST['opt2']!='0'){
				else{
					//echo $_POST['opt2'];
					$dep=$_POST['opt2'];
				?>
				
				<p align="center">Seleccione el trabajador: </p>
				
				<table width="100%"  border="1" align="center" cellpadding="3" cellspacing="0">
					<tr class="divSideboxHeader style1">
						<td width="5%">No. Prog.</td>
						<td width="21%"><strong>Nombre</strong></td>
						<td width="17%"><strong>Cargo</strong></td>
						<td width="24%"><strong>Adscripci&oacute;n Original </strong></td>
						<td width="21%"><strong>Comisionado en</strong></td>
						<td width="6%"><div align="center"><a href="#" class="hintanchor" onMouseOver="showhint('<center> Presione este �cono para <b>eliminar</b> el elemento. </center>', this, event, '150px')">[?]</a></div></td>
					</tr>
					<?php 
						
						mysql_select_db($database_bd2, $bd2);
						//$query_todos = "SELECT b.nombre_comp as nombre_comp,  b.cargo as cargo, c.nombre as area, a.comision as comision, a.clave as clave  FROM `irh-002` a, trabajador b, dependencia c where a.original='$dep' and a.clave_trab=b.numero and c.clave=a.original ORDER BY a.clave ASC";
						
						$query_todos = "SELECT b.nombre_comp AS nombre_comp, b.cargo AS cargo, c.nombre AS area, a.comision AS comision, a.clave AS clave FROM `irh-002` a, trabajador b, dependencia c WHERE a.original = '$dep' AND a.clave_trab = b.curp AND c.clave_dep = a.original ORDER BY a.clave ASC";
						
						//echo $query_todos;
						
						$todos = mysql_query($query_todos, $bd2) or die(mysql_error());
						$row_todos = mysql_fetch_assoc($todos);
						$totalRows_todos = mysql_num_rows($todos);				
						$i=1; 
						
						do { 
							$comis=$row_todos['comision']; 
							
							/*mysql_select_db($database_bd2, $bd2);
								$query_todos2 = "SELECT nombre as area2 FROM dependencia  where clave_dep='$comis' ORDER BY clave ASC";
								$todos2 = mysql_query($query_todos2, $bd2) or die(mysql_error());
								$row_todos2 = mysql_fetch_assoc($todos2);
							$totalRows_todos2 = mysql_num_rows($todos2);*/
						?>
						<tr bgcolor="#FFFFFF">
							<td><div align="center"><?php echo $i; ?>
							</div></td>
							<td><div align="left"><?php echo $row_todos['nombre_comp']; ?></div></td>
							<td><?php echo $row_todos['cargo']; ?></td>
							<td><div align="left"><?php echo $row_todos['area']; ?></div></td>
							<td><?php echo $row_todos['comision']; ?></td>
							<td><div align="center"><a href="#" onClick="confirma(<?php echo $row_todos['clave']; ?>);"><img src="../../../images/delete.png" width="16" height="16" border="0"></a></div></td>
						</tr>
					<?php $i++; } while ($row_todos = mysql_fetch_assoc($todos)); ?>
				</table>
				
				<?php
					mysql_free_result($todos);
				}
				
			?>
		<p align="center">&nbsp;</p></td>
		
	</div>
</form>

<!--<form name="form2" method="post" action="">
	<p align="left">&nbsp;  </p>
	<table width="100%"  border="1" align="center" cellpadding="0" cellspacing="0">
	<tr>
	<td><p align="center">
	<input type="checkbox" name="checkbox" value="checkbox">
	Reporte Finalizado
	<input name="acepta" type="submit" id="acepta" value="Aceptar">
	</p>
	<p align="center"><strong><span class="Estilo1">Nota:</span> Al finalizar el reporte, ya no tendr&aacute; acceso a agregar, modificar o eliminar registros </strong></p></td>
	</tr>
	</table>
	<p align="left">&nbsp;  </p>
</form>-->
<p align="center">&nbsp; </p>
</body>
</html>
