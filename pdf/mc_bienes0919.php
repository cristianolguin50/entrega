<?php
require('fpdf.php');

class PDF_MC_Table extends FPDF
{
var $widths;
var $aligns;
var $encabezado;
var $alinea;

function Header()
{
global $dia;
global $mes;
global $year;
global $title;
global $area;
global $ayuntam;
global $mpo;
global $usr;
global $puesto;
global $srio;
//global $encabezado;
    //Logo
	//$this->SetTopMargin(2);
	$this->SetMargins(5,2);
    $this->Image('../../../images/azul2.jpg',10,10,20);
    //Arial bold 15
    $this->SetFont('Arial','',8);
    //Movernos a la derecha
    $this->Image('../../../images/OSFEM.jpg', 390, 8, 20);
	$this->SetY(7);
	$this->Cell(130);
	$this->Cell(25,4,'AYUNTAMIENTO',0,0,'R');
	$this->Cell(10,4,'X',1,0,'C');
	$this->Cell(60);
	$this->Cell(25,4,'FECHA:',0,0,'R');
	$this->SetFont('Arial','B',8);
	$this->Cell(35,4,$dia . "/" . $mes . "/" . $year ,0,0,'L');
	$this->SetFont('Arial','',8);
	$this->Ln(5);
	$this->Cell(45);
	$this->Cell(25,4,'MUNICIPIO:',0,0,'R');
	$this->SetFont('Arial','B',8);
	$this->Cell(35,4,$ayuntam,0,0,'L');
	$this->SetFont('Arial','',8);
	$this->Cell(25);
	$this->Cell(25,4,'ODAS',0,0,'R');
	$this->Cell(10,4,'',1,0,'C');
	$this->Cell(60);
	$this->Cell(25,4,'ELABOR�:',0,0,'R');
	$this->SetFont('Arial','B',8);
	$this->Cell(25,4,$usr,0,0,'');
	$this->SetFont('Arial','',8);
	$this->Cell(30);
	$this->Cell(25,4,'CARGO:',0,0,'R');
	$this->SetFont('Arial','B',8);
	$this->Cell(25,4,$puesto,0,0,'L');
	$this->SetFont('Arial','',8);
	$this->Ln(5);
	$this->Cell(45);
	$this->Cell(25,4,'N�MERO:',0,0,'R');
	$this->SetFont('Arial','B',8);
	$this->Cell(35,4,$mpo,0,0,'L');
	$this->SetFont('Arial','',8);
	$this->Cell(25);
	$this->Cell(25,4,'DIF',0,0,'R');
	$this->Cell(10,4,'',1,0,'C');
	$this->Cell(60);
	$this->Cell(25,4,'REVIS�:',0,0,'R');
	$this->SetFont('Arial','B',8);
	$this->Cell(25,4,$srio,0,0,'');
	$this->SetFont('Arial','',8);
	$this->Cell(30);
	$this->Cell(25,4,'CARGO:',0,0,'R');
	$this->SetFont('Arial','B',8);
	$this->Cell(25,4,'SECRETARIO DEL AYUNTAMIENTO',0,0,'L');
	$this->SetFont('Arial','',8);
	$this->Ln(5);
	$this->Cell(130);
	$this->Cell(25,4,'OTROS',0,0,'R');
	$this->Cell(10,4,'',1,0,'C');
	$this->Cell(25,4,'ESPECIFICAR',0,0,'R');
	$this->Cell(35,4,'',0,0,'L');
//	$this->Cell(10);	
	$this->Cell(25,4,'HOJA:',0,0,'R');
	$this->SetFont('Arial','B',8);
	$this->Cell(30,4,$this->PageNo().' DE {nb}',0,0,'L');
	$this->SetFont('Arial','',8);	
	$this->Ln(6);
	$this->SetFont('Arial','B',15);
	$this->SetFillColor(27,107,40);
	$this->SetTextColor(255,255,255);
	
    //Calculamos ancho y posici�n del t�tulo.
/*     $w=$this->GetStringWidth($title)+6;
	$this->SetX((275-$w)/2);
	$this->Cell($w,9,$title,0,1,'C'); */
	$this->MultiCell(411,6,$title,1,'C', true);
    //Salto de l�nea
//	$this->Ln(3.5);
	
	
   $this->Ln(5);
}

//Pie de p�gina
function Footer()
{
	global $usr;
	global $forma;
	global $var;
	global $dia;
	global $mes;
	global $year;
	global $ent;
	global $presi;
	global $srio;
	global $sind;
	global $tesor;
	global $cont;
    //Posici�n: a 1,5 cm del final
    $this->SetY(-35);	
	$this->SetFont('Arial','B',8);
	$this->SetLineWidth(0.2);
    //Arial italic 8
	$this->Cell(10);
	$this->Cell(65, 0, $presi, 0, 0, 'C');
	$this->Cell(15);
	$this->Cell(65, 0, $srio, 0, 0, 'C');
	$this->Cell(15);
	$this->Cell(65, 0, $sind, 0, 0, 'C');
	$this->Cell(15);
	$this->Cell(65, 0, $tesor, 0, 0, 'C');
	$this->Cell(15);
	$this->Cell(65, 0, $cont, 0, 0, 'C');
	$this->Line(15, 265, 80, 265);
	$this->Line(95, 265, 160, 265);
	$this->Line(175, 265, 240, 265);
	$this->Line(255, 265, 320, 265);
	$this->Line(335, 265, 400, 265);
	$this->Ln(5);
	$this->Cell(10);
	$this->Cell(65, 1, 'PRESIDENTE', 0,0, 'C');
	$this->Cell(15);
	$this->Cell(65, 1, 'SECRETARIO', 0,0, 'C');
	$this->Cell(15);
	$this->Cell(65, 1, 'S�NDICO', 0,0, 'C');
	$this->Cell(15);
	$this->Cell(65, 1, 'TESORERO', 0,0, 'C');
	$this->Cell(15);
	$this->Cell(65, 1, 'CONTRALOR', 0,0, 'C');
	$this->SetLineWidth(0.5);
	//$this->Line(10, 180, 270, 180);
	$this->Ln(11);
	$this->SetFont('Arial','B',8);
	$this->MultiCell(300,3,$var,0,'L');
	
	//$this->Cell(70
	$this->Ln(5);
}



function SetWidths($w)
{
    //Set the array of column widths
    $this->widths=$w;
}

function SetAligns($a)
{
    //Set the array of column alignments
    $this->aligns=$a;
}
function Recibe($rec)
{
	//global $encabezado;
	$this->encabezado= $rec;
	//$this->Row($encabezado);
    //Set the array of column widths
    //$this->widths=$w;
}
function Alinea($alig)
{
	//global $encabezado;
	$this->alinea= $alig;
	//$this->Row($encabezado);
    //Set the array of column widths
    //$this->widths=$w;
}

function Row($data)
{
    //Calculate the height of the row
    $nb=0;
    for($i=0;$i<count($data);$i++)
        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
    $h=3*$nb;
    //Issue a page break first if needed
    $this->CheckPageBreak($h);
    //Draw the cells of the row
    for($i=0;$i<count($data);$i++)
    {
        $w=$this->widths[$i];
        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : $this->alinea;
        //Save the current position
        $x=$this->GetX();
        $y=$this->GetY();
        //Draw the border
        $this->Rect($x,$y,$w,$h);
        //Print the text
        $this->MultiCell($w,3,$data[$i],0,$a);
        //Put the position to the right of the cell
        $this->SetXY($x+$w,$y);
    }
    //Go to the next line
    $this->Ln($h);
	$this->SetLineWidth(0.2);
}

function CheckPageBreak($h)
{
    //If the height h would cause an overflow, add a new page immediately
    //if($this->GetY()+$h>$this->PageBreakTrigger){
	if($this->GetY()+$h>250){
        $this->AddPage($this->CurOrientation);
$this->SetFont('Arial','B',5);
	$this->SetFillColor(27,107,40);
	$this->SetTextColor(255,255,255);

$y=$this->GetY();
$this->SetLineWidth(0.4);
//$this->Line(10, $y, 270, $y);
$this->SetLineWidth(0.2);
$this->MultiCell(10,3,"N�M PROG",1,'C', true);
$this->SetXY(15,$this->GetY()-6);
$this->MultiCell(15,2,"N�M. DE LA PARTIDA DE EGRESOS",1,'C', true);
/*$this->SetXY(23,$this->GetY()-6);
$this->MultiCell(14,3,"N�M. DE SUBCUENTA",1,'C', true);
$this->SetXY(37,$this->GetY()-6);
$this->MultiCell(14,3,"N�M. DE SUB SUBCUENTA",1,'C', true);*/
$this->SetXY(30,$this->GetY()-6);
$this->MultiCell(30,6,"NOMBRE DE LA PARTIDA",1,'C', true);
$this->SetXY(60,$this->GetY()-6);
$this->MultiCell(15,3,"N�MERO DE INVENTARIO",1,'C', true);
$this->SetXY(75,$this->GetY()-6);
$this->MultiCell(15,3,"N�M. DE RESGUARDO",1,'C', true);
$this->SetXY(90,$this->GetY()-6);
$this->MultiCell(25,3,"NOMBRE DEL RESGUARDATARIO",1,'C', true);
$this->SetXY(115,$this->GetY()-6);
$this->MultiCell(25,6,"NOMBRE DEL MUEBLE",1,'C', true);
$this->SetXY(140,$this->GetY()-6);
$this->MultiCell(16,6,"MARCA",1,'C', true);
$this->SetXY(156,$this->GetY()-6);
$this->MultiCell(16,6,"MODELO",1,'C', true);
$this->SetXY(172,$this->GetY()-6);
/*$this->MultiCell(15,3,"N�MERO DE MOTOR",1,'C', true);
$this->SetXY(190,$this->GetY()-6);*/
$this->MultiCell(15,3,"N�MERO DE SERIE",1,'C', true);
$this->SetXY(187,$this->GetY()-6);
$this->MultiCell(15,3,"ESTADO DE USO",1,'C', true);
$this->SetXY(202,$this->GetY()-6);
$this->MultiCell(67,3,"FACTURA",1,'C', true);
$this->SetX(202);
$this->MultiCell(15,3,"N�MERO",1,'C', true);
$this->SetXY(217,$this->GetY()-3);
$this->MultiCell(15,3,"FECHA",1,'C', true);
$this->SetXY(232,$this->GetY()-3);
$this->MultiCell(25,3,"PROVEEDOR",1,'C', true);
$this->SetXY(257,$this->GetY()-3);
$this->MultiCell(12,3,"COSTO",1,'C', true);
$this->SetXY(269,$this->GetY()-6);
$this->MultiCell(36,3,"POLIZA",1,'C', true);
$this->SetX(269);
$this->MultiCell(12,3,"TIPO",1,'C', true);
$this->SetXY(281,$this->GetY()-3);
$this->MultiCell(12,3,"N�MERO",1,'C', true);
$this->SetXY(293,$this->GetY()-3);
$this->MultiCell(12,3,"FECHA",1,'C', true);
$this->SetXY(305 ,$this->GetY()-6);
$this->MultiCell(12,6,"RECURSO",1,'C', true);
$this->SetXY(317,$this->GetY()-6);
$this->MultiCell(28,3,"MOVIMIENTOS",1,'C', true);
$this->SetX(317);
$this->MultiCell(14,3,"FECHA/ALTA",1,'C', true);
$this->SetXY(331,$this->GetY()-3);
$this->MultiCell(14,3,"FECHA/BAJA",1,'C', true);
$this->SetXY(345,$this->GetY()-6);
$this->MultiCell(22,6,"�REA RESPONSABLE",1,'C', true);
$this->SetXY(367,$this->GetY()-6);
$this->MultiCell(25,6,"LOCALIDAD",1,'C', true);
$this->SetXY(392,$this->GetY()-6);
$this->MultiCell(25,6,"OBSERVACIONES",1,'C', true);


$this->SetTextColor("black");

$this->SetFont('Arial','',5);

	}
}

function NbLines($w,$txt)
{
    //Computes the number of lines a MultiCell of width w will take
    $cw=&$this->CurrentFont['cw'];
    if($w==0)
        $w=$this->w-$this->rMargin-$this->x;
    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
    $s=str_replace("\r",'',$txt);
    $nb=strlen($s);
    if($nb>0 and $s[$nb-1]=="\n")
        $nb--;
    $sep=-1;
    $i=0;
    $j=0;
    $l=0;
    $nl=1;
    while($i<$nb)
    {
        $c=$s[$i];
        if($c=="\n")
        {
            $i++;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
            continue;
        }
        if($c==' ')
            $sep=$i;
        $l+=$cw[$c];
        if($l>$wmax)
        {
            if($sep==-1)
            {
                if($i==$j)
                    $i++;
            }
            else
                $i=$sep+1;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
        }
        else
            $i++;
    }
    return $nl;
}
}
?>