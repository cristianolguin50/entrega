<?php
require('fpdf.php');
require('ufpdf.php');

class PDF_MC_Table extends FPDF
{
var $widths;
var $aligns;
var $encabezado;
var $alinea;

function Header()
{
global $title;
global $area;
global $ayuntam;
global $adminIni;
global $adminFin;
global $tipoEntidad;
//global $encabezado;
    //Logo
	//$this->SetTopMargin(2);
    $this->Image('../../../images/azul2.jpg',10,10,20);
    //Arial bold 15
    $this->SetFont('Arial','B',10);
    //Movernos a la derecha
	/*Todo este bloque*/
    //$this->Image('../../../images/image002.jpg', 200, 7, 30,20);
	$this->SetY(11);
	$this->Cell(25);
	$this->Cell(75,1,'',0,0,'L');
	$this->Cell(80);
    $this->Cell(75,1,'',0,0,'L');
	$this->Ln(3.5);
	$this->Cell(25);
	$this->Cell(75,1,'',0,0,'L');
	$this->Cell(80);
	$this->Cell(75,1,'',0,0,'L');
	$this->Ln(3.5);
	$this->Cell(25);
	$this->Cell(75,1,'',0,0,'L');
	$this->Cell(80);
	$this->Cell(75,1,'',0,0,'L');
	$this->Ln(3.5);
	$this->Cell(180);
	$this->SetFont('Arial','B',7);
	/*Todo este bloque*/
	
	$this->SetXY(178,28); //Aqui
	//$this->Cell(75,1,'PROGRAMA DE ENTREGA RECEPCI�N MUNICIPAL',0,0,'L'); //Aqui
	$this->SetXY(180,21); ///Aqui
	$this->SetFont('Arial','B',10);
	$this->Ln(3.5);
	$this->SetLineWidth(0.5);
	$this->Line(10, 30, 270, 30);
	$this->Ln(6);
	$this->SetFont('Arial','B',12);
    //Calculamos ancho y posici�n del t�tulo.
  /*   $w=$this->GetStringWidth($title)+6;
	$this->SetX((275-$w)/2);
	$this->Cell($w,9,$title,0,1,'C'); */
	$this->MultiCell(240,6,$title,0,'C');	
	//Linea que va debajodel titulo del formato
	$this->SetLineWidth(0.45);
	$this->Line(10, 42, 270, 42);
	$this->Ln(3);
    //Salto de l�nea
//	$this->Ln(3.5);	
	$this->SetLineWidth(0.2);
	$this->SetFont('Arial','',10);
	$this->Cell(15,4,'MUNICIPIO: '.$ayuntam,0,0,'L');
	$this->SetFont('Arial','',10);
	$this->Cell(60);
	$this->Cell(15,4,'ENTIDAD MUNICIPAL: '.$tipoEntidad,0,0,'L');
	$this->Cell(60);
	$this->Cell(15,4,'UNIDAD ADMINISTRATIVA: '.$area,0,0,'L');
	$this->Ln(4);
	$this->Cell(150);
	$this->Cell(15,4,'FECHA DE ELABORACI�N: '.date("d/m/y"),0,0,'L');
	$this->Ln(6);
/*
global $title;
global $area;
global $ayuntam;
//global $encabezado;
    //Logo
	//$this->SetTopMargin(2);
    $this->Image('../../../images/azul2.jpg',10,10,20);
    //Arial bold 15
    $this->SetFont('Arial','B',10);
    //Movernos a la derecha
    $this->Image('../../../images/image002.jpg', 165, 8, 20);
	$this->SetY(11);
	$this->Cell(25);
	$this->Cell(75,1,'H. AYUNTAMIENTO CONSTITUCIONAL',0,0,'L');
	$this->Cell(80);
    $this->Cell(75,1,'�RGANO SUPERIOR DE FISCALIZACI�N DEL',0,0,'L');
	$this->Ln(3.5);
	$this->Cell(25);
	$this->Cell(75,1,'DE ' .$ayuntam .', ESTADO DE M�XICO',0,0,'L');
	$this->Cell(80);
	$this->Cell(75,1,'ESTADO DE M�XICO',0,0,'L');
	$this->Ln(3.5);
	$this->Cell(25);
	$this->Cell(75,1,'XXXX-XXXX',0,0,'L');
	$this->Cell(80);
	$this->Cell(75,1,'ENTREGA-RECEPCI�N DE LA ADMINISTRACI�N ',0,0,'L');
	$this->Ln(3.5);
	$this->Cell(180);
	$this->Cell(75,1,'P�BLICA MUNICIPAL',0,0,'L');
	$this->Ln(3.5);
	$this->SetLineWidth(0.5);
	$this->Line(10, 30, 270, 30);
	$this->Ln(6);
	$this->SetFont('Arial','B',15);
    //Calculamos ancho y posici�n del t�tulo.
   $this->MultiCell(260,6,$title,0,'C');
    //Salto de l�nea
//	$this->Ln(3.5);
	$this->SetLineWidth(0.2);
	$this->SetFont('Arial','',10);
	$this->Cell(4,4,'X',1,0,'C');
	$this->Cell(30,4,'AYUNTAMIENTO',0,0,'L');
	$this->Cell(4,4,'',1,0,'C');
	$this->Cell(15,4,'ODAS',0,0,'L');
	$this->Cell(4,4,'',1,0,'L');
	$this->Cell(10,4,'DIF',0,0,'L');
	$this->Cell(4,4,'',1,0,'L');
	$this->Cell(15,4,'OTROS',0,0,'L');
	$this->Cell(10);
	$this->Cell(15,4,'MUNICIPIO:',0,0,'L');
	$this->Cell(10);
	$this->SetFont('Arial','B',10);
	$this->Cell(40,4, $ayuntam, 0,0, 'L');
	$this->SetFont('Arial','',10);
	$this->Cell(15,4,'�REA:',0,0,'L');
	$this->SetFont('Arial','B',10);
	$this->MultiCell(70,4,$area,0,'L');
	$this->SetFont('Arial','',10);
*/
}

//Pie de p�gina

//-------------------------------
/*function Footer()
{
	global $usr;
	global $forma;
	global $var;
	global $dia;
	global $mes;
	global $year;
	global $ent;
    //Posici�n: a 1,5 cm del final
    $this->SetY(-55);	
	$this->SetFont('Arial','B',10);
    //Arial italic 8
	$this->Cell(50, 0, $var, 0, 0, 'L');
	$this->Ln(12);
	$this->Cell(30);
	$this->Cell(80, 0,$usr, 0, 0, 'C');
	$this->Cell(26);
	$this->Cell(80, 0, $ent, 0, 0, 'C');
	$this->Line(40, 175, 120, 175);
	$this->Line(145, 175, 225, 175);
	$this->Ln(5);
	$this->Cell(65);
	$this->Cell(10, 1, 'ENTREGA', 0,0, 'C');
	$this->Cell(95);
	$this->Cell(10, 1, 'RECIBE', 0,0, 'C');
	$this->SetLineWidth(0.5);
	$this->Line(10, 182, 270, 182);
	$this->Ln(11);
	$this->SetFont('Arial','',8);
	$this->Cell(230);*/
//---------------------------------------------
function Footer()
{
	global $usr;
	global $forma;
	global $var;
	global $dia;
	global $mes;
	global $year;
	global $ent;
    //Posici�n: a 1,5 cm del final
    $this->SetY(-55);	
	$this->SetFont('Arial','B',10);
    //Arial italic 8
	$this->Cell(50, 0, $var, 0, 0, 'L');
	$this->Ln(12);
	$this->Cell(30);
	$this->Cell(195, 0, $usr, 0, 0, 'C');
	$this->Cell(26);
	//$this->Cell(80, 0, $ent, 0, 0, 'C');
	$this->Line(100, 175, 175, 175);
	//$this->Line(145, 175, 225, 175);
	$this->Ln(5);
	$this->Cell(65);
	$this->Cell(125, 1, 'ENTREGA', 0,0, 'C');
	$this->Cell(95);
	//$this->Cell(10, 1, 'RECIBE', 0,0, 'C');
	$this->SetLineWidth(0.5);
	$this->Line(10, 182, 270, 182);
	$this->Ln(11);
	$this->SetFont('Arial','',8);
	$this->Cell(230);
	/*$this->Cell(30, 1, 'FECHA DE ELABORACI�N', 0,0, 'C');
	$this->Ln(2);
	$this->Cell(230);
	
	$this->Cell(10, 4, 'D�A', 1,0, 'C');
	$this->Cell(10, 4, 'MES', 1,0, 'C');
	$this->Cell(10, 4, 'A�O', 1,0, 'C');
	$this->Ln(4);
	$this->Cell(230);
	$this->Cell(10, 4, $dia, 1,0, 'C');
	$this->Cell(10, 4, $mes, 1,0, 'C');
	$this->Cell(10, 4, $year, 1,0, 'C');
	*/
	$this->Ln(5);
	$this->Cell(10, 4, $forma, 0,0, 'C');
	//$this->Cell(70
	$this->Ln(5);
    $this->SetFont('Arial','I',8);
    //N�mero de p�gina
    $this->Cell(0,10,'P�gina '.$this->PageNo().'/{nb}',0,0,'C');
/*
	global $usr;
	global $forma;
	global $var;
	global $dia;
	global $mes;
	global $year;
	global $ent;
    //Posici�n: a 1,5 cm del final
    $this->SetY(-55);	
	$this->SetFont('Arial','B',10);
    //Arial italic 8
	$this->Cell(50, 0, $var, 0, 0, 'L');
	$this->Ln(12);
	$this->Cell(30);
	$this->Cell(80, 0, $usr, 0, 0, 'C');
	$this->Cell(26);
	$this->Cell(80, 0, $ent, 0, 0, 'C');
	$this->Line(40, 175, 120, 175);
	$this->Line(145, 175, 225, 175);
	$this->Ln(5);
	$this->Cell(65);
	$this->Cell(10, 1, 'ELABOR�', 0,0, 'C');
	$this->Cell(95);
	$this->Cell(10, 1, 'TESORERO', 0,0, 'C');
	$this->SetLineWidth(0.5);
	$this->Line(10, 180, 270, 180);
	$this->Ln(11);
	$this->SetFont('Arial','',8);
	$this->Cell(230);
	$this->Cell(30, 1, 'FECHA DE ELABORACI�N', 0,0, 'C');
	$this->Ln(2);
	$this->Cell(230);
	$this->Cell(10, 4, 'D�A', 1,0, 'C');
	$this->Cell(10, 4, 'MES', 1,0, 'C');
	$this->Cell(10, 4, 'A�O', 1,0, 'C');
	$this->Ln(4);
	$this->Cell(230);
	$this->Cell(10, 4, $dia, 1,0, 'C');
	$this->Cell(10, 4, $mes, 1,0, 'C');
	$this->Cell(10, 4, $year, 1,0, 'C');
	$this->Ln(5);
	$this->Cell(10, 4, $forma, 0,0, 'C');
	//$this->Cell(70
	$this->Ln(5);
    $this->SetFont('Arial','I',8);
    //N�mero de p�gina
    $this->Cell(0,10,'P�gina '.$this->PageNo().'/{nb}',0,0,'C');
*/
}


function SetWidths($w)
{
    //Set the array of column widths
    $this->widths=$w;
}

function SetAligns($a)
{
    //Set the array of column alignments
    $this->aligns=$a;
}
function Recibe($rec)
{
	//global $encabezado;
	$this->encabezado= $rec;
	//$this->Row($encabezado);
    //Set the array of column widths
    //$this->widths=$w;
}
function Alinea($alig)
{
	//global $encabezado;
	$this->alinea= $alig;
	//$this->Row($encabezado);
    //Set the array of column widths
    //$this->widths=$w;
}

function Row($data)
{
    //Calculate the height of the row
    $nb=0;
    for($i=0;$i<count($data);$i++)
        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
    $h=5*$nb;
    //Issue a page break first if needed
    $this->CheckPageBreak($h);
    //Draw the cells of the row
    for($i=0;$i<count($data);$i++)
    {
        $w=$this->widths[$i];
        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : $this->alinea;
        //Save the current position
        $x=$this->GetX();
        $y=$this->GetY();
        //Draw the border
        $this->Rect($x,$y,$w,$h);
        //Print the text
        $this->MultiCell($w,5,$data[$i],0,$a);
        //Put the position to the right of the cell
        $this->SetXY($x+$w,$y);
    }
    //Go to the next line
    $this->Ln($h);
	$this->SetDrawColor(255,255,255);
	$this->SetLineWidth(1);
	$this->Line(10, 45, 10, 200);
	$this->Line(270, 45, 270, 250);
	$this->SetDrawColor(1,1,1);
	$this->SetLineWidth(0.2);
}

function CheckPageBreak($h)
{
    //If the height h would cause an overflow, add a new page immediately
    //if($this->GetY()+$h>$this->PageBreakTrigger){
	if($this->GetY()+$h>160){
        $this->AddPage($this->CurOrientation);
		$y=$this->GetY();
		$alig='C';
		$this->SetLineWidth(0.5);
		$this->Line(10, $y, 270, $y);
		$this->SetLineWidth(0.2);
		$this->SetFont('Arial','B',10);	
		$this->alinea='C';	
		$this->Row($this->encabezado, $alig);
		$y=$this->GetY();
		$this->alinea='L';	
		$this->SetLineWidth(0.5);
		$this->Line(10, $y, 270, $y);
		$this->SetLineWidth(0.2);
		$this->SetFont('Arial','',8);
	}
}

function NbLines($w,$txt)
{
    //Computes the number of lines a MultiCell of width w will take
    $cw=&$this->CurrentFont['cw'];
    if($w==0)
        $w=$this->w-$this->rMargin-$this->x;
    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
    $s=str_replace("\r",'',$txt);
    $nb=strlen($s);
    if($nb>0 and $s[$nb-1]=="\n")
        $nb--;
    $sep=-1;
    $i=0;
    $j=0;
    $l=0;
    $nl=1;
    while($i<$nb)
    {
        $c=$s[$i];
        if($c=="\n")
        {
            $i++;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
            continue;
        }
        if($c==' ')
            $sep=$i;
        $l+=$cw[$c];
        if($l>$wmax)
        {
            if($sep==-1)
            {
                if($i==$j)
                    $i++;
            }
            else
                $i=$sep+1;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
        }
        else
            $i++;
    }
    return $nl;
}
}
?>