<?php require_once('../../../Connections/bd2.php'); ?>
<?php
/*	Elaborado por: Alma Leticia Pinacho
	Fecha: 22/12/2006
	Modificado por: Wenceslao Arroyo
	Fecha: 16/01/2007  */
session_start();
if (!isset($_SESSION['MM_Username'])){
	header("Location: ../../../adios.php");
}

//Recordset de los bienes
 
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="../../../css/idots.css" rel="stylesheet" type="text/css">
<title>Alta de un nuevo registro</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" type="text/javascript">
<!--
function cargaImagen(){
	var cadena=document.form1.archivo.value;
	document.form1.foto.src=cadena;
	//document.form1.ruta2.value=cadena;
}

function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

function upper(field){
field.value=field.value.toUpperCase();
}

function pasa(valor){
	document.form1.uso.value=valor;
	/*if document.form1.opcion.selectedIndex != 0{
		document.form1.criterio.readonly=false;
	}*/
}

function pasatipo(valor){

if (valor==1){
	document.form1.tipo1.value="A"
}

else if (valor==2){
	document.form1.tipo1.value="B"
}
else if (valor==3){
	document.form1.tipo1.value="C"
}
else if (valor==4){
	document.form1.tipo1.value="D"
}
}
//-->
function validate(valor){
var cad1=document.form1.clave_puesto.value;
var cad2=document.form1.nombre.value;
var cad3=document.form1.sueldo.value;
var cad4=document.form1.tipo.value;
var lon1;
var lon2;
var lon3;
var lon4;
lon1=cad1.length;
lon2=cad2.length;
lon3=cad3.length;
lon4=cad4.length;
if((lon1>0) && (lon2>0) && (lon3>0)){
	document.form1.guarda.disabled=false;
	}
if ((lon1<=0) || (lon2<=0) || (lon3<=0)){
document.form1.guarda.disabled=true;
}
}

function numer(valor){

if ((isNaN(valor.value)) || (valor.value=="")){
	valor.value=0;
}

}


</script>
<script language="JavaScript" src="_scripts.js" type="text/javascript"></script>
<script language="javascript" src="../../../js/validate.js" type="text/javascript"></script>
<script language="javascript" src="../../../js/cal2.js" type="text/javascript"></script>
<script language="javascript" src="../../../js/cal_conf2.js" type="text/javascript"></script>
<style type="text/css">
<!--
body {
	margin-top: 0px;
}
.style1 {
	font-size: 110%;
	font-weight: bold;
}
.style2 {
	color: #FF0000;
	font-style: italic;
}
-->
</style></head>

<body onLoad="document.form1.clave_puesto.focus();">

<p align="center" class="divSideboxHeader style1">CAT&Aacute;LOGO DE PUESTOS</p>
<form name="form1" method="post" action="guarda.php" enctype="multipart/form-data">
  <div align="center">
    <table width="90%"  border="1" align="center" cellpadding="3" cellspacing="0">
      <tr>
        <td width="40%"><div align="right"><strong>*Clave:</strong></div></td>
        <td bgcolor="#FFFFFF">
          <div align="left">
            <input name="clave_puesto" type="text" id="clave" tabindex="1" onBlur="validate();" onKeyPress="return pulsar(event)" onKeyUp="return handleEnter(this, event)" size="35">
          <a href="#" class="hintanchor" onMouseOver="showhint('<center> Clave con la que se identifica el puesto <i>Este campo es obligatorio</i> </center>', this, event, '150px')">[?]</a>          </div></td></tr>
      <tr>
        <td><div align="right"><strong>*Nombre del Puesto:</strong></div></td>
        <td bgcolor="#FFFFFF"><div align="left">
          <input name="nombre" type="text" id="nombre_de_plaza" tabindex="2" onBlur="validate();" onKeyPress="return pulsar(event)" onKeyUp="return handleEnter(this, event)" size="35">
		  <a href="#" class="hintanchor" onMouseOver="showhint('<center> Autor o autores <i>Este campo es obligatorio</i>  </center>', this, event, '150px')">[?]</a>

        </div></td>
      </tr>
      <tr>
        <td><div align="right"><strong>*Sueldo M&iacute;nimo:</strong></div></td>
        <td bgcolor="#FFFFFF">
          <div align="left">
            <input name="sueldo" type="text" id="sueldo" tabindex="3" onBlur="validate(), numer(this);" onKeyPress="return pulsar(event)" onKeyUp="return handleEnter(this, event)" size="20">
		  <a href="#" class="hintanchor" onMouseOver="showhint('<center> Editorial que publica la obra <i>Este campo es obligatorio</i> </center>', this, event, '150px')">[?]</a>          </div></td></tr>
      <tr>
        <td><p align="right"><strong>Tipo:</strong></p>        </td>
        <td bgcolor="#FFFFFF"><div align="left">
          <label>
          <select name="tipo" id="tipo" onKeyPress="return handleEnter(this, event)" onBlur="validate();">
            <option value="ADM">Administrativo</option>
            <option value="OPE">Operativo</option>
            <option value="SPE">Seguridad Publica</option>
          </select>
          </label>
        </div></td>
      </tr>
      <tr>
        <td colspan="2" valign="top"><div align="center"><span class="style2"><strong>Nota</strong>: Los campos marcados con * son obligatorios</span></div></td>
      </tr>
    </table>
    <p>&nbsp;</p>
  </div>
  <p align="center">
    <input name="Button" type="button" onClick="window.close()" value="Cerrar">
    <input name="limpia" type="reset" id="limpia" value="Limpiar Formulario">
    <input name="guarda" type="submit" id="guarda" value="Guardar datos" tabindex="8" disabled="true">
  </p>
</form>
<p align="center">&nbsp; </p>
</body>
</html>
