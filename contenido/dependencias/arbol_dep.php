<?php
	
	/*	Modificado por: Marco Robles
	Fecha: 20/09/2017	*/
	
	require('arbol.php');
	require("../../Connections/bd2.php");
	
	mysql_select_db($database_bd2, $bd2);	
	$query_inventa = "SELECT clave, clave_dep, nombre, depende FROM `dependencia` WHERE tipo='G' AND mostrar='1' ORDER BY clave ASC";
	$dtSocPrincipal = mysql_query($query_inventa, $bd2) or die(mysql_error());
	$num_principal = mysql_num_rows($dtSocPrincipal);
	
	$arrTotdo=array();
	$i = 0;
	
	//$arrTotdo[n][0] = nivel profundidad
	//$arrTotdo[n][1] = numero socio
	//$arrTotdo[n][2] = dependencia
	//$arrTotdo[n][3] = nombre socio
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Arbol de Unidades Administrativas</title>
		
		<link rel="stylesheet" type="text/css" href="../../js/jquery.treeview/jquery.treeview.css">
		<link rel="stylesheet" type="text/css" href="../../js/jquery.treeview/screen.css">
		
		
		<link rel="stylesheet" type="text/css" href="../../css/idots.css"/>
		<script type="text/javascript" src="../../js/jquery-1.7.1.js"></script>
		<script src="../../js/jquery.treeview/lib/jquery.cookie.js" type="text/javascript"></script>
		<script src="../../js/jquery.treeview/jquery.treeview.js" type="text/javascript"></script>
		
		<script type="text/javascript">
			$(function() {
				$(".tree").treeview({
					collapsed: true,
					animated: "medium",
					control:"#treecontrol",
					persist: "location"
				});
			})		
		</script>
		
	</head>
	<body>
		<p>&nbsp;</p>
		<div id="form_container">
			
			<?php
				
				while($row_principal=mysql_fetch_array($dtSocPrincipal)){
					//Para que el arreglo tenga incluidas las dependenicas que no tienen hijos
					$arrTotdo[$i][0] = 0;//Nivel profundidad
					$arrTotdo[$i][1] = $row_principal['clave_dep'];
					$arrTotdo[$i][2] = $row_principal['depende'];
					$arrTotdo[$i][3] = $row_principal['nombre'];	
					$i++;
				?>
				
				
				<div id="sidetree">
					<div id="treecontrol">
						<a title="Collapse the entire tree below" href="#"><img src="../../images/minus.gif" /> Contraer</a>
						<a title="Expand the entire tree below" href="#"><img src="../../images/plus.gif" /> Expandir</a>
					</div>
					
					<!-- <div class="treeheader">&nbsp;Red Socios</div> -->
					<ul class="tree">
						<li><a href="ver_consumo.php?idsoc=#"><strong><?php echo utf8_encode($row_principal['clave_dep'])," - ",utf8_encode($row_principal['nombre']); ?></strong></a>
							<ul>
								<?php
									//echo $row_principal['clave']," - ",$row_principal['nombre'],"<br>";
									//$dtHijosSoc = hijosSocio('28');
									$dtHijosSoc = hijosSocio($row_principal['clave']);
									
									$row_hijos=mysql_fetch_array($dtHijosSoc);
									recorreArbol($dtHijosSoc, $arrTotdo, $i, 0);
								?>
							</ul>						
						</div>    
						<?php
						}
						
						/*
							echo '<pre>';
							print_r($arrTotdo);
							echo '<pre>';
						*/
						
					?>
					
					
				</div>
				
				
				
				<?php
					
					//-------------------Reporte en excel---------------------
					require_once ('../../phpexcel/Classes/PHPExcel.php');
					require_once ('../../phpexcel/Classes/PHPExcel/Writer/Excel2007.php');
					//-------------------------------------------------------
					
					$maxElementos = count($arrTotdo)-1;
					$maxProfundidad=0;
					
					for($maxCol=0;$maxCol<=$maxElementos;$maxCol++){
						if($arrTotdo[$maxCol][0] > $maxProfundidad){
							$maxProfundidad = $arrTotdo[$maxCol][0];
						}
					}
					
					$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
					$cacheSettings = array( ' memoryCacheSize ' => '8MB');
					PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
					
					$objPHPExcel = new PHPExcel();
					
					//Le asignamos las propiedades basicas
					$objPHPExcel->getProperties()->setCreator("IIDESOFT Mexico");
					$objPHPExcel->getProperties()->setLastModifiedBy("IIDESOFT Mexico");
					$objPHPExcel->getProperties()->setTitle("Reporte");
					$objPHPExcel->getProperties()->setSubject("Reporte");
					$objPHPExcel->getProperties()->setDescription("Organigrama CREG E-R");
					//Nos colocamos en la index 0
					$objPHPExcel->setActiveSheetIndex(0);
					
					$chrVal = 65;
					
					
					
					//Cambiamos las propiedades de las celdas
					//Color de relleno
					$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
					$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->getStartColor()->setARGB('4682B4');
					//Negritas
					$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFont()->setBold(true);
					for($celCont=65;$celCont<=79;$celCont++){
						$Columna = chr($celCont);
						$objPHPExcel->getActiveSheet()->getColumnDimension($Columna)->setAutoSize(true);
						//	$objPHPExcel->getActiveSheet()->getColumndimension($Columna)->setWidth(20);
					}
					
					//Color de texto blanco en los encabezados
					$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFont()->getColor()->setARGB('FFFFFF');
					
					//Contador de columnas
					$filaCont=2;
					$chrVal=65;
					
					//-------------------Crea los encabezados de la tabla-----------------------------
					$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Nueva Clave');
					$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Clave Actual');
					$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Nombre');
					
					
					for($maxCol=0;$maxCol<=$maxElementos;$maxCol++){
						
						//echo $row[1]."<br>";
						$objPHPExcel->getActiveSheet()->SetCellValue(chr($chrVal).$filaCont, '');
						$chrVal += 1;
						$objPHPExcel->getActiveSheet()->SetCellValue(chr($chrVal).$filaCont, $arrTotdo[$maxCol][1]);
						$chrVal += 1;
						$objPHPExcel->getActiveSheet()->SetCellValue(chr($chrVal).$filaCont, utf8_encode($arrTotdo[$maxCol][3]));	
						$chrVal += 1;
						
						//Nos movemos una fila
						$filaCont +=1;
						$chrVal = 65;
					}//for
					
					
					
					/*
						header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
						header('Content-Disposition: attachment;filename="reporte.xls"');
						header('Cache-Control: max-age=0');
					*/
					
					$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
					$objWriter->setUseDiskCaching(true);
					$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
					
					$objWriter->save ('dependencias.xls');
					//$objWriter->save('php://output');
					
					
					echo '<div align="center"><a href="dependencias.xls" title="Click para descargar"><img src="../../images/download.png" width="48" height="48" /><br />Descargar Organigrama</a></div>';
					
					
				?>
				<p>&nbsp;
					
				</p>
			</body>
		</html>		