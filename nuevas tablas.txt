CREATE TABLE `iff-001` (
`clave` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`no_cuenta` VARCHAR( 50 ) NOT NULL ,
`concepto` VARCHAR( 150 ) NOT NULL ,
`fecha_ini` DATE NOT NULL ,
`fecha_ven` DATE NOT NULL ,
`plazo` VARCHAR( 50 ) NOT NULL ,
`tasa` DOUBLE NOT NULL ,
`monto` DOUBLE NOT NULL ,
`amortiza` DOUBLE NOT NULL ,
`pago_interes` DOUBLE NOT NULL ,
`saldo` DOUBLE NOT NULL ,
`porcentaje` DOUBLE NOT NULL ,
`fecha` DATETIME NOT NULL ,
`usuario` VARCHAR( 30 ) NOT NULL
) TYPE = MYISAM ;


CREATE TABLE `iff-002` (
`clave` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`formato` VARCHAR( 100 ) NOT NULL ,
`folio_ini` INT NOT NULL ,
`folio_fin` INT NOT NULL ,
`resguardo` VARCHAR( 30 ) NOT NULL ,
`observaciones` TINYBLOB NOT NULL ,
`fecha` DATETIME NOT NULL ,
`usuario` VARCHAR( 30 ) NOT NULL
) TYPE = MYISAM ;


INSERT INTO `iff-002` ( `clave` , `formato` , `folio_ini` , `folio_fin` , `resguardo` , `observaciones` , `fecha` , `usuario` )
VALUES (
NULL , 'FACTURAS', '0', '0', '', '', '0000-00-00 00:00:00', ''
);

INSERT INTO `iff-002` ( `clave` , `formato` , `folio_ini` , `folio_fin` , `resguardo` , `observaciones` , `fecha` , `usuario` )
VALUES (
NULL , 'RECIBOS OFICIALES DE INGRESOS', '0', '0', '', '', '0000-00-00 00:00:00', ''
), (
NULL , 'BOLETOS DE PISO DE PLAZA', '0', '0', '', '', '0000-00-00 00:00:00', ''
);


CREATE TABLE `iff-003` (
`clave` BIGINT NOT NULL AUTO_INCREMENT ,
`nombre` VARCHAR( 100 ) NOT NULL ,
`area_asig` VARCHAR( 30 ) NOT NULL ,
`no_inventario` VARCHAR( 50 ) NOT NULL ,
`no_serie` VARCHAR( 100 ) NOT NULL ,
`clave_acc` VARCHAR( 100 ) NOT NULL ,
`no_licencia` VARCHAR( 100 ) NOT NULL ,
`persona1` VARCHAR( 30 ) NOT NULL ,
`persona2` VARCHAR( 30 ) NOT NULL ,
`persona3` VARCHAR( 30 ) NOT NULL ,
`persona4` VARCHAR( 30 ) NOT NULL ,
`persona5` VARCHAR( 30 ) NOT NULL ,
`persona6` VARCHAR( 30 ) NOT NULL ,
`area` VARCHAR( 30 ) NOT NULL ,
`fecha` DATE NOT NULL ,
`usuario` VARCHAR( 30 ) NOT NULL ,
PRIMARY KEY ( `clave` )
);


CREATE TABLE `iff-004` (
`clave` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`programa` VARCHAR( 100 ) NOT NULL ,
`fecha_asig` DATE NOT NULL ,
`fecha_vig` DATE NOT NULL ,
`monto` DOUBLE NOT NULL ,
`comentarios` TINYBLOB NOT NULL ,
`area` VARCHAR( 30 ) NOT NULL ,
`fecha` DATETIME NOT NULL ,
`usuario` VARCHAR( 30 ) NOT NULL
) TYPE = MYISAM ;


-- Base de datos: `rec_hum2`
-- 

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `iff-005`
-- 

CREATE TABLE `iff-005` (
  `clave` bigint(20) NOT NULL auto_increment,
  `year` int(11) NOT NULL default '0',
  `enero` int(11) NOT NULL default '0',
  `febrero` int(11) NOT NULL default '0',
  `marzo` int(11) NOT NULL default '0',
  `abril` int(11) NOT NULL default '0',
  `mayo` int(11) NOT NULL default '0',
  `junio` int(11) NOT NULL default '0',
  `julio` int(11) NOT NULL default '0',
  `agosto` int(11) NOT NULL default '0',
  `septiembre` int(11) NOT NULL default '0',
  `octubre` int(11) NOT NULL default '0',
  `noviembre` int(11) NOT NULL default '0',
  `diciembre` int(11) NOT NULL default '0',
  `area` varchar(30) NOT NULL default '',
  `fecha` date NOT NULL default '0000-00-00',
  `usuario` varchar(30) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM;



INSERT INTO `iff-005` ( `clave` , `year` , `enero` , `febrero` , `marzo` , `abril` , `mayo` , `junio` , `julio` , `agosto` , `septiembre` , `octubre` , `noviembre` , `diciembre` , `area` , `fecha` , `usuario` )
VALUES (
NULL , '2003', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '2006-04-15', ''
), (
NULL , '2004', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '2006-04-15', ''
);


INSERT INTO `iff-005` ( `clave` , `year` , `enero` , `febrero` , `marzo` , `abril` , `mayo` , `junio` , `julio` , `agosto` , `septiembre` , `octubre` , `noviembre` , `diciembre` , `area` , `fecha` , `usuario` )
VALUES (
NULL , '2005', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '2006-04-15', ''
), (
NULL , '2006', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '2006-04-15', ''
);



UPDATE `iff-005` SET `enero` = '-1',
`febrero` = '-1',
`marzo` = '-1',
`abril` = '-1',
`mayo` = '-1',
`junio` = '-1',
`julio` = '-1' WHERE `clave` =1 LIMIT 1 ;

UPDATE `iff-005` SET `septiembre` = '-1',
`octubre` = '-1',
`noviembre` = '-1' WHERE `clave` =4 LIMIT 1 ;



CREATE TABLE `iff-0051` (
`clave` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`observa` TINYBLOB NOT NULL ,
`2003` DOUBLE NOT NULL ,
`2004` DOUBLE NOT NULL ,
`2005` DOUBLE NOT NULL ,
`observa2` TINYBLOB NOT NULL ,
`area` VARCHAR( 30 ) NOT NULL ,
`fecha` DATETIME NOT NULL ,
`usuario` VARCHAR( 30 ) NOT NULL
) TYPE = MYISAM ;


CREATE TABLE `iff-006` (
`clave` bigint( 20 ) NOT NULL AUTO_INCREMENT ,
`year` int( 11 ) NOT NULL default '0',
`enero` int( 11 ) NOT NULL default '0',
`febrero` int( 11 ) NOT NULL default '0',
`marzo` int( 11 ) NOT NULL default '0',
`abril` int( 11 ) NOT NULL default '0',
`mayo` int( 11 ) NOT NULL default '0',
`junio` int( 11 ) NOT NULL default '0',
`julio` int( 11 ) NOT NULL default '0',
`agosto` int( 11 ) NOT NULL default '0',
`septiembre` int( 11 ) NOT NULL default '0',
`octubre` int( 11 ) NOT NULL default '0',
`noviembre` int( 11 ) NOT NULL default '0',
`diciembre` int( 11 ) NOT NULL default '0',
`area` varchar( 30 ) NOT NULL default '',
`fecha` date NOT NULL default '0000-00-00',
`usuario` varchar( 30 ) NOT NULL default '',
PRIMARY KEY ( `clave` )
) TYPE = MYISAM ;


CREATE TABLE `iff-0061` (
`clave` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`observa` TINYBLOB NOT NULL ,
`2003` DOUBLE NOT NULL ,
`2004` DOUBLE NOT NULL ,
`2005` DOUBLE NOT NULL ,
`observa2` TINYBLOB NOT NULL ,
`area` VARCHAR( 30 ) NOT NULL ,
`fecha` DATETIME NOT NULL ,
`usuario` VARCHAR( 30 ) NOT NULL
) TYPE = MYISAM ;


CREATE TABLE `iff-007` (
  `clave` bigint(20) NOT NULL auto_increment,
  `year` int(11) NOT NULL default '0',
  `enero` int(11) NOT NULL default '0',
  `febrero` int(11) NOT NULL default '0',
  `marzo` int(11) NOT NULL default '0',
  `abril` int(11) NOT NULL default '0',
  `mayo` int(11) NOT NULL default '0',
  `junio` int(11) NOT NULL default '0',
  `julio` int(11) NOT NULL default '0',
  `agosto` int(11) NOT NULL default '0',
  `septiembre` int(11) NOT NULL default '0',
  `octubre` int(11) NOT NULL default '0',
  `noviembre` int(11) NOT NULL default '0',
  `diciembre` int(11) NOT NULL default '0',
  `observaciones` tinyblob NOT NULL,
  `area` varchar(30) NOT NULL default '',
  `fecha` date NOT NULL default '0000-00-00',
  `usuario` varchar(30) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;



CREATE TABLE `iff-008` (
`clave` bigint( 20 ) NOT NULL AUTO_INCREMENT ,
`year` int( 11 ) NOT NULL default '0',
`enero` int( 11 ) NOT NULL default '0',
`febrero` int( 11 ) NOT NULL default '0',
`marzo` int( 11 ) NOT NULL default '0',
`abril` int( 11 ) NOT NULL default '0',
`mayo` int( 11 ) NOT NULL default '0',
`junio` int( 11 ) NOT NULL default '0',
`julio` int( 11 ) NOT NULL default '0',
`agosto` int( 11 ) NOT NULL default '0',
`septiembre` int( 11 ) NOT NULL default '0',
`octubre` int( 11 ) NOT NULL default '0',
`noviembre` int( 11 ) NOT NULL default '0',
`diciembre` int( 11 ) NOT NULL default '0',
`area` varchar( 30 ) NOT NULL default '',
`fecha` date NOT NULL default '0000-00-00',
`usuario` varchar( 30 ) NOT NULL default '',
PRIMARY KEY ( `clave` )
) TYPE = MYISAM AUTO_INCREMENT =1;

CREATE TABLE `iff-0081` (
`clave` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`observa` TINYBLOB NOT NULL ,
`2003` DOUBLE NOT NULL ,
`2004` DOUBLE NOT NULL ,
`2005` DOUBLE NOT NULL ,
`observa2` TINYBLOB NOT NULL ,
`area` VARCHAR( 30 ) NOT NULL ,
`fecha` DATETIME NOT NULL ,
`usuario` VARCHAR( 30 ) NOT NULL
) TYPE = MYISAM ;



CREATE TABLE `iff-009` (
`clave` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`expediente` VARCHAR( 150 ) NOT NULL ,
`autoridad` VARCHAR( 150 ) NOT NULL ,
`observaciones` TINYBLOB NOT NULL ,
`area` VARCHAR( 30 ) NOT NULL ,
`fecha` DATETIME NOT NULL ,
`usuario` VARCHAR( 30 ) NOT NULL
) TYPE = MYISAM ;


CREATE TABLE `iff-010` (
`clave` bigint( 20 ) NOT NULL AUTO_INCREMENT ,
`year` int( 11 ) NOT NULL default '0',
`enero` int( 11 ) NOT NULL default '0',
`febrero` int( 11 ) NOT NULL default '0',
`marzo` int( 11 ) NOT NULL default '0',
`abril` int( 11 ) NOT NULL default '0',
`mayo` int( 11 ) NOT NULL default '0',
`junio` int( 11 ) NOT NULL default '0',
`julio` int( 11 ) NOT NULL default '0',
`agosto` int( 11 ) NOT NULL default '0',
`septiembre` int( 11 ) NOT NULL default '0',
`octubre` int( 11 ) NOT NULL default '0',
`noviembre` int( 11 ) NOT NULL default '0',
`diciembre` int( 11 ) NOT NULL default '0',
`area` varchar( 30 ) NOT NULL default '',
`fecha` date NOT NULL default '0000-00-00',
`usuario` varchar( 30 ) NOT NULL default '',
PRIMARY KEY ( `clave` )
) TYPE = MYISAM AUTO_INCREMENT =1;

CREATE TABLE `iff-011` (
`clave` BIGINT NOT NULL ,
`institucion` VARCHAR( 150 ) NOT NULL ,
`no_exps` INT NOT NULL ,
`saldo` DOUBLE NOT NULL ,
`area` VARCHAR( 30 ) NOT NULL ,
`fecha` DATETIME NOT NULL ,
`usuario` VARCHAR( 30 ) NOT NULL ,
PRIMARY KEY ( `clave` )
) TYPE = MYISAM ;


CREATE TABLE `iff-0101` (
`clave` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`saldo_ini` DOUBLE NOT NULL ,
`saldo_fin` DOUBLE NOT NULL ,
`negociaciones` MEDIUMBLOB NOT NULL ,
`observaciones` TINYBLOB NOT NULL ,
`area` VARCHAR( 30 ) NOT NULL ,
`fecha` DATETIME NOT NULL ,
`usuario` VARCHAR( 30 ) NOT NULL
) TYPE = MYISAM ;

CREATE TABLE `iff-012` (
`clave` bigint( 20 ) NOT NULL AUTO_INCREMENT ,
`year` int( 11 ) NOT NULL default '0',
`enero` int( 11 ) NOT NULL default '0',
`febrero` int( 11 ) NOT NULL default '0',
`marzo` int( 11 ) NOT NULL default '0',
`abril` int( 11 ) NOT NULL default '0',
`mayo` int( 11 ) NOT NULL default '0',
`junio` int( 11 ) NOT NULL default '0',
`julio` int( 11 ) NOT NULL default '0',
`agosto` int( 11 ) NOT NULL default '0',
`septiembre` int( 11 ) NOT NULL default '0',
`octubre` int( 11 ) NOT NULL default '0',
`noviembre` int( 11 ) NOT NULL default '0',
`diciembre` int( 11 ) NOT NULL default '0',
`area` varchar( 30 ) NOT NULL default '',
`fecha` date NOT NULL default '0000-00-00',
`usuario` varchar( 30 ) NOT NULL default '',
PRIMARY KEY ( `clave` )
) TYPE = MYISAM AUTO_INCREMENT =1;

CREATE TABLE `iff-0121` (
`clave` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`saldo_ini` DOUBLE NOT NULL ,
`saldo_fin` DOUBLE NOT NULL ,
`negociaciones` MEDIUMBLOB NOT NULL ,
`observaciones` TINYBLOB NOT NULL ,
`observa2` TINYBLOB NOT NULL ,
`area` VARCHAR( 30 ) NOT NULL ,
`fecha` DATETIME NOT NULL ,
`usuario` VARCHAR( 30 ) NOT NULL
) TYPE = MYISAM ;

CREATE TABLE `iff-013` (
  `clave` bigint(20) NOT NULL default '0',
  `institucion` varchar(150) NOT NULL default '',
  `no_exps` int(11) NOT NULL default '0',
  `saldo` double NOT NULL default '0',
  `area` varchar(30) NOT NULL default '',
  `fecha` datetime NOT NULL default '0000-00-00 00:00:00',
  `usuario` varchar(30) NOT NULL default '',
  PRIMARY KEY  (`clave`)
) TYPE=MyISAM;


CREATE TABLE `iff-014` (
`clave` bigint( 20 ) NOT NULL AUTO_INCREMENT ,
`year` int( 11 ) NOT NULL default '0',
`enero` int( 11 ) NOT NULL default '0',
`febrero` int( 11 ) NOT NULL default '0',
`marzo` int( 11 ) NOT NULL default '0',
`abril` int( 11 ) NOT NULL default '0',
`mayo` int( 11 ) NOT NULL default '0',
`junio` int( 11 ) NOT NULL default '0',
`julio` int( 11 ) NOT NULL default '0',
`agosto` int( 11 ) NOT NULL default '0',
`septiembre` int( 11 ) NOT NULL default '0',
`octubre` int( 11 ) NOT NULL default '0',
`noviembre` int( 11 ) NOT NULL default '0',
`diciembre` int( 11 ) NOT NULL default '0',
`area` varchar( 30 ) NOT NULL default '',
`fecha` date NOT NULL default '0000-00-00',
`usuario` varchar( 30 ) NOT NULL default '',
PRIMARY KEY ( `clave` )
) TYPE = MYISAM AUTO_INCREMENT 

CREATE TABLE `iff-0141` (
`clave` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`saldo_ini` DOUBLE NOT NULL ,
`saldo_fin` DOUBLE NOT NULL ,
`negociaciones` MEDIUMBLOB NOT NULL ,
`observaciones` TINYBLOB NOT NULL ,
`observa2` TINYBLOB NOT NULL ,
`area` VARCHAR( 30 ) NOT NULL ,
`fecha` DATETIME NOT NULL ,
`usuario` VARCHAR( 30 ) NOT NULL
) TYPE = MYISAM ;# MySQL ha devuelto un valor vac�o (i.e., cero columnas).


CREATE TABLE `iff-015` (
`clave` bigint( 20 ) NOT NULL default '0',
`institucion` varchar( 150 ) NOT NULL default '',
`no_exps` int( 11 ) NOT NULL default '0',
`saldo` double NOT NULL default '0',
`area` varchar( 30 ) NOT NULL default '',
`fecha` datetime NOT NULL default '0000-00-00 00:00:00',
`usuario` varchar( 30 ) NOT NULL default '',
PRIMARY KEY ( `clave` )
) TYPE = MYISAM ;

CREATE TABLE `iff-017` (
`clave` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`concepto` VARCHAR( 150 ) NOT NULL ,
`total` INT NOT NULL ,
`regular` INT NOT NULL ,
`rezago` INT NOT NULL ,
`observaciones` TINYBLOB NOT NULL ,
`area` VARCHAR( 30 ) NOT NULL ,
`fecha` DATETIME NOT NULL ,
`usuario` VARCHAR( 30 ) NOT NULL
) TYPE = MYISAM ;



CREATE TABLE `ido-0031` (
`clave` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`concepto` VARCHAR( 75 ) NOT NULL ,
`periodo` VARCHAR( 50 ) NOT NULL ,
`tiempo` VARCHAR( 50 ) NOT NULL ,
`observaciones` TINYBLOB NOT NULL ,
`no_caja` BIGINT NOT NULL ,
`fecha` DATETIME NOT NULL ,
`usuario` VARCHAR( 30 ) NOT NULL
) TYPE = MYISAM ;


ALTER TABLE `iff-0071` ADD `tipo` VARCHAR( 10 ) NOT NULL AFTER `clave` ,
ADD `ap` INT NOT NULL AFTER `tipo` ,
ADD `pn` INT NOT NULL AFTER `ap` ,
ADD `ps` INT NOT NULL AFTER `pn` ,
ADD `sc` INT NOT NULL AFTER `ps` ,
ADD `cp` INT NOT NULL AFTER `sc` ,
ADD `observaciones` TINYBLOB NOT NULL AFTER `cp` ;
