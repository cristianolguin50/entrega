<?php
	/*
		* Script:    DataTables server-side script for PHP and MySQL
		* Copyright: 2010 - Allan Jardine
		* License:   GPL v2 or BSD (3-point)
	*/
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		* Easy set variables
	*/
	
	/* Array of database columns which should be read and sent back to DataTables. Use a space where
		* you want to insert a non-database field (for example a counter or static image)
	*/
	
	session_start();
	
	require_once('../../Connections/bd2.php'); 
	
	
	$whereT=$_GET['where'];
	
	$aColumns = array("clave", "curp", "nombre_comp");
	
	/* Indexed column (used for fast and accurate table cardinality) */
	$sIndexColumn = "clave";
	
	/* DB table to use */
	$sTable = "trabajador";
	
	/* Database connection information */
	/*$gaSql['user']       = "root";
		$gaSql['password']   = "Fiscaliza13";
		$gaSql['db']         = "pv_tecsmart";
	$gaSql['server']     = "localhost";*/
	
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		* If you just want to use the basic configuration for DataTables with PHP server-side, there is
		* no need to edit below this line
	*/
	
	/* 
		* MySQL connection
	*/
	/*$gaSql['link'] =  mysql_pconnect( $gaSql['server'], $gaSql['user'], $gaSql['password']  ) or
		die( 'Could not open connection to server' );
		
		mysql_select_db( $gaSql['db'], $gaSql['link'] ) or 
	die( 'Could not select database '. $gaSql['db'] );*/
	
	
	/* 
		* Paging
	*/
	$sLimit = "";
	if ( isset($_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
	{
		$sLimit = "LIMIT ".$_GET['iDisplayStart'].", ".$_GET['iDisplayLength'];
	}
	
	/*
		* Ordering
	*/
	if ( isset($_GET['iSortCol_0'] ) )
	{
		$sOrder = "ORDER BY  ";
		for ( $i=0 ; $i<intval($_GET['iSortingCols'] ) ; $i++ )
		{
			if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
			{
				$sOrder .= $aColumns[ intval($_GET['iSortCol_'.$i]) ]."
				".$_GET['sSortDir_'.$i].", ";
			}
		}
		
		$sOrder = substr_replace( $sOrder, "", -2 );
		if ( $sOrder == "ORDER BY" )
		{
			$sOrder = "";
		}
	}
	
	
	/* 
		* Filtering
		* NOTE this does not match the built-in DataTables filtering which does it
		* word by word on any field. It's possible to do here, but concerned about efficiency
		* on very large tables, and MySQL's regex functionality is very limited
	*/
	
	$where_almacen=$whereT;
	
	$sWhere = "WHERE $where_almacen";
	if ( $_GET['sSearch'] != "" )
	{
		$sWhere = "WHERE $where_almacen AND (";
		$cont=count($aColumns);
		for ( $i=0 ; $i<$cont ; $i++ )
		{
			$sWhere .= $aColumns[$i]." LIKE '%".$_GET['sSearch']."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
	}
	
	/* Individual column filtering */
	$cont1=count($aColumns);
	for ( $i=0 ; $i<$cont1 ; $i++ )
	{
		if ($_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
		{
			if ( $sWhere == "" )
			{
				$sWhere = "";
			}
			else
			{
				$sWhere .= " AND ";
			}
			$sWhere .= $aColumns[$i]." LIKE '%".$_GET['sSearch_'.$i]."%' ";
		}
	}
	
	
	/*
		* SQL queries
		* Get data to display
	*/
	$sQueryall = "
	SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
	FROM   $sTable
	$sWhere 
	$sOrder
	$sLimit
	";
	
	//echo $sQueryall;
	mysql_select_db($database_bd2, $bd2);
	$rResult = mysql_query($sQueryall, $bd2);
	
	/* Data set length after filtering */
	$sQuery2 = "
	SELECT FOUND_ROWS() AS algo
	";
	mysql_select_db($database_bd2, $bd2);
	$rResultFilterTotal = mysql_query($sQuery2, $bd2);
	$aResultFilterTotal = mysql_fetch_assoc($rResultFilterTotal);
	$iFilteredTotal = $aResultFilterTotal['algo'];
	
	/* Total data set length */
	$sQuery1 = "
	SELECT COUNT(".$sIndexColumn.") AS algo
	FROM   $sTable WHERE $where_almacen
	";
	mysql_select_db($database_bd2, $bd2);
	$rResultTotal = mysql_query($sQuery1, $bd2);
	$aResultTotal = mysql_fetch_assoc($rResultTotal);
	$iTotal = $aResultTotal['algo'];
	
	
	/*
		* Output
	*/
	$output = array(
	"sEcho" => intval($_GET['sEcho']),
	"iTotalRecords" => $iTotal,
	"iTotalDisplayRecords" => $iFilteredTotal,
	"aaData" => array()
	);
	
	$j = 1;
	while ( $aRow =  mysql_fetch_assoc($rResult))
	{
		$row = array();
		$cont2=count($aColumns);
		/*for ( $i=0 ; $i<$cont2 ; $i++ )
			{
			if ( $aColumns[$i] == "version" )
			{
			Special output formatting for 'version' column 
			$row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : strtoupper(utf8_encode($aRow[ $aColumns[$i] ]));
			
			
			}
			else if ( $aColumns[$i] != ' ' )
			{
			/* General output 
			$row[] =  strtoupper(utf8_encode($aRow[ $aColumns[$i] ]));
			
			}
		}*/
		
		$row[]=$j;
		
		$row[]=$aRow['curp'];
		
		$row[]=utf8_encode($aRow['nombre_comp']);		
		
		$row[]="<div align='center'><a href='mod_trab.php?mod=".$aRow['clave']."' title='Modificar Registro'><img src='../../images/edit.png' width='16' height='16' border='0'></a></div>";
		$row[]="<div align='center'><a href='Bajas.php?mod=".$aRow['clave']."' title='Elimina Registro'><img src='../../images/BAJA.gif' width='16' height='16' border='0'></a></div>";
		$row[]="<div align='center'><a href='expediente.php?mod=".$aRow['clave']."' title='Ver Registro'><img src='../../images/imgfolder.gif' width='16' height='16' border='0'></a></div>";
		
		$j++;
		
		$output['aaData'][] = $row;
	}
	
	
	
	
	echo json_encode( $output );
?>