<!--
//Funcion de macromedia, excelente desempe�o!. Se le pasa objeto y donde buscar (por default el document actual)
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

//Variable para el preload. Suelo usar un gif, pero esto es mas liviano para el ejemplo.
var PreLoader = 'Cargando...';

//Esta funci�n es la que procesar� un archivo (pagina) y mostrar� el resultado en un objeto del document (destino), siempre sera procesando mediante origen (que es el iframe).
function cargarPagina( pagina, destino, origen ){
	//Primero muestro el preload
	mostrarPreLoader( destino );
	//Y luego le digo al iframe que me procese el php "pagina". Adem�s le paso "destino" as� se a donde muestro el resultado.
	frames[origen].location.href = pagina + '&destino=' + destino;
}

//Funci�n que mostrar� el preload
function mostrarPreLoader( destino ){
	var objeto = MM_findObj( destino );
	objeto.innerHTML = PreLoader;
}

//Esta funcion es solo para este ejemplo, asi separamos mas las cosas:
function recargarTrab( depend ){
	var clave_depend = depend.value;
	cargarPagina('new_user2.php?depend=' + clave_depend,'divCentral','iframeCentral');
	return false;
}

function recargarPuesto( trab ){
	var clave_trab = trab.value;
	cargarPagina('new_user3.php?trab=' + clave_trab,'divSiguiente','iframeCentral');
	return false;
}

