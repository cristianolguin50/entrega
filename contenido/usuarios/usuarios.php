<?php require_once('../../Connections/bd2.php'); ?>
<?php
	session_start();
	$colname_inventa = "1";
	if (isset($_SESSION['clave_dependencia'])) {
		$colname_inventa = (get_magic_quotes_gpc()) ? $_SESSION['clave_dependencia'] : addslashes($_SESSION['clave_dependencia']);
	}
	mysql_select_db($database_bd2, $bd2);
	$query_inventa = sprintf("SELECT a.clave as clave, a.usuario as usuario, b.nombre_comp as nombre_comp FROM  usuario a LEFT JOIN trabajador b ON a.nombre_comp=b.curp");
	$inventa = mysql_query($query_inventa, $bd2) or die(mysql_error());
	$row_inventa = mysql_fetch_assoc($inventa);
	$totalRows_inventa = mysql_num_rows($inventa);
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Untitled Document</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<script language="javascript" src="../../js/validate.js" type="text/javascript"></script>
		<script language="javascript" type="text/javascript">
			
			function MM_goToURL() { //v3.0
				var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
				for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}
			
			function confirma (valor){
				var val=valor;
				if (confirm ("�Desea eliminar este registro?" + val)){
					//cargarPagina('llenacombo.php?tipo=' + codigoTipo,'divCentral','iframeCentral');
					MM_goToURL('self', 'elimina2.php?oculto=' + val);
					//open("elimina2.php?oculto=valor");
				}
			}
			
		</script>
		
		
		<link href="../../css/idots.css" rel="stylesheet" type="text/css">
		<style type="text/css">
			<!--
				body {
				margin-top: 0px;
				}
				.style1 {
				font-size: 12px;
				font-weight: bold;
				}
			-->
		</style>
		
		
		<link type="text/css" href="../../js/jquery-ui-1.8.23.custom/css/south-street/jquery-ui-1.8.23.custom.css" rel="stylesheet" />	
		<script type="text/javascript" src="../../js/jquery-ui-1.8.23.custom/js/jquery-1.8.0.min.js"></script>
		<script type="text/javascript" src="../../js/jquery-ui-1.8.23.custom/js/jquery-ui-1.8.23.custom.min.js"></script>
		
		<script src="../../js/jquery-1.7.1.js" type="text/javascript"></script>
		<script src="../../js/jquery.dataTables.js" type="text/javascript"></script>
		
		<style type="text/css">
			@import "../../css/demo_table_jui.css";
			@import "media/themes/smoothness/jquery-ui-1.8.4.custom.css";
		</style>
		
		<style>
			*{
			font-family: arial;
			}
		</style>
		<script type="text/javascript" charset="utf-8">
			$(document).ready(function(){
				$('#datatables').dataTable({
					// "sPaginationType":"full_numbers",
					"aaSorting":[[2, "asc"]],
					"bJQueryUI":true
				});
			})
			
		</script>
		
	</head>
	
	<body>
		<div align="center">
			<div align="center"><h2 style="color: #0404B4">Usuarios del Sistema</h2></div>
			<table width="90%"  border="0" align="center">
				<tr>
					<td width="38%">
						<table width="100%"  border="1">
							<tr>
								<td bgcolor="#FFFFFF">
									<div align="center"><strong><a href="new_user.php">AGREGAR UN NUEVO USUARIO </a></strong></div>
								</td>
							</tr>
						</table>
					</td>
					<td width="62%">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			<form name="form1" method="post" action="elimina.php"><div align="center">
				<div align="right">
					<blockquote>
						<p>&nbsp; </p>
					</blockquote>
				</div>
				<table width="90%" id="datatables" class="display" border="1" align="center" cellpadding="3" cellspacing="0">
					<thead>
						<tr>
							<td width="6%">&nbsp;</td>
							<td width="21%"><div align="center"><strong>Nombre de Usuario</strong></div></td>
							<td width="53%"><div align="center"><strong>Nombre</strong></div></td>
							<td width="5%">
							<div align="center"><a href="#" class="hintanchor" onMouseOver="showhint('<center> Presione este &iacute;cono para <b>modificar</b> el acceso a los m�dulos del sistema. </center>', this, event, '150px')">[?]</a></div></td>
							<td width="5%">
							<div align="center"><a href="#" class="hintanchor" onMouseOver="showhint('<center> Presione este &iacute;cono para <b>modificar</b>la contrase�a de acceso. </center>', this, event, '150px')">[?]</a></div></td>
							<td width="5%">
							<div align="center"><a href="#" class="hintanchor" onMouseOver="showhint('<center> Presione este &iacute;cono para <b>eliminar</b> al Usuario. </center>', this, event, '150px')">[?]</a></div></td>
						</tr>
					</thead>
					<tbody>
						<?php $i=1; do { 
							if(strcmp($row_inventa['usuario'], 'osfem') != 0){ //usuario db == osfem
							?>
							<tr bgcolor="#FFFFFF" onMouseOver='this.style.background="#B4CAE9"' onmouseout='this.style.background="white"'>
								<td>
								<div align="center"><?php echo $i; ?> </div></td>
								<td>
								<div align="center"><?php echo $row_inventa['usuario']; ?></div></td>
								<td>&nbsp;
								<div align="center"><?php echo $row_inventa['nombre_comp']; ?></div></td>
								<td>
								<div align="center"><a href="mod_user.php?oculto2=<?php echo $row_inventa['clave']; ?>" title="Modificar Permiso a los M�dulos"><img src="../../images/configure.png" width="16" height="16" border="0"></a> </div></td>
								<td>
								<div align="center"><a href="mod_pass.php?oculto2=<?php echo $row_inventa['clave']; ?>" title="Modificar Contrase�a"><img src="../../images/groupevent.png" width="16" height="16" border="0"></a></div></td>
								<td>
								<div align="center"><a href="#" onClick="confirma(<?php echo $row_inventa['clave']; ?>);" title="Eliminar el Registro"><img src="../../images/delete.png" width="16" height="16"></a></div></td>
							</tr>
							<?php $i++; 
							}//== osfem
						} while ($row_inventa = mysql_fetch_assoc($inventa)); ?>
					</tbody>
				</table>
				
			</div>
			</form>
			
		</div>
	</body>
</html>
