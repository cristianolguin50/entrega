<?php
define('FPDF_FONTPATH','font/');
require('fpdi.php');
class concat_pdf extends fpdi {

    var $files = array();

    function concat_pdf($orientation='P',$unit='mm',$format='Letter') {
        parent::fpdi($orientation,$unit,$format);
    }

    function setFiles($files) {
        $this->files = $files;
    }

    function concat() {
		global $ayuntamiento;
		global $hora;
		global $minutos;
		global $dia;
		global $mes;
		global $year;
		/* global $entrega;
		global $testigo1;
		global $recibe;
		global $testigo2;
		global $cont;
		global $osfem1; */
		
        foreach($this->files AS $file) {
		
            $pagecount = $this->setSourceFile($file);
						
				$tplidx = $this->ImportPage(1);
                 $this->AddPage();
				 $this->SetY(63);
				 $this->SetFont('Arial','',10);
				 $this->Cell(45);
				 $this->Cell(40,4, $ayuntamiento, 0, 0, 'C');
				 $this->Cell(22);
				 $this->Cell(15,4, $hora, 0, 0, 'C');
				 $this->Cell(15);
				 $this->Cell(10,4, $minutos, 0, 0, 'C');
				 $this->Cell(30);
				 $this->Cell(17,4, $dia, 0, 0, 'C');
				 $this->SetY(68);	
				 $this->Cell(20);
				 $this->Cell(20,4, $mes, 0, 0, 'C');	
				 $this->Cell(25);
				 $this->Cell(12,4, $year, 0, 0, 'C');		
				 $this->SetY(90);	
				 $this->Cell(90);
				 $this->Cell(100,4, "DIRECCI�N DE AGUA POTABLE Y ALCANTARILLADO", 0, 0, 'C');
				 $this->SetY(95);	
				 $this->Cell(105);
				 $this->Cell(100,4, "PLANTA ALTA DEL PALACIO MUNICIPAL, SITO", 0, 0, 'C');
				 $this->SetY(100);	
				 $this->Cell(10);
				 $this->Cell(100,4, "PLAZA HIDALGO S/N, CABECERA MUNICIPAL", 0, 0, 'C');
				 
				 $this->Cell(32);
				 $this->Cell(20,4, "VICENTE GARF�AS PINEDA, ", 0, 0, 'C');
				 
				 $this->SetY(105);	
				 $this->Cell(40);
				 $this->Cell(100,4, "ROBERTO LUNA CAMACHO, DAVID PINEDA YA�EZ, IRENE MIREYA MORALES ROJAS Y", 0, 0, 'C');
				 $this->SetY(110);	
				 $this->Cell(20);
				 $this->Cell(100,4, "ARACELI TORRES L�PEZ ", 0, 0, 'L');
				 
				 $this->SetY(115);	
				 $this->Cell(110);
				 $this->Cell(40,4, "AGUA POTABLE Y ALCANTARILLADO ", 0, 0, 'L');
				 $this->SetY(120);	
				 $this->Cell(40);
				 $this->Cell(75,4, "VICENTE GARF�AS PINEDA", 0, 0, 'L');
				 
				 $this->Cell(43);
				 $this->Cell(50,4, "DIRECTOR", 0, 0, 'L');
				 
				 $this->SetY(125);	
				 $this->Cell(97);
				 $this->Cell(25,4, "18-08-2003", 0, 0, 'L');
				 
				 $this->Cell(5);
				 $this->Cell(25,4, "22-02-2006", 0, 0, 'L');
				 
				 $this->SetY(130);	
				 $this->Cell(40);
				 $this->Cell(85,4, "ROBERTO LUNA CAMACHO", 0, 0, 'L');
				 
				 $this->SetY(192);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(204);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(216);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(230);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(243);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				
				 /* $this->Cell(50);
				 $this->Cell(10,4, $ayuntamiento, 0, 0);  */
                 $this->useTemplate($tplidx);
				 
				 $tplidx = $this->ImportPage(2);
                 $this->AddPage();
				 
				 $this->SetY(38);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(51);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(64);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(76);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(88);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(101);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(113);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(134);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(155);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(175);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(191);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(216);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(232);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
                 $this->useTemplate($tplidx);
				 
				 $tplidx = $this->ImportPage(3);
                 $this->AddPage();
				 
				 $this->SetY(38);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(51);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(62);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(74);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 								 
				 $this->SetY(99);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(112);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(129);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(141);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(153);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(174);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(194);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(215);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(232);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
                 $this->useTemplate($tplidx);
				 
				 $tplidx = $this->ImportPage(4);
                 $this->AddPage();
				 
				 $this->SetY(41);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(54);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(67);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(79);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 								 
				 $this->SetY(92);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(108);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(120);	
				 $this->Cell(170);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(132);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(145);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(157);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(170);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(183);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(195);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(212);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(229);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
                 $this->useTemplate($tplidx);
				 
				 $tplidx = $this->ImportPage(5);
                 $this->AddPage();
				 
				 $this->SetY(41);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(58);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(74);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');				 				
				 								 
				 $this->SetY(92);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(108);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 				
				 $this->SetY(136);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(149);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(161);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(174);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(187);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(199);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(212);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(224);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(236);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 
                 $this->useTemplate($tplidx);
				 				 
				 $tplidx = $this->ImportPage(6);
                 $this->AddPage();								
				 
				 $this->SetY(54);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(66);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(78);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');				 				
				 								 
				 $this->SetY(92);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(103);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 				
				 $this->SetY(120);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(152);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(165);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(183);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(197);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');				
				 
				 $this->SetY(209);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(223);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');								
				 
                 $this->useTemplate($tplidx);
				 
				 $tplidx = $this->ImportPage(7);
                 $this->AddPage();	
				 
				 $this->SetY(38);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');							
				 				
				 $this->SetY(62);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');				
				  								
				 $this->SetY(91);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(112);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 				
				 $this->SetY(136);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(152);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(169);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(185);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');				 		
				 
				 $this->SetY(207);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(226);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');								
				 
                 $this->useTemplate($tplidx);
				 
				 $tplidx = $this->ImportPage(8);
                 $this->AddPage();							
				 				
				 $this->SetY(58);	
				 $this->Cell(170);
				 $this->Cell(2,4, "X", 0, 0, 'L');		
				 
				 $this->SetY(71);	
				 $this->Cell(170);
				 $this->Cell(2,4, "X", 0, 0, 'L');				
				 
				 $this->SetY(82);	
				 $this->Cell(170);
				 $this->Cell(2,4, "X", 0, 0, 'L');				
				  								
				 $this->SetY(95);	
				 $this->Cell(170);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(108);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(120);	
				 $this->Cell(170);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 				
				 $this->SetY(136);	
				 $this->Cell(170);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(153);	
				 $this->Cell(182);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(167);	
				 $this->Cell(170);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(198);	
				 $this->Cell(170);
				 $this->Cell(2,4, "X", 0, 0, 'L');				 		
				 
				 $this->SetY(210);	
				 $this->Cell(170);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 					
				 
                 $this->useTemplate($tplidx);
				 
				 $tplidx = $this->ImportPage(9);
                 $this->AddPage();			
				 				
				 $this->SetY(46);	
				 $this->Cell(170);
				 $this->Cell(2,4, "X", 0, 0, 'L');		
				 		
				 $this->SetY(58);	
				 $this->Cell(170);
				 $this->Cell(2,4, "X", 0, 0, 'L');		
				 
				 $this->SetY(71);	
				 $this->Cell(170);
				 $this->Cell(2,4, "X", 0, 0, 'L');				
				 				 
				  								
				 $this->SetY(95);	
				 $this->Cell(170);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(108);	
				 $this->Cell(170);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(120);	
				 $this->Cell(170);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 				
				 $this->SetY(133);	
				 $this->Cell(170);
				 $this->Cell(2,4, "X", 0, 0, 'L');
				 
				 $this->SetY(150);	
				 $this->Cell(170);
				 $this->Cell(2,4, "X", 0, 0, 'L');				 				 				 									 
                 $this->useTemplate($tplidx);
				 
				 $tplidx = $this->ImportPage(10);
                 $this->AddPage();		
				 $this->SetY(54);	
				 $this->Cell(80);
				 $this->Cell(15,4, $hora, 0, 0, 'C');
				 $this->Cell(25);
				 $this->Cell(10,4, $minutos, 0, 0, 'C');
				 
				 $this->SetY(84);	
				 $this->Cell(20);
				 $this->Cell(80,4, "VICENTE GARFIAS PINEDA", 0, 0, 'C');
				 
				;	
				 $this->Cell(10);
				 $this->Cell(80,4, "ROBERTO LUNA CAMACHO", 0, 0, 'C');
				 
				  $this->SetY(97);	
				 $this->Cell(20);
				 $this->Cell(80,4, "DIRECTOR", 0, 0, 'C');
				 
				
				 $this->Cell(10);
				 $this->Cell(80,4, "SUB-DIRECTOR", 0, 0, 'C');
				 
				 $this->SetY(113);	
				 $this->Cell(20);
				 $this->Cell(80,4, "AV. PRINCIPAL S/N", 0, 0, 'C');
				 
				
				 $this->Cell(10);
				 $this->Cell(80,4, "BO. SANTIAGO TLALTEPOXCO", 0, 0, 'C');
				 
				 $this->SetY(117);	
				 $this->Cell(20);
				 $this->Cell(80,4, "BO. PUENTE GRANDE", 0, 0, 'C');
				 
				
				 $this->Cell(10);
				 $this->Cell(80,4, "", 0, 0, 'C');			
				 
				 $this->SetY(122);	
				 $this->Cell(45);
				 $this->Cell(11,4, "593", 0, 0, 'L');
				 $this->Cell(5);
				 $this->Cell(50,4, "9180096", 0, 0, 'l');	 
				 
				 
				  $this->SetY(158);	
				 $this->Cell(20);
				 $this->Cell(80,4, "DAVID PINEDA YA�EZ", 0, 0, 'C');
				 
				 $this->Cell(10);
				 $this->Cell(80,4, "IRENE MIREYA MORALES ROJAS", 0, 0, 'C');
				 
				 $this->SetY(171);	
				 $this->Cell(20);
				 $this->Cell(80,4, "7mo REGIDOR", 0, 0, 'C');
				 
				 $this->Cell(10);
				 $this->Cell(80,4, "SECRETARIA", 0, 0, 'C');
				 
				 $this->SetY(228);	
				 $this->Cell(20);
				 $this->Cell(80,4, "ARACELI TORRES L�PEZ", 0, 0, 'C');
				 
				 $this->useTemplate($tplidx);	


        }
    }
}

?>
