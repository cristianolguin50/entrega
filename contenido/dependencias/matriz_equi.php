<?php 
	
	/*	Modificado por: Marco Robles
	Fecha: 20/09/2017	*/
	
	require_once('../../Connections/bd2.php'); 
	
	session_start();
	
	if (!isset($_SESSION['MM_Username'])){
		header("Location: ../../adios.php");
	}
	
	mysql_select_db($database_bd2, $bd2);
	$query_depen = "SELECT clave, clave_dep, nombre FROM `dependencia` WHERE mostrar='1' AND (tipo='G' OR tipo='P') ORDER BY tipo ASC, clave ASC";
	$depen = mysql_query($query_depen, $bd2) or die(mysql_error());
	
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link href="../../css/idots.css" rel="stylesheet" type="text/css">
		<title>Unidades Administrativas</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		
		<link type="text/css" href="../../js/jquery-ui-1.8.23.custom/css/south-street/jquery-ui-1.8.23.custom.css" rel="stylesheet" />	
		<script type="text/javascript" src="../../js/jquery-ui-1.8.23.custom/js/jquery-1.8.0.min.js"></script>
		<script type="text/javascript" src="../../js/jquery-ui-1.8.23.custom/js/jquery-ui-1.8.23.custom.min.js"></script>
		
		<link rel="stylesheet" type="text/css" media="screen" href="../../js/jquery.ketchup.0.3.2/css/jquery.ketchup.css" />
		<script type="text/javascript" src="../../js/jquery.ketchup.0.3.2/js/jquery.ketchup.all.min.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function() {
				$("#form1").ketchup(); 
			});
		</script>
		
		<script language="JavaScript" src="_scripts.js" type="text/javascript"></script>
		<script language="javascript" src="../../js/validate.js" type="text/javascript"></script>
		
		<script language="JavaScript" type="text/JavaScript">
			
			function Abrir_ventana (pagina) {
				var opciones="toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,width=300,height=300,top=85,left=140";
				window.open(pagina,"",opciones);
			}
			
			function validate(valor){
				var cad1=document.form1.clave.value;
				var cad2=document.form1.nombre.value;
				var lon1;
				var lon2;
				lon1=cad1.length;
				lon2=cad2.length;
				if((lon1>0) && (lon2>0)){
					document.form1.enviar.disabled=false;
				}
				if ((lon1<=0) || (lon2<=0)){
					document.form1.enviar.disabled=true;
				}
			}
			
			function MM_goToURL() {
				var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
				for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}
			
			function pasa(valor){
				document.form1.tipo.value=valor;
			}
		</script>
		
		<script type="text/javascript">			
			$( function() {
				$( "#dialog" ).dialog({
					autoOpen: false,
					resizable: false,
					modal: true,
					buttons: {
						"Aceptar": function() {
							$( this ).dialog( "close" );
						}
					}
				});
			});
			
			function abrirMensaje(){
				var e = document.getElementById("dialog");
				e.style.display = 'block';
				e.innerHTML = '<strong>Antes de cargar su nuevo organigrama y de realizar la matriz de equivalencia, se les recomienda hacer un respaldo de información.</strong>'; 
				$( "#dialog" ).dialog( "open" );
			}	
		</script>
		
		<script language="JavaScript" src="_scripts.js" type="text/javascript"></script>
		<script language="javascript" src="../../js/validate.js" type="text/javascript"></script>
		<style type="text/css">
			body {
			margin-top: 0px;
			}
			.style1 {
			color: #FF0000;
			font-style: italic;
			}
			.style2 {font-size: 110%;}
			.style3 {color: #FF0000;
			font-style: italic;
			}
		</style>
	</head>
	
	<body onLoad="abrirMensaje();">
		
		<div align="center"><h2 style="color: #0404B4">Unidades Administrativas</h2></div>
		
		<form id="form1" name="form1" method="post" action="carga_nueva_dep.php" enctype="multipart/form-data">
			
			<table width="56%" border="1" cellpadding="3" cellspacing="0" align="center">
				<tr>
					<td colspan="4"><div align="center"><strong>Ingrese los datos de las Unidades Administrativas</strong></div></td>
				</tr>
				<tr>
					<td width="21%"><div align="right"><strong>Unidad Administrativa Anterior:</strong></div></td>
					<td width="79%"><div align="left"><input type="text" name="dep_ant" id="dep_ant" data-validate="validate(required);">
						<a href="#" class="hintanchor" onMouseover="showhint('<center>Ingrese la clave de la Unidad Administrativa anterior a la cual estaban asociados los registros.<i> Este campo es obligatorio.</i></center>', this, event, '150px')">[?]</a>
					</div></td>
				</tr>
				<tr>
					<td><div align="right"><strong>Unidad Administrativa Nueva:</strong></div></td>
					<td><div align="left">
						<select name="dep_nueva" id="dep_nueva" tabindex="1" onKeyPress="return handleEnter(this, event)">
							<?php while ($row_depen = mysql_fetch_assoc($depen)) { ?>
								<option value="<?php echo $row_depen['clave_dep']?>"><?php echo $row_depen['clave_dep']."-".$row_depen['nombre']?></option>
							<?php } ?>
						</select>
						<a href="#" class="hintanchor" onMouseover="showhint('<center>Seleccione la nueva Unidad Administrativa a la cual se asociaran los registros. <i>Este campo es obligatorio.</i></center>', this, event, '150px')">[?]</a>
					</div></td>
				</tr>
			</table>
			
			<p align="center">
				<input name="regresa" type="button" onClick="history.back();" value="Regresar">
				<input type="submit" name="enviar" value="Almacenar Datos" >
			</p>
			
		</form>
		
		<div id="dialog" title="Aviso:"></div>
		
	</body>
</html>