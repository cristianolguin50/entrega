<?php
require('fpdf.php');
require('ufpdf.php');

class PDF_MC_Table extends FPDF
{
var $widths;
var $aligns;
var $encabezado;
var $alinea;

function Header()
{
global $title;
global $area;
global $ayuntam;
global $per1;
global $per2;
global $numero;
global $dia;
global $mes;
global $year;
//global $encabezado;
    //Logo
	//$this->SetTopMargin(2);
    $this->Image('../../../images/azul2.jpg',10,10,0,18);
    //Arial bold 15
    $this->SetFont('Arial','B',8);
    //Movernos a la derecha
    $this->Image('../../../images/osfem.jpg', 240, 8, 20);
	$this->SetY(11);
	$this->Cell(25);
	$this->Cell(75,1,'',0,0,'L');
	$this->Ln(3.5);
	$this->Cell(25);
    $this->Cell(75,1,'',0,0,'L');
	$this->Ln(3.5);
	$this->Cell(25);
	$this->Cell(75,1,'',0,0,'L');
	$this->Ln(3.5);
	$this->Cell(25);
	$this->Cell(75,1,'',0,0,'L');
	$this->Ln(3.5);
	$this->Cell(25);
	$this->Cell(75,1,"",0,0,'L');
	$this->SetLineWidth(0.3);
	$this->Line(10, 30, 270, 30);
	$this->Line(10, 39, 270, 39);
	$this->Ln(6);
	$this->SetFont('Arial','B',12);
	$this->SetFillColor(27,107,40);
	$this->SetTextColor(255,255,255);
	$this->MultiCell(260,7,$title,0,'C', true);

	

    //Salto de l�nea
//	$this->Ln(3.5);
$this->SetTextColor(0,0,0);

	$this->SetLineWidth(0.2);
	$this->SetFont('Arial','',8);
	$this->Cell(20,11,'MUNICIPIO:',0,0,'L');
	$this->Cell(70,11,$ayuntam,0,0,'L');
	$y=$this->GetY();
	$this->SetY($y+2);
	$this->SetX(100);
	$this->Cell(4,4,'X',1,0,'C');
	$this->Cell(25,4,'AYUNTAMIENTO',0,0,'L');
	$this->Cell(4,4,'',1,0,'C');
	$this->Cell(15,4,'ODAS',0,0,'L');
	$this->Cell(4,4,'',1,0,'L');
	$this->Cell(10,4,'DIF',0,0,'L');	
	$this->Cell(45,4,'UNIDAD ADMINISTRATIVA:',0,0,'R');
	$this->Cell(45,4,'PATRIMONIO',0,0,'L');
	$this->Ln(5);
	$this->Cell(90);
	$this->Cell(4,4,'',1,0,'L');
	$this->Cell(15,4,'OTRO',0,0,'L');
	$this->Cell(43);
	$this->Cell(45,4,'FECHA DE ELABORACI�N:',0,0,'R');
	$this->Cell(45,4, $dia.'/'. $mes . '/'. $year,0,0,'L');
	$this->Line(10, 50, 270, 50);
	$this->Ln(5);
	}

//Pie de p�gina
function Footer()
{
	global $usr;
	global $forma;
	global $var;
	global $dia;
	global $mes;
	global $year;
	global $ent;
	global $srio;
	global $nomtrab;
    //Posici�n: a 1,5 cm del final
    $this->SetY(-40);	
	$this->SetFont('Arial','B',8);
    //Arial italic 8
	$this->Cell(50, 0, $var, 0, 0, 'L');
	$this->Ln(12);
	$this->Cell(50);
	$this->Cell(60, 0, $usr, 0, 0, 'C');
	$this->Cell(25);
//	$this->Cell(60, 0, $nomtrab,0, 0, 'C');
	$this->Line(60, 190, 120, 190);
//	$this->Line(145, 190, 205, 190);
	$this->Ln(5);
	$this->Cell(50);
	$this->Cell(60, 1, 'ELABOR�', 0,0, 'C');
	$this->Cell(25);
	//$this->Cell(60, 1, 'ENTREGA', 0,0, 'C');
	$this->SetLineWidth(0.3);
	//$this->Line(10, 180, 270, 180);
	$this->Ln(5);
	$this->SetFont('Arial','',7);

	$this->Ln(5);
	//$this->Cell(70
	$this->Ln(0.5);
    $this->SetFont('Arial','I',7);
    //N�mero de p�gina
    $this->Cell(0,10,'P�gina '.$this->PageNo().'/{nb}',0,0,'C');
}



function SetWidths($w)
{
    //Set the array of column widths
    $this->widths=$w;
}

function SetAligns($a)
{
    //Set the array of column alignments
    $this->aligns=$a;
}
function Recibe($rec)
{
	//global $encabezado;
	$this->encabezado= $rec;
	//$this->Row($encabezado);
    //Set the array of column widths
    //$this->widths=$w;
}
function Alinea($alig)
{
	//global $encabezado;
	$this->alinea= $alig;
	//$this->Row($encabezado);
    //Set the array of column widths
    //$this->widths=$w;
}

function Row($data)


{
    //Calculate the height of the row
    $nb=0;
    for($i=0;$i<count($data);$i++)
        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
    $h=5*$nb;
    //Issue a page break first if needed
    $this->CheckPageBreak($h);
    //Draw the cells of the row
    for($i=0;$i<count($data);$i++)
    {
        $w=$this->widths[$i];
        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : $this->alinea;
        //Save the current position
        $x=$this->GetX();
        $y=$this->GetY();
        //Draw the border
        $this->Rect($x,$y,$w,$h);
        //Print the text
        $this->MultiCell($w,5,$data[$i],0,$a);
        //Put the position to the right of the cell
        $this->SetXY($x+$w,$y);
    }
    //Go to the next line
    $this->Ln($h);
	$this->SetDrawColor(255,255,255);
	$this->SetLineWidth(1);
	$this->Line(10, 45, 10, 200);
	$this->Line(270, 45, 270, 250);
	$this->SetDrawColor(1,1,1);
	$this->SetLineWidth(0.2);
}

function CheckPageBreak($h)
{
    //If the height h would cause an overflow, add a new page immediately
    //if($this->GetY()+$h>$this->PageBreakTrigger){
	if($this->GetY()+$h>160){
        $this->AddPage($this->CurOrientation);
		$y=$this->GetY();
		$alig='C';
		$this->SetLineWidth(0.5);
		$this->Line(10, $y, 270, $y);
		$this->SetLineWidth(0.2);
		$this->SetFont('Arial','B',9);	
		$this->alinea='C';	
$this->MultiCell(12,15,"No.",1,'C');
$this->SetXY(22,$this->GetY()-15);
$this->MultiCell(52,7.5,"REFERENCIA DEL INMUEBLE (NOMBRE Y UBICACI�N)",1,'C');
$this->SetXY(74,$this->GetY()-15);
$this->MultiCell(33,7.5,"SITUACI�N",1,'C');
$this->SetX(74);
$this->MultiCell(11,7.5,"A",1,'C');
$this->SetXY(85,$this->GetY()-7.5);
$this->MultiCell(11,7.5,"C",1,'C');
$this->SetXY(96,$this->GetY()-7.5);
$this->MultiCell(11,7.5,"U",1,'C');
$this->SetXY(107,$this->GetY()-15);
$this->MultiCell(27,7.5,"USO O DESTINO",1,'C');
$this->SetXY(134,$this->GetY()-15);
$this->MultiCell(36,5,"�REA ADMINISTRATIVA RESPONSABLE",1,'C');
$this->SetXY(170,$this->GetY()-15);
$this->MultiCell(42,7.5,"DOCUMENTO FUENTE (CONTRATO-CONVENIO)",1,'C');
$this->SetXY(212,$this->GetY()-15);
$this->MultiCell(24,15,"VIGENCIA",1,'C');
$this->SetXY(236,$this->GetY()-15);
$this->MultiCell(34,15,"OBSERVACIONES",1,'C');

		$y=$this->GetY();
		$this->alinea='L';	
		$this->SetLineWidth(0.5);
		$this->Line(10, $y, 270, $y);
		$this->SetLineWidth(0.2);
		$this->SetFont('Arial','',8);
	}
}

function NbLines($w,$txt)
{
    //Computes the number of lines a MultiCell of width w will take
    $cw=&$this->CurrentFont['cw'];
    if($w==0)
        $w=$this->w-$this->rMargin-$this->x;
    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
    $s=str_replace("\r",'',$txt);
    $nb=strlen($s);
    if($nb>0 and $s[$nb-1]=="\n")
        $nb--;
    $sep=-1;
    $i=0;
    $j=0;
    $l=0;
    $nl=1;
    while($i<$nb)
    {
        $c=$s[$i];
        if($c=="\n")
        {
            $i++;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
            continue;
        }
        if($c==' ')
            $sep=$i;
        $l+=$cw[$c];
        if($l>$wmax)
        {
            if($sep==-1)
            {
                if($i==$j)
                    $i++;
            }
            else
                $i=$sep+1;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
        }
        else
            $i++;
    }
    return $nl;
}
}
?>