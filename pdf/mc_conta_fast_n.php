<?php
	
	/*
		Modificado por Marco Robles
		Fecha: 09-01-2018
		Actualizaci�n: 3.3
	*/
	
	//require('fpdf.php');
	require('fpdf2file.php');
	//require('ufpdf.php');
	
	class PDF_MC_Table extends FPDF2File
	{
		var $widths;
		var $aligns;
		var $encabezado;
		var $alinea;
		//------
		var $_fontSize=15;
		//----
		function setFontS($_size){
			$this->_fontSize=$_size;
		}
		
		function Header()
		{
			global $title;
			global $area;
			global $ayuntam;
			global $adminIni;
			global $adminFin;
			global $tipoEntidad;
			//------
			global $_fontSize;
			//----
			//global $encabezado;
			//Logo
			//$this->SetTopMargin(2);
			$this->Image('../../../images/azul2.jpg',10,6,20);
			//Arial bold 15
			$this->SetFont('Arial','B',10);
			//Movernos a la derecha
			/*Todo este bloque*/
			//$this->Image('../../../images/image002.jpg', 200, 7, 30,20);
			$this->SetY(11);
			$this->Cell(25);
			$this->Cell(75,1,'',0,0,'L');
			$this->Cell(80);
			$this->Cell(75,1,'',0,0,'L');
			$this->Ln(3.5);
			$this->Cell(25);
			$this->Cell(75,1,'',0,0,'L');
			$this->Cell(80);
			$this->Cell(75,1,'',0,0,'L');
			$this->Ln(3.5);
			$this->Cell(25);
			$this->Cell(75,1,'',0,0,'L');
			$this->Cell(80);
			$this->Cell(75,1,'',0,0,'L');
			$this->Ln(3.5);
			$this->Cell(180);
			$this->SetFont('Arial','B',7);
			/*Todo este bloque*/
			
			$this->SetXY(178,28); //Aqui
			//$this->Cell(75,1,'PROGRAMA DE ENTREGA RECEPCI�N MUNICIPAL',0,0,'L'); //Aqui
			$this->SetXY(180,21); ///Aqui
			$this->SetFont('Arial','B',15);
			$this->Ln(3.5);
			$this->SetLineWidth(0.5);
			$this->Line(10, 30, 345, 30);
			$this->Ln(6);
			$this->SetFont('Arial','B', $this->_fontSize);
			
			$this->MultiCell(310,6,$title,0,'C');	
			//Linea que va debajodel titulo del formato
			$this->SetLineWidth(0.45);
			$this->Line(10, 36.7, 345, 36.7);
			$this->Ln(3);
			//Salto de l�nea
			//	$this->Ln(3.5);	
			$this->SetLineWidth(0.2);
			$this->SetFont('Arial','',9);
			$this->Cell(1);
			$this->MultiCell(330,4,'UNIDAD ADMINISTRATIVA: '.$area,0,'L');
			//$this->Ln(4);
			$this->Cell(1);
			$this->Cell(5,8,'ENTIDAD: '.$ayuntam,0,0,'L');
			$this->Cell(150);
			$this->Cell(5,8,'',0,0,'C');
			$this->Cell(150);
			$this->Cell(5,8,'FECHA DE ELABORACI�N: '.date("d/m/Y"),0,0,'R');
			$this->Ln(6);
			
		}
		
		//Pie de p�gina
		function Footer()
		{
			global $usr;
			global $forma;
			global $var;
			global $var1;
			global $dia;
			global $mes;
			global $year;
			global $ent;
			//Posici�n: a 1,5 cm del final
			$this->SetY(-60);		
			$this->SetFont('Arial','B',10);
			$this->Ln(1);
			//Arial italic 8
			$this->Cell(50, 0, $var, 0, 0, 'L');
			$this->Ln(5);
			if($var1==""){
				$lnSalto=11;
			}
			else{
				$lnSalto=8;
			}
			$this->Cell(50, 0, $var1, 0, 0, 'L');
			$this->Ln($lnSalto);
			$this->Cell(80);
			$this->Cell(165, 0, $usr, 0, 0, 'C');
			$this->Cell(26);
			//$this->Cell(80, 0, $ent, 0, 0, 'C');
			$this->Line(120, 175, 225, 175);
			//$this->Line(145, 175, 225, 175);
			$this->Ln(5);
			$this->Cell(95);
			$this->Cell(125, 1, 'ENTREGA', 0,0, 'C');
			$this->Cell(95);
			//$this->Cell(10, 1, 'RECIBE', 0,0, 'C');
			$this->SetLineWidth(0.5);
			$this->Line(10, 182, 345, 182);
			$this->Ln(11);
			$this->SetFont('Arial','',8);
			$this->Cell(230);
			
			$this->Ln(5);
			//$this->Cell(10, 4, $forma, 0,0, 'C');
			
			$this->Ln(5);
			$this->SetFont('Arial','I',8);
			
			// Print current and total page numbers
			$this->Cell(0,10,'P�gina '.$this->PageNo(),0,0,'C');
		}
		
		
		function SetWidths($w)
		{
			//Set the array of column widths
			$this->widths=$w;
		}
		
		function SetAligns($a)
		{
			//Set the array of column alignments
			$this->aligns=$a;
		}
		function Recibe($rec)
		{
			//global $encabezado;
			$this->encabezado= $rec;
			//$this->Row($encabezado);
			//Set the array of column widths
			//$this->widths=$w;
		}
		function Alinea($alig)
		{
			//global $encabezado;
			$this->alinea= $alig;
			//$this->Row($encabezado);
			//Set the array of column widths
			//$this->widths=$w;
		}
		
		function Row($data)
		{
			//Calculate the height of the row
			$nb=0;
			for($i=0;$i<count($data);$i++)
			$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
			$h=5*$nb;
			//Issue a page break first if needed
			$this->CheckPageBreak($h);
			//Draw the cells of the row
			for($i=0;$i<count($data);$i++)
			{
				$w=$this->widths[$i];
				$a=isset($this->aligns[$i]) ? $this->aligns[$i] : $this->alinea;
				//Save the current position
				$x=$this->GetX();
				$y=$this->GetY();
				//Draw the border
				$this->Rect($x,$y,$w,$h);
				//Print the text
				$this->MultiCell($w,5,$data[$i],0,$a);
				//Put the position to the right of the cell
				$this->SetXY($x+$w,$y);
			}
			//Go to the next line
			$this->Ln($h);	
		}
		
		function CheckPageBreak($h)
		{
			
			global $encabezado;
			
			//If the height h would cause an overflow, add a new page immediately
			//if($this->GetY()+$h>$this->PageBreakTrigger){
			if($this->GetY()+$h>160){
				$this->AddPage($this->CurOrientation);
				$y=$this->GetY();
				/*$alig='C';
					$this->SetLineWidth(0.5);
					$this->Line(10, $y, 345, $y);
					$this->SetLineWidth(0.2);
					$this->SetFont('Arial','B',10);	
					$this->alinea='C';	
					$this->Row($this->encabezado, $alig);
					$y=$this->GetY();
					$this->alinea='L';	
					$this->SetLineWidth(0.5);
					$this->Line(10, $y, 270, $y);
					$this->SetLineWidth(0.2);
				$this->SetFont('Arial','',7);*/
				$this->SetFont('Arial','B',8);
				$arrayWidth=array();
				$Y_AXIS = 49.5;
				$xAxis = 10;
				$tmpPrev='';
				
				$maxColumnas=count($encabezado)-1;
				for($i=0;$i<=$maxColumnas;$i++){
					$mcBGColor = FALSE;
					
					$nHeight = $encabezado[$i]['height'];
					if(isset($encabezado[$i]['sbt'])){
						$nHeight = $encabezado[$i]['height'] / 2;	
					}
					
					$this->SetXY($xAxis, $Y_AXIS);	
					
					if($encabezado[$i]['rtbg'] != ''){
						$rgbColorB = explode(',', $encabezado[$i]['rtbg']);
						$this->SetFillColor($rgbColorB[0], $rgbColorB[1], $rgbColorB[2]);
						$mcBGColor = TRUE;
					}
					
					$this->MultiCell($encabezado[$i]['width'], $nHeight, $encabezado[$i]['rt'], 1, 'C', $mcBGColor);
					
					if(!isset($encabezado[$i]['sbt'])){	
						$arrayWidth[]=$encabezado[$i]['width'];
						$arrayValores[]=$encabezado[$i]['rf'];
					}
					
					$tmpPrev = $encabezado[$i]['rf'];
					
					if(isset($encabezado[$i]['sbt'])){
						$maxSbt = count($encabezado[$i]['sbt'])-1;
						$tmpX = $xAxis;
						
						for($j=0;$j<=$maxSbt;$j++){
							$this->SetXY($tmpX, $Y_AXIS + $nHeight);
							$this->MultiCell($encabezado[$i]['sbt'][$j]['width'], $nHeight, $encabezado[$i]['sbt'][$j]['titulo'], 1, 'C', 0);
							$tmpX += $encabezado[$i]['sbt'][$j]['width'];
							
							$arrayWidth[]=$encabezado[$i]['sbt'][$j]['width'];
							
							if($encabezado[$i]['sbt'][$j]['rsbtf'] == 'phpSi' || $encabezado[$i]['sbt'][$j]['rsbtf'] == 'phpNo'){
								$arrayValores[]=$tmpPrev.'+_+'.$encabezado[$i]['sbt'][$j]['rsbtf'];
							}
							else{
								$arrayValores[]=$encabezado[$i]['sbt'][$j]['rsbtf'];
							}
						}
					}	
					$xAxis += $encabezado[$i]['width'];	
				}
				$this->SetFont('Arial','',7);
				
			}
		}
		
		function NbLines($w,$txt)
		{
			//Computes the number of lines a MultiCell of width w will take
			$cw=&$this->CurrentFont['cw'];
			if($w==0)
			$w=$this->w-$this->rMargin-$this->x;
			$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
			$s=str_replace("\r",'',$txt);
			$nb=strlen($s);
			if($nb>0 and $s[$nb-1]=="\n")
			$nb--;
			$sep=-1;
			$i=0;
			$j=0;
			$l=0;
			$nl=1;
			while($i<$nb)
			{
				$c=$s[$i];
				if($c=="\n")
				{
					$i++;
					$sep=-1;
					$j=$i;
					$l=0;
					$nl++;
					continue;
				}
				if($c==' ')
				$sep=$i;
				$l+=$cw[$c];
				if($l>$wmax)
				{
					if($sep==-1)
					{
						if($i==$j)
						$i++;
					}
					else
					$i=$sep+1;
					$sep=-1;
					$j=$i;
					$l=0;
					$nl++;
				}
				else
				$i++;
			}
			return $nl;
		}
	}
?>