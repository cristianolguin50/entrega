<?php
	define('FPDF_FONTPATH','font/');
	require('fpdi.php');
	class concat_pdf extends fpdi {
		
		var $files = array();
		
		function concat_pdf($orientation='P',$unit='mm',$format='Letter') {
			parent::fpdi($orientation,$unit,$format);
		}
		
		function setFiles($files) {
			$this->files = $files;
		}
		
		function concat() {
			global $dependencia;
			global $dependencia_nom;
			global $ayuntamiento;
			global $ubicacion;
			global $hora;
			global $minutos;
			global $hora_ini;
			global $minutos_ini;
			global $dia;
			global $mes;
			global $year;
			//global $tipoActa;
			
			/*
			if($tipoActa==4){
				global $presenta;
				global $curp_presenta;
				global $rfc_presenta;
				global $oficial_presenta;
				global $oficial_tipo_presenta;
				global $oficial_institucion_presenta;
				global $telefono_presenta;
				global $pre_direccion;
			}
			*/
			
			global $contralor;
			global $sindico;		
			global $osfem;
			global $cargoaer1;
			
			
			global $observa;
			global $testigo1;
			global $cargotest1;
			global $testigo2;
			global $cargotest2;
			
			global $ifetest1;
			global $ifetest1_tipo;
			global $ifetest1_institucion;
			
			global $ifetest2;
			global $ifetest2_tipo;
			global $ifetest2_institucion;
			
			global $representante_institucion;
			global $representante_tipo;
			global $representante_folio;		
			
			//global $interventor_cargo;
			
			global $tipoEntidad;
			
			global $aer_motivo;
			
			//Saliente
			global $ex_serv;
			global $oficial1;
			global $oficial1_tipo;
			global $oficial1_institucion;
			
			global $acta1;
			global $nombramiento1;
			global $constancia1;
			global $credencial1;
			global $cargo1;
			global $inicio1;
			global $fin1;
			global $edad1;
			global $edo_civil1;
			global $direccion1;		
			
			global $mail1;
			global $lugar_nac1;
			global $curp1;
			global $rfc1;
			global $issem1;
			global $tel1;
			global $tel11;
			global $domicilio1;
			global $persona1;
			global $direccion11;
			global $comprobante1;
			
			//Entrante
			global $entrante;
			global $oficial2;
			global $oficial2_tipo;
			global $oficial2_institucion;
			
			
			global $acta2;
			global $nombramiento2;
			global $cargo2;
			global $inicio2;
			global $edad2;
			global $edo_civil2;
			global $direccion2;
			global $mail2;
			global $lugar_nac2;
			global $curp2;
			global $rfc2;
			global $issem2;
			global $tel2;
			global $tel21;
			global $domicilio2;
			global $persona2;
			global $direccion21;
			global $comprobante2;
			global $observaciones;
			
			global $rsaFirmaActa;
			global $txtCadenaOriginal;
			
			global $activa;
			global $clave_area;
			global $fecha;
			global $usuario;
			global $ioe001;
			global $ioe004;
			global $ioe005;
			global $ioe0051;
			global $ioe006;
			global $ioe010;
			global $ioe011;
			global $ioe013;
			global $ioe014;
			global $ioe015;
			global $ioe016;
			global $ioe017;
			global $ioe018;
			global $ioe019;
			global $ioe021;
			global $ioe022;
			global $ioe023;
			global $iep001;
			global $iep002;
			global $iep003;
			global $iep004;
			global $iep005;
			global $iep006;
			global $iad001;
			global $iad002;
			global $iad004;
			global $iad005;
			global $iad006;
			global $iad007;
			global $iad008;
			global $iad009;
			global $iad0091;
			global $iad0092;
			global $iad010;
			global $iad0101;
			global $iad011;
			global $iad012;
			global $iad013;
			global $iad014;
			global $iad015;
			global $iad016;
			global $iad017;
			global $iad018;
			global $iad019;
			global $iad020;
			global $iad022;
			global $iad023;
			global $iad0231;
			global $iad024;
			global $iad025;
			global $iad026;
			global $iad027;
			global $iad028;
			global $iad029;
			global $iad030;
			global $iad031;
			global $iad032;
			global $iad033;
			global $iad034;
			global $iad035;
			global $iad036;
			global $ige001;
			global $ige002;
			global $ige003;
			global $ila001;
			global $ila005;
			global $ila006;
			global $ica001;
			global $ica002;
			global $ica003;
			global $ica004;
			global $ica005;
			global $ica006;
			global $ica007;
			global $ica008;
			global $ica009;
			global $iop001;
			global $iop002;
			global $iop003;
			global $ipe001;
			global $ipe002;
			global $ipe003;
			global $ipe004;
			global $ipe005;
			global $ipe006;
			global $ipe007;
			global $ipe008;
			$ial000=0;
			global $ial001;
			global $ial0011;
			global $ial002;
			global $ial003;
			global $ioe001_c;
			global $ioe004_c;
			global $ioe005_c;
			global $ioe0051_c;
			global $ioe006_c;
			global $ioe010_c;
			global $ioe011_c;
			global $ioe013_c;
			global $ioe014_c;
			global $ioe015_c;
			global $ioe016_c;
			global $ioe017_c;
			global $ioe018_c;
			global $ioe019_c;
			global $ioe021_c;
			global $ioe022_c;
			global $ioe023_c;
			global $iep001_c;
			global $iep002_c;
			global $iep003_c;
			global $iep004_c;
			global $iep005_c;
			global $iep006_c;
			global $iad001_c;
			global $iad002_c;
			global $iad004_c;
			global $iad005_c;
			global $iad006_c;
			global $iad007_c;
			global $iad008_c;
			global $iad009_c;
			global $iad0091_c;
			global $iad0091_c;
			global $iad0091_c;
			global $iad0092_c;
			global $iad010_c;
			global $iad0101_c;
			global $iad011_c;
			global $iad012_c;
			global $iad013_c;
			global $iad014_c;
			global $iad015_c;
			global $iad016_c;
			global $iad017_c;
			global $iad018_c;
			global $iad019_c;
			global $iad020_c;
			global $iad022_c;
			global $iad023_c;
			global $iad0231_c;
			global $iad024_c;
			global $iad025_c;
			global $iad026_c;
			global $iad027_c;
			global $iad028_c;
			global $iad029_c;
			global $iad030_c;
			global $iad031_c;
			global $iad032_c;
			global $iad033_c;
			global $iad034_c;
			global $iad035_c;
			global $iad036_c;
			global $ige001_c;
			global $ige002_c;
			global $ige003_c;
			global $ila001_c;
			global $ila005_c;
			global $ila006_c;
			global $ica001_c;
			global $ica002_c;
			global $ica003_c;
			global $ica004_c;
			global $ica005_c;
			global $ica006_c;
			global $ica007_c;
			global $ica008_c;
			global $ica009_c;
			global $iop001_c;
			global $iop002_c;
			global $iop003_c;
			global $ipe001_c;
			global $ipe002_c;
			global $ipe003_c;
			global $ipe004_c;
			global $ipe005_c;
			global $ipe006_c;
			global $ipe007_c;
			global $ipe008_c;
			global $ial000_c;
			global $ial001_c;
			global $ial0011_c;
			global $ial002_c;
			global $ial003_c;
			
			/*if($tipoActa==4){
				$ex_serv = $presenta;
					$curp1 = $curp_presenta;
					$rfc1 = $rfc_presenta;
					$oficial1 = $oficial_presenta;
					$oficial1_tipo = $oficial_tipo_presenta;
					$oficial1_institucion = $oficial_institucion_presenta;
				$tel1 = $telefono_presenta;
			}*/
			
			
			foreach($this->files AS $file) {
				
            $pagecount = $this->setSourceFile($file);
				
				$tplidx = $this->ImportPage(1);
				
				$this->AddPage();
				
				$xposT = $this->GetX();
				
				//-----------------
				$this->SetX(12);
				$this->SetY(10);
				
				$this->SetFont('Arial','', 5, 0);
				$this->MultiCell(166,2, $txtCadenaOriginal,0,'L');
				
				//Toponimo de la entidad
				$this->Image("../images/azul2.jpg" , 18 ,35, 20,20); 
				
				$this->SetX(12);
				$this->SetY(56);
				
				$this->SetFillColor(194,194,194);
				//$this->SetFillColor(189,189,189);
				$this->SetFont('Arial','B', 12, 0);
				$this->Cell(6);
				
				//Verificar si es encargado o titular
				$encargado_despacho = array();
				$encargado_despacho[0] = substr($cargo1, 0, 2);
				$encargado_despacho[1] = substr($cargo1, 2, strlen($cargo1));					
				
				if($encargado_despacho[0] == '_E'){
				 	//-----Tipo acta (encargado)--------
				 	$cargo1 = 'ENCARGADO DE DESPACHO DE '.$dependencia_nom;
				}
				//---------------------------
				
				/*if($tipoEntidad != 'Ayuntamiento'){
					$tipoActa = 3;
				}*/
				//-----------------------------------------
				
				//switch($tipoActa){
					//case(1):
					$this->Cell(187,6,"ACTA DE ENTREGA-RECEPCI�N",0,0,'C',1);
					//break;
					/*
					case(2):
					$this->Cell(187,7,"ACTA DE ENTREGA-RECEPCI�N AER-2",0,0,'C',1);
					break;
					
					case(3):
					$this->Cell(187,6,"ACTA DE ENTREGA-RECEPCI�N AER-3",0,0,'C',1);
					break;
					
					case(4):
					$this->Cell(187,6,"ACTA DE ENTREGA-RECEPCI�N AER-4",0,0,'C',1);
					break;
					*/
				//}					 
				//----------------
				$this->Ln(2);
				$this->SetXY($xposT+6, 63);				
				
				$this->SetFont('Arial','', 6.5);
				//$this->Ln(50);
				
				//echo "->".$ex_serv;
				
				//switch($tipoActa){ 
					//case(1):
					//-----------------------------------------------Acta ER1--------------------------------------------------------------------------
					
					$this->MultiCell(187,4.5,"En el municipio de $ayuntamiento, M�xico, siendo las ".$hora_ini." horas con ".$minutos_ini." minutos del d�a ".$dia." de $mes del a�o dos mil ".$year.", en las oficinas que ocupa ".$dependencia_nom." con domicilio en ".$ubicacion."; reunidos los ciudadanos ".$ex_serv.", SERVIDOR P�BLICO SALIENTE; ".$entrante.", SERVIDOR P�BLICO ENTRANTE; ".$testigo1.", TESTIGO DEL SERVIDOR P�BLICO SALIENTE; ".$testigo2.", TESTIGO DEL SERVIDOR P�BLICO ENTRANTE y ".$osfem.", ".$cargoaer1."; para dar cumplimiento a lo dispuesto en los art�culos 19 de la Ley Org�nica Municipal del Estado de M�xico; art�culo 4, 8, 14, 16 y dem�s relativos aplicables de los Lineamientos que Regulan la Entrega-Recepci�n de la Administraci�n P�blica Municipal del Estado de M�xico, se procede a llevar cabo el acto de entrega-recepci�n del �rea de ".$dependencia_nom."; donde el (la) ciudadano (a) ".$ex_serv.", quien ocup� el cargo de ".$cargo1." por el per�odo comprendido del ".$inicio1." al ".$fin1.", entrega al (a la) ciudadano (a) ".$entrante.", el despacho de la unidad administrativa, con sus recursos, documentos e informaci�n inherente a las atribuciones, funciones, facultades y actividades del �rea. ---------------------------------------------------------------------------------
Acto continuo el (la) ciudadano (a) ".$ex_serv.", SERVIDOR P�BLICO SALIENTE, manifiesta llamarse como ha quedado escrito, con domicilio actual en ".$direccion1.", con Clave �nica de Registro de Poblaci�n (CURP) ".$curp1.", con Registro Federal de Contribuyentes (RFC) ".$rfc1.", con tel�fono particular ".$tel1.", quien se identifica con ".$oficial1_tipo." con folio ".$oficial1." expedida por el ".$oficial1_institucion.", de la que se obtiene copia fotost�tica y se anexa a la presente, as� mismo para los efectos previstos en el art�culo 16, fracciones XVII y XVIII de los Lineamientos que Regulan la Entrega-Recepci�n de la Administraci�n P�blica Municipal del Estado de M�xico, se�ala como domicilio para o�r y recibir todo tipo de notificaciones el ubicado en ".$domicilio1.", el cual se encuentra dentro del territorio del Estado de M�xico, renunciando a cualquier otro que por raz�n de su domicilio presente o futuro le pudiera corresponder y autoriza al (los) ciudadano (s): ".$persona1.", para que a su nombre y representaci�n reciba (n) todo tipo de documentos relacionados con el ejercicio del cargo que tuvo conferido, con la presente entrega-recepci�n y con la revisi�n y fiscalizaci�n que realice el �rgano Superior de Fiscalizaci�n del Estado de M�xico; as� mismo, el SERVIDOR P�BLICO SALIENTE; contin�a manifestando que conforme al art�culo 156, fracci�n I del C�digo Penal del Estado de M�xico, conoce que comete el delito de falso testimonio, el que interrogado por alguna autoridad p�blica o fedatario en ejercicio de sus funciones o con motivo de ellas, faltare a la verdad; asi mismo, reconoce que al responsable de este delito se le impondr�n de dos a seis a�os de prisi�n y de treinta a setecientos cincuenta d�as multa; por lo que, en este acto bajo protesta de decir verdad, asevera que lo asentado en la presente acta, la informaci�n contenida en sus anexos y lo archivado y procesado en los medios �pticos es ver�dico, oportuno y confiable, toda vez que dicha informaci�n, se encuentra soportada con los documentos y constancias, las cuales se encuentran custodiadas y conservadas en los archivos de la oficina que se entrega. ----------------------------------------------------------------------------------------------------------------------
En uso de la palabra el (la) ciudadano (a) ".$entrante.", SERVIDOR P�BLICO ENTRANTE, manifiesta llamarse como ha quedado escrito, con domicilio actual en ".$direccion2.", el cual se encuentra dentro del territorio del Estado de M�xico, con Clave �nica de Registro de Poblaci�n (CURP) ".$curp2.", con Registro Federal de Contribuyentes (RFC) ".$rfc2.", con tel�fono particular ".$tel2.", quien  se identifica con ".$oficial2_tipo." con folio ".$oficial2." expedida por el ".$oficial2_institucion.", de la que se obtiene copia fotost�tica y se anexa a la presente, quien fue designado para ocupar el cargo de ".$cargo2." a partir del ".$inicio2.". ----------------------------------------------------------------------------------------------------------------------
El (la) ciudadano (a) ".$testigo1.", TESTIGO DEL SERVIDOR P�BLICO SALIENTE, quien se identifica con  ".$ifetest1_tipo." con folio ".$ifetest1." expedida por el ".$ifetest1_institucion.", de la que se obtiene copia fotost�tica y se anexa a la presente. ----------------------------------------------------------------------------------------------------------------------
El (la) ciudadano (a) ".$testigo2.", TESTIGO DEL SERVIDOR P�BLICO ENTRANTE, quien se identifica con ".$ifetest2_tipo."  con folio ".$ifetest2." expedida por el ".$ifetest2_institucion.", de la que se obtiene copia fotost�tica y se anexa a la presente. ----------------------------------------------------------------------------------------------------------------------
El (la) ciudadano (a) ".$osfem.", ".$cargoaer1.", quien se identifica con ".$representante_tipo." con folio ".$representante_folio." expedida por el ".$representante_institucion.", de la que se obtiene copia fotost�tica y se anexa a la presente. Lo anterior, en los t�rminos legales correspondientes y conforme a la siguiente informaci�n:",0,'J');		
					//---------------------------------------------------------------------------------------------------------------------------------
					
					//break;
					/*
					case(2):	
					//-----------------------------------------------------Acta ER2---------------------------------------------------------------------	
					$this->MultiCell(187,5,"En el Municipio de ".$ayuntamiento.", M�xico, siendo las ".$hora_ini." horas con ".$minutos_ini." minutos del d�a ".$dia." de ".$mes." del a�o dos mil ".$year.", en las oficinas que ocupa ".$dependencia_nom." con domicilio en ".$ubicacion."; reunidos los ciudadanos ".$ex_serv.", SERVIDOR P�BLICO SALIENTE; ".$entrante.", SERVIDOR P�BLICO ENTRANTE; ".$testigo1.", TESTIGO DEL SERVIDOR P�BLICO SALIENTE; ".$testigo2.", TESTIGO DEL SERVIDOR P�BLICO ENTRANTE y ".$sindico.", ".$cargoaer1."; para dar cumplimiento a lo dispuesto en los art�culos 19 de la Ley Org�nica Municipal del Estado de M�xico; articulos 10, 11 y dem�s relativos aplicables de los Lineamientos que Regulan  la Entrega-Recepci�n de la Administraci�n P�blica Municipal del Estado de M�xico, se procede a llevar cabo el acto de entrega-recepci�n del �rea de ".$dependencia_nom."; donde el (la) ciudadano (a) ".$ex_serv.", quien ocup� el cargo de ".$cargo1." por el periodo comprendido del ".$inicio1." al ".$fin1.", entrega al (a la) ciudadano (a) ".$entrante.", la oficina con los recursos financieros, program�ticos, humanos, materiales, documentales, legales, laborales, sistemas de informaci�n, organizaci�n y m�todos, seg�n sea el caso, as� como aquellos que resultaron necesarios.---------------------------------------------------------------------------------------------
					Acto continuo el (la) ciudadano (a) ".$ex_serv.", SERVIDOR P�BLICO SALIENTE, manifiesta llamarse como ha quedado escrito, con domicilio actual en ".$direccion1.", con CURP ".$curp1. ", con Registro Federal de Contribuyentes  ".$rfc1.", con tel�fono  particular ".$tel1.", quien se identifica con " .$oficial1_tipo. " con folio " .$oficial1.  " expedida por el " .$oficial1_institucion. ", de la que se obtiene copia simple y se anexa a la presente, as� mismo para los efectos previstos en el art�culo 60, fracci�n X y XI de los Lineamientos que Regulan  la Entrega-Recepci�n de la Administraci�n P�blica Municipal del Estado de M�xico, se�ala como domicilio para o�r y recibir todo tipo de notificaciones el ubicado en ".$domicilio1.", el cual se encuentra dentro del territorio del Estado de M�xico, renunciando a cualquier otro que por raz�n de su domicilio presente o futuro le pudiera corresponder y autoriza al (los) ciudadano (s): ".$persona1.", para que a su nombre y representaci�n reciba (n) todo tipo de documentos relacionados con el ejercicio del cargo que tuvo conferido, con la presente entrega-recepci�n y con la revisi�n y  fiscalizaci�n que realice el �rgano Superior de Fiscalizaci�n del Estado de M�xico; as� mismo, el SERVIDOR P�BLICO SALIENTE, contin�a manifestando que conforme al art�culo 156, fracci�n I del C�digo Penal del Estado de M�xico, conoce que comete el delito de falso testimonio, el que interrogado por alguna autoridad p�blica o fedatario en ejercicio de sus funciones o con motivo de ellas, faltare a la verdad; asimismo, reconoce que al responsable de este delito se le impondr�n de dos a seis a�os de prisi�n y de treinta a setecientos cincuenta d�as multa; por lo que, en este acto bajo protesta de decir verdad, asevera que lo asentado en la presente acta, la informaci�n contenida en sus anexos y lo archivado y procesado en los medios �pticos es ver�dico, oportuno y confiable, toda vez que dicha informaci�n, se encuentra soportada con los documentos y constancias, las cuales se encuentran custodiadas y conservadas en los archivos de la oficina que se entrega.-------------------------------------------------------------------------------------------------
					En uso de la palabra el (la) ciudadano (a) ".$entrante.", SERVIDOR P�BLICO ENTRANTE, manifiesta llamarse como ha quedado escrito, con domicilio actual en ".$direccion2.", con CURP " .$curp2.", con Registro Federal de Contribuyentes ".$rfc2.", con tel�fono particular ".$tel2.", quien  se identifica con ".$oficial2_tipo."  con folio ".$oficial2."  expedida por el ".$oficial2_institucion.", de la que se obtiene copia simple y se anexa a la presente, quien fue designado para ocupar el cargo de ".$cargo2."  a partir del ".$inicio2.".-------------------------------------------------------------------------------------------------------
					El (la) ciudadano (a) ".$testigo1.", TESTIGO DEL SERVIDOR P�BLICO SALIENTE, quien se identifica con ".$ifetest1_tipo." con folio ".$ifetest1." expedida por el ".$ifetest1_institucion.", de la que se obtiene copia simple y se anexa a la presente.---------------------------------------------
					El (la) ciudadano (a) ".$testigo2.", TESTIGO DEL SERVIDOR P�BLICO ENTRANTE, quien se identifica con ".$ifetest2_tipo." con folio ".$ifetest2." expedida por el ".$ifetest2_institucion.", de la que se obtiene copia simple y se anexa a la presente ---------------------------------------------------------------------------------------------------------
					El (la) ciudadano (a) ".$sindico.", ".$cargoaer1." de ".$ayuntamiento.", M�xico, quien se identifica con ".$representante_tipo." con folio ".$representante_folio." expedida por el ".$representante_institucion.", de la que se obtiene copia simple y se anexa a la presente. Lo anterior, en los t�rminos legales correspondientes y conforme a la siguiente informaci�n:",0,'J');
					//---------------------------------------------------------------------------------------------------------------------------------
					break;
					*/
					
					/*
					case(3):
					
					//-----------------------------------------------Acta ER3--------------------------------------------------------------------------
					$this->MultiCell(187,5,"En el Municipio de ".$ayuntamiento.", M�xico, siendo las ".$hora_ini." horas con ".$minutos_ini." minutos del d�a ".$dia." de ".$mes." del a�o dos mil ".$year.", en las oficinas que ocupa ".$dependencia_nom." con domicilio en ".$ubicacion."; reunidos los ciudadanos ".$ex_serv.", SERVIDOR P�BLICO SALIENTE; ".$entrante.", SERVIDOR P�BLICO ENTRANTE; ".$testigo1.", TESTIGO DEL SERVIDOR P�BLICO SALIENTE; ".$testigo2.", TESTIGO DEL SERVIDOR P�BLICO ENTRANTE y ".$contralor.", ".$cargoaer1." DEL �RGANO DE CONTROL INTERNO; para dar cumplimiento a lo dispuesto en los art�culos 19 de la Ley Org�nica Municipal del Estado de M�xico; art�culos 10, 12 y dem�s aplicables de los Lineamientos que Regulan la Entrega-Recepci�n de la Administraci�n P�blica Municipal del Estado de M�xico, se procede a llevar cabo el acto de entrega-recepci�n del �rea de ".$dependencia_nom."; donde el (la) ciudadano (a) ".$ex_serv.", quien ocup� el cargo de ".$cargo1." por el periodo comprendido del ".$inicio1." al ".$fin1.", entrega al (a la) ciudadano (a) ".$entrante.", la oficina con los recursos financieros, program�ticos, humanos, materiales, documentales, legales, laborales, sistemas de informaci�n, organizaci�n y m�todos, seg�n sea el caso, as� como aquellos que resultaron necesarios.------------------------------------------------------------------------------------------------
					Acto continuo el (la) ciudadano (a) ".$ex_serv.", SERVIDOR P�BLICO SALIENTE, manifiesta llamarse como ha quedado escrito, con domicilio actual en ".$direccion1.", con CURP ".$curp1.", con Registro Federal de Contribuyentes ".$rfc1.", con tel�fono particular ".$tel1.", quien se identifica con ".$ifetest1_tipo." con folio ".$ifetest1." expedida por el  ".$ifetest1_institucion.", de la que se obtiene copia simple y se anexa a la presente, as� mismo para los efectos previstos en el art�culo 60, fracci�n  X y XI de los Lineamientos que Regulan la Entrega-Recepci�n de la Administraci�n P�blica Municipal del Estado de M�xico, se�ala como domicilio para o�r y recibir todo tipo de notificaciones el ubicado en ".$domicilio1.", el cual se encuentra dentro del territorio del Estado de M�xico, renunciando a cualquier otro que por raz�n de su domicilio presente o futuro le pudiera corresponder y autoriza al (los) ciudadano (s): ".$persona1.", para que a su nombre y representaci�n reciba (n) todo tipo de documentos relacionados con el ejercicio del cargo que tuvo conferido, con la presente entrega-recepci�n y con la revisi�n y  fiscalizaci�n que realice el �rgano Superior de Fiscalizaci�n del Estado de M�xico; as� mismo, el SERVIDOR P�BLICO SALIENTE, contin�a manifestando que conforme al art�culo 156 fracci�n I del C�digo Penal del Estado de M�xico, conoce que comete el delito de falso testimonio, el que interrogado por alguna autoridad p�blica o fedatario en ejercicio de sus funciones o con motivo de ellas, faltare a la verdad; asimismo, reconoce que al responsable de este delito se le impondr�n de dos a seis a�os de prisi�n y de treinta a setecientos cincuenta d�as multa; por lo que, en este acto bajo protesta de decir verdad, asevera que lo asentado en la presente acta, la informaci�n contenida en sus anexos y lo archivado y procesado en los medios �pticos es ver�dico, oportuno y confiable, toda vez que dicha informaci�n, se encuentra soportada con los documentos y constancias, las cuales se encuentran custodiadas y conservadas en los archivos de la oficina que se entrega.---------------------------------------------------------------------------------------------------
					En uso de la palabra el (la) ciudadano (a) ".$entrante.", SERVIDOR P�BLICO ENTRANTE, manifiesta llamarse como ha quedado escrito, con domicilio actual en ".$direccion2.", con CURP ".$curp2.", con Registro Federal de Contribuyentes ".$rfc2.", con tel�fono particular ".$tel2.", quien  se identifica con ".$oficial2_tipo." con folio ".$oficial2." expedida por el ".$oficial2_institucion.", de la que se obtiene copia simple y se anexa a la presente, quien fue designado para ocupar el cargo de ".$cargo2." a partir del ".$inicio2.".------------------
					El (la) ciudadano (a) ".$testigo1.", TESTIGO DEL SERVIDOR P�BLICO SALIENTE, quien se identifica con ".$ifetest1_tipo." con folio ".$ifetest1." expedida por el ".$ifetest1_institucion.", de la que se obtiene copia simple y se anexa a la presente.--------------------------------------------------------------------------------------------------
					El (la) ciudadano (a) ".$testigo2.", TESTIGO DEL SERVIDOR P�BLICO ENTRANTE, quien se identifica con ".$ifetest2_tipo." con folio ".$ifetest2." expedida por el ".$ifetest2_institucion.", de la que se obtiene copia simple y se anexa a la presente. --------------------------------------------
					El (la) ciudadano (a) ".$contralor.", ".$cargoaer1." DEL �RGANO DE CONTROL INTERNO de ".$ayuntamiento.", M�xico, quien se identifica con ".$representante_tipo."  con folio ".$representante_folio." expedida por el ".$representante_institucion.", de la que se obtiene copia simple y se anexa a la presente. Lo anterior, en los t�rminos legales correspondientes y conforme a la siguiente informaci�n:",0,'J');
					//---------------------------------------------------------------------------------------------------------------------------------
					break;
					*/
					
					/*
					case(4):
					
					//verificamos quien interviene en el acto
					$nombreParaMostrar='';
					
					//osfem
					if($contralor == NULL && $sindico== NULL){
						$nombreParaMostrar = $osfem;
					}
					
					//sindico
					if($contralor == NULL && $osfem== NULL){
						$nombreParaMostrar = $sindico;
					}
					
					//contralor
					if($sindico == NULL && $osfem== NULL){
						$nombreParaMostrar = $contralor;
					}
					
					//-----------------------------------------------Acta ER4--------------------------------------------------------------------------
					$this->MultiCell(187,5,"En el Municipio de ".$ayuntamiento.", M�xico, siendo las ".$hora_ini." horas con ".$minutos_ini." minutos del d�a ".$dia." de ".$mes." del a�o dos mil ".$year.", en las oficinas que ocupa ".$dependencia_nom." con domicilio en ".$ubicacion."; reunidos los ciudadanos ".$presenta.", SERVIDOR P�BLICO QUE PRESENTA LA INFORMACI�N; ".$entrante.", SERVIDOR P�BLICO ENTRANTE; ".$testigo1.", TESTIGO DEL SERVIDOR P�BLICO QUE PRESENTA LA INFORMACI�N; ".$testigo2.", TESTIGO DEL SERVIDOR P�BLICO ENTRANTE y ".$nombreParaMostrar.", ".$cargoaer1.", para dar cumplimiento a lo dispuesto en los art�culos 19 de la Ley Org�nica Municipal del Estado de M�xico; articulo 9, y dem�s relativos aplicables de los Lineamientos que Regulan la Entrega-Recepci�n de la Administraci�n P�blica Municipal del Estado de M�xico, se procede a llevar cabo el acto de entrega-recepci�n del �rea de ".$dependencia_nom." por el (la) ".$aer_motivo."; donde el (la) ciudadano (a) " .$ex_serv. ", ocup� el cargo de ".$cargo1." por el periodo comprendido del ".$inicio1." al " .$fin1. " y con domicilio referido en ".$direccion1.".-----------------------
					Acto continuo el (la) ciudadano (a) ".$presenta.", SERVIDOR P�BLICO QUE PRESENTA LA INFORMACI�N, manifiesta llamarse como ha quedado escrito, con domicilio actual en ".$pre_direccion.", con CURP ".$curp_presenta.", con Registro Federal de Contribuyentes ".$rfc_presenta." y con tel�fono  particular ".$telefono_presenta.";  quien  se identifica con ".$oficial_tipo_presenta." con folio ".$oficial_presenta." expedida por el ".$oficial_institucion_presenta.", de la que se obtiene copia simple y se anexa a la presente; as� mismo, para los efectos previstos en el art�culo 60, fracciones X y XI de los Lineamientos que Regulan  la Entrega-Recepci�n de la Administraci�n P�blica Municipal del Estado de M�xico, se�ala como domicilio para o�r y recibir todo tipo de notificaciones el ubicado en ".$domicilio1.", el cual se encuentra dentro del territorio del Estado de M�xico, renunciando a cualquier otro que por raz�n de su domicilio presente o futuro le pudiera corresponder y autoriza al (los) ciudadano (s): ".$persona1.", para que a su nombre y representaci�n reciba (n) todo tipo de documentos; as� mismo, asevera que lo asentado en la presente acta, se encuentra soportado con los documentos y constancias, las cuales se encuentran custodiadas y conservadas en los archivos de la oficina que se entrega.-----------------------------------------------------------------
					Acto continuo el (la) ciudadano (a) ".$entrante.", SERVIDOR P�BLICO ENTRANTE, manifiesta llamarse como ha quedado escrito, con domicilio actual en ".$direccion2.", con CURP ".$curp2.", con Registro Federal de Contribuyentes ".$rfc2.", con tel�fono particular ".$tel2.", quien  se identifica con ".$oficial2_tipo." con folio ".$oficial2 ." expedida por el ".$oficial2_institucion.", de la que se obtiene copia simple y se anexa a la presente, quien fue designado para ocupar el cargo de ".$cargo2." a partir del ".$inicio2.".--------------------------------------------------
					El (la) ciudadano (a) ".$testigo1.", TESTIGO DEL SERVIDOR P�BLICO QUE PRESENTA LA INFORMACI�N, quien se identifica con ".$ifetest1_tipo." con folio ".$ifetest1." expedida por el ".$ifetest1_institucion.", de la que se obtiene copia simple y se anexa a la presente.-----------------------------------
					El (la) ciudadano (a) ".$testigo2.", TESTIGO DEL SERVIDOR P�BLICO ENTRANTE, quien se identifica con ".$ifetest2_tipo." con folio ".$ifetest2." expedida por el ".$ifetest2_institucion.", de la que se obtiene copia simple y se anexa a la presente. ----------------------------------------------------------------------------------------------------
					El (la) ciudadano (a) ".$nombreParaMostrar.", ".$cargoaer1.", se identifica con ".$representante_tipo." con folio ".$representante_folio." expedida por el (la) ".$representante_institucion.", de la que se obtiene copia simple y se anexa a la presente.--------------------------------------------------------------------------------
					Lo anterior, en los t�rminos legales correspondientes y conforme a la siguiente informaci�n: ",0,'J');
					//---------------------------------------------------------------------------------------------------------------------------------
					break;
					*/
				//}					
				//--------------------
				
				$this->SetFont('Arial','', 9);
				
				$this->useTemplate($tplidx);
				
				$tplidx = $this->ImportPage(2);
				$this->AddPage();
				
				/************************/
				$cAnexoSi = 162.5; //168
				$cAnexoNo = 177;
				/************************/
				
				//Modificamos X global				
				$this->SetX($xposT+60);
				
				
				//-------------1-----------------
				//echo ' el valor de ioe001 ', $ioe001;
				//exit;
				$this->SetY(50); //50
				
				if ($ioe001==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $ioe001_c, 0, 0, 'L');
				
				//-------------2-----------------
				
				$this->SetY(59);	
				if ($ioe004==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $ioe004_c, 0, 0, 'L');
				//-----------------3A--------------------------				 
				$this->SetY(72);	
				if ($ioe005==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $ioe005_c, 0, 0, 'L');
				//--------------------3B-----------------------				
				$this->SetY(80);	
				if ($ioe0051==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $ioe0051_c, 0, 0, 'L');
				//---------------4-----------
				$this->SetY(89);	
				if ($ioe006==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				//$this->SetY(90);
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $ioe006_c, 0, 0, 'L');
				//--------------------5A------------------------------
				$this->SetY(102.5);	
				if ($ioe010==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				//$this->SetY(90);
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $ioe010_c, 0, 0, 'L');
				
				//*****************5B*********************
				$this->SetY(110.5);	
				if ($ioe011==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $ioe011_c, 0, 0, 'L');
				//--------------------6A--------------------------
				$this->SetY(124);	
				if ($ioe013==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $ioe013_c, 0, 0, 'L');
				//-------------------6B----------------------------				 				 	 
				$this->SetY(132);	
				if ($ioe014==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $ioe014_c, 0, 0, 'L');
				//--------------------6C---------------------------				 
				$this->SetY(140);	
				if ($ioe015==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $ioe015_c, 0, 0, 'L');
				//--------------------7A---------------------------
				$this->SetY(153);	
				if ($ioe016==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}	
				
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $ioe016_c, 0, 0, 'L');
				//-------------------7B----------------------------
				$this->SetY(161.5);	
				if ($ioe017==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $ioe017_c, 0, 0, 'L');
				//-------------------7C----------------------------				  
				$this->SetY(169);	
				if ($ioe018==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $ioe018_c, 0, 0, 'L');
				//--------------------7D---------------------------				
				$this->SetY(177.5);	
				if ($ioe019==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $ioe019_c, 0, 0, 'L');
				//-----------------------7E------------------------
				$this->SetY(185);	
				if ($ioe021==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $ioe021_c, 0, 0, 'L');
				//----------------------8-------------------------
				
				$this->SetY(195);	
				if ($ioe023==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $ioe023_c, 0, 0, 'L');
				
				
				//-----------------------9------------------------
				$this->SetY(212.5);	
				if ($iep001==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(7);
				$this->Cell(40);
				$this->Cell(120,4, $iep001_c, 0, 0, 'L');				 
				
				//-----------------------10------------------------
				$this->SetY(225.5);	
				if ($iep002==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $iep002_c, 0, 0, 'L');				 
				
				//----------------------11-------------------------
				$this->SetY(239);	
				if ($iep003==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(0.5);
				$this->Cell(40);
				$this->Cell(120,4, $iep003_c, 0, 0, 'L');
				//-----------------------------------------------
				
				/*****************PAG3*///////////////////
				
				$this->useTemplate($tplidx);
				
				$tplidx = $this->ImportPage(3);
				$this->AddPage();
				$this->SetAutoPageBreak(false, 2);
				
				//-----------------12---------------------------
				$this->SetY(44);	
				if ($iep004==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(7);
				$this->Cell(41);
				$this->Cell(120,4, $iep004_c, 0, 0, 'L');
				
				//-------------------13----------------------------
				$this->SetY(56.5);	
				if ($iep005==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iep005_c, 0, 0, 'L');
				//---------------------14--------------------------				 
				
				$this->SetY(65);	
				if ($iep006==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iep006_c, 0, 0, 'L');
				//----------------------15-------------------------
				
				$this->SetY(82.5);	
				if ($iad001==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad001_c, 0, 0, 'L');
				//----------------------16-------------------------
				
				$this->SetY(91.5);	
				if ($iad002==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $iad002_c, 0, 0, 'L');
				
				//-----------------17-------------
				
				$this->SetY(100.5);	
				if ($iad004==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad004_c, 0, 0, 'L');
				//---------------------18----------------------
				
				$this->SetY(109);	
				if ($iad005==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad005_c, 0, 0, 'L');
				//------------------19--------
				
				$this->SetY(118);	
				if ($iad006==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad006_c, 0, 0, 'L');
				
				//----------------------20----------------------------
				$this->SetY(127);	
				if ($iad007==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(3.5);
				$this->Cell(41);
				$this->Cell(120,4, $iad007_c, 0, 0, 'L');
				//********************21******************
				
				$this->SetY(135.5);	
				if ($iad008==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad008_c, 0, 0, 'L');
				//--------------------22A---------------------------					 
				$this->SetY(149);
				if ($iad009==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad009_c, 0, 0, 'L');
				//--------------------22B---------------------------
				/*Informe mensual*/			 
				$this->SetY(157);	
				if ($iad0091==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad0091_c, 0, 0, 'L');
				//---------------------22C--------------------------
				/*Informe mensual*/			 
				$this->SetY(165);	
				if ($iad0092==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad0092_c, 0, 0, 'L');
				//------------------23-----------------------------
				/*Informe mensual*/			 
				$this->SetY(174.5);	
				if ($iad010==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $iad010_c, 0, 0, 'L');
				//---------------------24--------------------------					
				$this->SetY(184);	
				if ($iad0101==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(7);
				$this->Cell(40);
				$this->Cell(120,4, $iad0101_c, 0, 0, 'L');
				
				//-----------------------25------------------------
				$this->SetY(197);	
				if ($iad011==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad011_c, 0, 0, 'L');
				//--------------------26---------------------------
				
				$this->SetY(206.5);	
				if ($iad012==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(8);
				$this->Cell(41);
				$this->Cell(120,4, $iad012_c, 0, 0, 'L');
				//---------------------27--------------------------
				
				$this->SetY(219.5);	
				if ($iad013==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad013_c, 0, 0, 'L');
				//------------------------28-----------------------
				
				$this->SetY(229);	
				if ($iad014==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad014_c, 0, 0, 'L');
				
				//-----------------------29------------------------
				
				$this->SetY(238.5);	
				if ($iad015==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad015_c, 0, 0, 'L');	
				
				/*****************PAG4*///////////////////
				
				$this->useTemplate($tplidx);
				
				$tplidx = $this->ImportPage(4);
				$this->AddPage();
				$this->SetAutoPageBreak(false, 2);
				
				//-----------------------30------------------------
				$this->SetY(46.5);	
				if ($iad016==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad016_c, 0, 0, 'L');
				
				//----------------------31-------------------------
				
				$this->SetY(55);	
				if ($iad017==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad017_c, 0, 0, 'L');
				
				//---------------------32--------------------------
				
				$this->SetY(65);	
				if ($iad018==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad018_c, 0, 0, 'L');
				
				//-----------------------33------------------------				 
				$this->SetY(74);	
				if ($iad019==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad019_c, 0, 0, 'L');
				//--------------------34---------------------------
				
				$this->SetY(83.5);	
				if ($iad020==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad020_c, 0, 0, 'L');
				//-------------------------35----------------------
				
				$this->SetY(93);	
				if ($iad022==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad022_c, 0, 0, 'L');
				//-------------------36---------------------------
				
				$this->SetY(102);	
				if ($iad023==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(8);
				$this->Cell(41);
				$this->Cell(120,4, $iad023_c, 0, 0, 'L');
				
				//----------------------------37-------------------
				
				$this->SetY(115.5);	
				if ($iad0231==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad0231_c, 0, 0, 'L');
				//-------------------38----------------------------
				
				$this->SetY(125);	
				if ($iad024==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad024_c, 0, 0, 'L');
				//--------------------39---------------------------
				
				$this->SetY(134.5);	
				if ($iad025==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad025_c, 0, 0, 'L');
				//---------------------40--------------------------
				
				$this->SetY(151.5);	
				if ($iad026==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad026_c, 0, 0, 'L');
				//---------------------41--------------------------
				
				$this->SetY(161);	
				if ($iad027==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad027_c, 0, 0, 'L');
				//------------------------42-----------------------
				
				$this->SetY(170.5);	
				if ($iad028==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad028_c, 0, 0, 'L');
				//-----------------------43------------------------	
				
				$this->SetY(180);	
				if ($iad029==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad029_c, 0, 0, 'L');
				//-------------------------44----------------------	
				
				$this->SetY(189);	
				if ($iad030==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad030_c, 0, 0, 'L');
				//----------------------45-------------------------	
				
				$this->SetY(198);	
				if ($iad031==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad031_c, 0, 0, 'L');
				//-----------------------46------------------------	
				
				$this->SetY(208);	
				if ($iad032==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad032_c, 0, 0, 'L');
				
				//------------------47---------------------
				$this->SetY(217);	
				if ($iad033==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad033_c, 0, 0, 'L');
				
				//----------------------48-------------------------	
				
				$this->SetY(226);	
				if ($iad034==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad034_c, 0, 0, 'L');
				
				//--------------------49---------------------------	
				
				$this->SetY(234.5);	
				if ($iad035==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad035_c, 0, 0, 'L');
				
				//----------------------50-------------------------	
				
				$this->SetY(243);	
				if ($iad036==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iad036_c, 0, 0, 'L');
				
				
				/************pag 5 **********/				 
				$this->useTemplate($tplidx);
				$tplidx = $this->ImportPage(5);
				
				$this->AddPage();
				$this->SetAutoPageBreak(false, 2);
				
				//--------------------51---------------------------				 
				$this->SetY(48);	
				if ($ila005==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $ila005_c, 0, 0, 'L');
				
				//-----------------------52------------------------	
				
				$this->SetY(58);	
				if ($ila006==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $ila006_c, 0, 0, 'L');
				//--------------------53---------------------------
				
				$this->SetY(74);	
				if ($ica006==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $ica006_c, 0, 0, 'L');
				
				//---------------54---------------
				
				$this->SetY(84);	
				if ($ica007==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $ica007_c, 0, 0, 'L');
				
				//--------------55----------------
				
				$this->SetY(94);	
				if ($ica008==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $ica008_c, 0, 0, 'L');
				//------------------56-----------------------------		
				
				$this->SetY(103);	
				if ($ica009==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $ica009_c, 0, 0, 'L');
				//----------------------57-------------------------
				
				$this->SetY(120.5);	
				if ($iop001==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $iop001_c, 0, 0, 'L');
				//-----------------------58------------------------
				
				$this->SetY(130);	
				if ($iop002==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $iop002_c, 0, 0, 'L');
				//------------------------59-----------------------
				
				$this->SetY(140);	
				if ($iop003==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $iop003_c, 0, 0, 'L');
				//-----------------------60------------------------
				
				$this->SetY(156.5);	
				if ($ige001==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $ige001_c, 0, 0, 'L');
				//----------------------61-------------------------
				
				$this->SetY(165.5);	
				if ($ige002==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $ige002_c, 0, 0, 'L');
				//--------------------62---------------------------
				
				$this->SetY(175);	
				if ($ige003==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $ige003_c, 0, 0, 'L');
				//-----------------------63------------------------
				
				$this->SetY(185);	
				if ($ila001==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $ila001_c, 0, 0, 'L');
				//-----------------------64------------------------
				
				$this->SetY(194);	
				if ($ial003==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $ial003_c, 0, 0, 'L');
				
				//-----------------------65A------------------------
				
				$this->SetY(214);	
				if ($ipe001==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $ipe001_c, 0, 0, 'L');
				//---------------------65B--------------------------
				
				$this->SetY(223);	
				if ($ipe002==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $ipe002_c, 0, 0, 'L');
				//------------------------65C-----------------------
				
				$this->SetY(231);	
				if ($ipe003==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(41);
				$this->Cell(120,4, $ipe003_c, 0, 0, 'L');
				
				
				/************pag 6 **********/				 
				$this->useTemplate($tplidx);
				$tplidx = $this->ImportPage(6);
				
				$this->AddPage();
				$this->SetAutoPageBreak(false, 2);
				
				//-------------------66----------------------------	
				$this->SetY(47.5);	
				if ($ipe004==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, " X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, " X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $ipe004_c, 0, 0, 'L');
				
				//---------------------67--------------------------
				
				$this->SetY(57);	
				if ($ipe005==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, " X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, " X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $ipe005_c, 0, 0, 'L');
				//-----------------------68------------------------
				
				$this->SetY(66);	
				if ($ipe006==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, " X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, " X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $ipe006_c, 0, 0, 'L');
				//----------------------69A-------------------------
				
				$this->SetY(79.5);	
				if ($ipe007==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, " X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, " X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $ipe007_c, 0, 0, 'L');
				//----------------------69B-------------------------
				
				$this->SetY(88);	
				if ($ipe008==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, " X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, " X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $ipe008_c, 0, 0, 'L');
				//-----------------------70------------------------
				
				$this->SetY(105);	
				if ($ial001==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, " X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, " X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $ial001_c, 0, 0, 'L');
				//------------------------71-----------------------
				
				$this->SetY(115);	
				if ($ial0011==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, " X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $ial0011_c, 0, 0, 'L');
				
				//-----------------------72------------------------
				$this->SetY(125);	
				if ($ial002==1){
				 	$this->Cell($cAnexoSi);
				 	$this->Cell(2,4, " X", 0, 0, 'L');
				}
				else{
				 	$this->Cell($cAnexoNo);
				 	$this->Cell(2,4, " X", 0, 0, 'L');
				}
				
				$this->Ln(4);
				$this->Cell(40);
				$this->Cell(120,4, $ial002_c, 0, 0, 'L');
				
				
				$this->SetY(143);
				$this->SetX($this->getX()+12);
				
				//$this->Cell(1);
				$this->SetFont('Arial','', 8.5, 0);
					 
				
				//$this->MultiCell(0,4, $	);
				/******pagina 7******/////
				
				$this->useTemplate($tplidx);
				
				$tplidx = $this->ImportPage(7);
				$this->SetTopMargin(31);
				$this->SetAutoPageBreak(true, 35);
				$this->MultiCell(172,3, $observa,0,'J');
				/*$this->Line(20, 30, 20, $this->GetY());
				$this->Line(195, 30, 195, $this->GetY());
				$this->Line(20, $this->GetY(), 195, $this->GetY());*/
				$this->AddPage();

				
				//$this->AddPage();
				$this->SetAutoPageBreak(true, 10);
				
				$this->SetTopMargin(0);
				$this->useTemplate($tplidx);
				
				/*$tplidx = $this->ImportPage(8);
				$this->AddPage();*/
				
				$firmasActa = array();
				
				
				
				$this->SetY(93);	
				$this->Cell(20);
				$this->Cell(75,4, $ex_serv, 0, 0, 'C');
				
				$this->Cell(13);
				$this->Cell(75,4, $entrante, 0, 0, 'C');
				
				//-----------Firmas----------
				//if($tipoActa == 1 || $tipoActa == 2 || $tipoActa == 3){ 
					$firmasActa[]='SERVIDOR P�BLICO SALIENTE';//0
					$firmasActa[]='SERVIDOR P�BLICO ENTRANTE';//1
					
					$firmasActa[]='TESTIGO DEL SERVIDOR P�BLICO';//2
					$firmasActa[]='SALIENTE';//3
					
					$firmasActa[]='TESTIGO DEL SERVIDOR P�BLICO';//4
					$firmasActa[]='ENTRANTE';//5			 
					
					$this->SetXY(30, 80);				 
					$this->SetFillColor(207,207,207);
					$this->SetFont('Arial','B', 9, 0);
					$this->MultiCell(75,6,$firmasActa[0],0,'C',1);
					
					$this->SetXY(115, 80);
					$this->SetFont('Arial','B', 9, 0);
					$this->MultiCell(75,6,$firmasActa[1],0,'C',1);	
					
					$this->SetXY(30, 112);
					$this->SetFont('Arial','B', 9, 0);
					$this->MultiCell(75,4,$firmasActa[2],0,'C',1);	
					$this->SetXY(30, 116);
					$this->MultiCell(75,4,$firmasActa[3],0,'C',1);	
					
					
					$this->SetXY(115, 112);
					$this->SetFont('Arial','B', 9, 0);
					$this->MultiCell(75,4,$firmasActa[4],0,'C',1);
					$this->SetXY(115, 116);
					$this->MultiCell(75,4,$firmasActa[5],0,'C',1);
				//}
				
				/*
				if($tipoActa == 4){
					$firmasActa[]='SERVIDOR P�BLICO QUE PRESENTA';//0
					$firmasActa[]='LA INFORMACI�N';//1
					$firmasActa[]='PARA LA ENTREGA-RECEPCI�N';//2
					
					$firmasActa[]='SERVIDOR P�BLICO ENTRANTE';//3
					
					$firmasActa[]='TESTIGO DEL SERVIDOR P�BLICO';//4
					$firmasActa[]='QUE PRESENTA LA INFORMACI�N';//5
					
					$firmasActa[]='TESTIGO DEL SERVIDOR P�BLICO';//8
					$firmasActa[]='ENTRANTE';//9
					
					$this->SetXY(30, 76);				 
					$this->SetFillColor(207,207,207);
					$this->SetFont('Arial','B', 9, 0);
					
					$this->MultiCell(75,4,$firmasActa[0],0,'C',1);
					$this->SetXY(30, 80);
					$this->MultiCell(75,4,$firmasActa[1],0,'C',1);
					$this->SetXY(25, 90);
					//$this->MultiCell(75,4,$firmasActa[2],0,'C',1);
					
					$this->SetXY(115, 76);;
					$this->MultiCell(75,4,$firmasActa[3],0,'C',1);
					$this->SetXY(115, 80);
					$this->MultiCell(75,4,'',0,'C',1);
					$this->SetXY(115, 90);
					//$this->MultiCell(75,4,'',0,'C',1);
					
					$this->SetXY(30, 110);
					$this->SetFont('Arial','B', 9, 0);
					
					$this->MultiCell(75,4,$firmasActa[4],0,'C',1);	
					$this->SetXY(30, 114);
					$this->MultiCell(75,4,$firmasActa[5],0,'C',1);	
					$this->SetXY(30, 115);
					//$this->MultiCell(75,4,$firmasActa[7],0,'C',1);	
					
					$this->SetXY(115, 110);
					$this->MultiCell(75,4,$firmasActa[6],0,'C',1);
					$this->SetXY(115, 114);
					$this->MultiCell(75,4,$firmasActa[7],0,'C',1);
					$this->SetXY(115, 114);;
					//$this->MultiCell(75,4,'',0,'C',1);
				}*/
				
				
				
				//------------------------------
				
				$this->SetY(132);	//5
				$this->Cell(20);
				$this->Cell(75,4, $testigo1, 0, 0, 'C');
				
				$this->Cell(12);
				$this->Cell(75,4, $testigo2, 0, 0, 'C');
				
				$this->SetY(152); //75	
				$this->Cell(20);
				$this->Cell(75,4, $cargotest1, 0, 0, 'C');
				
				$this->Cell(12);
				$this->Cell(75,4,$cargotest2, 0, 0, 'C');
				
				$this->SetXY(70,165);
				//				 $this->Ln();
				
				//---------------Firmas---------------
				$this->SetFillColor(207,207,207);
				$this->SetFont('Arial','B', 9, 0);
				
				//$this->Cell(55);
				
				//switch($tipoActa){
					//case(1):
					/*
						$this->Cell(80,4,'REPRESENTANTE DEL �RGANO',0,0,'C',0);
						$this->SetXY(70,168);
						$this->Cell(80,4,'SUPERIOR DE FISCALIZACI�N',0,0,'C',0);
						$this->SetXY(70,171);
						$this->Cell(80,4,'DEL ESTADO DE M�XICO',0,0,'C',0);
					*/
					
					$this->Cell(80,4,$cargoaer1,0,0,'C',0);
					
					//break;
					
					/*
					case(2):
					$this->Cell(80,6,'S�NDICO MUNICIPAL',0,0,'C',0);
					break;
					
					case(3):
					$this->Cell(80,4,'DEL �RGANO DE CONTROL',0,1,'C',0);
					$this->SetX(70);							
					$this->Cell(80,4,'INTERNO',0,0,'C',0);
					break;
					
					case(4):
					$caracterDe='CAR�CTER DE '.$cargoaer1;
					
					$cellWidth = 80;
					if($cargoaer1 != ''){
						$cellWidth = $this->GetStringWidth($caracterDe);
					}
					$this->MultiCell(75,4,$caracterDe,0,'C',0);
					break;
					*/
				//}
				//-------------------------------------
				
				//-----------------------------------------------------------------------------------------------------
				$this->SetXY(60,185);	//35
				
				$this->SetFont('Arial','', 9);
				
				//switch($tipoActa){				 
				 	//case(1):
					$this->Cell(95,4,$osfem, 0, 0, 'C');
					//break;
					
					/*
					case(2):
					$this->Cell(95,4,$sindico, 0, 0, 'C');
					break;
					
					case(3):
					$this->Cell(95,4,$contralor, 0, 0, 'C');
					break;
					
					case(4):
					$this->Cell(95,4,$nombreParaMostrar, 0, 0, 'C');
					break;
					*/
					
				//}
				
				$this->SetFont('Arial','', 6, 0);
				
				$this->Ln(12);
				
				/*if($tipoActa == 1){
					$this->Cell(6);
					$this->MultiCell(176,3,"�En t�rminos de los art�culos 6 fracci�n II y 16 p�rrafo segundo de la Constituci�n Pol�tica de los Estados Unidos Mexicanos; 5 fracci�n II de la Constituci�n Pol�tica del Estado Libre y Soberano de M�xico; 6, 9 y 42 de la Ley de Fiscalizaci�n Superior del Estado de M�xico; 19 y 25 de la Ley de Transparencia y Acceso a la Informaci�n P�blica del Estado de M�xico y Municipios; 1, 2, 3 fracciones II y IV, 6, 7 y dem�s relativos aplicables de la Ley de Protecci�n de Datos Personales del Estado de M�xico y 3.16, 3.20 y 3.22 del Reglamento de la Ley de Transparencia y Acceso a la Informaci�n P�blica del Estado de M�xico y Municipios, la presente informaci�n contiene datos que son considerados como confidenciales; por lo que se deber�n adoptar las medidas necesarias que garanticen la seguridad de los datos personales. Si usted no es el destinatario, se le proh�be su utilizaci�n total o parcial para cualquier fin. La difusi�n de la informaci�n que no se apegue a la normatividad en comento podr�a constituir responsabilidad administrativa, civil o penal en t�rminos de la Ley de Responsabilidades de los Servidores P�blicos del Estado de M�xico y Municipios, C�digo Civil del Estado de M�xico y el C�digo Penal del Estado de M�xico.�",0,'J');	 
				}*/
				
				//switch($tipoActa){
					//case(1):
					$this->Cell(6);
					$this->MultiCell(176,3,"�En t�rminos de los art�culos 6, Apartado A), fracci�n II, y 16, p�rrafo segundo de la Constituci�n Pol�tica de los Estados Unidos Mexicanos; los art�culos 1 y 31 de la Ley General de Protecci�n de Datos Personales en Posesi�n de Sujetos Obligados; el art�culo 5, fracci�n II de la Constituci�n Pol�tica del Estado Libre y Soberano de M�xico; los art�culos 1, 6, 38, 40 y dem�s relativos aplicables de la Ley de Protecci�n de Datos Personales en Posesi�n de Sujetos Obligados del Estado de M�xico y Municipios; los art�culos 6 y 143 de la Ley de Transparencia y Acceso a la Informaci�n P�blica del Estado de M�xico y Municipios, y los art�culos 6, 9 y 42 de la Ley de Fiscalizaci�n Superior del Estado de M�xico; la presente informaci�n contiene datos que son considerados como confidenciales; por lo que se deber�n adoptar las medidas necesarias que garanticen la integridad, disponibilidad y confidencialidad de los datos personales. Si usted no es el destinatario, se le proh�be su utilizaci�n total o parcial para cualquier fin. El tratamiento de la informaci�n que no se apegue a la normatividad en comento podr�a constituir responsabilidad administrativa, civil o penal en t�rminos de la Ley General de Responsabilidades Administrativas; la Ley de Responsabilidades Administrativas del Estado de M�xico y Municipios, el C�digo Civil del Estado de M�xico y el C�digo Penal del Estado de M�xico. La informaci�n contenida en el apartado de observaciones del acta y/o formatos puede incrementar la cantidad de hojas que se encuentran publicadas en los Lineamientos que Regulan la Entrega-Recepci�n de la Administraci�n P�blica Municipal del Estado de M�xico�",0,'J');
					//break;
					
					/*
					case(2):
					$this->Cell(6);
					$this->MultiCell(176,3,"�En t�rminos de los art�culos 6 Apartado A) fracci�n II y 16 p�rrafo segundo de la Constituci�n Pol�tica de los Estados Unidos Mexicanos; 1 y 31 de la Ley General de Protecci�n de Datos Personales en Posesi�n de Sujetos Obligados; 5 fracci�n II de la Constituci�n Pol�tica del Estado Libre y Soberano de M�xico; 1, 6, 38 y dem�s relativos aplicables de la Ley de Protecci�n de Datos Personales en Posesi�n de Sujetos Obligados del Estado de M�xico; 6 y 143 de la Ley de Transparencia y Acceso a la Informaci�n P�blica del Estado de M�xico y Municipios y 6, 9 y 42 de la ley de Fiscalizaci�n Superior del Estado de M�xico; la presente informaci�n contiene datos que son considerados como confidenciales; por lo que se deber�n adoptar las medidas necesarias que garanticen la integridad, disponibilidad y confidencialidad de los datos personales. Si usted no es el destinatario, se le proh�be su utilizaci�n total o parcial para cualquier fin. El tratamiento de la informaci�n que no se apegue a la normatividad en comento podr�a constituir responsabilidad administrativa, civil o penal en t�rminos de la Ley General de Responsabilidades Administrativas; Ley de Responsabilidades Administrativas del Estado de M�xico y Municipios, C�digo Civil del Estado de M�xico y el C�digo Penal del Estado de M�xico. La informaci�n contenida en el apartado de observaciones del acta y/o formatos puede incrementar la cantidad de hojas que se encuentran publicadas en los Lineamientos que Regulan la Entrega-Recepci�n de la Administraci�n P�blica Municipal del Estado de M�xico.�",0,'J');
					break;
					
					case(3):
					$this->Cell(6);
					$this->MultiCell(176,3,"�En t�rminos de los art�culos 6 Apartado A) fracci�n II y 16 p�rrafo segundo de la Constituci�n Pol�tica de los Estados Unidos Mexicanos; 1 y 31 de la Ley General de Protecci�n de Datos Personales en Posesi�n de Sujetos Obligados; 5 fracci�n II de la Constituci�n Pol�tica del Estado Libre y Soberano de M�xico; 1, 6, 38 y dem�s relativos aplicables de la Ley de Protecci�n de Datos Personales en Posesi�n de Sujetos Obligados del Estado de M�xico; 6 y 143 de la Ley de Transparencia y Acceso a la Informaci�n P�blica del Estado de M�xico y Municipios y 6, 9 y 42 de la ley de Fiscalizaci�n Superior del Estado de M�xico; la presente informaci�n contiene datos que son considerados como confidenciales; por lo que se deber�n adoptar las medidas necesarias que garanticen la integridad, disponibilidad y confidencialidad de los datos personales. Si usted no es el destinatario, se le proh�be su utilizaci�n total o parcial para cualquier fin. El tratamiento de la informaci�n que no se apegue a la normatividad en comento podr�a constituir responsabilidad administrativa, civil o penal en t�rminos de la Ley General de Responsabilidades Administrativas; Ley de Responsabilidades Administrativas del Estado de M�xico y Municipios, C�digo Civil del Estado de M�xico y el C�digo Penal del Estado de M�xico. La informaci�n contenida en el apartado de observaciones del acta y/o formatos puede incrementar la cantidad de hojas que se encuentran publicadas en los Lineamientos que Regulan la Entrega-Recepci�n de la Administraci�n P�blica Municipal del Estado de M�xico.�",0,'J');
					break;
					
					case(4):
					$this->Cell(6);
					$this->MultiCell(176,3,"�En t�rminos de los art�culos 6 Apartado A) fracci�n II y 16 p�rrafo segundo de la Constituci�n Pol�tica de los Estados Unidos Mexicanos; 1 y 31 de la Ley General de Protecci�n de Datos Personales en Posesi�n de Sujetos Obligados; 5 fracci�n II de la Constituci�n Pol�tica del Estado Libre y Soberano de M�xico; 1, 6, 38 y dem�s relativos aplicables de la Ley de Protecci�n de Datos Personales en Posesi�n de Sujetos Obligados del Estado de M�xico; 6 y 143 de la Ley de Transparencia y Acceso a la Informaci�n P�blica del Estado de M�xico y Municipios y 6, 9 y 42 de la ley de Fiscalizaci�n Superior del Estado de M�xico; la presente informaci�n contiene datos que son considerados como confidenciales; por lo que se deber�n adoptar las medidas necesarias que garanticen la integridad, disponibilidad y confidencialidad de los datos personales. Si usted no es el destinatario, se le proh�be su utilizaci�n total o parcial para cualquier fin. El tratamiento de la informaci�n que no se apegue a la normatividad en comento podr�a constituir responsabilidad administrativa, civil o penal en t�rminos de la Ley General de Responsabilidades Administrativas; Ley de Responsabilidades Administrativas del Estado de M�xico y Municipios, C�digo Civil del Estado de M�xico y el C�digo Penal del Estado de M�xico. La informaci�n contenida en el apartado de observaciones del acta y/o formatos puede incrementar la cantidad de hojas que se encuentran publicadas en los Lineamientos que Regulan la Entrega-Recepci�n de la Administraci�n P�blica Municipal del Estado de M�xico.�",0,'J');
					break;
					*/
				//}		
				
				$this->SetFont('Arial','', 4.5, 0);
				$this->Ln(2);
				$this->Cell(6);
				$this->MultiCell(176,2,$rsaFirmaActa,0,'L');
				
				$this->useTemplate($tplidx);
			}
		}
	}
	
?>
