<?php

define("zendidHostA", "H:KBQ97-42SYA-GW6U2-7GGRY");

function verificaValidez($hostZnd){
	$zendidServer = zend_get_id(true);	
	$maxZend = count($zendidServer) -1;
	
	for($i=0;$i<=$maxZend;$i++){
			if(strcmp($hostZnd, $zendidServer[$i]) == 0){
				return TRUE;
			}
	}	
  return FALSE;
}

function obtenerClavePublica(){		
$clavePublica = "-----BEGIN PUBLIC KEY-----
MIGJAoGBALwJSI7fhHIjRzZecnGXdXYPwD+He0kkDQwysenOZzoNohgCksm2a8mbuYh+5k6Ii5O2
asf/FQrBVzsxTZGgLQhh36V7wo3wtY6Pe1Mm/NEjDalMT1KYh56jcHRRKhXWRzDBUC7hPwroyWCu
m5m5+W5pt7iwkMb+75afmzRV6z6PAgMBAAE=
-----END PUBLIC KEY-----";
	
	if(verificaValidez(zendidHostA)){
		return $clavePublica;	
	}
	else {
		die("Su certificado es invalido por favor contacte al administrador del sistema");
	}  
}

function obtenerClavePrivada(){	
	$clavePrivada = "-----BEGIN RSA PRIVATE KEY-----
MIICXgIBAAKBgQC8CUiO34RyI0c2XnJxl3V2D8A/h3tJJA0MMrHpzmc6DaIYApLJtmvJm7mIfuZO
iIuTtmrH/xUKwVc7MU2RoC0IYd+le8KN8LWOj3tTJvzRIw2pTE9SmIeeo3B0USoV1kcwwVAu4T8K
6MlgrpuZufluabe4sJDG/u+Wn5s0Ves+jwIDAQABAoGAI3rk9AB870R01W9wzLo1cbd7IQGhY6Wn
yJtwMpu9opib80fZBrKjGAGIOpcnQb5GB/Mtptscz1C38vVUm5vLoywJTQJpI17pQSgJF3TzRya2
ORw1pq2XWBF2yYBdBRClr71dvMPsaxDXMaLDyb8IDI8echMHMb93yJGGAvLwiHECQQDGEdfvMKzm
lQ5xS50+uMPe/cqrAHwoXIcbTAckT7jvXHJg8bhGtPdLZrX6T9RHbarhHEUrsgIBqZIgjMlPT9CF
AkEA8wgzvWHFtkVuaWl6vYWXjhBJkTo91tlzWUv9EjgyI4RC2NLYx3q8mWe/52Z3V8ML4iSY9YD5
RMmjv/cuRESpAwJBAKOJkg+00d1kjtMuinuCFH9mbtK4CMtOcmMwZ+ksRdakLvAqRGtk+ZXxDSnK
hcwgGWnabKZCSHPU7YkglQp+LP0CQQDva44lLNPs+7uJB0vOt3QVlUAUXS8coL49I1od/9SNHrgI
CLdc1TJFYmswmMZLZDApyeoSOcvXk1nJPMaMpKNnAkEAlI8vOOuaDOd6/LuCdZoHQVYEKjcznJTv
Aii9wA9GyXUab6XGNT/DdUA56TeK45qXOnQPsJ3ZaiKj1FSqa0iQYQ==
-----END RSA PRIVATE KEY-----";

	if(verificaValidez(zendidHostA)){
		return $clavePrivada;	
	}
	else {
		die("Su certificado es invalido por favor contacte al administrador del sistema");
	}  
}
?>