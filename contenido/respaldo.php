<?php
	require_once('../Connections/bd2.php'); 
	
	session_start();
	#############################################
	#
	# Tarea de copia seguridad de todas las bases de datos en MySQL
	#
	# CopyLeft, puedes hacer con este script lo que te de la gana
	#
	#############################################
	set_time_limit(0);
	header("Content-type: text/plain; charset=UTF-8");
	echo date("Y-m-d H:i", time()) . " Creando copia de seguridad MySQL...\r\n";
	
	// Datos de acceso a MySQL
	$myhost = $hostname_bd2;
	$myuser = $username_bd2;
	$mypass = $password_bd2;
	$DB = mysql_connect($myhost, $myuser, $mypass) or die(date("Y-m-d H:i", time()) . " ERROR!! No se pudo conectar a MySQL.\r\n");
	
	// Otros par�metros
	//$nom=$_SESSION[''];
	
	mysql_select_db($database_bd2, $bd2);
	$strQuery = "SELECT `nombre` FROM `ayuntamiento` WHERE clave = 1";
	$dtAlter = mysql_query($strQuery, $bd2) or die(mysql_error());
	$row_nombre = mysql_fetch_array($dtAlter);
	$nom= eliminar_tildes($row_nombre['nombre']);
	
	$OUTDIR = "../temporal-zip/"; // Cambiar segun necesidades y tipo servidor
	$now = date("YmdHi", time());
	$outfile = "CREG-ER_" .$nom ."_$now.zip";
	$periodo = time() - 259200; // Los archivos anteriores a este periodo (3 dias = 259200 segundos) ser�n borrados
	
	// Crear nuevo archivo ZIP
	# M�s informaci�n sobre la clase PHP
	# http://es.php.net/manual/en/class.ziparchive.php
	echo date("Y-m-d H:i", time()) . " Creando '$OUTDIR$outfile.zip' ... ";
	$zip = new ZipArchive;
	if (!$zip->open("$OUTDIR$outfile", ZIPARCHIVE::CREATE)) die("ERROR!!\r\n");
	echo "OK.\r\n";
	
	// Tomar un listado de bases de datos
	$q = mysql_query("SHOW DATABASES");
	
	// Volcar todas las bases
	while ($database = mysql_fetch_row($q))
	if ($database[0] ==$database_bd2)
	{
		// Nombrar archivo
		$filename = "{$database[0]}.sql";
		$tempfile = date("YmdHis", time()) . ".~swap";
		
		echo date("Y-m-d H:i", time()) . " Volcando '$filename' ... ";
		
		// Volcar datos
		system("mysqldump -h $myhost -u $myuser -p$mypass --opt {$database[0]} > $OUTDIR$tempfile --default-character-set=latin1");
		
		echo "OK.\r\n"
		. date("Y-m-d H:i", time()) . " Agregando '$filename' a '$outfile' ... ";
		
		//  Agregar archivo al ZIP
		$zip->addFile($OUTDIR.$tempfile, $filename);
		
		// Recordar los temporales utilizados
		$DUMPFILES[] = $OUTDIR.$tempfile;
		
		echo "OK.\r\n";
	}
	
	// Desconectar de la base de datos
	mysql_close($DB);
	
	// Cerrar archivo ZIP
	$zip->close();
	
	// Eliminar temporales. Importante hacerlo DESPU�S de cerrar el ZIP
	foreach($DUMPFILES as $file)
	unlink($file);
	
	// Elminar archivos antiguos
	echo date("Y-m-d H:i", time()) . " Eliminando copias antiguas...\r\n";
	$D = opendir($OUTDIR);
	while ($F = readdir($D))
	if ($F != "." && $F != "..")
	if (filectime($OUTDIR.$F) < $periodo)
	if (!unlink($OUTDIR.$F))
	echo date("Y-m-d H:i", time()) . " No se pudo eliminar el archivo $F.\r\n";        
	closedir($D);
	
	echo date("Y-m-d H:i", time()) . " Tarea finalizada.\r\n";
	
	
	mysql_select_db($database_bd2, $bd2);
	$strQuery = "INSERT INTO respaldos (fecha_respaldo) VALUES (NOW());";
	$dtAlter = mysql_query($strQuery, $bd2);	
	
	
	if(isset($_GET['area'])){
		$area = $_GET['area'];
		copy("../temporal-zip/" . $outfile, "../entrega/paquete/$area/".$outfile);
		echo "../entrega/paquete/$area/".$outfile;
	}
	else{
		header("Location: ../temporal-zip/" . $outfile);	
	}
	
	function eliminar_tildes($cadena){
		
		//Ahora reemplazamos las letras
		$cadena = str_replace(
		array('�', '�', '�', '�', '�', '�', '�', '�', '�'),
		array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
		$cadena
		);
		
		$cadena = str_replace(
		array('�', '�', '�', '�', '�', '�', '�', '�'),
		array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
		$cadena );
		
		$cadena = str_replace(
		array('�', '�', '�', '�', '�', '�', '�', '�'),
		array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
		$cadena );
		
		$cadena = str_replace(
		array('�', '�', '�', '�', '�', '�', '�', '�'),
		array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
		$cadena );
		
		$cadena = str_replace(
		array('�', '�', '�', '�', '�', '�', '�', '�'),
		array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
		$cadena );
		
		$cadena = str_replace(
		array('�', '�', '�', '�'),
		array('n', 'N', 'c', 'C'),
		$cadena
		);
		
		$cadena = str_replace(
		array(' '),
		array('_'),
		$cadena
		);
		
		return $cadena;
	}
	
?>