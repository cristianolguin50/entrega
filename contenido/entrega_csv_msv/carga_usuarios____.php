<?php 
require_once('Connections/bd2.php'); 
require_once('username.php');

session_start();

$borra = isset($_POST['borra']) ? $_POST['borra'] : 0;

//---------------------------Borramos la db antes de subir-----------------------
if(isset($_GET['borra'])){
	if($_GET['borra'] == 1){
		mysql_select_db($database_bd2, $bd2);
		$sql2 = "truncate table usuarios";
		$result2 = mysql_query($sql2, $bd2);
		echo mysql_error();
	}
}
//-------------------------------------------------------------------------------

//-----------------
if($_GET['archivo']) {
	$target_path = $_GET['archivo'];
			$handle = fopen("tmpcsv/$target_path" , "r");

			$row=0;
			$eval=0;
			$ingresados = 0;
			$user = 'crg_msv';
			$errores = '';
			
			while (($data = fgetcsv($handle, 0, ",")) !== FALSE){
				$eval++;	
				$datosCorrectos = true;				
							
				$nombre_usuario = trim($data[0]);
				if($nombre_usuario == ''){
					$nombre_usuario = construir_nombre();
				}
				
				$usuario_passwd = trim($data[1]);
				if($usuario_passwd == ''){
					$datosCorrectos = false;
					$errores = 'PASSWORD';
					echo "Sin password<br>";
				}
				else {
					$usuario_passwd	= md5($usuario_passwd);
				}
				
				$responsable = trim($data[2]);				
				$sqlQuery = "SELECT cargo, area FROM trabajador WHERE curp = '$responsable'";												
				mysql_select_db($database_bd2, $bd2);					
				$dtResponsable = mysql_query($sqlQuery, $bd2);					
				$totalRes = mysql_num_rows($dtResponsable);		
					
				if($totalRes == 0){
					$datosCorrectos = false;
					$errores =  $errores." ".'SIN CARGO Y TRABAJADOR';
				}
				else{
					$row_usuario = mysql_fetch_array($dtResponsable);
					$cargo_usuario = $row_usuario['cargo'];
					$area_usuario = $row_usuario['area'];
				}
								
				$admin_er = trim($data[4]);
				if($admin_er == ''){
					$admin_er = 0;	
				}
				elseif($admin_er != 1 && $admin_er != 0){
					$admin_er = 0;
				}
				
				$formatos_qr = trim($data[5]);
				if($formatos_qr == ''){
					$formatos_qr = 0;	
				}
				elseif($formatos_qr != 1 && $formatos_qr != 0){
					$formatos_qr = 0;
				}
				
				$acta_er = trim($data[6]);
				if($acta_er == ''){
					$acta_er = 0;	
				}
				elseif($acta_er != 1 && $acta_er != 0){
					$acta_er = 0;
				}
				
				//---permiso misterioso---
				$formatos_er = trim($data[7]);
				if($formatos_er == ''){
					$formatos_er = 0;	
				}
				elseif($formatos_er != 1 && $formatos_er != 0){
					$formatos_er = 0;
				}
				//------
				
				$previas = trim($data[8]);
				if($previas == ''){
					$previas = 0;	
				}
				elseif($previas != 1 && $previas != 0){
					$previas = 0;
				}
				
				$reporte_avance = trim($data[9]);
				if($reporte_avance == ''){
					$reporte_avance = 0;	
				}
				elseif($reporte_avance != 1 && $reporte_avance != 0){
					$reporte_avance = 0;
				}
				
				$documentacion = trim($data[10]);
				if($documentacion == ''){
					$documentacion = 0;	
				}
				elseif($documentacion != 1 && $documentacion != 0){
					$documentacion = 0;
				}
				
				$paquetes_er = trim($data[11]);
				if($paquetes_er == ''){
					$paquetes_er = 0;	
				}
				elseif($paquetes_er != 1 && $paquetes_er != 0){
					$paquetes_er = 0;
				}
				
				$alta_registros = trim($data[12]);
				if($alta_registros == ''){
					$alta_registros = 0;	
				}
				elseif($alta_registros != 1 && $alta_registros != 0){
					$alta_registros = 0;
				}
				
				$mod_registros = trim($data[13]);
				if($mod_registros == ''){
					$mod_registros = 0;	
				}
				elseif($mod_registros != 1 && $mod_registros != 0){
					$mod_registros = 0;
				}
				
				$baja_registros = trim($data[14]);
				if($baja_registros == ''){
					$baja_registros = 0;	
				}
				elseif($baja_registros != 1 && $baja_registros != 0){
					$baja_registros = 0;
				}
				
				$sqlQuery = "SELECT clave FROM usuario WHERE nombre_comp = '$responsable'";												
				mysql_select_db($database_bd2, $bd2);					
				$dtResponsable = mysql_query($sqlQuery, $bd2);					
				$totalRes = mysql_num_rows($dtResponsable);
				
				//------------usuario duplicado-----------------
				$sqlQuery = "SELECT usuario FROM usuario WHERE usuario = '$nombre_usuario'";												
				mysql_select_db($database_bd2, $bd2);					
				$dtResponsable = mysql_query($sqlQuery, $bd2);					
				$totalResUser = mysql_num_rows($dtResponsable);
				//----------------------------------------------
				
				if($totalRes > 0){
					$datosCorrectos = false;
					echo "<br>El trabajador ya tiene usuario<br>";				
				}
				
				if($totalResUser > 0){
					$datosCorrectos = false;
					echo "<br>El nombre de usuario esta duplicado<br>";				
				}
											
				if($datosCorrectos == true){				
					$sqlInsert = "INSERT INTO `usuario` (`usuario`, `passwd`, `tipo`, `nombre_comp`, `puesto`, `clave_area`, `rh`, `rh1`, `rh2`, `rh3`, `rh4`, 
								 `rh5`, `rh6`, `rh7`, `rh8`, `er`, `er1`, `er2`, `er3`, `er4`, `er5`, `er6`, `er7`, `er8`, `admin`, `nuevo`, `modificar`, `eliminar`, 
								 `paquetes`, `fecha`) VALUES ('$nombre_usuario', '$usuario_passwd', NULL, '$responsable', '$cargo_usuario',
								 '$area_usuario', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, $admin_er, $formatos_qr, $acta_er, $reporte_avance, $formatos_qr, $previas, $documentacion, 
								  0, $admin_er, $alta_registros, $mod_registros, $baja_registros, $paquetes_er, NOW())"; 
					//echo $sqlInsert."<br>";
					mysql_select_db($database_bd2, $bd2);
					$result = mysql_query($sqlInsert, $bd2);
					echo "<br>".mysql_error();
					
					$newUserID = mysql_insert_id();	
					
					//-----------------------FORMATOS DE USUARIO-----------------------------
					mysql_select_db($database_bd2, $bd2);
					$sqlFormatosPermisos = "SELECT ioe001,	ioe004,	ioe005,	ioe0051, ioe006, ioe010, ioe011, ioe013, ioe014, ioe015, ioe016, ioe017, ioe018, ioe019, ioe021, ioe022, iep001, iep002, iep003,	iad001,	iad002,	iad004,	iad005,	iad006,	iad007,	iad008,	iad009,	iad010,	iad0101, iad011, iad012, iad013, iad014, iad015, iad016, iad017, iad018,	iad019,	iad020,	iad022,	iad023,	iad0231, iad024, iad025, ila001, ila005, ila006, ica001, ica002, ica003, ica004, ica005, ica006, ica007, ica008,	ica009,	iop001,	iop002,	iop003,	ipe001,	ipe002,	ipe003,	ipe004,	ipe005,	ial001,	ial0011, ial002, ial003 FROM proceso_dep WHERE dependencia = '$area_usuario'";
					$dtFormatosPermisos = mysql_query($sqlFormatosPermisos, $bd2);
					echo mysql_error();
										
					$arrayPermisos = array();
					$row_permisosFormato = mysql_fetch_array($dtFormatosPermisos, MYSQL_NUM);
	
					$sql2 = "INSERT INTO `user_form` (`clave_usuario`, `usuario`, `clave_dependencia`, `proceso`, `fecha`, `user`, `ioe001`, `ioe004`, `ioe005`, `ioe0051`,	`ioe006`, `ioe010`, `ioe011`, `ioe013`, `ioe014`, `ioe015`, `ioe016`, `ioe017`, `ioe018`, `ioe019`, `ioe021`, `ioe022`, `iep001`, `iep002`, `iep003`, `iad001`, `iad002`, `iad004`, `iad005`, `iad006`, `iad007`, `iad008`, `iad009`, `iad010`, `iad0101`, `iad011`, `iad012`, `iad013`, `iad014`, `iad015`, `iad016`, `iad017`, `iad018`, `iad019`, `iad020`, `iad022`, `iad023`, `iad0231`, `iad024`, `iad025`, `ila001`, `ila005`, `ila006`, `ica001`, `ica002`, `ica003`, `ica004`, `ica005`, `ica006`, `ica007`, `ica008`, `ica009`, `iop001`, `iop002`, `iop003`, `ipe001`, `ipe002`, `ipe003`, `ipe004`, `ipe005`, `ial001`, `ial0011`, `ial002`, `ial003`) VALUES ('$newUserID', '$nombre_usuario', '$area_usuario', '1', NOW(), 'creg_msv'";
	
					$maxPerm = count($row_permisosFormato)-1;
					for($i=0;$i<=$maxPerm;$i++){
					if($i < $maxPerm)
						$sql2 .= ", ".$row_permisosFormato[$i];
					else
						$sql2 .= ", ".$row_permisosFormato[$i].")";
					}
	
					mysql_select_db($database_bd2, $bd2);
					$result2 = mysql_query($sql2);
					echo mysql_error(); 					
					//-----------------------------------------------------------------------
					
					if($result){
						echo "Correcto! se agrego el registo numero: $eval <br />";
						$ingresados++;
					}					
				}
				else {
					echo "<br><strong>No se puede guardar el registro numero: ".$eval."  ".$errores." "."</strong><br>";
				}
			}
}	
echo "Total registros: ", $eval;
echo "<br>Registros creados: ", $ingresados;
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="../../css/idots.css" rel="stylesheet" type="text/css">
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
//-->
</script>

<style type="text/css">
<!--
body {
	margin-top: 0px;
}
.style1 {
	font-size: 110%;
	font-weight: bold;
}
-->
</style></head>
<body>	
	
</body>
</html>