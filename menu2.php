<?php
	
	require_once('Connections/bd2.php'); 
	
	//initialize the session
	session_start();
	
	// ** Logout the current user. **
	$logoutAction = $_SERVER['PHP_SELF']."?doLogout=true";
	if ((isset($_SERVER['QUERY_STRING'])) && ($_SERVER['QUERY_STRING'] != "")){
		$logoutAction .="&". htmlentities($_SERVER['QUERY_STRING']);
	}
	
	if ((isset($_GET['doLogout'])) &&($_GET['doLogout']=="true")){
		//to fully log out a visitor we need to clear the session varialbles
		/*session_unregister('MM_Username');
			session_unregister('MM_UserGroup');
		session_unregister('clave_dependencia');*/
		
		session_destroy();
		
		$logoutGoTo = "salida.htm";
		if ($logoutGoTo) {
			header("Location: $logoutGoTo");
			exit;
		}
	}
	
	//initialize the session
	$colname_usuario = "1";
	if (isset($_SESSION['MM_Username'])) {
		$colname_usuario = (get_magic_quotes_gpc()) ? $_SESSION['MM_Username'] : addslashes($_SESSION['MM_Username']);
	}
	
	mysql_select_db($database_bd2, $bd2);
	$query_usuario = sprintf("SELECT * FROM usuario WHERE usuario = '%s'", $colname_usuario);
	$usuario = mysql_query($query_usuario, $bd2) or die(mysql_error());
	$row_usuario = mysql_fetch_assoc($usuario);
	
	//print_r($row_usuario);
	
	
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="charset=iso-8859-1">
		<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
		</script>
		<script type="text/javascript">
			_uacct = "UA-1905873-2";
			urchinTracker();
		</script>
		<title>IIDESOFT M&eacute;xico, S.A. de C.V.</title>
		
		<link rel="stylesheet" href="js/jquery_ui_new/menu/jquery-ui.css">
		<script src="js/jquery-3.2.1.min.js"></script>
		<script src="js/jquery_ui_new/menu/jquery-ui.js"></script>
		
		<script>
			$( function() {
				$( "#accordion" ).accordion({
					collapsible: true,
					heightStyle: "content",
					active: false
				});
			} );
		</script>
		
		<style type="text/css">
			body{
			font-family: Trebuchet MS, Lucida Sans Unicode, Arial, sans-serif;
			font-size:0.7em;
			margin:0px;
			padding:0px;
			background-image:url(images/body-background.png);
			padding:5px;
			
			height:100%;
			text-align:center;
			}
			.clear{
			clear:both;
			}
			
			#accordion div{
			margin:0px;
			padding:0px;
			}
			
			/* 	Layout CSS */	
			#accordion{    
			
			border:0px solid #000;
			padding:1px;
			
			width: 200px;
			}	
			
			/* All A tags - i.e menu items. */
			#accordion a{
			color: #000;
			text-decoration:none;	
			display:block;
			clear:both;
			width:170px;	
			padding-left:2px;	
			
			}
			
			#accordion h3{  /* Main menu items */
			margin-top:1px;
			font-weight:bold;
			background-color:#317082;
			color:#FFF;
			height:20px;
			//line-height:30px;
			//vertical-align:middle;
			//padding-left:10px;
			//border-left:1px solid #000;
			//border-right:1px solid #000;
			}  
			
			#accordion .ui-icon { display: none; }
			#accordion .ui-accordion-header a { padding-left: 0; }
		</style>
		
	</head>
	
	<body>
		
		<div id="accordion" align="center" style="background-color:#FFFFFF ">
			
			<?php if ($row_usuario['admin']==1) { ?>
				<h3> CONFIGURACI&Oacute;N DEL SISTEMA</h3>
				<div>
					<hr>
					<a href="contenido/entidad/datos_entidad.php" style="text-decoration:none;" target="mainFrame"> <img src="images/menu_main/datosgenerales.png" width="30" height="30" border="0"> <br> 
					DATOS GENERALES DE LA ENTIDAD DE GOBIERNO</a>
					<hr>
					
					<a href="contenido/dependencias/depend2.php" target="mainFrame"> <img src="images/menu_main/unidadesadministrativas.png" width="30" height="30" border="0"> <br> 
					UNIDADES ADMINISTRATIVAS DE LA ENTIDAD DE GOBIERNO </a>
					<hr>
					
					<a href="contenido/usuarios/usuarios.php" target="mainFrame"> <img src="images/menu_img/Users-mixed-gender-48.png" width="40" height="40" border="0"> <br> 
					ADMINISTRACI&Oacute;N DE USUARIOS</a>
					<hr>
					
					<a href="entrega/remote_connection/datos.php" target="mainFrame"> <img src="images/menu_img/Database-Connect-32.png"  border="0"> <br> 
					CONEXI�N CREG PATRIMONIAL</a>
					<hr>
					
					<a href="contenido/backup_db.php" target="mainFrame"> <img src="images/menu_main/respaldarbase.png" width="40" height="40" border="0"> <br> 
					RESPALDAR BASE DE DATOS</a>
					<hr>
					
					<a href="full_backup/backup.php" target="mainFrame"> <img src="images/menu_main/respaldocompleto2.png" width="40" height="40" border="0"> <br>
					RESPALDO COMPLETO DEL SISTEMA</a>
					<hr>
					
					<a href="entrega/semaforo/semaforo.php"" target="mainFrame"> <img src="images/menu_main/Chart_bar-48.png" width="40" height="40" border="0"><br> 
					REPORTES DE AVANCE POR UNIDAD ADMINISTRATIVA</a>
					<hr>
					
					<a href="actualizaciones/manuales/manuales.php" target="mainFrame"><img src="images/menu_main/manuales.png" width="36" height="36" border="0"><br> 
					MANUALES</a>
					<hr>
					
					<a href="actualizaciones/desc_csv/descarga_csv.php" target="mainFrame"><img src="images/menu_main/descargables2.png" width="36" height="36" border="0"><br> 
					DESCARGABLES .CSV</a>
					<hr>
					
					<a href="entrega/repartir_info/mover.php" target="mainFrame"><img src="images/move-to.png" width="36" height="36" border="0"><br> 
					MOVER INFORMACI�N</a>
					<hr>
					
					
					<!----- Administrador 
					<a href="contenido/entrega_csv_msv/index.php" target="mainFrame"><img src="images/Upload_CSV.png" width="36" height="36" border="0"><br> 
					ENTREGA CSV</a>
					<hr>
					----->
					
					
					<a href="entrega/logs/ver.php" target="mainFrame"><img src="images/menu_main/logsistema.png" width="36" height="36" border="0"><br> 
					LOG DEL SISTEMA</a>
					<hr>	
				</div>
				
				<?php } if ($row_usuario['rh']==1){ ?>
				
				<h3> RECURSOS HUMANOS</h3>
				<div>
					<?php if ($row_usuario['rh5']==1) { ?>
						
						<hr>
						<a href="contenido/rec_hum/trabajador1.php" target="mainFrame"> <img src="images/menu_main/agregar.png" width="32" height="32" border="0"> <br> 
						AGREGAR SERVIDOR P&Uacute;BLICO </a>
						<hr>
						
						<a href="contenido/rec_hum/bajas2.php" target="mainFrame"> <img src="images/menu_main/reingresarpersonal.png" width="31" height="31" border="0"> <br> 
						REINGRESO DE SERVIDOR P&Uacute;BLICO </a>
						<?php } if ($row_usuario['rh6']==1) { ?>					  
						<hr>
						
						<a href="contenido/rec_hum/busca_trab.php" target="mainFrame">  <img src="images/menu_main/buscar.png" width="30" height="30" border="0"><br> 
						BUSCAR SERVIDOR P&Uacute;BLICO </a>
						<hr>
						
						<?php } if ($row_usuario['rh3']==1){ ?>					  
						
						<a  href="contenido/rec_hum/comisionado/comision.php" target="mainFrame"><img src="images/menu_main/Briefcase-48.png" width="30" height="30" border="0"> <br> 
						PERSONAL COMISIONADO </a>
						<hr>
						
						<?php } if ($row_usuario['rh7']==1){ ?>	
						
						<a href="contenido/rec_hum/sindicalizado/personal_sind.php" target="mainFrame"> <img src="images/menu_main/relacionsindicato.png" width="30" height="30" border="0"> <br> 
						RELACI&Oacute;N DE PERSONAL SINDICALIZADO </a>
						<hr>
						
						<a href="contenido/rec_hum/sindicalizado/convenios_sindicales.php" target="mainFrame"> <img src="images/menu_img/convenios_sindicales.gif" width="30" height="31" border="0"> <br> 
						CONVENIOS SINDICALES </a>
						<hr>
						
						<a href="contenido/rec_hum/sindicalizado/juicios_laborales.php" target="mainFrame"> <img src="images/menu_main/juicioslab1.png" width="30" height="30" border="0"> <br> 
						JUICIOS LABORALES </a>
						<hr>
					<?php } ?>					  								
				</div>
				
			<?php } ?>		
			
			<h3> ENTREGA-RECEPCI&Oacute;N</h3>
			<div>
				<?php if ($row_usuario['er1']==1) { 	?>
					<hr>
					<a href="entrega/proceso/config1.php" target="mainFrame"> <img src="images/menu_main/asignacionformatos.png" width="36" height="36" border="0"> <br> 
					ASIGNACI&Oacute;N DE FORMATOS ENTREGA - RECEPCI&Oacute;N</a>					
					<?php } if ($row_usuario['er5']==1){ ?>					  
					<hr>
					<a href="entrega/menu_entrega.php" target="mainFrame"> <img src="images/menu_main/formter.png" width="32" height="32" border="0"> <br> 
					FORMATOS DE ENTREGA RECEPCI&Oacute;N</a>
					
					<?php } if ($row_usuario['er3']==1){ ?>					  
					<hr>
					<a href="entrega/acta.php" target="mainFrame"> <img src="images/menu_main/actaer.png" width="40" height="40" border="0"> <br> 
					ACTA DE ENTREGA RECEPCI&Oacute;N</a>
					<?php } 										
					
					//----------Le permitimos descargar su paquete-------------
					if ($row_usuario['er1']!=1 && $row_usuario['paquetes'] == 1){
					?>
					<hr>
					<a href="entrega/redirecciona.php" target="mainFrame"> <img src="images/menu_main/descargarpaquete.png" width="38" height="38" border="0"> <br> 
					DESCARGAR PAQUETE DE ENTREGA - RECEPCI&Oacute;N </a>
					
				<?php } ?>
				
				<hr>
				<a href="entrega/pdf_cat/ver_anexos/anexos.php" target="mainFrame"> <img src="images/menu_main/anexosubordinados.png" width="36" height="36" border="0"> <br> 
				ANEXOS SUBORDINADOS</a>
				
				<hr>
				<a href="chat/chat.php" target="mainFrame"> <img src="images/menu_main/soportetec.png" width="32" height="32" border="0"> <br> 
				SOPORTE T�CNICO</a>
				
				<hr>
				<a href="entrega/semaforo/semaforo.php" target="mainFrame"><img src="images/menu_main/reportes.png" width="40" height="40" border="0"><br> 
				AVANCE DE LLENADO</a>
				
				<hr>
				<a href="entrega/archivo/index.php" target="mainFrame"><img src="images/menu_main/archivos.png" width="40" height="40" border="0"><br> 
				ARCHIVO</a>
				
				<?php if ($row_usuario['er7']==1){ ?>					  	
					<hr>
					<a href="actualizaciones/manuales/manuales.php" target="mainFrame"> <img src="images/menu_main/lineamientos.png" width="40" height="40" border="0"> <br> 
					LINEAMIENTOS E/R Y MANUALES CREG E/R </a>			
					
				<?php } ?> 
				<hr>		
				<a href="contenido/requisitos_digital.php" target="mainFrame"><img src="images/menu_img/Scanner-32.png" width="32" height="32" border="0"><br> 
				REQUISITOS DE DIGITALIZACI&Oacute;N</a>
				<hr>
				
				<?php    
					$user=$_SESSION['MM_Username'];
					$area=$_SESSION['clave_dependencia'];
					
					$sql = "SELECT admin FROM usuario WHERE usuario='$user' LIMIT 1";
					mysql_select_db($database_bd2, $bd2);
					$depen = mysql_query($sql, $bd2) or die(mysql_error());
					$totalRows_depen = mysql_num_rows($depen);
					
					if($totalRows_depen > 0){
						$row_depen = mysql_fetch_assoc($depen);
						$admin = $row_depen['admin'];
						
						
					?>
					
					<?php 	if($admin == 1){  ?>
						
						
						<?php } else { ?>
						
						<!----- Usuarios ----->
						<!--<a href="contenido/entrega_csv_msv/index2.php" target="mainFrame"><img src="images/Upload_CSV.png" width="36" height="36" border="0"><br> 
						ENTREGA CSV</a>
						<hr>-->
						<!-----  ----->
					<?php 	} }?>
					
					
					
			</div>
			
			<h3> SALIR DEL SISTEMA</h3>
			<div>
				<hr>
				<a href="<?php echo $logoutAction ?>" target="_parent"> <img src="images/menu_main/logout.png" width="32" height="32" border="0"> <br> 
				CERRAR SESI&Oacute;N</a>
				<hr>																				
			</div>
		</div>	
		
		<br />
		<div align="center">
			<a href="http://www.iidesoft.com.mx" target="_blank">
				<img src="images/LOGO_menu.gif" alt="Inform&aacute;tica, Ingenier&iacute;a y Desarrollo de Software | M&eacute;xico" width="141" height="46" border="0" />
			</a>
		</div>
	</body>
</html>
