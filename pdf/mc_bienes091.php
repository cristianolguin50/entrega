<?php
	require('fpdf.php');
	
	class PDF_MC_Table extends FPDF
	{
		var $widths;
		var $aligns;
		var $encabezado;
		var $alinea;
		
		function Header()
		{
			global $dia;
			global $mes;
			global $year;
			global $title;
			global $area;
			global $ayuntam;
			global $mpo;
			global $usr;
			global $puesto;
			global $srio;
			global $tipo_entidad;
			global $funcionario2;
			//global $encabezado;
			//Logo
			//$this->SetTopMargin(2);
			$this->SetMargins(5,2);
			$this->Image('../../../images/azul2.jpg',10,10,20);
			//Arial bold 15
			$this->SetFont('Arial','',8);
			//Movernos a la derecha
			$this->Image('../../../images/image002.jpg', 390, 8, 20);
			
			$check0 =' ';
			$check2 =' ';
			$check3 =' ';
			$check4 =' ';
			switch ($tipo_entidad)
			{
				case 0:
				$check0='X';
				break;
				
				case 2:
				$check2='X';
				break;
				
				case 3:
				$check3='X';
				break;
				
				case 4:
				$check4='X';
				break;
				
				default: 
				$check0='X';
			}
			
			
			switch ($tipo_entidad)
			{
				case 0:
				$funcionario[0]="Presidente";
				$funcionario[1]="SECRETARIO DEL AYUNTAMIENTO";
				$funcionario[2]="Síndico";
				$funcionario[3]="Tesorero";
				$funcionario[4]="Contralor";
				break;
				
				case 3:
				$funcionario[0]="Presidente del Consejo";
				$funcionario[1]="DIRECTOR GENERAL";
				$funcionario[2]="Director de Finanzas";
				$funcionario[3]="Comisario";
				$funcionario[4]="Contralor";
				break;
				
				case 2:
				$funcionario[0]="Presidente";
				$funcionario[1]="DIRECTOR";
				$funcionario[2]="Tesorero";
				$funcionario[3]="Contralor";
				break;
				
				case 4:
				$funcionario[0]="Director";
				$funcionario[1]="DIRECTOR";
				$funcionario[2]="Tesorero";
				$funcionario[3]="Contralor";
				break;
				
				default: 
				$funcionario[0]="Presidente";
				$funcionario[1]="SECRETARIO DEL AYUNTAMIENTO";
				$funcionario[2]="Síndico";
				$funcionario[3]="Tesorero";
				$funcionario[4]="Contralor";
			}
			
			$this->SetY(7);
			$this->Cell(130);
			$this->Cell(25,4,'AYUNTAMIENTO',0,0,'R');
			$this->Cell(10,4,$check0,1,0,'C');
			$this->Cell(60);
			$this->Cell(25,4,'FECHA:',0,0,'R');
			$this->SetFont('Arial','B',8);
			$this->Cell(35,4,$dia . "/" . $mes . "/" . $year ,0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Ln(5);
			$this->Cell(45);
			$this->Cell(25,4,'MUNICIPIO:',0,0,'R');
			$this->SetFont('Arial','B',8);
			$this->Cell(35,4,$ayuntam,0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(25);
			$this->Cell(25,4,'ODAS',0,0,'R');
			$this->Cell(10,4,$check2,1,0,'C');
			$this->Cell(60);
			$this->Cell(25,4,'ELABOR�:',0,0,'R');
			$this->SetFont('Arial','B',8);
			$this->Cell(25,4,$usr,0,0,'');
			$this->SetFont('Arial','',8);
			$this->Cell(30);
			$this->Cell(25,4,'CARGO:',0,0,'R');
			$this->SetFont('Arial','B',8);
			$this->Cell(25,4,$puesto,0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Ln(5);
			$this->Cell(45);
			$this->Cell(25,4,'N�MERO:',0,0,'R');
			$this->SetFont('Arial','B',8);
			$this->Cell(35,4,$mpo,0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Cell(25);
			$this->Cell(25,4,'DIF',0,0,'R');
			$this->Cell(10,4,$check3,1,0,'C');
			$this->Cell(60);
			$this->Cell(25,4,'REVIS�:',0,0,'R');
			$this->SetFont('Arial','B',8);
			$this->Cell(25,4,$srio,0,0,'');
			$this->SetFont('Arial','',8);
			$this->Cell(30);
			$this->Cell(25,4,'CARGO:',0,0,'R');
			$this->SetFont('Arial','B',8);
			$this->Cell(25,4,$funcionario[1],0,0,'L');
			$this->SetFont('Arial','',8);
			$this->Ln(5);
			$this->Cell(130);
			$this->Cell(25,4,'OTROS',0,0,'R');
			$this->Cell(10,4,$check4,1,0,'C');
			$this->Cell(25,4,'ESPECIFICAR',0,0,'R');
			$this->Cell(35,4,'',0,0,'L');
			//	$this->Cell(10);	
			$this->Cell(25,4,'HOJA:',0,0,'R');
			$this->SetFont('Arial','B',8);
			$this->Cell(30,4,$this->PageNo().' DE {nb}',0,0,'L');
			$this->SetFont('Arial','',8);	
			$this->Ln(6);
			$this->SetFont('Arial','B',15);
			$this->SetFillColor(27,107,40);
			$this->SetTextColor(255,255,255);
			
			//Calculamos ancho y posici�n del t�tulo.
			/*     $w=$this->GetStringWidth($title)+6;
				$this->SetX((275-$w)/2);
			$this->Cell($w,9,$title,0,1,'C'); */
			$this->MultiCell(440,6,$title,1,'C', true);
			//Salto de l�nea
			//	$this->Ln(3.5);
			
			
			$this->Ln(5);
		}
		
		//Pie de p�gina
		function Footer()
		{
			global $usr;
			global $forma;
			global $var;
			global $dia;
			global $mes;
			global $year;
			global $ent;
			global $presi;
			global $srio;
			global $sind;
			global $tesor;
			global $cont;
			global $funcionario1;
			global $funcionario2;
			global $funcionario3;
			global $funcionario4;
			global $funcionario5;
			global $tipo_entidad;
			//Posici�n: a 1,5 cm del final
			$this->SetY(-17);	
			$this->SetFont('Arial','B',8);
			$this->SetLineWidth(0.2);
			//Arial italic 8
			$check0 =' ';
			$check2 =' ';
			$check3 =' ';
			$check4 =' ';
			switch ($tipo_entidad)
			{
				case 0:
				$check0 ='PRESIDENTE';
				$check1 ='SINDICO';
				$check2 ='SECRETARIO';
				$check3 ='TESORERO';
				$check4 ='CONTRALOR';
				
				$this->Cell(10);
				$this->Cell(65, 0, $presi, 0, 0, 'C');
				$this->Cell(25);
				$this->Cell(65, 0, $sind, 0, 0, 'C');
				$this->Cell(25);
				$this->Cell(65, 0, $srio, 0, 0, 'C');
				$this->Cell(25);
				$this->Cell(65, 0, $tesor, 0, 0, 'C');
				$this->Cell(25);
				$this->Cell(65, 0, $cont, 0, 0, 'C');
				break;
				
				case 2:
				$check0 ='DIRECTOR GENERAL';
				$check1 ='DIRECTOR DE FINANZAS';
				$check2 ='COMISARIO';
				$check3 ='CONTRALOR';
				
				$this->Cell(10);
				$this->Cell(65, 0, $srio, 0, 0, 'C');
				$this->Cell(25);
				$this->Cell(65, 0, $sind, 0, 0, 'C');
				$this->Cell(25);
				$this->Cell(65, 0, $tesor, 0, 0, 'C');
				$this->Cell(25);
				$this->Cell(65, 0, $cont, 0, 0, 'C');
				break;
				
				case 3:
				$check0 ='PRESIDENTE';
				$check1 ='DIRECTOR GENERAL';
				$check2 ='TESORERO';
				$check3 ='CONTRALOR';
				
				$this->Cell(10);
				$this->Cell(65, 0, $presi, 0, 0, 'C');
				$this->Cell(25);
				$this->Cell(65, 0, $srio, 0, 0, 'C');
				$this->Cell(25);
				$this->Cell(65, 0, $sind, 0, 0, 'C');
				$this->Cell(25);
				$this->Cell(65, 0, $tesor, 0, 0, 'C');
				break;
				
				case 4:
				$check0 ='DIRECTOR GENERAL';
				$check1 ='DIRECTOR DE FINANZAS';
				$check2 ='CONTRALOR';
				
				$this->Cell(10);
				$this->Cell(65, 0, $presi, 0, 0, 'C');
				$this->Cell(25);
				$this->Cell(65, 0, $srio, 0, 0, 'C');
				$this->Cell(25);
				$this->Cell(65, 0, $sind, 0, 0, 'C');
				break;
				
				default: 
				$check0 ='PRESIDENTE';
				$check1 ='SINDICO';
				$check2 ='SECRETARIO';
				$check3 ='TESORERO';
				$check4 ='CONTRALOR';
				
				$this->Cell(10);
				$this->Cell(65, 0, $presi, 0, 0, 'C');
				$this->Cell(25);
				$this->Cell(65, 0, $sind, 0, 0, 'C');
				$this->Cell(25);
				$this->Cell(65, 0, $srio, 0, 0, 'C');
				$this->Cell(25);
				$this->Cell(65, 0, $tesor, 0, 0, 'C');
				$this->Cell(25);
				$this->Cell(65, 0, $cont, 0, 0, 'C');
			}
			
			$this->Line(15, 265, 80, 265);
			$this->Line(105, 265, 170, 265);
			$this->Line(195, 265, 260, 265);
			if($tipo_entidad != 4){
				$this->Line(285, 265, 350, 265);
			}
			if($tipo_entidad==0){
				$this->Line(375, 265, 440, 265);
			}
			$this->Ln(5);
			$this->Cell(10);
			$this->Cell(65, 1, $check0, 0,0, 'C');
			$this->Cell(25);
			$this->Cell(65, 1, $check1, 0,0, 'C');
			$this->Cell(25);
			$this->Cell(65, 1, $check2, 0,0, 'C');
			if($tipo_entidad != 4){
				$this->Cell(25);
				$this->Cell(65, 1, $check3, 0,0, 'C');
			}
			if($tipo_entidad==0){
				$this->Cell(25);
				$this->Cell(65, 1, $check4, 0,0, 'C');
			}
			$this->SetLineWidth(0.5);
			//$this->Line(10, 180, 270, 180);
			$this->Ln(11);
			$this->SetFont('Arial','B',8);
			//$this->MultiCell(400,3,$var,0,'J');
			
			//$this->Cell(70
			$this->Ln(5);
		}
		
		
		
		function SetWidths($w)
		{
			//Set the array of column widths
			$this->widths=$w;
		}
		
		function SetAligns($a)
		{
			//Set the array of column alignments
			$this->aligns=$a;
		}
		function Recibe($rec)
		{
			//global $encabezado;
			$this->encabezado= $rec;
			//$this->Row($encabezado);
			//Set the array of column widths
			//$this->widths=$w;
		}
		function Alinea($alig)
		{
			//global $encabezado;
			$this->alinea= $alig;
			//$this->Row($encabezado);
			//Set the array of column widths
			//$this->widths=$w;
		}
		
		function Row($data)
		{
			//Calculate the height of the row
			$nb=0;
			for($i=0;$i<count($data);$i++)
			$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
			$h=3*$nb;
			//Issue a page break first if needed
			$this->CheckPageBreak($h);
			//Draw the cells of the row
			for($i=0;$i<count($data);$i++)
			{
				$w=$this->widths[$i];
				$a=isset($this->aligns[$i]) ? $this->aligns[$i] : $this->alinea;
				//Save the current position
				$x=$this->GetX();
				$y=$this->GetY();
				//Draw the border
				$this->Rect($x,$y,$w,$h);
				//Print the text
				$this->MultiCell($w,3,$data[$i],0,$a);
				//Put the position to the right of the cell
				$this->SetXY($x+$w,$y);
			}
			//Go to the next line
			$this->Ln($h);
			$this->SetLineWidth(0.2);
		}
		
		function CheckPageBreak($h)
		{
		global $tamanios;
			//If the height h would cause an overflow, add a new page immediately
			//if($this->GetY()+$h>$this->PageBreakTrigger){
			if($this->GetY()+$h>250){
				$this->AddPage($this->CurOrientation);
				$this->SetFont('Arial','B',5);
				$this->SetFillColor(27,107,40);
				$this->SetTextColor(255,255,255);
				
				$y=$this->GetY();
				$x = 5;
				$this->SetLineWidth(0.4);
				//$this->Line(10, $y, 270, $y);
				$this->SetLineWidth(0.2);
				$this->MultiCell($tamanios[0],3,"N�M PROG",1,'C', true);
				$this->SetXY($x = $x + $tamanios[0],$this->GetY()-6);
				$this->MultiCell($tamanios[1],2,"N�M. DE LA PARTIDA DE EGRESOS",1,'C', true);
				$this->SetXY($x = $x + $tamanios[1],$this->GetY()-6);
				$this->MultiCell($tamanios[2],6,"NOMBRE DE LA PARTIDA",1,'C', true);
				$this->SetXY($x = $x + $tamanios[2],$this->GetY()-6);
				$this->MultiCell($tamanios[3],3,"N�MERO DE    INVENTARIO",1,'C', true);
				$this->SetXY($x = $x + $tamanios[3],$this->GetY()-6);
				$this->MultiCell($tamanios[4],3,"N�M. DE RESGUARDO",1,'C', true);
				$this->SetXY($x = $x + $tamanios[4],$this->GetY()-6);
				$this->MultiCell($tamanios[5],3,"NOMBRE DEL RESGUARDATARIO",1,'C', true);
				$this->SetXY($x = $x + $tamanios[5],$this->GetY()-6);
				$this->MultiCell($tamanios[6],6,"NOMBRE DEL MUEBLE",1,'C', true);
				$this->SetXY($x = $x + $tamanios[6],$this->GetY()-6);
				$this->MultiCell($tamanios[7],6,"MARCA",1,'C', true);
				$this->SetXY($x = $x + $tamanios[7],$this->GetY()-6);
				$this->MultiCell($tamanios[8],6,"MODELO",1,'C', true);
				$this->SetXY($x = $x + $tamanios[8],$this->GetY()-6);
				$this->MultiCell($tamanios[9],3,"N�MERO DE SERIE",1,'C', true);
				$this->SetXY($x = $x + $tamanios[9],$this->GetY()-6);
				$this->MultiCell($tamanios[10],3,"ESTADO DE USO",1,'C', true);
				$this->SetXY($x = $x + $tamanios[10],$this->GetY()-6);
				$this->MultiCell($colFact = $tamanios[11] + $tamanios[12] + $tamanios[13] + $tamanios[14],3,"FACTURA",1,'C', true);
				$this->SetX($x);
				$this->MultiCell($tamanios[11],3,"N�MERO",1,'C', true);
				$this->SetXY($x = $x + $tamanios[11],$this->GetY()-3);
				$this->MultiCell($tamanios[12],3,"FECHA",1,'C', true);
				$this->SetXY($x = $x + $tamanios[12],$this->GetY()-3);
				$this->MultiCell($tamanios[13],3,"PROVEEDOR",1,'C', true);
				$this->SetXY($x = $x + $tamanios[13],$this->GetY()-3);
				$this->MultiCell($tamanios[14],3,"COSTO",1,'C', true);
				$this->SetXY($x = $x + $tamanios[14],$this->GetY()-6);
				$this->MultiCell($colPol = $tamanios[15] + $tamanios[16] + $tamanios[17],3,"POLIZA",1,'C', true);
				$this->SetX($x);
				$this->MultiCell($tamanios[15],3,"TIPO",1,'C', true);
				$this->SetXY($x = $x + $tamanios[15],$this->GetY()-3);
				$this->MultiCell($tamanios[16],3,"N�MERO",1,'C', true);
				$this->SetXY($x = $x + $tamanios[16],$this->GetY()-3);
				$this->MultiCell($tamanios[17],3,"FECHA",1,'C', true);
				$this->SetXY($x = $x + $tamanios[17] ,$this->GetY()-6);
				$this->MultiCell($tamanios[18],6,"RECURSO",1,'C', true);
				$this->SetXY($x = $x + $tamanios[18],$this->GetY()-6);
				$this->MultiCell($colMov = $tamanios[19] + $tamanios[20],3,"MOVIMIENTOS",1,'C', true);
				$this->SetX($x);
				$this->MultiCell($tamanios[19],3,"FECHA/ALTA",1,'C', true);
				$this->SetXY($x = $x + $tamanios[19],$this->GetY()-3);
				$this->MultiCell($tamanios[20],3,"FECHA/BAJA",1,'C', true);
				$this->SetXY($x = $x + $tamanios[20],$this->GetY()-6);
				$this->MultiCell($tamanios[21],6,"�REA RESPONSABLE",1,'C', true);
				$this->SetXY($x = $x + $tamanios[21],$this->GetY()-6);
				$this->MultiCell($tamanios[22],6,"LOCALIDAD",1,'C', true);
				$this->SetXY($x = $x + $tamanios[22],$this->GetY()-6);
				$this->MultiCell($tamanios[23],6,"OBSERVACIONES",1,'C', true);
				
				
				$this->SetTextColor("black");
				
				$this->SetFont('Arial','',5);
				
			}
		}
		
		function NbLines($w,$txt)
		{
			//Computes the number of lines a MultiCell of width w will take
			$cw=&$this->CurrentFont['cw'];
			if($w==0)
			$w=$this->w-$this->rMargin-$this->x;
			$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
			$s=str_replace("\r",'',$txt);
			$nb=strlen($s);
			if($nb>0 and $s[$nb-1]=="\n")
			$nb--;
			$sep=-1;
			$i=0;
			$j=0;
			$l=0;
			$nl=1;
			while($i<$nb)
			{
				$c=$s[$i];
				if($c=="\n")
				{
					$i++;
					$sep=-1;
					$j=$i;
					$l=0;
					$nl++;
					continue;
				}
				if($c==' ')
            $sep=$i;
				$l+=$cw[$c];
				if($l>$wmax)
				{
					if($sep==-1)
					{
						if($i==$j)
						$i++;
					}
					else
					$i=$sep+1;
					$sep=-1;
					$j=$i;
					$l=0;
					$nl++;
				}
				else
            $i++;
			}
			return $nl;
		}
	}
?>