<?php
define('FPDF_FONTPATH','font/');
require('fpdi.php');
class concat_pdf extends fpdi {

    var $files = array();

    function concat_pdf($orientation='P',$unit='mm',$format='Letter') {
        parent::fpdi($orientation,$unit,$format);
    }

    function setFiles($files) {
        $this->files = $files;
    }

    function concat() {
		global $dependencia;
		global $dependencia_nom;
		global $ayuntamiento;
		global $ubicacion;
		global $hora;
		global $minutos;
		global $hora_ini;
		global $minutos_ini;
		global $dia;
		global $mes;
		global $year;
		global $contralor;
		global $osfem;
		global $observa;
		global $testigo1;
		global $cargotest1;
		global $testigo2;
		global $cargotest2;
		
	global $ex_serv;
global $oficial1;
global $acta1;
global $nombramiento1;
global $constancia1;
global $credencial1;
global $cargo1;
global $inicio1;
global $fin1;
global $edad1;
global $edo_civil1;
global $direccion1;
global $mail1;
global $lugar_nac1;
global $rfc1;
global $issem1;
global $tel1;
global $tel11;
global $domicilio1;
global $persona1;
global $direccion11;
global $comprobante1;
global $entrante;
global $oficial2;
global $acta2;
global $nombramiento2;
global $cargo2;
global $inicio2;
global $edad2;
global $edo_civil2;
global $direccion2;
global $mail2;
global $lugar_nac2;
global $rfc2;
global $issem2;
global $tel2;
global $tel21;
global $domicilio2;
global $persona2;
global $direccion21;
global $comprobante2;
global $observaciones;
global $activa;
global $clave_area;
global $fecha;
global $usuario;
global $ioe001;
global $ioe004;
global $ioe005;
global $ioe0051;
global $ioe006;
global $ioe010;
global $ioe011;
global $ioe013;
global $ioe014;
global $ioe015;
global $ioe016;
global $ioe017;
global $ioe018;
global $ioe019;
global $ioe021;
global $ioe022;
global $iep001;
global $iep002;
global $iep003;
global $iad001;
global $iad002;
global $iad004;
global $iad005;
global $iad006;
global $iad007;
global $iad008;
global $iad009;
global $iad010;
global $iad0101;
global $iad011;
global $iad012;
global $iad013;
global $iad014;
global $iad015;
global $iad016;
global $iad017;
global $iad018;
global $iad019;
global $iad020;
global $iad022;
global $iad023;
global $iad0231;
global $iad024;
global $iad025;
global $ila001;
global $ila005;
global $ila006;
global $ica001;
global $ica002;
global $ica003;
global $ica004;
global $ica005;
global $ica006;
global $ica007;
global $ica008;
global $ica009;
global $iop001;
global $iop002;
global $iop003;
global $ipe001;
global $ipe002;
global $ipe003;
global $ipe004;
global $ipe005;
global $ial001;
global $ial0011;
global $ial002;
global $ial003;
global $ioe001_c;
global $ioe004_c;
global $ioe005_c;
global $ioe0051_c;
global $ioe006_c;
global $ioe010_c;
global $ioe011_c;
global $ioe013_c;
global $ioe014_c;
global $ioe015_c;
global $ioe016_c;
global $ioe017_c;
global $ioe018_c;
global $ioe019_c;
global $ioe021_c;
global $ioe022_c;
global $iep001_c;
global $iep002_c;
global $iep003_c;
global $iad001_c;
global $iad002_c;
global $iad004_c;
global $iad005_c;
global $iad006_c;
global $iad007_c;
global $iad008_c;
global $iad009_c;
global $iad010_c;
global $iad0101_c;
global $iad011_c;
global $iad012_c;
global $iad013_c;
global $iad014_c;
global $iad015_c;
global $iad016_c;
global $iad017_c;
global $iad018_c;
global $iad019_c;
global $iad020_c;
global $iad022_c;
global $iad023_c;
global $iad0231_c;
global $iad024_c;
global $iad025_c;
global $ila001_c;
global $ila005_c;
global $ila006_c;
global $ica001_c;
global $ica002_c;
global $ica003_c;
global $ica004_c;
global $ica005_c;
global $ica006_c;
global $ica007_c;
global $ica008_c;
global $ica009_c;
global $iop001_c;
global $iop002_c;
global $iop003_c;
global $ipe001_c;
global $ipe002_c;
global $ipe003_c;
global $ipe004_c;
global $ipe005_c;
global $ial001_c;
global $ial0011_c;
global $ial002_c;
global $ial003_c;

	
        foreach($this->files AS $file) {
		
            $pagecount = $this->setSourceFile($file);
						
				$tplidx = $this->ImportPage(1);
				
                 $this->AddPage();
					 
				 $this->SetY(72);			
				 
				 $this->SetFont('Arial','', 10);
				 $this->Cell(5);
				 
				 
				 //--------------AER 1-----------------
			//	 if (1==0){
				 
				 $this->MultiCell(170,5,"En el municipio de " . 	$ayuntamiento . ", ESTADO DE M�XICO, siendo las " . $hora_ini . "	horas con " . $minutos_ini . " minutos del d�a " . $dia . " de ". $mes . " del a�o dos mil doce	para dar cumplimiento a lo dispuesto por los art�culos 61 fracciones XXXIII y XXXV;  129 �ltimo p�rrafo y  130 de la Constituci�n  Pol�tica  del Estado  Libre y Soberano de M�xico; 7, 8 fracciones XI, XIX y XXXIII, 10, 13 fracciones II y XXII, 24 fracciones  VIII y X, 43, 44 y 45 de la Ley de Fiscalizaci�n Superior del Estado de M�xico; 1 y 3 del Reglamento Interior del OSFEM; en concordancia con los art�culos 19 y 112 fracci�n XII de la Ley Org�nica  Municipal del Estado de M�xico, 42 fracciones V, X, XXVI y XXVII, 43 y 62 de  la  Ley de Responsabilidades de los Servidores P�blicos del Estado de M�xico y Municipios, 22 del C�digo Financiero del Estado de M�xico y Municipios y 134 y 156 del C�digo Penal del Estado de M�xico. Encontr�ndose presentes y protestados en t�rminos de ley para que se conduzcan con verdad en la diligencia en que intervienen, sabedores de las penas en que incurren quienes declaran con falsedad ante autoridad, se levanta la presente en las instalaciones que ocupan las oficinas de ". $dependencia_nom . " ubicadas en ". $ubicacion . " los CC. ". $ex_serv . ", " . $entrante . ", " . $testigo1. ", " . $testigo2	. " , " .  $contralor  . " , " .  $osfem  .", reunidos para llevar a cabo el acto de entrega-recepci�n del �rea de " . $dependencia_nom."; donde el C. " . $ex_serv . ", con RFC " .$rfc1 . " y clave de ISSEMyM " .$issem1 . " quien ocup� el cargo de  " . $cargo1 . " por el periodo comprendido del " . $inicio1 . "	al	" . $fin1	 . ",  quien se identifica con credencial oficial n�mero IFE " . $oficial1 . " , que por sus generales manifiesta ser originario (a) de "  . $lugar_nac1 .  " , con domicilio actual en " . $direccion1 . " , tel�fono particular " . $tel1  . "tener " . $edad1	 ." a�os de edad, estado civil " . $edo_civil1	 .", se�alando adem�s como domicilio para o�r y recibir notificaciones el ubicado en " . $domicilio1	 . " y autorizando al (los) C (C): " . $persona1	 . "  para que a su nombre y representaci�n reciba (n) todo tipo de documentos; procedi� a entregar al (la) C. " . $entrante	 . ", con RFC ". $rfc2	 . " y clave de ISSEMyM " . $issem2	 ." , quien se identifica con credencial oficial n�mero IFE " . $oficial2	 . " por sus generales manifiesta ser originario (a) de " . $lugar_nac2 . "  con domicilio actual en " . $direccion2	 .", tel�fono particular " . $tel2	 . " tener " . $edad2	 ."  a�os de edad, estado civil " . $edo_civil2	 . ", se�alando adem�s como domicilio para o�r y recibir notificaciones el ubicado en " . $domicilio2	 . " y autorizando al (los) C (C): " . $persona2	 . " para que a su nombre y representaci�n reciba (n) todo tipo de documentos; quien fue designado (a) con el cargo de " . $cargo2	 . ", a partir del " .$inicio2 .", en los t�rminos legales correspondientes y conforme a la siguiente informaci�n: ",0,'J');				 
		
			
				 
				 
				 $this->SetFont('Arial','', 9);
				 
				 $this->useTemplate($tplidx);
				 
				 $tplidx = $this->ImportPage(2);
                 $this->AddPage();
				 
				 $this->SetY(56);	
				 if ($ioe001==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $ioe001_c, 0, 0, 'L');
				 
				 //------------------------------
				
				 $this->SetY(67);	
				 if ($ioe004==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $ioe004_c, 0, 0, 'L');
				 //-------------------------------------------
				 
				 $this->SetY(81);	
				 if ($ioe005==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 		 
				 $this->SetY(85);	
				 if ($ioe0051==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $ioe005_c, 0, 0, 'L');
				 //--------------------------
				 $this->SetY(96);	
				 if ($ioe006==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $ioe006_c, 0, 0, 'L');
				 //--------------------------------------------------
				 $this->SetY(110);	
				 if ($ioe010==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 //**************************************
				 $this->SetY(114);	
				 if ($ioe011==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $ioe011_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 $this->SetY(129);	
				 if ($ioe013==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 //-----------------------------------------------
				 
				 	 $this->SetY(133);	
				 if ($ioe014==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 //-----------------------------------------------
				 
				 	 $this->SetY(138);	
				 if ($ioe015==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $ioe015_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 $this->SetY(155);	
				 if ($ioe016==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
			 //-----------------------------------------------
				 
				 $this->SetY(159);	
				 if ($ioe017==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 //-----------------------------------------------
				 
				 	 $this->SetY(163);	
				 if ($ioe018==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }

				 //-----------------------------------------------
				 
				 	 $this->SetY(167);	
				 if ($ioe019==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 //-----------------------------------------------
				 
				 	 $this->SetY(171);	
				 if ($ioe021==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $ioe021_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 	 $this->SetY(181);	
				 if ($ioe022==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $ioe022_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 $this->SetY(210);	
				 if ($iep001==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iep001_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 $this->SetY(226);	
				 if ($iep002==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iep002_c, 0, 0, 'L');
				 //-----------------------------------------------
				 $this->SetY(238);	
				 if ($iep003==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iep003_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
			/*****************PAG3*///////////////////	 
					 
                $this->useTemplate($tplidx);
				 
				 $tplidx = $this->ImportPage(3);
                 $this->AddPage();
				 $this->SetAutoPageBreak(false, 2);
				 

 $this->SetY(48);	
				 if ($iad001==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iad001_c, 0, 0, 'L');
				 
				 //------------------------------
				
				 $this->SetY(58);	
				 if ($iad002==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iad002_c, 0, 0, 'L');
				 //-------------------------------------------
				 
						 
				 $this->SetY(69);	
				 if ($iad004==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iad004_c, 0, 0, 'L');
				 //--------------------------
				 $this->SetY(80);	
				 if ($iad005==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iad005_c, 0, 0, 'L');
				 
				 //--------------------------------------------------
				 $this->SetY(92);	
				 if ($iad006==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iad006_c, 0, 0, 'L');
				 //**************************************
				 $this->SetY(106);	
				 if ($iad007==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iad007_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 $this->SetY(116);	
				 if ($iad008==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iad008_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 	 $this->SetY(127);	
				 if ($iad009==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iad009_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 	 $this->SetY(138);	
				 if ($iad010==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iad010_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				  	 $this->SetY(148);	
				 if ($iad0101==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iad0101_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 $this->SetY(157);	
				 if ($iad011==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iad011_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 $this->SetY(169);	
				 if ($iad012==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iad012_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 	 $this->SetY(180);	
				 if ($iad013==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iad013_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 	 $this->SetY(190);	
				 if ($iad014==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iad014_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 	 $this->SetY(201);	
				 if ($iad015==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iad015_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 	 $this->SetY(212);	
				 if ($iad016==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iad016_c, 0, 0, 'L');
				 //-----------------------------------------------
		 /************pag 4 **********/
				 
				 
                $this->useTemplate($tplidx);
				 
				  $tplidx = $this->ImportPage(4);
				  
				  $this->AddPage();
				 $this->SetAutoPageBreak(false, 2);
				 
				 $this->SetY(41);	
				 if ($iad017==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iad017_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 $this->SetY(52);	
				 if ($iad018==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iad018_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 $this->SetY(62);	
				 if ($iad019==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iad019_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 		 $this->SetY(73);	
				 if ($iad020==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iad020_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 		 $this->SetY(84);	
				 if ($iad022==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iad022_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 		 $this->SetY(96);	
				 if ($iad023==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iad023_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 		 $this->SetY(110);	
				 if ($iad0231==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iad0231_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 		 $this->SetY(120);	
				 if ($iad024==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iad024_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 		 $this->SetY(131);	
				 if ($iad025==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iad025_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 
		 $this->SetY(156);	
				 if ($ila001==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $ila001_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 
		
				 
				  $this->SetY(166);	
				 if ($ila005==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(6);
				 $this->Cell(40);
				 $this->Cell(120,4, $ila005_c, 0, 0, 'L');
				 
				 //------------------------------
				 
				 $this->SetY(174);	
				 if ($ila006==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $ila006_c, 0, 0, 'L');
				 
				 //------------------------------*/
				  
				 
			 $this->SetY(199);	
				 if ($ica001==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $ica001_c, 0, 0, 'L');
				 //-----------------------------------------------
				  $this->SetY(210);	
				 if ($ica002==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $ica002_c, 0, 0, 'L');
				 //-----------------------------------------------
				  $this->SetY(221);	
				 if ($ica003==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $ica003_c, 0, 0, 'L');
				 //-----------------------------------------------
				  $this->SetY(231);	
				 if ($ica004==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $ica004_c, 0, 0, 'L');
				 //-----------------------------------------------

				 
				  //-----------------------------------------------
		 /************pag 5 **********/
				 
				 
                $this->useTemplate($tplidx);
				 
				  $tplidx = $this->ImportPage(5);
				  
				  $this->AddPage();
				 $this->SetAutoPageBreak(false, 2);
				 
				 $this->SetY(41);	
				 if ($ica005==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $ica005_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 $this->SetY(52);	
				 if ($ica006==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $ica006_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 $this->SetY(62);	
				 if ($ica007==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $ica007_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 		 $this->SetY(73);	
				 if ($ica008==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $ica008_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 $this->SetY(84);	
				 if ($ica009==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $ica009_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 $this->SetY(106);	
				 if ($iop001==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iop001_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 $this->SetY(116);	
				 if ($iop002==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iop002_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 	 $this->SetY(127);	
				 if ($iop003==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $iop003_c, 0, 0, 'L');
				 //-----------------------------------------------
				  $this->SetY(152);	
				 if ($ipe001==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 //-----------------------------------------------
				 
				 $this->SetY(156);	
				 if ($ipe002==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }

				 //-----------------------------------------------
				 
				 	 $this->SetY(160);	
				 if ($ipe003==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $ipe003_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 		 
				 	 $this->SetY(170);	
				 if ($ipe004==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $ipe004_c, 0, 0, 'L');
				 //-----------------------------------------------
				 		 
				 	 $this->SetY(181);	
				 if ($ipe005==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $ipe005_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				  $this->SetY(203);	
				 if ($ial001==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $ial001_c, 0, 0, 'L');
				 //-----------------------------------------------
				 
				 		 
				 	 $this->SetY(213);	
				 if ($ial0011==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $ial0011_c, 0, 0, 'L');
				 //-----------------------------------------------
				 		 
				 	 $this->SetY(224);	
				 if ($ial002==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $ial002_c, 0, 0, 'L');
				 	 //-----------------------------------------------
					 
				 		 
				 	 $this->SetY(233);	
				 if ($ial003==1){
				 	$this->Cell(150);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 else{
				 	$this->Cell(164);
				 	$this->Cell(2,4, "X", 0, 0, 'L');
				 }
				 
				 $this->Ln(4);
				 $this->Cell(40);
				 $this->Cell(120,4, $ial003_c, 0, 0, 'L');
				 
				/******pagina 6******/////
				 
                 $this->useTemplate($tplidx);
				 
				 $tplidx = $this->ImportPage(6);
                 $this->AddPage();
				 
				
				 $this->SetY(42);	
				 
				 $this->Cell(5);
				 
				 $this->MultiCell(165,3, $observa,0,'J');
				 					
				 
                 $this->useTemplate($tplidx);
				 
				 $tplidx = $this->ImportPage(7);
                 $this->AddPage();							
				 				
				
				 
				 $this->SetY(110);	
			 $this->Cell(5);
				 $this->Cell(80,4, $ex_serv, 0, 0, 'C');
				 
				 $this->Cell(10);
				 $this->Cell(80,4, $entrante, 0, 0, 'C');
				 
				 
				  $this->SetY(165);	
				 $this->Cell(5);
				 $this->Cell(80,4, $testigo1, 0, 0, 'C');
				 
				 $this->Cell(10);
				 $this->Cell(80,4, $testigo2, 0, 0, 'C');
				 
				 $this->SetY(175);	
				 $this->Cell(5);
				 $this->Cell(80,4, $cargotest1, 0, 0, 'C');
				 
				 $this->Cell(10);
				 $this->Cell(80,4,$cargotest2, 0, 0, 'C');
				 
				 $this->SetY(235);	

				 $this->Cell(180,4,$osfem, 0, 0, 'C');
				 
				 $this->useTemplate($tplidx);	


        }
    }
}

?>
