<?php

class PdfTools{

	function imprimeHeaders($nombrePDF){		
		//App type
		header("Content-type:application/pdf");
		// It will be called downloaded.pdf
		header("Content-Disposition:attachment;filename=$nombrePDF");
		// The PDF source is in original.pdf
		readfile("$nombrePDF");
		//echo "<a href='$nTitle'>Ver</a>";
		
	}

	function clearString($text) {		
		$a_tofind = array('�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�'
   , '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�' , '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', ' ', '/', ',');
   		$a_replac = array('A', 'A', 'A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a', 'a'
   , 'O', 'O', 'O', 'O', 'O', 'O', 'o', 'o', 'o', 'o', 'o', 'o', 'E', 'E', 'E', 'E', 'e', 'e', 'e', 'e', 'C', 'c' , 'I', 'I', 'I', 'I', 'i', 'i', 'i', 'i', 'U', 'U', 'U', 'U', 'u', 'u', 'u', 'u', 'y', 'N', 'n', '_', '', '');
   		$cadenaSinAcentos = str_replace($a_tofind, $a_replac, $text);
		
		return $cadenaSinAcentos;
	}
	
	function construyeCadena($strPrefijo, $strNumeroEntidad, $strDependencia, $strFormato, $intRegistros, $strDocto){
		$strCadenaQR = '';
		$strCadenaQR = $strPrefijo.'|'.$strNumeroEntidad.'|'.$strDependencia.'|'.$strFormato.'|'.$intRegistros.'|'.date('Y-m-d H:i:s').'|'.$strDocto;
	
		return $strCadenaQR;	
	}
	
	function buscaAnexos($claveFormato, $dirAnexo, $pdfAnexo, $cla_dep, $regConsecutivo){
		//-----------------------ANEXOS OPT-------------------------
		require('../../lista_dir.php');
		//------------------Colocar la clave del formato---------------
	  	$directorio = "../../anexos/$claveFormato/";	
		//-------------------------------------------------------------
	  	$archivo = getDirectoryList($directorio);
		$genAnexo = false;	
		//$archivo = getDirectoryList($directorio);		
		$directorioActual = getcwd();
		
		chdir('../../paquete/');
		
		
		for($i=0;$i<=count($archivo)-1;$i++){						
			$partes = array();		
			$partes = explode('.', $archivo[$i]);		
			
			if(in_array($partes[0], $pdfAnexo)){
				if(strtolower($partes[1]) == 'pdf'){					
					$directorioAnexo = "../../paquete/$cla_dep/$dirAnexo/";		
					$partes = array();		
					$partes = explode('.', $archivo[$i]);		
					
					if($numAnexo = array_search($partes[0], $pdfAnexo)){
					
						$zipFile = exec("anexos_er.exe".' '.$cla_dep.' '.$dirAnexo.' '.$regConsecutivo[$numAnexo].'.pdf'.' '.$claveFormato.' '.$archivo[$i], $output);
					
						//echo "<br>anexos_er.exe".' '.$cla_dep.' '.$dirAnexo.' '.$regConsecutivo[$numAnexo].'.pdf'.' '.$claveFormato.' '.$archivo[$i];
												
						/*if(!file_exists($directorioAnexo)){
							mkdir($directorioAnexo, 0777);
							//copy($directorio.$archivo[$i],$directorioAnexo.$regConsecutivo[$numAnexo].'.pdf');								
							$zipFile = exec("../../paquete/anexos_er.exe".' '.$cla_dep.' '.$dirAnexo.' '.$regConsecutivo[$numAnexo].'.pdf'.' '.$claveFormato.' '.$archivo[$i], $output);
						}
						else{	
							//copy($directorio.$archivo[$i],$directorioAnexo.$regConsecutivo[$numAnexo].'.pdf');
							$zipFile = exec("../../paquete/anexos_er.exe".' '.$cla_dep.' '.$dirAnexo.' '.$regConsecutivo[$numAnexo].'.pdf'.' '.$claveFormato.' '.$archivo[$i], $output);
						}*/							
					}					
				}
			}						
		}
		chdir($directorioActual);
		//-----------------------------------------------------------
	}
	//Verifica que el usuario tenga sesion de lo contrario redirecciona fuera del sistema
	function tieneSesion(){
		if (!isset($_SESSION['MM_Username'])){
			header("Location: ../../../adios.php");
		}
	}
	//Regresa la dependencia a la cual pertenece el servidor publico
	function claveDependencia(){
		$colname_inventa = isset($_GET['area']) ? $_GET['area'] : "1";
		if (isset($_SESSION['clave_dependencia'])) {
 			$colname_inventa = (get_magic_quotes_gpc()) ? $_SESSION['clave_dependencia'] : addslashes($_SESSION['clave_dependencia']);
		}
		return $colname_inventa;
	}
	//Obtiene los datos basicos del municipio
	function datosMunicipio(){
		include('../../../Connections/bd2.php');
		
		mysql_select_db($database_bd2, $bd2);
		$query_ayunta = sprintf("SELECT nombre, numero, per_ini, per_fin, prefijo, tipo_entidad FROM ayuntamiento WHERE clave=1");
		$ayunta = mysql_query($query_ayunta, $bd2) or die(mysql_error());
		$row_ayunta = mysql_fetch_assoc($ayunta);
		
		return $row_ayunta;
	}
	//Obtiene el nombre de usuario de quien genera el formato
	function obtenerUsuario(){
		$colname_user = "1";
		if (isset($_SESSION['MM_Username'])) {
			$colname_user = (get_magic_quotes_gpc()) ? $_SESSION['MM_Username'] : addslashes($_SESSION['MM_Username']);
		}
		else{
			$colname_user = $_GET['user'];
		}
		return $colname_user;
	}
	//Verifica si el usuario tiene permiso para generar formatos originales
	function verificaPermisoOriginales($colname_user){
		include('../../../Connections/bd2.php');

		mysql_select_db($database_bd2, $bd2);
		$dtPermisos = mysql_query("SELECT er2 as originales FROM usuario WHERE usuario='$colname_user'", $bd2);
		$row_previas = mysql_fetch_array($dtPermisos);
		if($row_previas['originales'] == 0){
			header("Location: ../../error_permisos.php");	
		}	
	}
	//Obtiene los datos basicos del usuario
	function obtenerDatosUsuario($colname_user){
		include('../../../Connections/bd2.php');
		
		mysql_select_db($database_bd2, $bd2);
		$query_user = "SELECT a.usuario AS usuario, b.nombre_comp AS nombre, b.cargo AS cargo  FROM `usuario` a, trabajador b WHERE a.usuario = '$colname_user' AND a.nombre_comp = b.curp";
		
		$user = mysql_query($query_user, $bd2) or die(mysql_error());
		$row_user = mysql_fetch_assoc($user);
		
		return $row_user;
	}
	//Obiene los datos basicos de la dependencia
	function obtenerDatosDependencia($colname_inventa){
		include('../../../Connections/bd2.php');
		
		mysql_select_db($database_bd2, $bd2);
		$query_dep = "SELECT a.clave as clave, a.nombre as nombre, b.nombre_comp as nombre_comp FROM dependencia a, trabajador b WHERE a.clave_dep = '$colname_inventa' and a.resp_nombre=b.curp";
		
		$dep = mysql_query($query_dep, $bd2) or die(mysql_error());
		$row_dep = mysql_fetch_assoc($dep);		
		
		return $row_dep;
	}
}
?>