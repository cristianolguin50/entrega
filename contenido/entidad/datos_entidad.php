<?php
	
	require_once('../../Connections/bd2.php'); 
	
	mysql_select_db($database_bd2, $bd2);
	$query_ayunta = "SELECT * FROM ayuntamiento WHERE clave = 1";
	$ayunta = mysql_query($query_ayunta, $bd2) or die(mysql_error());
	$row_ayunta = mysql_fetch_assoc($ayunta);
	$entidad = $row_ayunta['tipo_entidad'];
	
	mysql_select_db($database_bd2, $bd2);
	$sqlMunicipio = "SELECT id, municipio, numero, prefijo FROM t_municipio_datos";
	$dtMunicipio = mysql_query($sqlMunicipio, $bd2) or die(mysql_error());
	
	//--------------------------------
	//requiere .2
	require_once('../../xajax1/xajax.inc.php');
	
	$xajax=new xajax();
	
	//debug .2
	//$xajax->debugOn();
	
	$xajax->setCharEncoding('iso-8859-1');
	
	//external 0.2
	//$xajax->registerExternalFunction("buscarSerie", "../../xajaxFuncion.php",XAJAX_POST);	
	$xajax->registerExternalFunction("cargaDatosMunicipio", "../../entrega/xajaxFuncion.php",XAJAX_POST);	
	//process .2
	$xajax->processRequests();	
	//---------------------------------
	
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Datos Generales</title>
		<meta charset="iso-8859-1">
		<meta http-equiv="Expires" content="0">
		<meta http-equiv="Last-Modified" content="0">
		<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
		<meta http-equiv="Pragma" content="no-cache">
		
		<link rel="stylesheet" href="../../js/jquery_ui_new/menu/jquery-ui.css">
		<script src="../../js/jquery-3.2.1.min.js"></script>
		<script src="../../js/jquery_ui_new/menu/jquery-ui.js"></script>
		
		<!--<script type="text/javascript" src="../js/jquery-1.7.1.js"></script>-->
		<link rel="stylesheet" type="text/css" media="screen" href="../../js/jquery.ketchup.0.3.2/css/jquery.ketchup.css" />
		<script type="text/javascript" src="../../js/jquery.ketchup.0.3.2/js/jquery.ketchup.all.min.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function() {
				$("#form1").ketchup(); 
			});
		</script>
		
		<script>
			$( function() {
				$( document ).tooltip();
			} );
		</script>
		
		<script language="javascript" src="../../js/validate.js"></script>
		<script language="javascript" type="text/javascript">
			function MM_goToURL() { //v3.0
				var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
				for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
			}
			
		</script>
		<link href="../../css/idots.css" rel="stylesheet" type="text/css">
		<style type="text/css">
			<!--
				body {
				margin-top: 0px;
				}
				.style1 {	color: #FF0000;
				font-style: italic;
				}
				.Estilo1 {
				font-size: 11px;
				font-weight: bold;
				}
			-->
		</style>		
	</head>
	
	<?php 
		//xajax .2
		$xajax->printJavascript('../../xajax1/');
	?>
	
	<body>
		<div align="center"><h2 style="color: #0404B4">Datos Generales</h2></div>
		<form id="form1" name="form1" method="post" action="guarda_datos.php" enctype="multipart/form-data"  autocomplete="off">
			<table width="70%" border="1" align="center" cellpadding="3" cellspacing="0">
				<tr class="divLoginboxHeader">
					<td colspan="2" bgcolor="#E5E5E5"><strong>DATOS DE LA ENTIDAD DE GOBIERNO</strong></td>
				</tr>
				<tr>
					<td width="18%"><div align="right"><strong>*Nombre:</strong></div></td>
					<td bgcolor="#FFFFFF"><div align="left">
						<input id="nombre" name="nombre" type="text" size="90" value="<?php echo $row_ayunta['nombre']; ?>" required />
						<a href="#" class="hintanchor" onMouseOver="showhint('<center>Este campo deber&aacute; llenarse con el Nombre.</b> <i>Este campo debe llenarse solo con letras</i></center>', this, event, '150px')">[?]</a>
					</div></td>
				</tr>
				<tr>
					<td><div align="right"><strong>*N&uacute;mero:</strong></div></td>
					<td bgcolor="#FFFFFF"><div align="left">
						<input name="numero" type="text" id="numero" onKeyPress="return pulsar(event)" onKeyUp="return handleEnter(this, event)" size="10" maxlength="3"  tabindex="2" value="<?php echo $row_ayunta['numero']; ?>" data-validate="validate(number);" >
						<a href="#" class="hintanchor" onMouseOver="showhint('<center>Ingrese el n&uacute;mero que le corresponde.</center>', this, event, '150px')">[?]</a>
					</div></td>
				</tr>
				<tr>
					<td><div align="right"><strong>*Direcci&oacute;n:</strong></div></td>
					<td bgcolor="#FFFFFF"><div align="left">
						<input name="direccion" type="text" id="direccion" onKeyPress="return pulsar(event)" onKeyUp="return handleEnter(this, event)" tabindex="3" value="<?php echo $row_ayunta['direccion']; ?>" size="90" data-validate="validate(required);">
						<a href="#" class="hintanchor" onMouseOver="showhint('<center>Ingrese la Calle, N&uacute;mero Exterior, N&uacute;mero Interior, Colonia y C.P. donde se ubica. </center>', this, event, '150px')" data-validate="validate(required);" >[?]</a>
					</div></td>
				</tr>
				<tr>
					<td><div align="right"><strong>Tel&eacute;fono 1: </strong></div></td>
					<td bgcolor="#FFFFFF"><div align="left">
						<input name="tel1" type="text" id="tel1" onKeyPress="return pulsar(event)" onKeyUp="return handleEnter(this, event)" tabindex="4" value="<?php echo $row_ayunta['tel1']; ?>")>
					<a href="#" class="hintanchor" onMouseOver="showhint('<center>Tel&eacute;fono con clave lada o l&iacute;nea directa al conmutador. <i>Este campo debe llenarse solo con n&uacute;meros, sin espacios y sin guiones.</i></center>', this, event, '150px')">[?]</a> </div></td>
				</tr>
				<tr>
					<td><div align="right"><strong>Tel&eacute;fono 2: </strong></div></td>
					<td bgcolor="#FFFFFF"><div align="left">
						<input name="tel2" type="text" id="tel2" onKeyPress="return pulsar(event)" onKeyUp="return handleEnter(this, event)" value="<?php echo $row_ayunta['tel2']; ?>" tabindex="5">
					<a href="#" class="hintanchor" onMouseOver="showhint('<center>Tel&eacute;fono con clave lada o l&iacute;nea directa al conmutador. <i>Este campo debe llenarse solo con n&uacute;meros, sin espacios y sin guiones.</i></center>', this, event, '150px')">[?]</a></div></td>
				</tr>
				
				<tr>
					<td><div align="right"><strong>*Per&iacute;odo:</strong></div></td>
					<td bgcolor="#FFFFFF"><div align="left">
						<input name="per_ini" type="text" id="per_ini" size="10" maxlength="4" tabindex="6" value="<?php echo $row_ayunta['per_ini']; ?>" data-validate="validate(number);"> 
						- 
						<input name="per_fin" type="text" id="per_fin" size="10" maxlength="4" tabindex="6" value="<?php echo $row_ayunta['per_fin']; ?>" data-validate="validate(number);">
						<a href="#" class="hintanchor" onMouseOver="showhint('<center>El per&iacute;odo de tiempo de la administraci&oacute;n.<i> Este campo debe llenarse solo con n&uacute;meros.</i></center>', this, event, '150px')">[?]</a>
					</div></td>
				</tr>
				
				<tr>
					<td><div align="right"><strong>*Prefijo:</strong>
					</div></td>
					<td bgcolor="#FFFFFF"><div align="left">
						<input name="prefijo" type="text" id="prefijo" size="10" maxlength="4" tabindex="6" value="<?php echo $row_ayunta['prefijo']; ?>" onBlur="validate();" data-validate="validate(required);">   
						<a href="#" class="hintanchor" onMouseOver="showhint('<center>En el prefijo van las primeras 3 letras del nombre.<i> Este campo debe llenarse solo con letra.</i></center>', this, event, '150px')">[?]</a>      
					</tr>
					
					<tr>
						<td><div align="right"><strong> Top&oacute;nimo: </strong></div></td>
						<td bgcolor="#FFFFFF"><div align="left">
							<input type="file" name="archivo" id="archivo" tabindex="6"  onChange="LimitAttach(this.form, this.form.archivo.value, this.form.archivo.name); " onBlur="validate();">
							<a href="#" class="hintanchor" onMouseOver="showhint('<center>Top&oacute;nimo de la entidad. <i> Agregar imagen en formato .jpg</i></center>', this, event, '150px')">[?]</a>
							<br /><br />
							<img id="toponimo" name="toponimo" src="../../images/azul2.jpg" width="150" height="150" />
							<input type="hidden" id="imagen" name="imagen" value="../../images/azul2.jpg" />
						</div></td>
					</tr>
					
					<tr>
						<td colspan="2"><div align="center"><span class="style1"><strong>Nota</strong>: Los campos marcados con * son obligatorios</span></div></td>
					</tr>
					</table>
					<p>&nbsp;</p>
					<p align="center">
						<input name="Submit3" type="button" onClick="MM_goToURL('self','../menu1.php');return document.MM_returnValue" value="Salir">
						<input type="reset" name="Submit2" value="Borrar">
						<input name="acepta" type="submit" id="acepta" value="Modificar datos" tabindex="11">
					</p>
					</form>
					</body>
					</html>
					<?php
					mysql_free_result($ayunta);
					?>
										